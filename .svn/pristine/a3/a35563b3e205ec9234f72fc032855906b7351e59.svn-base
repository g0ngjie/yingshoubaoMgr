package com.cosin.web.entity;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 * SysArea entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "sys_area", catalog = "revenue")
public class SysArea implements java.io.Serializable {

	// Fields

	private String areaId;
	private SysArea sysArea;
	private String areaName;
	private Integer enable;
	private Integer type;
	private Set<SysUser> sysUsers = new HashSet<SysUser>(0);
	private Set<SysArea> sysAreas = new HashSet<SysArea>(0);

	// Constructors

	/** default constructor */
	public SysArea() {
	}

	/** full constructor */
	public SysArea(SysArea sysArea, String areaName, Integer enable,
			Integer type, Set<SysUser> sysUsers, Set<SysArea> sysAreas) {
		this.sysArea = sysArea;
		this.areaName = areaName;
		this.enable = enable;
		this.type = type;
		this.sysUsers = sysUsers;
		this.sysAreas = sysAreas;
	}

	// Property accessors
	@GenericGenerator(name = "generator", strategy = "uuid.hex")
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name = "areaId", unique = true, nullable = false, length = 32)
	public String getAreaId() {
		return this.areaId;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "parentAreaId")
	public SysArea getSysArea() {
		return this.sysArea;
	}

	public void setSysArea(SysArea sysArea) {
		this.sysArea = sysArea;
	}

	@Column(name = "areaName", length = 32)
	public String getAreaName() {
		return this.areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	@Column(name = "enable")
	public Integer getEnable() {
		return this.enable;
	}

	public void setEnable(Integer enable) {
		this.enable = enable;
	}

	@Column(name = "type")
	public Integer getType() {
		return this.type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "sysArea")
	public Set<SysUser> getSysUsers() {
		return this.sysUsers;
	}

	public void setSysUsers(Set<SysUser> sysUsers) {
		this.sysUsers = sysUsers;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "sysArea")
	public Set<SysArea> getSysAreas() {
		return this.sysAreas;
	}

	public void setSysAreas(Set<SysArea> sysAreas) {
		this.sysAreas = sysAreas;
	}

}