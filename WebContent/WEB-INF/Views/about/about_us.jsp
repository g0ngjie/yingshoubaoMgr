<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/commons/taglibs.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>关于我们</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">    
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<link href="<%=basePath%>/Public/css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/icon.css">
<link href="<%=basePath%>/Public/uploadify/uploadify.css" type="text/css" rel="stylesheet" />
<!--装载文件-->

<script src="<%=basePath%>/Public/js/jquery-1.7.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/Public/js/jquery.easyui.min.js"></script>

<script type="text/javascript" src="<%=basePath%>/Public/uploadify/jquery.uploadify-3.1.js"></script>
<script type="text/javascript" src="<%=basePath%>/Public/uploadify/swfobject.js"></script>

<script src="<%=basePath%>/Public/js/shijian.js" type="text/javascript"></script>
<script src="<%=basePath%>/Public/layer/layer.js"></script>
<style type="text/css">
	.datagrid-btable tr{
		height: 500px;
	}
	.datagrid-row-over,datagrid-header td.datagrid-header-over{
		background: #FFF;
	}
</style>
<script>
$(document).ready(function(){
	var IsCheckFlag = true;
	$("#grid").datagrid({
	 	 onClickCell: function (rowIndex, field, value) {
	         IsCheckFlag = false;
	     },
	 	 onSelect: function (rowIndex, rowData) {
	         if (!IsCheckFlag) {
	             IsCheckFlag = true;
	             $("#grid").datagrid("unselectRow", rowIndex);
	         }
	     },    
	 	 onUnselect: function (rowIndex, rowData) {
	         if (!IsCheckFlag) {
	             IsCheckFlag = true;
	             $("#grid").datagrid("selectRow", rowIndex);
	         }
	     }
	})
});


/* 操作 */

function formatOper(value,row,index){
	var aboutId = row.aboutId;
	return "<a href='javascript:openchakan(\"" + aboutId + "\");'>编辑</a>";
} 
/* 添加 保存 角色 */
function saveAddRole()
{	
	var imgUp = $("#imgCns").val();
	if(imgUp==""||imgUp==null){
		layer.msg("请上传图片")
		return 0;
	}
	var content = $('#content').val();
	if(content==""||content==null){
		layer.msg("请填写正文")
		return 0;
	}
	$('#formAddRole').form('submit', {
		url : '<%=basePath%>/about/saveAbout.do',
		onSubmit : function() {
			var index = layer.load(1, {
				  shade: [0.1,'#fff'] //0.1透明度的白色背景
			})
			return $(this).form('validate');
		},
		success : function(result) {
			data = eval("(" + result + ")");
			if(data.code == 100)
			{
				layer.closeAll('loading');
				$('#adddlg').dialog('close');
				$('#grid').datagrid('reload');
				$.serviceTyper.show({
					title:'提示',
					msg:'<div class="msgs">保存成功</div>',
					timeout: 1000, 
					showType:'fade',
					style:{right:'', bottom:''}
				});
			}
			else
			{
				layer.closeAll('loading');
				$('#adddlg').dialog('close');
				$.messager.show({
								title:'提示',
								msg:'<div class="msgs">'+ data.msg +'</div>',
								timeout: 1000, 
								showType:'fade',
								style:{right:'', bottom:''}
				});
			}
		}
	}); 
	
}


/* 编辑 */
function openchakan(aboutId){
	$.post("<%=basePath%>/about/aboutBj.do",{aboutId:aboutId},function(result){
		data = eval("(" + result + ")");
		$('#imgCns').val(data.img);
		$("#content").val(data.content);
		var img0 = data.img;
		if(img0 == null || img0.trim() == "")
			$('#imgCn').attr("src", '<%=basePath2%>img/null.png');
		else{
			$('#imgCn').attr("src", '<%=basePath2%>'+'img/'+img0);
			$('#imgCns').attr("src", '<%=basePath2%>'+'img/'+img0);
			}
		$('#aboutId').val(data.aboutId);
		$('#adddlg').panel({title: "编辑"});
		$('#adddlg').dialog('open');
	}); 
}
/*显示图片*/
function xianshiIcon(value,row,index){ 
	var lu = row.img;
	if(lu == null || lu.trim() == "")
		return '<img src="<%=basePath2%>img/null.png"  style="width:400px;height:400px;"/>';
	else
		return '<img src="<%=basePath2%>img/'+lu+'"  style="width:400px;height:400px;"/>';
}
</script>
</head>
<body class="easyui-layout">

		<table id="grid" class="easyui-datagrid" style="width:100%;height:100%;display: none;" title="<img alt='推送消息管理' style='width:17px;height:17px;' src='<%=basePath%>/img/titleImg.png'><div style='height:20px;line-height:22px;margin-left:25px;margin-top:-20;'>关于我们</div>"
			nowrap="false" url="<%=basePath%>/about/chaAbout.do" pagination="true" singleSelect="false" loadMsg="正在努力为您加载数据">
			<thead>
				<tr>
					<th field="img" width="33%" align="center"  formatter="xianshiIcon">图片</th>
					<th field="content" width="47%" align="center">正文</th>
					<th field="oper" width="20%" align="center" formatter="formatOper" >操作</th>
				</tr>
			</thead>
		</table>
<!---------------------------------- 添加界面 ----------------------------->
<div id="adddlg" class="easyui-dialog" style="width: 500px; height:620px; padding: 10px 20px;display: none;" closed="true"  buttons="#adddlg-buttons" singleSelect="false" loadMsg="正在努力为您加载数据">
	<form id="formAddRole" name="formAddRole" method="post" >
	
    
            <div class="fitem" style="width: 130px;height:140px;">
                
				<div style="width: 200px;height: 150px;float: left;">
					<div style="width:100%;height:200px;floay:left;margin-left: 50px;margin-top: 10px;">
						<img id="imgCn" alt="" src="<%=basePath%>/Public/images/null.png" style="width:350px;height:100%;">
					</div>
					<div style="width:100%;height:50px;floay:left;margin-left: 50px;margin-top: 5px;">
						<input id="file_upload_cn" name="file_upload_cn" type="file" >
					</div>
				</div>
			</div>
			
			<div style="width:100%;height:100px;margin-top:120px;">
				<div style="border: 1px solid #DCD; width: 350px; height: 250px; margin-left: 50px">
					<textarea id="content" name="content" style="width: 100%;height: 100%; padding: 10px"></textarea>
				</div>
		    <input type="hidden" name="mode" id="mode" value=""/>
		   <input type="hidden" name="aboutId" id="aboutId" value=""/>
		    <input type="hidden" name="imgCns" id="imgCns" value=""/>
	 </form>
</div>

 
<!-------------------------------------------- 确认取消按钮 -------------------------------------------------->
<div id="adddlg-buttons" style="height: 30px;display: none;">
	<div style="width:140px;height:35px;float: right;">
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="saveAddRole()" iconcls="icon-save">保存</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="javascript:$('#adddlg').dialog('close')"
		iconcls="icon-cancel">取消</a>	
	</div>
</div> 
</body>
 

<style>
.uploadify-queue{ display: none;}
</style>
<script type="text/javascript">
    $('#file_upload_cn').uploadify({
        'swf'      : '<%=basePath%>/Public/uploadify/uploadify.swf',
        'uploader' :  '<%=basePath3%>/FileManagerService/fileupload/uploadImg.do', 
        'buttonText' : '上传图片',
        'height'   : 20,
        'width':347,
        'fileTypeExts': '*.gif; *.jpg; *.png',
        'multi': false,
        'onUploadStart':function(file){
        	layer.msg("<b id='tests'>上传进度 0%</b>",{time: -1,shade: [0.4, '#393D49']});
        },
        'onUploadProgress': function(file, bytesUploaded, bytesTotal, totalBytesUploaded, totalBytesTotal){
       		var process = Math.floor(totalBytesUploaded/totalBytesTotal*100);
			$('#tests').html("上传进度  " + process + "%");
        },
        'onUploadSuccess': function (file, data, response) { 
        	layer.closeAll();     
        	var resObj = eval("(" + data + ")"); 
        	if(resObj.code == 100)
        	{
        		var lin ="<%=basePath2%>"+resObj.filePath;
        		$('#imgCn').attr("src", lin);
        		var reg = /\\img/;
        		var filePath = resObj.filePath;
        		var result = filePath.replace(reg,'');
        		$('#imgCns').val(result);
        		//alert(resObj.fullImgUrl);
        		//alert(resObj.filePath);
        		//alert(resObj.fileSize);
        		//alert(resObj.extName);
        	}
        	else
        	{
        		//上传失败，失败原因
        		alert(resObj.msg);
        	}
        }     
        
    });
    
     </script>
</html>