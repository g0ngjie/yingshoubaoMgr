<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/commons/taglibs.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>商家粉丝</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">    
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<link href="<%=basePath%>/Public/css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/icon.css">
<link href="<%=basePath%>/Public/uploadify/uploadify.css" type="text/css" rel="stylesheet" />
<!--装载文件-->

<script src="<%=basePath%>/Public/js/jquery-1.7.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/Public/js/jquery.easyui.min.js"></script>

<script type="text/javascript" src="<%=basePath%>/Public/uploadify/jquery.uploadify-3.1.js"></script>
<script type="text/javascript" src="<%=basePath%>/Public/uploadify/swfobject.js"></script>

<script src="<%=basePath%>/Public/js/shijian.js" type="text/javascript"></script>
<script src="<%=basePath%>/Public/layer/layer.js"></script>
<link href="<%=basePath%>/Public/css/order.css" rel="stylesheet" type="text/css" />
<script>
$(document).ready(function(){
//回车点击事件--搜索功能
	$(document).keydown(function(event){
		if(event.keyCode==13){
			//$("#login").click();
			sousuo();
		}
	}) ;
});

$(document).ready(function(){
	var IsCheckFlag = true;
	$("#grid").datagrid({
	 	 onClickCell: function (rowIndex, field, value) {
	         IsCheckFlag = false;
	     },
	 	 onSelect: function (rowIndex, rowData) {
	         if (!IsCheckFlag) {
	             IsCheckFlag = true;
	             $("#grid").datagrid("unselectRow", rowIndex);
	         }
	     },    
	 	 onUnselect: function (rowIndex, rowData) {
	         if (!IsCheckFlag) {
	             IsCheckFlag = true;
	             $("#grid").datagrid("selectRow", rowIndex);
	         }
	     }
	})
});




/* 操作 */

function formatOper(value,row,index){
	var shopId = row.shopId;
	return "<a href='javascript:openChakan(\"" + shopId + "\");'>查看粉丝</a>";
} 
function formatOper1(value,row,index){
	var fansId = row.fansId;
	return "<a href='javascript:openChakan1(\"" + fansId + "\");'>查看粉丝</a>";
} 
function formatOper2(value,row,index){
	var fansId1 = row.fansId1;
	return "<a href='javascript:openChakan2(\"" + fansId1 + "\");'>查看粉丝</a>";
} 

/* 查看粉丝详情 */
function openChakan(shopId)
{	
	$('#grids1').datagrid({
		url:"<%=basePath%>/shopFans/chaXiangqing1.do?shopId="+shopId,
		   });
		$.post("<%=basePath%>/shopFans/chaXiangqings1.do",{shopId:shopId},function(result){
		layer.closeAll('loading');
		data = eval("(" + result + ")");
		document.getElementById('zongPrice1').innerHTML = data.zong;
		document.getElementById('zongTi1').innerHTML = data.zongTi;
		document.getElementById('zongNum1').innerHTML = data.zongNum;
		}); 
		$('#grids1').datagrid('load',{});
		$('#fensi1').panel({title: "查看粉丝详情"});
		$('#fensi1').dialog('open');
}
/*查看粉丝*/
function openChakan1(fansId)
{	
	alert(fansId)
	$('#grids2').datagrid({
		url:"<%=basePath%>/shopFans/chaXiangqing2.do?fansId="+fansId,
		   });
		$.post("<%=basePath%>/shopFans/chaXiangqings2.do",{fansId:fansId},function(result){
		layer.closeAll('loading');
		data = eval("(" + result + ")");
		document.getElementById('zongPrice2').innerHTML = data.zong2;
		document.getElementById('zongTi2').innerHTML = data.zongTi2;
		document.getElementById('zongNum2').innerHTML = data.zongNum2;
		
	});    
		$('#grids2').datagrid('load',{});
		$('#fensi2').panel({title: "查看粉丝详情"});
		$('#fensi2').dialog('open');
}
/*查看粉丝*/
function openChakan2(fansId)
{	
	$('#grids3').datagrid({
		url:"<%=basePath%>/shopFans/chaXiangqing3.do?fansId="+fansId,
		   });
		$.post("<%=basePath%>/shopFans/chaXiangqings2.do",{fansId:fansId},function(result){
		layer.closeAll('loading');
		data = eval("(" + result + ")");
		document.getElementById('zongPrice3').innerHTML = data.zong2;
		document.getElementById('zongTi3').innerHTML = data.zongTi2;
		document.getElementById('zongNum3').innerHTML = data.zongNum2;
		
	});    
		$('#grids3').datagrid('load',{});
		$('#fensi3').panel({title: "查看粉丝详情"});
		$('#fensi3').dialog('open');
}
//搜索
function sousuo()
{
	var SsUserName = $('#SsUserName').val();
	var ssksDate = $('#ssksDate').datebox('getValue');
	$('#grid').datagrid('load', {SsUserName:SsUserName,SsShopName:SsShopName,SsOrderNum:SsOrderNum,ssksDate:ssksDate});
}
function sousuo1()
{
	var SsFensiName1 = $('#SsFensiName1').val();
	$('#grids1').datagrid('load', {SsFensiName1:SsFensiName1});
}
function sousuo2()
{
	var SsFensiName2 = $('#SsFensiName2').val();
	$('#grids2').datagrid('load', {SsFensiName2:SsFensiName2});
}
function sousuo3()
{
	var SsFensiName3 = $('#SsFensiName3').val();
	$('#grids3').datagrid('load', {SsFensiName3:SsFensiName3});
}
</script>
</head>
<body class="easyui-layout">
<!----------------------- 功能按钮 ----------------------->
<div id="tb" style="height:auto;display: none;">   
	
	  <div style="text-align: list;background-color: #f4f4f4;width:100%;height:30px; border-top: 2px solid #D9D9D9;border-bottom: 0px solid #D9D9D9;">
	  		<div style="float: left;height: 30px;line-height: 30px;"><label> &nbsp;&nbsp;商家名称: </label>
	   		<input name="SsShopName" id="SsShopName" style="margin-top: 0px;border:1px #95B8E7 solid;width: 150px;height: 23px"  />
		   </div>
		   <div onclick="sousuo()" style=" float: left;border:1px solid #000;margin-left: 25px;cursor:pointer;text-align:center;line-height:24px;background-color: #FFF;color:#000;width: 100px;height:23px;margin-top: 3px;border-radius: 5px;"
	    class="easyui-linkbutton" iconCls="icon-search">搜索	   
		</div>  
	 </div>
</div>

<div id="tf1" style="height:auto;display: none;">
	 <div style="text-align: list;background-color: #f4f4f4;width:100%;height:30px; border-top: 2px solid #F4F4F4;border-bottom: 0px solid #f4f4f4;margin-left: 150px;">
	 <label style="float: left;font-size: 18px;"> &nbsp;&nbsp;消费总额: &nbsp;&nbsp;&nbsp;</label>
	 <div id="zongPrice1" style="float: left;font-size: 18px;"></div>
	 
	  <label style="float: left;font-size: 18px;"> &nbsp;&nbsp;提现总额: &nbsp;&nbsp;&nbsp;</label>
	  <div id="zongTi1" style="float: left;font-size: 18px;"></div>
	  <label style="float: left;font-size: 18px;"> &nbsp;&nbsp;订单总数: &nbsp;&nbsp;&nbsp;</label>
	  <div id="zongNum1" style="float: left;font-size: 18px;"></div>
	</div>
	 <div style="text-align: list;background-color: #f4f4f4;width:100%;height:30px; border-top: 2px solid #D9D9D9;border-bottom: 0px solid #D9D9D9;">
	  		<div style="float: left;height: 30px;line-height: 30px;"><label> &nbsp;&nbsp;粉丝名称: </label>
	   		<input name="SsFensiName1" id="SsFensiName1" style="margin-top: 0px;border:1px #95B8E7 solid;width: 150px;height: 23px"  />
		   </div>
		   <div onclick="sousuo1()" style=" float: left;border:1px solid #000;margin-left: 25px;cursor:pointer;text-align:center;line-height:24px;background-color: #FFF;color:#000;width: 100px;height:23px;margin-top: 3px;border-radius: 5px;"
	    class="easyui-linkbutton" iconCls="icon-search">搜索	   
		</div>  
	 </div>
 </div>
 <div id="tf2" style="height:auto;display: none;">   
 	 <div style="text-align: list;background-color: #f4f4f4;width:100%;height:30px; border-top: 2px solid #F4F4F4;border-bottom: 0px solid #f4f4f4;margin-left: 150px;">
		 <label style="float: left;font-size: 18px;"> &nbsp;&nbsp;消费总额: &nbsp;&nbsp;&nbsp;</label>
	 	 <div id="zongPrice2" style="float: left;font-size: 18px;"></div>
	 
	 	 <label style="float: left;font-size: 18px;"> &nbsp;&nbsp;提现总额: &nbsp;&nbsp;&nbsp;</label>
	 	 <div id="zongTi2" style="float: left;font-size: 18px;"></div>
		 <label style="float: left;font-size: 18px;"> &nbsp;&nbsp;订单总数: &nbsp;&nbsp;&nbsp;</label>
		 <div id="zongNum2" style="float: left;font-size: 18px;"></div>
	</div>
	   <div style="text-align: list;background-color: #f4f4f4;width:100%;height:30px; border-top: 2px solid #D9D9D9;border-bottom: 0px solid #D9D9D9;">
	  		<div style="float: left;height: 30px;line-height: 30px;"><label> &nbsp;&nbsp;粉丝名称: </label>
	   		<input name="SsFensiName2" id="SsFensiName2" style="margin-top: 0px;border:1px #95B8E7 solid;width: 150px;height: 23px"  />
		   </div>
		   <div onclick="sousuo2()" style=" float: left;border:1px solid #000;margin-left: 25px;cursor:pointer;text-align:center;line-height:24px;background-color: #FFF;color:#000;width: 100px;height:23px;margin-top: 3px;border-radius: 5px;"
	    class="easyui-linkbutton" iconCls="icon-search">搜索	   
		</div>  
	 </div>
 </div>
 <div id="tf3" style="height:auto;display: none;">   
	<div style="text-align: list;background-color: #f4f4f4;width:100%;height:30px; border-top: 2px solid #F4F4F4;border-bottom: 0px solid #f4f4f4;margin-left: 150px;">
		 <label style="float: left;font-size: 18px;"> &nbsp;&nbsp;消费总额: &nbsp;&nbsp;&nbsp;</label>
	 	 <div id="zongPrice3" style="float: left;font-size: 18px;"></div>
	 
	 	 <label style="float: left;font-size: 18px;"> &nbsp;&nbsp;提现总额: &nbsp;&nbsp;&nbsp;</label>
	 	 <div id="zongTi3" style="float: left;font-size: 18px;"></div>
		 <label style="float: left;font-size: 18px;"> &nbsp;&nbsp;订单总数: &nbsp;&nbsp;&nbsp;</label>
		 <div id="zongNum3" style="float: left;font-size: 18px;"></div>
	</div>
	   <div style="text-align: list;background-color: #f4f4f4;width:100%;height:30px; border-top: 2px solid #D9D9D9;border-bottom: 0px solid #D9D9D9;">
	  		<div style="float: left;height: 30px;line-height: 30px;"><label> &nbsp;&nbsp;粉丝名称: </label>
	   		<input name="SsFensiName3" id="SsFensiName3" style="margin-top: 0px;border:1px #95B8E7 solid;width: 150px;height: 23px"  />
		   </div>
		   <div onclick="sousuo3()" style=" float: left;border:1px solid #000;margin-left: 25px;cursor:pointer;text-align:center;line-height:24px;background-color: #FFF;color:#000;width: 100px;height:23px;margin-top: 3px;border-radius: 5px;"
	    class="easyui-linkbutton" iconCls="icon-search">搜索	   
		</div>  
	 </div>
 </div>
		<table id="grid" class="easyui-datagrid" style="width:100%;height:100%;display: none;" title="<img alt='推送消息管理' style='width:17px;height:17px;' src='<%=basePath%>/img/titleImg.png'><div style='height:20px;line-height:22px;margin-left:25px;margin-top:-20;'>订单管理</div>"
			url="<%=basePath%>/shopFans/shopFansListData.do" toolbar="#tb" data-options="nowrap: false" 
			rownumbers="true" pagination="true" singleSelect="false" loadMsg="正在努力为您加载数据">
			<thead>
				<tr>
					<th field="ck" checkbox="true" id="ck" name="ck"></th>
					<th field="shopName" width="20%" align="center">商家名称</th>
					<th field="typeName" width="15%" align="center">服务类型</th>
					<th field="commentNum" width="10%" align="center">评价次数</th>
					<th field="commScore" width="10%" align="center">评价总分</th>
					<th field="createDate" width="20%" align="center">创建日期</th>
					<th field="oper" width="15%" align="center" formatter="formatOper" >操作</th>
				</tr>
			</thead>
		</table>
 
 <div id="fensi1" class="easyui-dialog"  closed="true" >
		<table id="grids1" class="easyui-datagrid" data-options="nowrap: false" style="width:700px;height:500px;display: none;"
			toolbar="#tf1" rownumbers="true" pagination="true" singleSelect="false" data-options="nowrap: false" loadMsg="正在努力为您加载数据">
			<thead>
				<tr>
					<th field="ck1" checkbox="true" id="ck1" name="ck1"></th>
					<th field="name" width="15%" align="center">粉丝名称</th>
					<th field="mobile" width="20%" align="center">手机号</th>
					<th field="dan" width="10%" align="center">消费总额</th>
					<th field="num" width="10%" align="center">订单数量</th>
					<th field="ti" width="10%" align="center">提现总额</th>
					<th field="createDate" width="15%" align="center">创建日期</th>
					<th field="oper" width="13%" align="center" formatter="formatOper1" >操作</th>
				</tr>
			</thead>
		</table>
</div>
<div id="fensi2" class="easyui-dialog"  closed="true" >
		<table id="grids2" class="easyui-datagrid" data-options="nowrap: false" style="width:700px;height:500px;display: none;"
			toolbar="#tf2" rownumbers="true" pagination="true" singleSelect="false" data-options="nowrap: false" loadMsg="正在努力为您加载数据">
			<thead>
				<tr>
					<th field="ck2" checkbox="true" id="ck2" name="ck2"></th>
					<th field="name1" width="15%" align="center">粉丝名称</th>
					<th field="mobile1" width="20%" align="center">手机号</th>
					<th field="dan1" width="10%" align="center">消费总额</th>
					<th field="num1" width="10%" align="center">订单数量</th>
					<th field="ti1" width="10%" align="center">提现总额</th>
					<th field="createDate1" width="15%" align="center">创建日期</th>
					<th field="oper1" width="13%" align="center" formatter="formatOper2" >操作</th>
				</tr>
			</thead>
		</table>
</div>
<div id="fensi3" class="easyui-dialog"  closed="true" >
		<table id="grids3" class="easyui-datagrid" data-options="nowrap: false" style="width:700px;height:500px;display: none;"
			toolbar="#tf3" rownumbers="true" pagination="true" singleSelect="false" data-options="nowrap: false" loadMsg="正在努力为您加载数据">
			<thead>
				<tr>
					<th field="ck3" checkbox="true" id="ck3" name="ck3"></th>
					<th field="name2" width="15%" align="center">粉丝名称</th>
					<th field="mobile2" width="20%" align="center">手机号</th>
					<th field="dan2" width="10%" align="center">消费总额</th>
					<th field="num2" width="10%" align="center">订单数量</th>
					<th field="ti2" width="10%" align="center">提现总额</th>
					<th field="createDate2" width="25%" align="center">创建日期</th>
				</tr>
			</thead>
		</table>
</div>
</body>
<style>
.uploadify-queue{ display: none;}
</style>

</html>