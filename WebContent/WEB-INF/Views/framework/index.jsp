<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/commons/taglibs.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>营收宝管理后台</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">    
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<link rel="icon" href="<%=basePath%>/Public/images/logoy.png" type="image/x-icon" />
<link rel="shortcut icon" href="<%=basePath%>/Public/images/logoy.png" type="image/x-icon" />
<link rel="bookmark" href="<%=basePath%>/Public/images/logoy.png" type="image/x-icon" />
<script type="text/javascript" src="<%=basePath%>/Public/js/topTotle.js"></script>
<script src="<%=basePath%>/Public/js/jquery-1.7.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/Public/js/jquery.easyui.min.js"></script>
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/icon.css">
<style>
html,body{
 height:100%;
}
</style>
<script type="text/javascript">
	
	
	
</script>
</head>

<body style="margin: 0 auto;height:100%;width:100%;">
	
  <div style="width:100%;height:100%;z-index:0;overflow: hidden;margin: 0 auto;">
  
   <iframe src="<%=basePath%>framework/mainindex.do" width="100%" height="100%" marginwidth="0" scrolling="no" marginwidth="0" frameborder="0" ></iframe>
  
  </div>
 	
 		<div id="adddlg" class="easyui-dialog" style="width: 420px; height: 210px; padding: 10px 20px;display: none;" closed="true"  buttons="#adddlg-buttons" >
			<form id="formAddNotice" name="formAddNotice" method="post">
			<!-- 原密码  输入  -->
		   <div class="fitem" >
			   <label class="tile" style="display:inline-block;width:80px;color:#000;font-size:15px;margin-left: 40px;margin-top: 10px;">原密码</label>
			   <input name="beginPassword" id="beginPassword" class="easyui-textbox" style=" width:181px;vertical-align:middle;font-size:15px;color:#000;margin-top: -6px"; type="password" />
		   </div>
		   <!-- 新密码  输入  -->
		   <div class="fitem" >
			   <label class="tile" style="display:inline-block;width:80px;color:#000;font-size:15px;margin-left: 40px;margin-top: 10px;">新密码</label>
			   <input name="endPassword" id="endPassword" class="easyui-textbox" style=" width:181px;vertical-align:middle;font-size:15px;color:#000;margin-top: -6px"; validType="length[6,18]" type="password" />
		   </div>
		   <!-- 确认密码  输入  -->
		   <div class="fitem" >
			   <label class="tile" style="display:inline-block;width:80px;color:#000;font-size:15px;margin-left: 40px;margin-top: 10px;">确认密码</label>
			   <input name="doublePassword" id="doublePassword" class="easyui-textbox" style=" width:181px;vertical-align:middle;font-size:15px;color:#000;margin-top: -6px;" validType="length[6,18]" type="password" />
		   </div>
		   </form>
		</div>
 		
 		<!------------------------------ 确认取消按钮 ----------------------------------->
<div id="adddlg-buttons" style="height: 30px;display: none;">
	<div style="width:140px;height:35px;float: right;">
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="savePassWord()" iconcls="icon-save">保存</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="javascript:$('#adddlg').dialog('close')"
		iconcls="icon-cancel">取消</a>	
	</div>
</div> 

</body>
<script type="text/javascript">
	
	function fuClick(){
	
		$('#beginPassword').textbox('setValue','');
		$('#endPassword').textbox('setValue','');
		$('#doublePassword').textbox('setValue','');
		
		$('#adddlg').panel({title: "修改密码"});
		$('#adddlg').dialog('open');
		
	}
	
	function savePassWord(){
		
		
		var beginPassword = $('#beginPassword').val();
		
		if(beginPassword == "" || beginPassword == null){
			alert("请输入原始密码")
			return null;
		}
		
		var endPassword = $('#endPassword').val();
		if(endPassword == "" || endPassword == null){
			alert("请输入新密码")
			return null;
		}
		
		var doublePassword = $('#doublePassword').val();
		if(doublePassword == "" || doublePassword == null){
			alert("请在一次确认新密码")
			return null;
		}
		
		
		
	$.messager.confirm('确认','确认修改?',function(row){	
	
	if(row){	
		$('#formAddNotice').form('submit', {
		url : '<%=basePath%>/income/passwordEdit.do',
		success : function(result) {
			data = eval("(" + result + ")");
			/* alert(data.code)
			alert(data.msg) */
			
			if(data.code == 100){
				/* layer.closeAll('loading'); */
				$.messager.show({
					title:'提示',
					msg:'<div class="msgs">'+data.msg+'</div>',
					timeout: 1000, 
					showType:'fade',
					style:{right:'', bottom:''}
				});
				$('#adddlg').dialog('close');
				$('#grid').datagrid('reload');
			}
			else
			{
				$.messager.show({
					title:'提示',
					msg:'<div class="msgs">'+data.msg+'</div>',
					timeout: 500,  
					showType:'fade',
					style:{right:'', bottom:''}
					});
			}
		
		}
	}); 
	$('#adddlg').dialog('close');
	/*alert('修改成功');*/
	}
	});
}
</script>
</html>
