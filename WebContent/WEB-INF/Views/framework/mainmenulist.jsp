<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/commons/taglibs.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title></title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">    
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<link href="<%=basePath%>/Public/css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/icon.css">
<script src="<%=basePath%>/Public/js/jquery-1.7.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/Public/js/jquery.easyui.min.js"></script>

<script> 
$(document).ready(function(){

});
//是否显示
function formatterEnable(value){
	if(value == '1')
		return "<a style='color:green'>显示</a>";
	return "<a style='color:red'>隐藏</a>";
} 
//添加下级菜单
function openAddMenu(menukey){
    document.getElementById("name").value = "";
   
    document.getElementById("code").value = "";
    document.getElementById("url").value = "";
    document.getElementById("icon").value = "";
    document.getElementById("order").value = "";
    
    document.getElementById("enable").value = "1"; 
    document.getElementById("icon").value = "icon01.png";
  $('#menukey').val(menukey);
  $('#adddlg').dialog('open');
}
//保存
function saveAddMenu(){
	$('#formAddMenu').form('submit', {
		url : '<%=basePath%>/framework/savemenu.do',
		onSubmit : function() {
			return $(this).form('validate');
		},
		success : function(result) {
			data = eval("(" + result + ")");
			location.replace(location) 
			if(data.code == 100)
			{
				$('#adddlg').dialog('close');
				$('#grid').datagrid('reload');
				$.messager.show({
					title:'提示',
					msg:'保存成功',
					timeout: 1000,  
					showType:'fade',
					style:{right:'', bottom:''}
				});
			}
			else
			{
				$.messager.show({
					title:'提示',
					msg:data.msg,
					timeout: 1000,  
					showType:'fade',
					style:{right:'', bottom:''}
					
				});
			}
		}
	}); 
}

//编辑主菜单

function openEditMenu( menukey){
$.post("<%=basePath%>/framework/getedit.do",{menukey:menukey},function(result){
	alert(result)
		data = eval("(" + result + ")");
		if(data.code == 100)
	    {
	    	$('#mode').val("edit");
			$('#name').val(data.name);
			$('#code').val(data.code);
			$('#url').val(data.url);
			$('#menukey').val(data.menuId);
			$('#order').val(data.orderNo);
			if(data.sysMenu != 1)
				$('#tupian').css("display","block");
			else
				$('#tupian').css("display","none");
			
			$('#adddlg').dialog('open');
		}
	});
}

/* 删除操作 */  
function openDelMenu(menukey){  
   $.messager.confirm('确认','确认删除?',function(row){  
       if(row){  
	       	$.get("<%=basePath%>/framework/delmenu.do?menukey="+menukey,function(data,status){
		       alert(data)
		       alert(status)
		        $.messager.show({
							title:'提示',
							msg:'删除成功',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
				});
	        });
       }  
   }) 
 }

 /*隐藏操作*/
function openHideMenu(menukey){
$.messager.confirm('隐藏','确认隐藏?',function(row){  
       if(row){  
	       	$.get("<%=basePath%>/framework/hideMenu.do?menuId="+menukey,function(data,status){
	       	
		        $.messager.show({
							title:'提示',
							msg:'隐藏成功',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
				});
	        });
       }  
   }) 

} 

//添加系统菜单  对话框内容    相关操作（后面有添加，编辑，删除，隐藏 命令）
function formatOper(value,row,index){
    var menuKey = row.menuKey;
    var str = "<a href='javascript:openAddMenu(\"" + menuKey + "\");'>添加下级菜单</a>"
    + " | <a href='javascript:openEditMenu(\"" + menuKey + "\");'>编辑菜单</a>"
    + " | <a href='javascript:openDelMenu(\"" + menuKey + "\");'>删除菜单</a>"
    + " | <a href='javascript:openHideMenu(\"" + menuKey + "\");'>隐藏菜单</a>";
    return str;
}

//图片显示
function picture(){
	var jpg = $('#icon option:selected').text();
	$('#img').attr("src", "<%=basePath%>/Public/images/"+jpg);
}
</script>

</head>
<body class="easyui-layout">
<!-----------ChinaView 工作区表格内容------------->
<table id="grid" class="easyui-treegrid" style="width:100%;height:100%" title="菜单管理"
    data-options="
                url: '<%=basePath%>/framework/menulistdata.do',
                method: 'get',
                rownumbers: true,
                idField: 'menuKey',
                treeField: 'menuName',
              ">
	<thead>
		<tr>
			<th data-options="field:'menuName'" width="40%" align="left">系统</th>
			<th field="enable" width="30%" formatter="formatterEnable" align="center">是否显示</th>
			<th field="Oper" width="30%" formatter="formatOper" align="center">操作</th>
		</tr>
	</thead>
</table>
	
<!-- ------------------------------------------------------------------------------- -->
<!-- 点击添加菜单，出现添加对话框的内容 -->
<div id="adddlg" class="easyui-dialog" style="width: 400px; height: 450px; padding: 10px 20px;" closed="true" buttons="#adddlg-buttons" title="添加主菜单">
   <div class="ftitle">添加顶级菜单</div>
   <form id="formAddMenu" name="formAddMenu" method="post">
   <div class="fitem">
	   <label>菜单名称:</label>
	   <input name="name" id="name" class="easyui-validatebox textbox" required="true"  style="width: 230px;height: 20px "/>
   </div>
   <div class="fitem">
	   <label>菜单编码:</label>
	   <input name="code" id="code" class="easyui-validatebox textbox" required="true"style="width: 230px;height: 20px " />
   </div>
   <div class="fitem">
   		<label>菜单地址:</label>
   		<input name="url" id="url" class="easyui-validatebox textbox" required="true"style="width: 230px;height: 20px ">
   </div>
   <div class="fitem">
   		<label>菜单排序:</label>
   		<input name="order" id="order" class="easyui-validatebox textbox" required="true"style="width: 230px;height: 20px ">
   		
   </div>
   <div class="fitem">
   		<label>是否显示:</label>
   		<select class="combobox textbox" name="enable" id="enable" style="width: 230px;height: 20px ">
   		<option value="1">显示</option>
   		<option value="0">隐藏</option>
   		</select>
   </div>
   
   <div id="tupian" class="fitem" >
	   <label>菜单图标:</label>
	   <select class="combobox textbox" name="icon" id="icon" style="width:137px; " onchange="picture()">  
	   <%
		   for(int i=0; i<=8; i++)
		   {
		      out.print("<option value='icon" + (new Integer(i+1).toString().length()==1?("0"+(i+1)):(i+1)) + ".png'>" + "icon" + (new Integer(i+1).toString().length()==1?("0"+(i+1)):(i+1)) + ".png" + "</option>");
		   }
	   %>   
	   </select> 
   </div>
   <div id="picture" ><img id="img" src="<%=basePath%>/Public/images/icon01.png"> </div>
   <input type="hidden" name="mode" id="mode" value="1"/>
   <input type="hidden" name="menukey" id="menukey" value=""/>
   <input type="hidden" name="type" id="type" value="1"/>
   <input type="hidden" name="parentMenukey" id="parentMenukey" value="-1"/>
   <input type="hidden" name="level" id="level" value="1"/>
   </form>
</div>
<!--保存按钮  取消按钮  -->
	<div id="adddlg-buttons">
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="saveAddMenu()" iconcls="icon-save">保存</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="javascript:$('#adddlg').dialog('close')"
			iconcls="icon-cancel">取消</a>
	</div> 
<!------显示对话框  弹出框- 添加系统菜单-----保存 取消-->
<div id="add-system" class="easyui-dialog" style="width: 360px; height: 160px; padding: 20px 20px;" closed="true" buttons="" title="会话">
  <form id="formAddOrg" name="formAddOrg" method="post">
	   <div class="fitem">
		   <label>系统名称</label>
		   <input name="orgName" id="orgName" class="easyui-validatebox textbox" required="true" style="width:137px;"/>
	   </div>
	   <input type="hidden" name="orgType" id="orgType" />
	   <input type="hidden" name="orgKey" id="orgKey" />
	   <input type="hidden" name="mode" id="mode" />
	</form>
</div>

</body>
</html>
