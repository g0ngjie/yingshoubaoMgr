﻿<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/commons/taglibs.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<base href="<%=basePath%>">
<title>营收宝管理后台</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
<link href="<%=basePath%>/Public/css/style.css" rel="stylesheet" type="text/css" />
<script src="<%=basePath%>/Public/js/jquery-1.7.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/Public/js/jquery.easyui.min.js"></script>
<script src="<%=basePath%>/Public/layer/layer.js"></script>

<script src="<%=basePath%>/Public/js/jquery-1.7.min.js" type="text/javascript"></script>
<script src="js/cloud.js" type="text/javascript"></script>
<style type="text/css">
	/* body{  background: repeat;background-size:100%;} */
	html,body{width:100%;height:100%;margin:0px auto;padding:0px auto;}
	#bodyId{width:100%;height:100%;margin:0px auto;padding:0px auto;background: repeat;background-size:100%;
			position: absolute;z-index: -1px;
	}
	#loginBTN{
		background-color: #006DBF;
	}
	#loginBTN:HOVER{
		background-color: #2AABD2;
	}
	#loginBTN:ACTIVE {
		background-color: #265A88;
	}
	#loginbox{
		box-shadow: 3px 3px 3px #868686;
		border-radius:5px;
	}
	input:focus {
		border-color: rgba(82, 168, 236, 0.8);
		outline: 0;
		outline: thin dotted \9;
		/* IE6-9 */
		-webkit-box-shadow: inset 0px 1px 5px rgba(0,0,0,.175), 0 0 8px rgba(82,168,236,.6);
		-moz-box-shadow: inset 0 1px 5px rgba(0,0,0,.175), 0 0 8px rgba(82,168,236,.6);
		box-shadow: inset 0 1px 5px rgba(0,0,0,.175), 0 0 8px rgba(82,168,236,.6);
	}
</style>
<script language="javascript">
$(document).ready(function(){
	var num = Math.floor(Math.random()*10)%3+1;
	$("#bodyId").css("background-image","url('Public/images/"+ num +"backGroundImg.jpg')").fadeOut(0).fadeIn(2000);
	if(num == 3){
		$("#loginPwd").css('backgroundColor','#DCDCDC');
		$("#loginUser").css('backgroundColor','#DCDCDC');
	}
	//回车点击事件
	$('#loginUser').val('');
	$(document).keydown(function(event){
		if(event.keyCode==13){
			$("#loginBTN").click();
		}
	});
	$('#loginUser').val();
	$('#loginPwd').val();
});

$(function(){
    $('#loginbody').css({'position':'absolute','left':($(window).width()-630)});
	$(window).resize(function(){  
    	$('#loginbody').css({'position':'absolute','left':($(window).width()-630)});
    });
});  

/* 登录验证 */
function login(){
	var loginUser = $('#loginUser').val();
	var loginPwd = $('#loginPwd').val();
	if(loginUser == "")
	{
		alert("请输入用户名");
		return;
	}
	if(loginPwd == "")
	{
		alert("请输入密码");
		return;
	}
	$.post("<%=basePath%>system/loginYZ.do",
		{loginName:loginUser, pwd:loginPwd},
		function(result){
			data = eval("(" + result + ")");
			var token = data.userId;
			if(data.code == 100)
			{	
			   	var url = encodeURI("<%=basePath%>framework/index.do?token="+ token);
				window.location.href= url;
			}
			else{
				alert(data.msg);
			}
		}
	);
}
</script>
 
</head>
<body ><!-- style="background:url(<%=basePath%>Public/images/3backGroundImg.jpg) no-repeat ; width:100%; height:100%; overflow:scroll; -->
	<div id="bodyId">
	</div>
		<div id="loginbody" style="padding: 15px; width: 350px;height: 400px;margin-left: 50px;position: absolute;margin-top: 10%">
			<div style="position: absolute;margin-top: -35px;width: 95%;text-align: center;color: #FFF;">
				<span style="font-size: 50px;display: block;">营收宝</span>
				<span style="font-size: 15px;">BACKGROUND MANAGEMENT SYSTEM</span>
			</div>
			<div id="loginbox" style="width: 100%;background-color: rgba(0,0,0,0.4);height: 100%;padding: 10px">
				<div style="width: 100%;height: 90px;"></div>
				<div style="padding: 25px">
					<div style="width: 100%;height: 50px;">
						<div style="float: left; width: 25%;height: 30px;">
							<span class="loginbox_div_lable" style="color:#FFF;font-size: 15px; margin-top: 10px">用户名：</span>
				 		</div>
				 		<input id="loginUser" name="loginUser" type="text" style="width:70%;height:40px;font-size:15px;line-height:30px;
				 			 border-radius:4px;border:1px solid rgba(0,0,0,0.1);background-color: rgba(0,0,0,0.1)" placeholder="请输入用户名"/>
					</div>
					<div style="width: 100%;height: 50px; margin-top: 40px">
						<div style="float: left; width: 25%;height: 30px;">
							<span class="loginbox_div_lable" style="color:#FFF;font-size: 15px;margin-top: 10px">密&nbsp;&nbsp;&nbsp;码：</span>
						</div>
				 		<input id="loginPwd" name="loginPwd" type="password" placeholder="请输入密码" style="width:70%;height:40px;font-size:15px;line-height:30px; 
				 			border-radius:4px;border:1px solid rgba(0,0,0,0.1);background-color: rgba(0,0,0,0.1)" />
					</div>
					<div style="width: 100%;height: 50px;margin-top: 20px;text-align: center;">
						<input id="loginBTN" type="button" style="margin-top:25px; width: 95%;height: 45px;border-radius:13px;margin-left: -10px; color: #FFF;font-size: 15px"value="登陆" onclick="login()"/>
					</div>
				</div>
			</div>
		</div>
</body>
</html>
