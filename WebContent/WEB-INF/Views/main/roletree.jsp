<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/commons/taglibs.jsp" %>

<!DOCTYPE html>
<HTML>
<HEAD>
	<TITLE> ZTREE DEMO - Async</TITLE>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" href="<%=basePath%>Public/zTree_v3/css/demo.css" type="text/css">
	<link rel="stylesheet" href="<%=basePath%>Public/zTree_v3/css/zTreeStyle/zTreeStyle.css" type="text/css">
	<script type="text/javascript" src="<%=basePath%>/Public/zTree_v3/js/jquery-1.4.4.min.js"></script>
	<script type="text/javascript" src="<%=basePath%>/Public/zTree_v3/js/jquery.ztree.core-3.5.js"></script>

	<!--  <script type="text/javascript" src="../../../js/jquery.ztree.excheck-3.5.js"></script>
	  <script type="text/javascript" src="../../../js/jquery.ztree.exedit-3.5.js"></script>-->
	<SCRIPT type="text/javascript">
		function searchM() {
		  var key = $.trim($("input[name='param']").val());
		  //alert(param);
		  var treeObj = $.fn.zTree.getZTreeObj("treeDemo");
		  //var node = treeObj.getNodeByParam("id", 0, null);
		  var nodeList=treeObj.getNodesByParamFuzzy("name", key);
		  //alert(nodeList.length);
		  treeObj.expandNode(nodeList[0], true, true, false);
		  treeObj.selectNode(nodeList[0]);
		  //treeObj.expandAll(true);
		/*
		  if(param != ""){
			param = encodeURI(encodeURI(param));
			treeObj.setting.async.otherParam=["param", param];
			alert(11);
		  }else {
			//搜索参数为空时必须将参数数组设为空
			treeObj.setting.async.otherParam=[];
		  }
		  treeObj.reAsyncChildNodes(node, "refresh");*/
		}
		
		function aaa(){
			//alert(111);
		}
		/*
		var setting = {
			async: {
				enable: true,
				url:"/index.php/Admin/Member/tree_data",
				autoParam:["id", "name=n", "level=lv"],
				otherParam:{"otherParam":"zTreeAsyncTest"},
				dataFilter: filter
			}
		};

		function filter(treeId, parentNode, childNodes) {
			if (!childNodes) return null;
			for (var i=0, l=childNodes.length; i<l; i++) {
				childNodes[i].name = childNodes[i].name.replace(/\.n/g, '.');
			}
			return childNodes;
		}

		$(document).ready(function(){
			$.fn.zTree.init($("#treeDemo"), setting);
		});
		*/
		
		
		var setting = {
			data: {
			simpleData: {
			enable: true
			// idKey:"id",
			// pIdKey:"pId",
			}
			}
			,async: {
			enable: true,
			//url:"<%=basePath%>/main/tree_data.do",
			autoParam:["id", "name"],
			otherParam:{"otherParam":"zTreeAsyncTest"},
			// dataType: "text",//默认text
			// type:"get",//默认post
			dataFilter: filter //异步返回后经过Filter
			}
			,callback:{
			// beforeAsync: zTreeBeforeAsync, // 异步加载事件之前得到相应信息
			asyncSuccess: zTreeOnAsyncSuccess,//异步加载成功的fun
			asyncError: zTreeOnAsyncError, //加载错误的fun
			beforeClick:beforeClick //捕获单击节点之前的事件回调函数
			}
			};
			//treeId是treeDemo
			function filter(treeId, parentNode, childNodes) {
			if (!childNodes) return null;
			for (var i=0, l=childNodes.length; i<l; i++) {
				childNodes[i].name = childNodes[i].name.replace('','');
			}
			return childNodes;
			}
			function beforeClick(treeId,treeNode){
				
				if(!treeNode.isParent){
					alert("请选择父节点");
					//alert(document.getElementById('frame').src);
					
					
					////document.getElementById('frame').src='/index.php/Admin/Member/editmember?memberkey='+treeNode.id;

					return false;
				}
				else{
					if(treeNode.id != 0)
					////document.getElementById('frame').src='/index.php/Admin/Member/editmember?memberkey='+treeNode.id;
					
					
					//alert(document.getElementById('frame').src);
					//alert(treeNode.id);
					//document.getElementById('frame').crc='http://ww.baidu.com';
					return true;
				}
			}

			function zTreeOnAsyncError(event, treeId, treeNode){
				alert("异步加载失败!");
			}
			function zTreeOnAsyncSuccess(event, treeId, treeNode, msg){
			}
			/***********************当你点击父节点是,会异步访问servlet,把id传过去*****************************/
			var zNodes=[];
			/*
			 var zNodes =[
			{ id:1, pId:0, name:"parentNode 1", open:true},
			{ id:11, pId:1, name:"parentNode 11"},
			{ id:111, pId:11, name:"leafNode 111"},
			{ id:112, pId:11, name:"leafNode 112"},
			{ id:113, pId:11, name:"leafNode 113"},
			{ id:114, pId:11, name:"leafNode 114"},
			{ id:12, pId:1, name:"parentNode 12"},
			{ id:121, pId:12, name:"leafNode 121"},
			{ id:122, pId:12, name:"leafNode 122"},
			{ id:123, pId:12, name:"leafNode 123"},
			{ id:13, pId:1, name:"parentNode 13", isParent:true},
			{ id:2, pId:0, name:"parentNode 2", isParent:true}
			]; */
			$(document).ready(function(){
				$.get("<%=basePath%>/main/tree_data.do", function(resu){
					var zNodes = eval("(" + resu + ")");
					$.fn.zTree.init($("#treeDemo"), setting, zNodes);
				});
			}); 
	
	</SCRIPT>
</HEAD>

<BODY>
<div id="divFrame" style="width:100%;height:17px">
<div class="content_wrap" style="width:100%">
	<div class="zTreeDemoBackground left" style="width:35%;">
		<div class="zTreeDemoBackground left" style="width:90%">
			&nbsp;
			<input type="text" name="param" style="width:70%">
			<input type="button" style="width:20%" value="搜索" onclick="searchM();">
			<ul id="treeDemo" class="ztree" style="width:100%"></ul>
		</div> 
		<div style="width:10%;float:left;height:1px"></div>
	</div>
	<div style="width:65%;float:left;">
		
		<iframe id="frame" runat="server" src="/index.php/Admin/Member/editmember?memberkey=109" width="100%" height="50" style=""  frameborder="no" border="0" marginwidth="0" marginheight="0" scrolling="yes" allowtransparency="yes"></iframe>
	</div>
</div>
</div>
</BODY>
<script>
var hh = $(document).height() - 13;
$("#divFrame").css("height", hh + "px");
$("#frame").attr("height", hh);

//alert( $(document).height() );
</script>
</HTML>