<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/commons/taglibs.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>银行管理</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">    
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<link href="<%=basePath%>/Public/css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/icon.css">
<script src="<%=basePath%>/Public/js/jquery-1.7.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/Public/js/jquery.easyui.min.js"></script>
<script src="<%=basePath%>/Public/js/shijian.js" type="text/javascript"></script>
<script src="<%=basePath%>/Public/layer/layer.js"></script>
<style type="text/css">
	.datagrid-row {
		height: 50px;
	}
</style>
<script>
$(document).ready(function(){
	var IsCheckFlag = true;
	$("#grid").datagrid({
	 	 onClickCell: function (rowIndex, field, value) {
	         IsCheckFlag = false;
	     },
	 	 onSelect: function (rowIndex, rowData) {
	         if (!IsCheckFlag) {
	             IsCheckFlag = true;
	             $("#grid").datagrid("unselectRow", rowIndex);
	         }
	     },    
	 	 onUnselect: function (rowIndex, rowData) {
	         if (!IsCheckFlag) {
	             IsCheckFlag = true;
	             $("#grid").datagrid("selectRow", rowIndex);
	         }
	     }
	})
});

/* 操作 */

function formatOper(value,row,index){
	var bankCardId = row.bankCardId;
	return "<a href='javascript:openDel(\"" + bankCardId + "\");'>删除</a>";
}
/* 多启用按钮 */
function openAbles()
{	
	var select = $('#grid').datagrid('getSelected');
	if(select){
		 var index = layer.load(1, {
	  shade: [0.1,'#fff'] //0.1透明度的白色背景
	});
	 var str = "";
	var rows  = $('#grid').datagrid("getRows");
	var ckArr = $("input[name='ck']");
	for(var i=0; i<ckArr.length; i++)
	{
		if(ckArr[i].checked)
		{
			if(str != "")
			{
				str += ",";
			}
			str += rows[i].bankCardId;
		}
	}
	
	$.post("<%=basePath%>/system/startEnable.do",{delKeys:str},function(result){
		layer.closeAll('loading');
		data = eval("(" + result + ")");
		//alert(result)
		if(data.code == 100){
			$('#grid').datagrid('reload');
			$.messager.show({
							title:'提示',
							msg:'<div class="msgs">已启用</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
		}
	}); 
	}else{
		$.messager.show({
							title:'提示',
							msg:'<div class="msgs">请选择你要启用的数据</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
	}
	
}

/* 多禁用按钮 */
function openDisables()
{	
	var select = $('#grid').datagrid('getSelected');
	if(select){
		 var index = layer.load(1, {
	  shade: [0.1,'#fff'] //0.1透明度的白色背景
	});
	 var str = "";
	var rows  = $('#grid').datagrid("getRows");
	var ckArr = $("input[name='ck']");
	for(var i=0; i<ckArr.length; i++)
	{
		if(ckArr[i].checked)
		{
			if(str != "")
			{
				str += ",";
			}
			str += rows[i].bankCardId;
		}
	}
	
	$.post("<%=basePath%>/system/forbidden.do",{delKeys:str},function(result){
		layer.closeAll('loading');
		data = eval("(" + result + ")");
		//alert(result)
		if(data.code == 100){
			$('#grid').datagrid('reload');
			$.messager.show({
							title:'提示',
							msg:'<div class="msgs">已禁用</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
		}
	}); 
	}else{
		$.messager.show({
							title:'提示',
							msg:'<div class="msgs">请选择您要禁用的数据</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
	}
	
}
function formatEnable(row,value,index){
	if(value.enable == '0')
		return "<a style='color:#FF0000'>禁用</a>";
	return "<a style='color:#008000'>启用</a>";
}

/* 添加按钮 */
function add()
{	
	$("#bankName").val('');
    $('#adddlg').panel({title: "添加银行卡"});
	$('#adddlg').dialog('open');
}

/* 添加 保存 角色 */
function saveAddRole()
{	
	var bankName = $('#bankName').val();
	if(bankName==""||bankName==null){
		layer.msg("请填写银行卡名称")
		return 0;
	}
	$('#formAddRole').form('submit', {
		url : '<%=basePath%>/system/saveBank.do',
		onSubmit : function() {
			var index = layer.load(1, {
				  shade: [0.1,'#fff'] //0.1透明度的白色背景
			})
			return $(this).form('validate');
		},
		success : function(result) {
			data = eval("(" + result + ")");
			if(data.code == 100)
			{
				layer.closeAll('loading');
				$('#adddlg').dialog('close');
				$('#grid').datagrid('reload');
				$.serviceTyper.show({
					title:'提示',
					msg:'<div class="msgs">添加成功</div>',
					timeout: 1000, 
					showType:'fade',
					style:{right:'', bottom:''}
				});
			}
			else
			{
				layer.closeAll('loading');
				$('#adddlg').dialog('close');
				$.messager.show({
								title:'提示',
								msg:'<div class="msgs">'+ data.msg +'</div>',
								timeout: 1000, 
								showType:'fade',
								style:{right:'', bottom:''}
				});
			}
		}
	}); 
	
}

function openDel(bankCardId){
	if(window.confirm('你确定要删除吗？')){
		$.post("<%=basePath%>/system/bankDel.do",{delKeys:bankCardId},function(result){
			layer.closeAll('loading');
			data = eval("(" + result + ")");
			if(data.code == 100){
				$('#grid').datagrid('reload');
				$.serviceTyper.show({
								title:'提示',
								msg:'<div class="msgs">删除成功</div>',
								timeout: 1000, 
								showType:'fade',
								style:{right:'', bottom:''}
				});
			}else{
				$.messager.show({
								title:'提示',
								msg:'<div class="msgs">'+ data.msg + '</div>',
								timeout: 1000, 
								showType:'fade',
								style:{right:'', bottom:''}
				});
			}
		}); 
	}
}
</script>
</head>
<body class="easyui-layout">
<!----------------------- 功能按钮 ----------------------->
<div id="tb" style="height:auto;display: none;">   
	 <div style="width:110%; margin-bottom:3px;text-align: left;height:35px;"> 
	 	<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="add()" >添加银行卡</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-ok" plain="true" onclick="openAbles()" >启用</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-lock" plain="true" onclick="openDisables()" >禁用</a>
	 </div>
</div>
		<table id="grid" class="easyui-datagrid" style="width:100%;height:100%;display: none;" title="<img alt='推送消息管理' style='width:17px;height:17px;' src='<%=basePath%>/img/titleImg.png'><div style='height:20px;line-height:22px;margin-left:25px;margin-top:-20;'>银行管理</div>"
			url="<%=basePath%>/system/bankListData.do" toolbar="#tb"
			rownumbers="true" pagination="true" striped="true" singleSelect="false" loadMsg="正在努力为您加载数据">
			<thead>
				<tr>
					<th field="ck" checkbox="true" id="ck" name="ck"></th>
					<th field="bankName" width="50%" align="center">用户名</th>
					<th field="enable" width="47%" align="center" formatter="formatEnable">状态</th>
					<!-- <th field="oper" width="17%" align="center"  formatter="formatOper">操作</th> -->
				</tr>
			</thead>
		</table>
		
<!---------------------------------- 添加界面 ----------------------------->
<div id="adddlg" class="easyui-dialog" style="width: 400px; height:200px; padding: 10px 20px;display: none;" closed="true"  buttons="#adddlg-buttons" singleSelect="false" loadMsg="正在努力为您加载数据">
	<form id="formAddRole" name="formAddRole" method="post" >
	
            <div style="width:100%;height:21px;">
            	<div style="width: 80%;height: 30px;"></div>	
	        	<label style="display:inline-block;width:80px;color:#000;font-size:15px;">银行卡名称:</label>
	            <input style="width:214px;height:28px; margin-top:-9px;vertical-align:middle;font-size:15px;color:#000;border:1px solid #95B8E7" type="text"  id="bankName" name="bankName" value="" />
	        </div>
    
	 </form>
</div>

 
<!-------------------------------------------- 确认取消按钮 -------------------------------------------------->
<div id="adddlg-buttons" style="height: 30px;display: none;">
	<div style="width:140px;height:35px;float: right;">
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="saveAddRole()" iconcls="icon-save">保存</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="javascript:$('#adddlg').dialog('close')"
		iconcls="icon-cancel">取消</a>	
	</div>
</div>		
		
</body>
 

</html>