<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/commons/taglibs.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>后台操作日志</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">    
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<link href="<%=basePath%>/Public/css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/icon.css">
<script src="<%=basePath%>/Public/js/jquery-1.7.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/Public/js/jquery.easyui.min.js"></script>
<script src="<%=basePath%>/Public/js/shijian.js" type="text/javascript"></script>
<script src="<%=basePath%>/Public/layer/layer.js"></script>
<script>
$(document).ready(function(){
	var IsCheckFlag = true;
	$("#grid").datagrid({
	 	 onClickCell: function (rowIndex, field, value) {
	         IsCheckFlag = false;
	     },
	 	 onSelect: function (rowIndex, rowData) {
	         if (!IsCheckFlag) {
	             IsCheckFlag = true;
	             $("#grid").datagrid("unselectRow", rowIndex);
	         }
	     },    
	 	 onUnselect: function (rowIndex, rowData) {
	         if (!IsCheckFlag) {
	             IsCheckFlag = true;
	             $("#grid").datagrid("selectRow", rowIndex);
	         }
	     }
	})
});




/* 操作 */

function formatOper(value,row,index){
	var logId = row.logId;
	return "<a href='javascript:openchakan(\"" + logId + "\");'>查看</a>"
	+ " | <a href='javascript:openshanchudan(\"" + logId + "\");'>删除</a>"
	+ " | <a href='javascript:openzaifa(\"" + logId + "\");'>再次发送</a>";
} 
/* 添加 保存 角色 */
function saveAddRole()
{	
	var now = new Date();
	var biaoti = $('#title').val();
	if(biaoti==""||biaoti==null){
		layer.msg("请填写消息标题")
		return 0;
	}
	var enable = $('#content').val();
	if(enable==""||enable==null){
		layer.msg("请填写消息内容")
		return 0;
	}

	$('#formAddRole').form('submit', {
		url : '<%=basePath%>/message/saveMessage.do',
		onSubmit : function() {
			var index = layer.load(1, {
				  shade: [0.1,'#fff'] //0.1透明度的白色背景
			})
			return $(this).form('validate');
		},
		success : function(result) {
			data = eval("(" + result + ")");
			if(data.code == 100)
			{
				layer.closeAll('loading');
				$('#adddlg').dialog('close');
				$('#grid').datagrid('reload');
				$.messager.show({
					title:'提示',
					msg:'<div class="msgs">保存成功</div>',
					timeout: 1000, 
					showType:'fade',
					style:{right:'', bottom:''}
				});
			}
			else
			{
				$.messager.show({
					title:'提示',
					msg:data.msg,
					timeout: 1000,  
					showType:'fade',
					style:{right:'', bottom:''}
				});
			}
		}
	}); 
	
}


/* 添加按钮 */
function add()
{	
	//$('#name').combobox('clear');
    document.getElementById("title").value = "";
    document.getElementById("content").value = "";
    $('#mode').val("add");	
    $('#adddlg').panel({title: "添加推送消息"});
	$('#adddlg').dialog('open');
}
/* 删除按钮 */
function openshanchu()
{	
	var select = $('#grid').datagrid('getSelected');
	if(select){
		if(window.confirm('你确定要删除吗？')){
		 var index = layer.load(1, {
	  shade: [0.1,'#fff'] //0.1透明度的白色背景
	});
	 var str = "";
	var rows  = $('#grid').datagrid("getRows");
	var ckArr = $("input[name='ck']");
	for(var i=0; i<ckArr.length; i++)
	{
		if(ckArr[i].checked)
		{
			if(str != "")
			{
				str += ",";
			}
			str += rows[i].messageId;
		}
	}
	
	$.post("<%=basePath%>/message/shachuMessage.do",{delKeys:str},function(result){
		layer.closeAll('loading');
		data = eval("(" + result + ")");
		if(data.code == 100){
			$('#grid').datagrid('reload');
			$.messager.show({
							title:'提示',
							msg:'<div class="msgs">删除成功</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
		}
		else{
			$('#grid').datagrid('reload');
			$.messager.show({
							title:'提示',
							msg:'<div class="msgs">删除失败，您没有权限</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
		}
	}); 
	}
	}else{
		$.messager.show({
							title:'提示',
							msg:'<div class="msgs">请选择您要删除的数据</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
	}
	
}
//单删
function openshanchudan(messageId)
{	
	if(window.confirm('你确定要删除吗？')){
	$.post("<%=basePath%>/message/shachuMessage.do",{delKeys:messageId},function(result){
		layer.closeAll('loading');
		data = eval("(" + result + ")");
		if(data.code == 100){
			$('#grid').datagrid('reload');
			$.messager.show({
							title:'提示',
							msg:'<div class="msgs">删除成功</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
		}
		else{
			$('#grid').datagrid('reload');
			$.messager.show({
							title:'提示',
							msg:'<div class="msgs">删除失败，您没有权限</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
		}
	}); 
	}
}

//再次发送
function openzaifa(messageId)
{	
	 var index = layer.load(1, {
	  shade: [0.1,'#fff'] //0.1透明度的白色背景
	});
	$.post("<%=basePath%>/message/openzaifa.do",{delKeys:messageId},function(result){
	layer.closeAll('loading');
	
	data = eval("(" + result + ")");
	if(data.code == 100){
		layer.closeAll('loading');
		$('#grid').datagrid('reload');
		$.messager.show({
						
						title:'提示',
						msg:'<div class="msgs">发送成功</div>',
						timeout: 1000, 
						showType:'fade',
						style:{right:'', bottom:''}
		});
	}
}); 
}

/* 查看推送消息 */
function openchakan(messageId){
	$.post("<%=basePath%>/message/bianji.do",{messageId:messageId},function(result){
		data = eval("(" + result + ")");
		$('#title1').val(data.title1);
		$('#content1').val(data.content1);
		$('#adddlg1').panel({title: "查看推送"});
		//$('#name').combobox('setValue', data.name);
		$('#adddlg1').dialog('open');
	}); 
}

//搜索
function sousuo()
{
	var ssuser = $("#ssuser").val();
	var startDate = $("#startDate").datebox('getValue');
	var endDate = $("#endDate").datebox('getValue');
	$('#grid').datagrid('load', {startDate:startDate,endDate:endDate,ssuser:ssuser});
}

</script>
</head>
<body class="easyui-layout">
<!----------------------- 功能按钮 ----------------------->
<div id="tb" style="height:auto;display: none;">   
	<div style="width:110%; margin-bottom:3px;text-align: left;height:35px;" > 
		<div style="float: left;height: 30px;line-height: 30px;"><label> &nbsp;&nbsp; 用户查询: </label>
   		  <input name="ssuser" id="ssuser" style="margin-top: 0px;border:1px #95B8E7 solid;width: 150px;height: 23px"  />
	   </div>
        <div style="float: left;height: 30px;line-height: 30px;"><label> &nbsp;&nbsp; 开始日期: </label>
		 	<input  id="startDate" nastartDate" type= "text" class= "easyui-datebox" data-options="formatter:myformatter,parser:myparser"> </input>
		 </div>
		 <div style="float: left;height: 30px;line-height: 30px;"><label> &nbsp;&nbsp; 结束日期: </label>
		 	<input  id="endDate" name="endDate" type= "text" class= "easyui-datebox" data-options="formatter:myformatter,parser:myparser"> </input>
		 </div>
		 <div onclick="sousuo()" style=" float: left;border:1px solid #000;margin-left: 25px;cursor:pointer;text-align:center;line-height:24px;background-color: #FFF;color:#000;width: 100px;height:23px;margin-top: 3px;border-radius: 5px;"
	    	class="easyui-linkbutton" iconCls="icon-search">搜索	   
		</div> 
	 </div> 
</div>
		<table id="grid" class="easyui-datagrid" style="width:100%;height:100%;display: none;" title="<img alt='推送消息管理' style='width:17px;height:17px;' src='<%=basePath%>/img/titleImg.png'><div style='height:20px;line-height:22px;margin-left:25px;margin-top:-20;'>推送消息管理</div>"
			url="<%=basePath%>/system/logListData.do" toolbar="#tb"
			rownumbers="true" pagination="true" pageList="[15,20,25,30]" striped="true" singleSelect="false" loadMsg="正在努力为您加载数据">
			<thead>
				<tr>
					<th field="userName" width="15%" align="center">操作者</th>
					<th field="funName" width="17%" align="center">功能模块名</th>
					<th field="action" width="17%" align="center">操作动作</th>
					<th field="content" width="30%" align="center">内容</th>
					<th field="createDate" width="20%" align="center">时间</th>
					<!-- <th field="oper" width="20%" align="center" formatter="formatOper" align="center">操作</th> -->
				</tr>
			</thead>
		</table>
<!---------------------------------- 添加界面 ----------------------------->
<div id="adddlg" class="easyui-dialog" style="width: 400px; height: 220px; padding: 10px 20px;display: none;" closed="true"  buttons="#adddlg-buttons" singleSelect="false" loadMsg="正在努力为您加载数据">
	<form id="formAddRole" name="formAddRole" method="post" >
		<div style="float:left;width:100%;height:21px;margin-top: 15px;">
        	<label style="display:inline-block;width:100px;color:#000;font-size:15px;" >消息标题:</label>
            <input style="vertical-align:middle;font-size:15px;width:220px; color:#000;margin-top: -6px;border:1px solid #95B8E7" type="text"  id="title" name="title" /></div>
 		
       
        <div style="float:left;width:100%;height:21px;margin-top: 30px;">
        	<label style="display:inline-block;width:100px;color:#000;font-size:15px;" >消息内容:</label>
            <textarea  style="vertical-align:middle;font-size:15px;width:220px;color:#000;margin-top: -6px;border:1px solid #95B8E7" type="text"  id="content" name="content" /></textarea></div>
 		
		  
		    <input type="hidden" name="mode" id="mode" value=""/>
		   <input type="hidden" name="messageId" id="messageId" value=""/>
		    <input type="hidden" name="iId" id="iId" value=""/>
	 </form>
	
</div>


<!-------------------------------------------- 确认取消按钮 -------------------------------------------------->
<div id="adddlg-buttons" style="height: 30px;display: none;">
	<div style="width:140px;height:35px;float: right;">
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="saveAddRole()" iconcls="icon-save">推送</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="javascript:$('#adddlg').dialog('close')"
		iconcls="icon-cancel">取消</a>	
	</div>
</div> 
</body>
 

</html>