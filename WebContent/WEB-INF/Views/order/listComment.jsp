<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/commons/taglibs.jsp" %>
 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>  
<base href="<%=basePath%>">
<title>商家评价(没用)</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">    
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<link href="<%=basePath%>/Public/css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/icon.css">
<script src="<%=basePath%>/Public/js/jquery-1.7.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/Public/js/jquery.easyui.min.js"></script>
<script src="<%=basePath%>/Public/layer/layer.js"></script>
<link href="<%=basePath%>/Public/css/pinglun.css" rel="stylesheet" type="text/css" />
<script src="<%=basePath%>/Public/js/shijian.js" type="text/javascript"></script>
<script>
var yidu = 0;
$(document).ready(function(){
//回车点击事件--搜索功能
	$(document).keydown(function(event){
		if(event.keyCode==13){
			//$("#login").click();
			sousuo();
		}
	}) ;
var IsCheckFlag = true;
$("#grid").datagrid({
 	 onClickCell: function (rowIndex, field, value) {
         IsCheckFlag = false;
     },
 	 onSelect: function (rowIndex, rowData) {
         if (!IsCheckFlag) {
             IsCheckFlag = true;
             $("#grid").datagrid("unselectRow", rowIndex);
         }
     },    
 	 onUnselect: function (rowIndex, rowData) {
         if (!IsCheckFlag) {
             IsCheckFlag = true;
             $("#grid").datagrid("selectRow", rowIndex);
         }
     }
})


});

/* 操作 */
function formatOper(value,row,index){
	return "<a href='javascript:openDel(\"" + row['commentId'] + "\");'>删除</a>";
} 


/* 多删除按钮 */
function openDels(commentId)
{	
	var select = $('#grid').datagrid('getSelected');
	if(select){
	if(window.confirm('你确定要删除吗？')){
		 var index = layer.load(1, {
	  shade: [0.1,'#fff'] //0.1透明度的白色背景
	});
	 var str = "";
	var rows  = $('#grid').datagrid("getRows");
	var ckArr = $("input[name='ck']");
	for(var i=0; i<ckArr.length; i++)
	{
		if(ckArr[i].checked)
		{
			if(str != "")
			{
				str += ",";
			}
			str += rows[i].commentId;
		}
	}
	
	$.post("<%=basePath%>/order/delShopComment.do",{delKeys:str},function(result){
		layer.closeAll('loading');
		data = eval("(" + result + ")");
		//alert(result)
		if(data.code == 100){
			$('#grid').datagrid('reload');
			$.messager.show({
							title:'提示',
							msg:'<div class="msgs">删除成功</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
		}
		else{
			$('#grid').datagrid('reload');
			$.messager.show({
							title:'提示',
							msg:'<div class="msgs">删除失败，您没有权限</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
		}
	}); 
	}
	}else{
		$.messager.show({
							title:'提示',
							msg:'<div class="msgs">请选择您要删除的数据</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
	}
	
}

/* 单删除 */
function openDel(commentId)
{	
	if(window.confirm('你确定要删除吗？')){
	$.post("<%=basePath%>/order/delShopComment.do",{delKeys:commentId},function(result){
		layer.closeAll('loading');
		data = eval("(" + result + ")");
		//alert(result)
		if(data.code == 100){
			$('#grid').datagrid('reload');
			$.messager.show({
							title:'提示',
							msg:'<div class="msgs">删除成功</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
		}
		else{
			$('#grid').datagrid('reload');
			$.messager.show({
							title:'提示',
							msg:'<div class="msgs">删除失败，您没有权限</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
		}
	}); 
	}
}


/* 多标记按钮 */
function openreads(feedbackId)
{	
	var select = $('#grid').datagrid('getSelected');
	if(select){
		 var index = layer.load(1, {
	  shade: [0.1,'#fff'] //0.1透明度的白色背景
	});
	 var str = "";
	var rows  = $('#grid').datagrid("getRows");
	var ckArr = $("input[name='ck']");
	for(var i=0; i<ckArr.length; i++)
	{
		if(ckArr[i].checked)
		{
			if(str != "")
			{
				str += ",";
			}
			str += rows[i].feedbackId;
		}
	}
	
	$.post("<%=basePath%>/fedback/readFedback.do",{delKeys:str},function(result){
		layer.closeAll('loading');
		data = eval("(" + result + ")");
		//alert(result)
		if(data.code == 100){
			$('#grid').datagrid('reload');
			$.messager.show({
							title:'提示',
							msg:'<div class="msgs">标记成功</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
		}
	}); 
	}else{
		$.messager.show({
							title:'提示',
							msg:'<div class="msgs">请选择您要标记的数据</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
	}
	
}

/* 单标记 */
function openread(feedbackId)
{	
	$.post("<%=basePath%>/fedback/readFedback.do",{delKeys:feedbackId},function(result){
		layer.closeAll('loading');
		data = eval("(" + result + ")");
		//alert(result)
		if(data.code == 100){
			$('#grid').datagrid('reload');
			$.messager.show({
							title:'提示',
							msg:'<div class="msgs">标记成功</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
		}
	}); 
}
//搜索
function sousuo()
{
	var ssuser = document.getElementById("ssuser").value;
	var ssOrderNum = document.getElementById("ssOrderNum").value;
	var ssShop = document.getElementById("ssShop").value;
	$('#grid').datagrid('load', {ssuser:ssuser,ssOrderNum:ssOrderNum,ssShop:ssShop});
}


</script>
</head>
<body class="easyui-layout">
<!--------------------------------------- 功能按钮 -------------------------------------------->
<div id="tb" style="height:auto;display: none;">   
	<div style="width:110%; margin-bottom:3px;text-align: left;height:35px;"> 
			<!-- <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-back" plain="true" onclick="openreads()" >标为已读</a> -->
	        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="openDels()" >删除</a>
	 </div> 
<!------------------------------------ 搜索 ---------------------------------------->
	 <div style="text-align: list;background-color: #f4f4f4;width:100%;height:30px; border-top: 2px solid #D9D9D9;border-bottom: 0px solid #D9D9D9;">
		    <div style="float: left;height: 30px;line-height: 30px;"><label> &nbsp;&nbsp; 评论用户: </label>
	   		<input name="ssuser" id="ssuser" style="margin-top: 0px;border:1px #95B8E7 solid;width: 150px;height: 23px"  />
		   </div>
		   <div style="float: left;height: 30px;line-height: 30px;"><label> &nbsp;&nbsp; 订单号: </label>
	   		<input name="ssOrderNum" id="ssOrderNum" style="margin-top: 0px;border:1px #95B8E7 solid;width: 150px;height: 23px"  />
		   </div>
		   <div style="float: left;height: 30px;line-height: 30px;"><label> &nbsp;&nbsp; 商家: </label>
	   		<input name="ssShop" id="ssShop" style="margin-top: 0px;border:1px #95B8E7 solid;width: 150px;height: 23px"  />
		   </div>
		   <div onclick="sousuo()" style=" float: left;border:1px solid #000;margin-left: 25px;cursor:pointer;text-align:center;line-height:24px;background-color: #FFF;color:#000;width: 100px;height:23px;margin-top: 3px;border-radius: 5px;"
	    class="easyui-linkbutton" iconCls="icon-search">搜索	   
		</div>  
	</div>
	
		
</div>	    
		<table id="grid" class="easyui-datagrid"  data-options="nowrap: false" style="width:100%;height:100%;display: none;" title="<img alt='帖子管理' style='width:17px;height:17px;' src='<%=basePath%>/img/titleImg.png'><div style='height:20px;line-height:22px;margin-left:25px;margin-top:-20;'>评论</div>"
			url="<%=basePath%>/order/CommentListData.do" toolbar="#tb"
			rownumbers="true" pagination="true" singleSelect="false" loadMsg="正在努力为您加载数据">
			<thead>
				<tr>
					<th field="ck" checkbox="true" id="ck" name="ck"></th>
					<th field="userName" width="10%" align="center">评论用户</th>
					<th field="orderNum" width="10%" align="center" >评论订单</th>
					<th field="shopName" width="10%" align="center" >所属商家</th>
					<th field="content" width="15%" align="center">内容</th>
					<th field="star" width="10%" align="center">店铺环境</th>
					<th field="starTwo" width="10%" align="center">服务态度</th>
					<th field="starThree" width="10%" align="center">消费体验</th>
					<th field="createDate" width="10%" align="center">评论时间</th>
					<th field="oper" width="10%" align="center" formatter="formatOper" >操作</th>
				</tr>
			</thead>
		</table>
</body>
 
</html>
