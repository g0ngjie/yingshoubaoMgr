<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/commons/taglibs.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>订单信息统计</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">    
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<link href="<%=basePath%>/Public/css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/icon.css">
<link href="<%=basePath%>/Public/uploadify/uploadify.css" type="text/css" rel="stylesheet" />
<!--装载文件-->

<script src="<%=basePath%>/Public/js/jquery-1.7.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/Public/js/jquery.easyui.min.js"></script>

<script type="text/javascript" src="<%=basePath%>/Public/uploadify/jquery.uploadify-3.1.js"></script>
<script type="text/javascript" src="<%=basePath%>/Public/uploadify/swfobject.js"></script>

<script src="<%=basePath%>/Public/js/shijian.js" type="text/javascript"></script>
<script src="<%=basePath%>/Public/layer/layer.js"></script>
<link href="<%=basePath%>/Public/css/order.css" rel="stylesheet" type="text/css" />
<style type="text/css">
	.datagrid-btable tr{
		height: 50px;
	}
</style>
<script>
$(document).ready(function(){
});

$(document).ready(function(){
	var IsCheckFlag = true;
	$("#grid").datagrid({
	 	 onClickCell: function (rowIndex, field, value) {
	         IsCheckFlag = false;
	     },
	 	 onSelect: function (rowIndex, rowData) {
	         if (!IsCheckFlag) {
	             IsCheckFlag = true;
	             $("#grid").datagrid("unselectRow", rowIndex);
	         }
	     },    
	 	 onUnselect: function (rowIndex, rowData) {
	         if (!IsCheckFlag) {
	             IsCheckFlag = true;
	             $("#grid").datagrid("selectRow", rowIndex);
	         }
	     }
	})
});




/* 操作 */

function formatOper(value,row,index){
	var orderId = row.orderId;
	/* if(pan==0)
		return "<a href='javascript:openChakan(\"" + orderId + "\");'>查看详情</a>"
		+ " | <a href='javascript:openQuxiao(\"" + orderId + "\");'>取消订单</a>";
	if(pan==1)
		return "<a href='javascript:openZhipai(\"" + orderId + "\");'>重新指派</a>"
		+ " | <a href='javascript:openChakan(\"" + orderId + "\");'>查看详情</a>"
		+ " | <a href='javascript:openGaiqiang(\"" + orderId + "\");'>改为抢单</a>"
		+ " | <a href='javascript:openQuxiao(\"" + orderId + "\");'>取消订单</a>";
	if(pan==2)
		return "<a href='javascript:openChakan(\"" + orderId + "\");'>查看详情</a>";
	if(pan==3)
		return "<a href='javascript:openChakan(\"" + orderId + "\");'>查看详情</a>"
		+ " | <a href='javascript:openChakanjindu(\"" + orderId + "\");'>查看进度详情</a>";
	return "<a href='javascript:openChakan(\"" + orderId + "\");'>查看详情</a>"
		+ " | <a href='javascript:openChakanjindu(\"" + orderId + "\");'>查看进度详情</a>"
		+ " | <a href='javascript:openOkWancheng(\"" + orderId + "\");'>确认完成</a>";	 */
} 

/* 显示状态 */
function formatZhifuh(value,row,index){
	if(value == '0'){
		return "<a style='color:#000'>余额支付</a>";
		}
	return "<a style='color:#000'>微信支付</a>";
}


/* 显示状态 */
function formatSnatch(value,row,index){
	if(value == '0'){
		return "<a style='color:#FF0000'>未评价</a>";
		}
	else{
		return "<a style='color:#008000'>已评价</a>";
		}
}

/* 添加 保存 角色 */
function saveAddRole()
{	
	var name = $('#name').val();
	if(name==""||name==null){
		layer.msg("请填写维修名称")
		return 0;
	}
	var leibie = $('#leibie').combobox('getValue');
	if(leibie==""||leibie==null){
		layer.msg("请选择类别")
		return 0;
	}
	var dizhi = $('#dizhi').combobox('getValue');
	if(dizhi==""||dizhi==null){
		layer.msg("请选择地址")
		return 0;
	}
	var renyuan = $('#renyuan').combobox('getValue');
	if(cang == 0 && (renyuan==""||renyuan==null)){
		layer.msg("请选择人员")
		return 0;
	}
	
	var speAddress = $('#speAddress').val();
	if(speAddress==""||speAddress==null){
		layer.msg("请填写具体地址")
		return 0;
	}
	var introduction = $('#introduction').val();
	if(introduction==""||introduction==null){
		layer.msg("请填写说明")
		return 0;
	}
	
	var remarks = $('#remarks').val();
	if(remarks==""||remarks==null){
		layer.msg("请填写备注")
		return 0;
	}

	var budgetPrice = $('#budgetPrice').numberbox('getValue');
	if(budgetPrice==""||budgetPrice==null){
		layer.msg("请填写预算金额")
		return 0;
	}

	$('#formAddRole').form('submit', {
		url : '<%=basePath%>/order/saveOrder.do',
		onSubmit : function() {
			var index = layer.load(1, {
				  shade: [0.1,'#fff'] //0.1透明度的白色背景
			})
			return $(this).form('validate');
		},
		success : function(result) {
			data = eval("(" + result + ")");
			if(data.code == 100)
			{
				layer.closeAll('loading');
				$('#adddlg').dialog('close');
				$('#grid').datagrid('reload');
				$.userr.show({
					title:'提示',
					msg:'<div class="msgs">保存成功</div>',
					timeout: 1000, 
					showType:'fade',
					style:{right:'', bottom:''}
				});
			}
			else
			{
				$.userr.show({
					title:'提示',
					msg:data.msg,
					timeout: 1000,  
					showType:'fade',
					style:{right:'', bottom:''}
				});
			}
		}
	}); 
	
}


/* 添加按钮 */
function openAdd()
{	
	cang = 1;
	$("#xycaidan").show();
    $('#name').val("");
    $('#leibie').combobox('setValue', '');
    $('#dizhi').combobox('setValue', '');
    $('#renyuan').combobox('setValue', '');
    $('#speAddress').val("");
    $('#introduction').val("");
    $('#speAddress').val("");
    $('#remarks').val("");
    $('#budgetPrice').numberbox('setValue', '');
    $('#img1').attr("src", '<%=basePath2%>img/null.png');
    $('#img2').attr("src", '<%=basePath2%>img/null.png');
    $('#img3').attr("src", '<%=basePath2%>img/null.png');
    $('#img4').attr("src", '<%=basePath2%>img/null.png');
    $('#img5').attr("src", '<%=basePath2%>img/null.png');
    $('#mode').val("add");	
    $('#adddlg').panel({title: "新建订单"});
	$('#adddlg').dialog('open');
}

/* 重新指派 */
function openZhipai(orderId)
{	
	$('#orderId').val(orderId);
	$.post("<%=basePath%>/order/chaZhipai.do",{orderId:orderId},function(result){
		layer.closeAll('loading');
		data = eval("(" + result + ")");
		$('#renyuans').combobox({    
	    	url:'<%=basePath%>/order/listxuanRenyuan.do', 
	    	queryParams: {
				"area":data.quyu
			}
	    	
		});  
		  
		$('#zhipai').panel({title: "指派人员"});
		$('#zhipai').dialog('open');
	}); 
}

/* 保存指派 */
function savezhipai()
{	
	var userId = $('#userId').val();
	var orderId = $('#orderId').val();
	$.post("<%=basePath%>/order/saveZhipai.do",{delKeys:userId,orderId:orderId},function(result){
		layer.closeAll('loading');
		data = eval("(" + result + ")");
		//alert(result)
		if(data.code == 100){
			$('#grid').datagrid('reload');
			$.messager.show({
							title:'提示',
							msg:'<div class="msgs">指派成功</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
		}
	}); 
	$('#zhipai').dialog('close');
	
}

/* 确认完成 */
function openOkWancheng(orderId)
{	
	$.post("<%=basePath%>/order/saveWancheng.do",{orderId:orderId},function(result){
		layer.closeAll('loading');
		data = eval("(" + result + ")");
		//alert(result)
		if(data.code == 100){
			$('#grid').datagrid('reload');
			$.messager.show({
							title:'提示',
							msg:'<div class="msgs">确认完成</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
		}
	}); 
	$('#zhipai').dialog('close');
	
}
//取消订单
function openQuxiao(orderId)
{	
	if(window.confirm('你确定要取消吗？')){
	$.post("<%=basePath%>/order/orderQuxiao.do",{orderId:orderId},function(result){
		layer.closeAll('loading');
		data = eval("(" + result + ")");
		if(data.code == 100){
			$('#grid').datagrid('reload');
			$.messager.show({
							title:'提示',
							msg:'<div class="msgs">已取消</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
		}
	}); 
	}
}

/*改为抢单*/
function openGaiqiang(orderId)
{	
	if(window.confirm('你确定要将该订单改为抢单吗？')){
	$.post("<%=basePath%>/order/orderGaiqiang.do",{orderId:orderId},function(result){
		layer.closeAll('loading');
		data = eval("(" + result + ")");
		if(data.code == 100){
			$('#grid').datagrid('reload');
			$.messager.show({
							title:'提示',
							msg:'<div class="msgs">已改为抢单</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
		}
	}); 
	}
}

/* 查看进度详情 */
function openChakanjindu(orderId)
{	
	$('#grids1').datagrid({
		url:"<%=basePath%>/order/chaXiangqing.do?orderId="+orderId,
		   });
		$('#grids1').datagrid('load',{});
		$('#adddlg2').panel({title: "查看进度详情"});
		$('#adddlg2').dialog('open');
}
/* 查看详情 */
function openChakan(orderId){
	$.post("<%=basePath%>/order/chakanOrder.do",{orderId:orderId},function(result){
		data = eval("(" + result + ")");
		$('#name1').val(data.name);
		//$('#type').val(data.type);
		if(data.isSnatch==0)
			$('#accuracy1').val("派单");
		else
			$('#accuracy1').val("抢单");
		$('#speAddress1').val(data.speAddress);
		$('#introduction1').val(data.introduction);
		$('#remarks1').val(data.remarks);
		$('#budgetPrice1').val(data.budgetPrice);
		$("#budgetPrice1").numberbox("setValue", data.budgetPrice);
	//	$('#regionId').val(data.regionId);
		//$('#userId').val(data.userId);
		//$('#orderId').val(data.orderId);
		$('#renyuan1').val(data.renyuan);
		$('#leibie1').val(data.leibie);
		$('#dizhi1').val(data.dizhi);
		
		
		var img1 = data.img.split(",")[0];
		if(img1 == null || img1.trim() == "")
			$('#img11').attr("src", '<%=basePath2%>img/null.png');
		else{
			$('#img11').attr("src", '<%=basePath2%>'+'img/'+img1);
			}
		var img2 = data.img.split(",")[1];
		if(img2 == null || img2.trim() == "")
			$('#img21').attr("src", '<%=basePath2%>img/null.png');
		else{
			$('#img21').attr("src", '<%=basePath2%>'+'img/'+img2);
			}
		var img3 = data.img.split(",")[2];
		if(img3 == null || img3.trim() == "")
			$('#img31').attr("src", '<%=basePath2%>img/null.png');
		else{
			$('#img31').attr("src", '<%=basePath2%>'+'img/'+img3);
			}
		var img4 = data.img.split(",")[3];
		if(img4 == null || img4.trim() == "")
			$('#img41').attr("src", '<%=basePath2%>img/null.png');
		else{
			$('#img41').attr("src", '<%=basePath2%>'+'img/'+img4);
			}
		var img5 = data.img.split(",")[4];
		if(img5 == null || img5.trim() == "")
			$('#img51').attr("src", '<%=basePath2%>img/null.png');
		else{
			$('#img51').attr("src", '<%=basePath2%>'+'img/'+img5);
			}
		$('#adddlg1').panel({title: "查看详情"});
		$('#adddlg1').dialog('open');
	}); 
}

/*select 选中事件*/
$(document).ready(function () { 
  $("#accuracy").bind("change",function(){ 
    if($(this).val()==0){
    	cang = 1;
        $("#xycaidan").show();
    }  
    else{
    	$("#userId").val("");
    	$("#xycaidan").hide();
    	}
  }); 
});
 /*显示图片*/
function xianshit(value,row,index){ 
	var lu = row.icon;
	if(lu == null || lu.trim() == "")
		return '<img src="<%=basePath2%>img/null.png"  style="width:130px;height:100px;"/>';
	else
		return '<img src="<%=basePath2%>img/'+lu+'"  style="width:130px;height:100px;"/>';
}



 /*显示图片*/
function formatTupian(value,row,index){ 
	var lu = row.icon;
	if(lu == null || lu.trim() == "")
		return '<img src="<%=basePath2%>img/null.png"  style="width:130px;height:100px;"/>';
	else
		return '<img src="<%=basePath2%>img/'+lu+'"  style="width:130px;height:100px;"/>';
}
//搜索
function sousuo()
{
	var proName = $("#proName").val();
	var cityName = $("#cityName").val();
	var SsUserName = $('#SsUserName').val();
	var SsShopName = $('#SsShopName').val();
	var SsOrderNum = $('#SsOrderNum').val();
	var startDate = $('#startDate').datebox('getValue');
	var endDate = $('#endDate').datebox('getValue');
	$('#grid').datagrid('load', {proName:proName,cityName:cityName,SsUserName:SsUserName,SsShopName:SsShopName,SsOrderNum:SsOrderNum,startDate:startDate,endDate:endDate});
}
//导出
function exportWps(){
	var proName = $("#proName").val();
	var cityName = $("#cityName").val();
	var SsUserName = $('#SsUserName').val();
	var SsShopName = $('#SsShopName').val();
	var SsOrderNum = $('#SsOrderNum').val();
	var startDate = $('#startDate').datebox('getValue');
	var endDate = $('#endDate').datebox('getValue');
	window.parent.location="<%=basePath%>/order/exportWpsByOrder.do?proName="+proName+"&cityName="+cityName+"&SsUserName="+SsUserName+"&SsShopName="+SsShopName+"&SsOrderNum="+SsOrderNum+"&startDate="+startDate+"&endDate="+endDate;
}

</script>
</head>
<body class="easyui-layout">
<!----------------------- 功能按钮 ----------------------->
<div id="tb" style="height:auto;display: none;">   
		<div style="width:100px; margin-top:3px; text-align: left;height:35px;float: left; "> 
	   	    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="brick_go" plain="true" onclick="exportWps()" >导出表格</a>
		 </div>
	 	<div style="float: left;height: 30px;line-height: 30px;"><label> &nbsp;&nbsp; 省份: </label>
	   		<input id="proName" name="proName" style="width: 100px" class="easyui-combobox" data-options="    
			        valueField: 'proId',    
			        textField: 'proName',    
			        url: '<%=basePath%>statistics/chaProvince',    
			        onSelect: function(rec){    
			            var url = '<%=basePath%>statistics/chaCity?proId='+rec.proId;    
			        	 $('#cityName').combobox('setValue','');
			            $('#cityName').combobox('reload', url);    
			            $('#proName').val(rec.proName);
			        }" />
		</div>
		<div style="float: left;height: 30px;line-height: 30px;"><label> &nbsp;&nbsp; 城市: </label>
			<input id="cityName" name="cityName" style="width: 100px" class="easyui-combobox" data-options="
					valueField:'cityId',
					textField:'cityName',
					onSelect: function(rec){ 
					 	$('#cityName').val(rec.cityName);
			        }" />
		</div>
	  <div style="text-align: list;background-color: #f4f4f4;width:100%;height:30px; border-top: 2px solid #D9D9D9;border-bottom: 0px solid #D9D9D9;">
	  		<div style="float: left;height: 30px;line-height: 30px;"><label> &nbsp;&nbsp;商家名称: </label>
	   		<input name="SsShopName" id="SsShopName" style="margin-top: 0px;border:1px #95B8E7 solid;width: 150px;height: 23px"  />
		   </div>
		    <div style="float: left;height: 30px;line-height: 30px;"><label> &nbsp;&nbsp;用户名称: </label>
	   		<input name="SsUserName" id="SsUserName" style="margin-top: 0px;border:1px #95B8E7 solid;width: 150px;height: 23px"  />
		   </div>
		     <div style="float: left;height: 30px;line-height: 30px;"><label> &nbsp;&nbsp;订单号: </label>
	   		<input name="SsOrderNum" id="SsOrderNum" style="margin-top: 0px;border:1px #95B8E7 solid;width: 150px;height: 23px"  />
		   </div>
		    <div style="float: left;height: 30px;line-height: 30px;"><label> &nbsp;&nbsp; 开始日期: </label>
	   			<input name="startDate" id="startDate" class="easyui-datebox"  style="margin-top: 0px;border:1px #95B8E7 solid;width: 120px;height: 23px"    data-options="formatter:myformatter,parser:myparser"/>
		   </div>
		    <div style="float: left;height: 30px;line-height: 30px;"><label> &nbsp;&nbsp; 结束日期: </label>
	   			<input name="endDate" id="endDate" class="easyui-datebox"  style="margin-top: 0px;border:1px #95B8E7 solid;width: 120px;height: 23px"    data-options="formatter:myformatter,parser:myparser"/>
		   </div>
		   <div onclick="sousuo()" style=" float: left;border:1px solid #000;margin-left: 25px;cursor:pointer;text-align:center;line-height:24px;background-color: #FFF;color:#000;width: 100px;height:23px;margin-top: 3px;border-radius: 5px;"
	    class="easyui-linkbutton" iconCls="icon-search">搜索	   
		</div>  
	</div>
	<input type="hidden" id="proName" name="proName"/>
	<input type="hidden" id="cityName" name="cityName"/>
</div>
		<table id="grid" class="easyui-datagrid" style="width:100%;height:100%;display: none;" title="<img alt='推送消息管理' style='width:17px;height:17px;' src='<%=basePath%>/img/titleImg.png'><div style='height:20px;line-height:22px;margin-left:25px;margin-top:-20;'>订单信息统计</div>"
			url="<%=basePath%>/order/orderListData.do" toolbar="#tb" data-options="nowrap: false" 
			rownumbers="true" striped="true" pagination="true" singleSelect="false" loadMsg="正在努力为您加载数据">
			<thead>
				<tr>
					<th field="shopName" width="10%" align="center">商家名称</th>
					<th field="shopAddress" width="15%" align="center">商家地址</th>
					<th field="orderNum" width="19%" align="center">订单号</th>
					<th field="userName" width="10%" align="center">用户名称</th>
					<th field="orderPrice" width="10%" align="center">价格</th>
					<th field="payFor" width="10%" align="center" formatter="formatZhifuh">支付方式</th>
					<th field="createDate" width="15%" align="center">创建日期</th>
					<th field="isCommen" width="10%" align="center" formatter="formatSnatch">状态</th>
					<!-- <th field="oper" width="15%" align="center" formatter="formatOper" >操作</th> -->
				</tr>
			</thead>
		</table>
 
</body>
<style>
.uploadify-queue{ display: none;}
</style>

</html>