<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/commons/taglibs.jsp" %>
 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>  
<base href="<%=basePath%>">
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">    
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<link href="<%=basePath%>/Public/css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/icon.css">
<script src="<%=basePath%>/Public/js/jquery-1.7.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/Public/js/jquery.easyui.min.js"></script>
<script src="<%=basePath%>/Public/layer/layer.js"></script>
<link href="<%=basePath%>/Public/css/pinglun.css" rel="stylesheet" type="text/css" />
<script src="<%=basePath%>/Public/js/shijian.js" type="text/javascript"></script>
<script>
var yidu = 0;
$(document).ready(function(){
//回车点击事件--搜索功能
	$(document).keydown(function(event){
		if(event.keyCode==13){
			//$("#login").click();
			sousuo();
		}
	}) ;
var IsCheckFlag = true;
$("#grid").datagrid({
 	 onClickCell: function (rowIndex, field, value) {
         IsCheckFlag = false;
     },
 	 onSelect: function (rowIndex, rowData) {
         if (!IsCheckFlag) {
             IsCheckFlag = true;
             $("#grid").datagrid("unselectRow", rowIndex);
         }
     },    
 	 onUnselect: function (rowIndex, rowData) {
         if (!IsCheckFlag) {
             IsCheckFlag = true;
             $("#grid").datagrid("selectRow", rowIndex);
         }
     }
})


});

/* 操作 */
function formatOper(value,row,index){
		return "<a href='javascript:openDel(\"" + row['shopActivityId'] + "\");'>删除</a>";
} 
/* 显示状态 */
function formatType(value,row,index){
	if(value == '1'){
		return "<a style='color:#000'>界面咨询</a>";
		}
	if(value == '2'){
		return "<a style='color:#000'>资金问题</a>";
		}
	if(value == '3'){
		return "<a style='color:#000'>业务咨询</a>";
		}
	if(value == '4'){
	return "<a style='color:#000'>其他问题</a>";
	}
}
/* 发布 */
function saveAddRole()
{	
	var price=$('#startDate').datebox('getValue');
	var prices=$('#endDate').datebox('getValue');
	var content=$('#content').val();
	if(price == "" || price == null){
		layer.msg("开始时间不能为空")
		return 0;
	}
	if(prices == "" || prices == null){
		layer.msg("结束时间不能为空")
		return 0;
	}
	if(price > prices){
		layer.msg("开始时间必须小于结束时间")
		return 0;
		}
	if(content == "" || content == null){
		layer.msg("内容不能为空")
		return 0;
		}
	/* var st = price.substring(0,4)+price.substring(6,7)+price.substring(9,10);
	var et = prices.substring(0,4)+prices.substring(6,7)+prices.substring(9,10);
	if(!(st == et)){
		layer.msg("日期必须为同一天内")
		return 0;
	} */
	$('#formAddRole').form('submit', {
		url : '<%=basePath%>/order/issueShopActivity.do',
		onSubmit : function() {
			var index = layer.load(1, {
	  shade: [0.1,'#fff'] //0.1透明度的白色背景
	})
			return $(this).form('validate');
	
		},
		success : function(result) {
			data = eval("(" + result + ")");
			if(data.code == 100)
			{
				layer.closeAll('loading');
				$('#adddlg').dialog('close');
				$('#grid').datagrid('reload');
				$.messager.show({
					title:'提示',
					msg:'<div class="msgs">保存成功</div>',
					timeout: 1000, 
					showType:'fade',
					style:{right:'', bottom:''}
				});
			}
			else
			{
				$.messager.show({
					title:'提示',
					msg:data.msg,
					timeout: 1000,  
					showType:'fade',
					style:{right:'', bottom:''}
				});
			}
		}
	}); 
	
}

/* 添加按钮 */
function openAdd()
{	
    $('#name').val("");
    $('#leibie').combobox('setValue', '');
    $('#dizhi').combobox('setValue', '');
    $('#renyuan').combobox('setValue', '');
    $('#speAddress').val("");
    $('#introduction').val("");
    $('#speAddress').val("");
    $('#remarks').val("");
    $('#budgetPrice').numberbox('setValue', '');
    $('#mode').val("add");	
    $('#adddlg').panel({title: "新建活动"});
	$('#adddlg').dialog('open');
}
/* 多删除按钮 */
function openDels(postId)
{	
	var select = $('#grid').datagrid('getSelected');
	if(select){
	if(window.confirm('你确定要删除吗？')){
		 var index = layer.load(1, {
	  shade: [0.1,'#fff'] //0.1透明度的白色背景
	});
	 var str = "";
	var rows  = $('#grid').datagrid("getRows");
	var ckArr = $("input[name='ck']");
	for(var i=0; i<ckArr.length; i++)
	{
		if(ckArr[i].checked)
		{
			if(str != "")
			{
				str += ",";
			}
			str += rows[i].shopActivityId;
		}
	}
	
	$.post("<%=basePath%>/order/delshopActivity.do",{delKeys:str},function(result){
		layer.closeAll('loading');
		data = eval("(" + result + ")");
		//alert(result)
		if(data.code == 100){
			$('#grid').datagrid('reload');
			$.messager.show({
							title:'提示',
							msg:'<div class="msgs">删除成功</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
		}
		
	}); 
	}
	}else{
		$.messager.show({
							title:'提示',
							msg:'<div class="msgs">请选择您要删除的数据</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
	}
	
}

/* 单删除 */
function openDel(shopActivityId)
{	
	if(window.confirm('你确定要删除吗？')){
	$.post("<%=basePath%>/order/delshopActivity.do",{delKeys:shopActivityId},function(result){
		layer.closeAll('loading');
		data = eval("(" + result + ")");
		//alert(result)
		if(data.code == 100){
			$('#grid').datagrid('reload');
			$.messager.show({
							title:'提示',
							msg:'<div class="msgs">删除成功</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
		}
		else{
			$('#grid').datagrid('reload');
			$.messager.show({
							title:'提示',
							msg:'<div class="msgs">删除失败，您没有权限</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
		}
	}); 
	}
}



//搜索
function sousuo()
{
	var ssuser = document.getElementById("ssuser").value;
	var ssksDate = $('#ssksDate').datebox('getValue'); 
	var ssjsDate = $('#ssjsDate').datebox('getValue'); 
	$('#grid').datagrid('load', {ssuser:ssuser,ssksDate:ssksDate,ssjsDate:ssjsDate});
}


</script>
</head>
<body class="easyui-layout">
<!--------------------------------------- 功能按钮 -------------------------------------------->
<div id="tb" style="height:auto;display: none;">   
	<div style="width:110%; margin-bottom:3px;text-align: left;height:35px;"> 
			<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="openAdd()" >添加活动</a>
	        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="openDels()" >删除</a>
	 </div> 
<!------------------------------------ 搜索 ---------------------------------------->
	 <div style="text-align: list;background-color: #f4f4f4;width:100%;height:30px; border-top: 2px solid #D9D9D9;border-bottom: 0px solid #D9D9D9;">
		    <div style="float: left;height: 30px;line-height: 30px;"><label> &nbsp;&nbsp; 活动商家: </label>
	   		<input name="ssuser" id="ssuser" style="margin-top: 0px;border:1px #95B8E7 solid;width: 150px;height: 23px"  />
		   </div>
		    <div style="float: left;height: 30px;line-height: 30px;"><label> &nbsp;&nbsp; 开始时间: </label>
	   		<input name="ssksDate" id="ssksDate" class="easyui-datebox"  style="margin-top: 0px;border:1px #95B8E7 solid;width: 200px;height: 23px"    data-options="formatter:myformatter,parser:myparser"/>
		   </div>
		    <div style="float: left;height: 30px;line-height: 30px;"><label> &nbsp;&nbsp; 结束时间: </label>
	   		<input name="ssjsDate" id="ssjsDate" class="easyui-datebox"  style="margin-top: 0px;border:1px #95B8E7 solid;width: 200px;height: 23px"    data-options="formatter:myformatter,parser:myparser"/>
		   </div>
		   <div onclick="sousuo()" style=" float: left;border:1px solid #000;margin-left: 25px;cursor:pointer;text-align:center;line-height:24px;background-color: #FFF;color:#000;width: 100px;height:23px;margin-top: 3px;border-radius: 5px;"
	    class="easyui-linkbutton" iconCls="icon-search">搜索	   
		</div>  
	</div>
	
		
</div>	    
		<table id="grid" class="easyui-datagrid"  data-options="nowrap: false" style="width:100%;height:100%;display: none;" title="<img alt='帖子管理' style='width:17px;height:17px;' src='<%=basePath%>/img/titleImg.png'><div style='height:20px;line-height:22px;margin-left:25px;margin-top:-20;'>商家活动</div>"
			url="<%=basePath%>/order/shopActivityListData.do" toolbar="#tb" data-options="nowrap: false"
			rownumbers="true" pagination="true" singleSelect="false" loadMsg="正在努力为您加载数据">
			<thead>
				<tr>
					<th field="ck" checkbox="true" id="ck" name="ck"></th>
					<th field="userName" width="10%" align="center">活动商家</th>
					<th field="content" width="25%" align="center" >活动内容</th>
					<th field="startDate" width="15%" align="center">开始时间</th>
					<th field="endDate" width="15%" align="center">结束时间</th>
					<th field="date" width="15%" align="center">创建时间</th>
					<th field="oper" width="10%" align="center" formatter="formatOper" >操作</th>
				</tr>
			</thead>
			</table>
			
			
<!---------------------------------- 添加界面 ----------------------------->
<div id="adddlg" class="easyui-dialog" style="width: 400px; height: 350px; padding: 10px 20px;display: none;" closed="true"  buttons="#adddlg-buttons" singleSelect="false" loadMsg="正在努力为您加载数据">
	<form id="formAddRole" name="formAddRole" method="post" >
        <div style="float:left;width:100%;height:21px;margin-top: 30px;">
        	<label style="display:inline-block;width:100px;color:#000;font-size:15px;">开始时间:</label>
            <input  class="easyui-datetimebox" style="vertical-align:middle;font-size:15px;color:#000;margin-top: -6px;border:1px solid #95B8E7;width: 220px" wtype="text"  id="startDate" name="startDate" value=""  editable="false" data-options="formatter:ww4,parser:w4" />
			</div>
		 <div style="float:left;width:100%;height:21px;margin-top: 30px;">
        	<label style="display:inline-block;width:100px;color:#000;font-size:15px;">结束时间:</label>
             <input  class="easyui-datetimebox" style="vertical-align:middle;font-size:15px;color:#000;margin-top: -6px;border:1px solid #95B8E7;width: 220px" wtype="text"  id="endDate" name="endDate" value=""  editable="false" data-options="formatter:ww4,parser:w4" />
			</div>
		  <div style="float:left;width:100%;height:21px;margin-top: 30px;">
        	<label style="display:inline-block;width:100px;color:#000;font-size:15px;"> 商家名称:</label>
            <input  id="shopName" editable="false" name="shopName" class="easyui-combobox" 
            style="vertical-align:middle;font-size:15px;color:#FFF;margin-top: -6px;width: 220px;" type="text" 
            data-options="
            	valueField: 'shopId',
				textField: 'userName',
				url: '<%=basePath%>/order/readShop.do',
				onSelect: function(rec){
				$('#userName').val(rec.userName); 
				$('#shopId').val(rec.shopId); 
				}" />
            </div>
        <div style="float:left;width:100%;height:21px;margin-top: 30px;">
        	<label style="display:inline-block;width:100px;color:#000;font-size:15px;" >活动内容:</label>
            <textarea  style="vertical-align:middle;font-size:15px;width:220px;color:#000;margin-top: -6px;border:1px solid #95B8E7" type="text"  id="content" name="content" /></textarea></div>
 		
		  
		    <input type="hidden" name="mode" id="mode" value=""/>
		   <input type="hidden" name="shopActivityId" id="shopActivityId" value=""/>
		   <input type="hidden" name="shopId" id="shopId" value=""/>
	 </form>
	
</div>

<!-------------------------------------------- 确认取消按钮 -------------------------------------------------->
<div id="adddlg-buttons" style="height: 30px;display: none;">
	<div style="width:140px;height:35px;float: right;">
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="saveAddRole()" iconcls="icon-save">保存</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="javascript:$('#adddlg').dialog('close')"
		iconcls="icon-cancel">取消</a>	
	</div>
</div> 
</body>
 
</html>
