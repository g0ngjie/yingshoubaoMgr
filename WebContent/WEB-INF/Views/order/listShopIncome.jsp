<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/commons/taglibs.jsp" %>
 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>  
<base href="<%=basePath%>">
<title>商户收入统计</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">    
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<link href="<%=basePath%>/Public/css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/icon.css">
<script src="<%=basePath%>/Public/js/jquery-1.7.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/Public/js/jquery.easyui.min.js"></script>
<script src="<%=basePath%>/Public/layer/layer.js"></script>
<link href="<%=basePath%>/Public/css/pinglun.css" rel="stylesheet" type="text/css" />
<script src="<%=basePath%>/Public/js/shijian.js" type="text/javascript"></script>
<script src="Public/js/highcharts.js"></script>
<style type="text/css">
	.datagrid-row {
		height: 50px;
	}
</style>
<script>
var typeChar = '';

function YearAndMonthSelectOrderNum(shopId,forYear,forMonth,type){
	$.post("<%=basePath%>/income/YearAndMonthSelectOrderNum.do",{shopId:shopId,forYear:forYear,forMonth:forMonth},function(result){
		data = eval("(" + result + ")");
		var aa=new Array(); 
		var co=new Array(); 
		for(var i=0;i<data.length;i++){
			var month = data[i].month;
			var count = data[i].count;
			aa[i] = month;
			co[i] = count;
		}
		
		$('#container').highcharts({
		chart : {
	      type: type
	   },
        title: {
            text: '商家统计',
            x: -20 //center
        },
       
        xAxis: {
            categories: aa
        },
        yAxis: {
            title: {
                text: '订单数'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: '数'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: '订单',
            data: co
        }]
    });
	
	});	
}

$(document).ready(function(){
	var IsCheckFlag = true;
	$("#grid").datagrid({
	 	 onClickCell: function (rowIndex, field, value) {
	         IsCheckFlag = false;
	     },
	 	 onSelect: function (rowIndex, rowData) {
	         if (!IsCheckFlag) {
	             IsCheckFlag = true;
	             $("#grid").datagrid("unselectRow", rowIndex);
	         }
	     },    
	 	 onUnselect: function (rowIndex, rowData) {
	         if (!IsCheckFlag) {
	             IsCheckFlag = true;
	             $("#grid").datagrid("selectRow", rowIndex);
	         }
	     }
	})
	
	var IsCheckFlag1 = true;
	$("#shopOrderTab").datagrid({
	 	 onClickCell: function (rowIndex, field, value) {
	         IsCheckFlag1 = false;
	     },
	 	 onSelect: function (rowIndex, rowData) {
	         if (!IsCheckFlag1) {
	             IsCheckFlag1 = true;
	             $("#shopOrderTab").datagrid("unselectRow", rowIndex);
	         }
	     },    
	 	 onUnselect: function (rowIndex, rowData) {
	         if (!IsCheckFlag1) {
	             IsCheckFlag1 = true;
	             $("#shopOrderTab").datagrid("selectRow", rowIndex);
	         }
	     }
	})
	
		
		var SS = new Date().getFullYear(); //获得当前时间
		var data,json;
		data = [];
	    data.push({ "text": SS-3, "id": SS-3},
				    { "text": SS-2, "id": SS-2},
				    { "text": SS-1, "id": SS-1},
				    { "text": SS, "id": SS},
				    { "text": SS+1, "id": SS+1},
				    { "text": SS+2, "id": SS+2},
				    { "text": SS+3, "id": SS+3}
			    );
	    $("#intoYear").combobox("loadData", data);
		$("#intoYear").combobox('setValue',SS);
		//折线图
		soDate();
});

/**
 * 时间格式化
 * @param value
 * @returns {string}
 */
function dateFormatter (value) {
    var date = new Date(value);
    var year = date.getFullYear();
    var month = (date.getMonth() + 1);
    var day = date.getDate();
    var hour = date.getHours();
    var minutes = date.getMinutes();
    var seconds = date.getSeconds();
    if (month < 10) {
        month = "0" + month;
    }
    if (day < 10) {
        day = "0" + day;
    }
    if (hour < 10) {
        hour = "0" + hour;
    }
    if (minutes < 10) {
        minutes = "0" + minutes;
    }
    if (seconds < 10) {
        seconds = "0" + seconds;
    }
    return year + "-" + month + "-" + day + " " + hour + ":" + minutes + ":" + seconds;
}

$.fn.datebox.defaults.parser = function(s){
	var t = Date.parse(s);
	if (!isNaN(t)){
		return new Date(t);
	} else {
		return new Date();
	}
}

//搜索
function sousuo()
{	
	var proName = $("#proName").val();
	var cityName = $("#cityName").val();
	var ssuser = $("#ssuser").val();
	var shopTypeId = $('#shopTypeId').val();
	var beginDate = $('#startDate').datebox('getValue'); 
	var endDate = $('#endDate').datebox('getValue'); 
	$('#grid').datagrid('load', {ssuser:ssuser,beginDate:beginDate,endDate:endDate,proName:proName,cityName:cityName,shopTypeId:shopTypeId});
}

function exportWps(){
	var proName = $("#proName").val();
	var cityName = $("#cityName").val();
	var ssuser = $("#ssuser").val();
	var beginDate = $("#startDate").datebox('getValue');
	var endDate = $("#endDate").datebox('getValue');
	window.parent.location="<%=basePath%>/statistics/exportWpsByShop.do?proName="+proName+"&cityName="+cityName+"&beginDate="+beginDate+"&endDate="+endDate+"&ssuser="+ssuser;
}
/* 操作 */
function formatOper(value,row,index){
	return "<a href='javascript:openOrder(\"" + row['shopId'] + "\");'>订单详情</a>"
		+ " | <a href='javascript:openHistogram(\"" + row['shopId'] + "\");'>柱状图</a>"
		+ " | <a href='javascript:openChart(\"" + row['shopId'] + "\");'>折线图</a>";
} 
//打开 订单详情
function openOrder(shopId){
	$("#shopIdd").val(shopId);
	$('#shopOrderId').dialog('open');
	$("#shopOrderTab").datagrid({
		url:"<%=basePath%>/income/chaShopOrder.do?shopIdd="+shopId
	});
}

//支付方式
function forMatParFor(row,value,index){
	if(value == 1)
		return "微信";
	else
		return "余额";	
}

function sousuoOrder(){
	var shopIdd = $("#shopIdd").val();
	var soOrderNum = $("#soOrderNum").val();
	var startDateOrder = $('#startDateOrder').datebox('getValue'); 
	var endDateOrder = $('#endDateOrder').datebox('getValue'); 
	$('#shopOrderTab').datagrid('load', {soOrderNum:soOrderNum,startDateOrder:startDateOrder,endDateOrder:endDateOrder,shopIdd:shopIdd});
}
/* 打开柱状图 */
function openHistogram(shopId){
	$("#forShopId").val(shopId);
	 
	typeChar ='column';
	YearAndMonthSelectOrderNum(shopId, '', '','column');
	
	$('#chaPanelH').dialog('open');
}
/* 打开折线图 */
function openChart(shopId){
	$("#forShopId").val(shopId);
	typeChar ='';
	YearAndMonthSelectOrderNum(shopId, '', '','');
	$('#chaPanelH').dialog('open');
}

/* 搜索 日期 折线图 */
function soDate(){
	var shopId = $("#forShopId").val();
	var forYear = $("#intoYear").combobox('getValue');
	var forMonth = $("#yueFen").combobox('getValue');
	YearAndMonthSelectOrderNum(shopId, forYear, forMonth,typeChar);
	
}


</script>
</head>
<body class="easyui-layout">
<!--------------------------------------- 功能按钮 -------------------------------------------->
<div id="tb" style="height:auto;display: none;">   
	<div style="width:100px; margin-top:3px; text-align: left;height:35px;float: left; "> 
   	    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="brick_go" plain="true" onclick="exportWps()" >导出表格</a>
	 </div>
	<div style="width:110%; margin-bottom:3px;text-align: left;height:35px;"> 
       <div style="float: left;height: 30px;line-height: 30px;"><label> &nbsp;&nbsp; 省份: </label>
	   		<input id="proName" name="proName" style="width: 100px" class="easyui-combobox" data-options="    
			        valueField: 'proId',    
			        textField: 'proName',    
			        url: '<%=basePath%>statistics/chaProvince',    
			        onSelect: function(rec){    
			            var url = '<%=basePath%>statistics/chaCity?proId='+rec.proId;    
			        	 $('#cityName').combobox('setValue','');
			            $('#cityName').combobox('reload', url);    
			            $('#proName').val(rec.proName);
			        }" />
		</div>
		<div style="float: left;height: 30px;line-height: 30px;"><label> &nbsp;&nbsp; 城市: </label>
			<input id="cityName" name="cityName" style="width: 100px" class="easyui-combobox" data-options="
					valueField:'cityId',
					textField:'cityName',
					onSelect: function(rec){ 
					 	$('#cityName').val(rec.cityName);
			        }" />
		</div>
		<div style="float: left;height: 30px;line-height: 30px;"><label> &nbsp;&nbsp; 商户类型: </label>
	   		<input id="shopType" name="shopType" style="width: 100px;" class="easyui-combobox" data-options="    
			        valueField: 'shopTypeId',    
			        textField: 'typeName',    
			        url: '<%=basePath%>statistics/chaShopType',    
			        onSelect: function(rec){    
			        	$('#shopTypeId').val(rec.shopTypeId);
			        }" />
		</div>
		 <div style="text-align: list;background-color: #f4f4f4;width:100%;height:30px; border-top: 2px solid #D9D9D9;border-bottom: 0px solid #D9D9D9;">
			   <div style="float: left;height: 30px;line-height: 30px;"><label> &nbsp;&nbsp; 店铺名称: </label>
		   		  <input name="ssuser" id="ssuser" style="margin-top: 0px;border:1px #95B8E7 solid;width: 100px;height: 23px"  />
			   </div>
	   		 <div style="float: left;height: 30px;line-height: 30px;"><label> &nbsp;&nbsp; 开始日期: </label>
			 	<input  id="startDate" name="startDate" type= "text" class= "easyui-datebox" data-options="formatter:myformatter,parser:myparser"> </input>
			 </div>
			 <div style="float: left;height: 30px;line-height: 30px;"><label> &nbsp;&nbsp; 结束日期: </label>
			 	<input  id="endDate" name="endDate" type= "text" class= "easyui-datebox" data-options="formatter:myformatter,parser:myparser"> </input>
			 </div>
			   <div onclick="sousuo()" style=" float: left;border:1px solid #000;margin-left: 25px;cursor:pointer;text-align:center;line-height:24px;background-color: #FFF;color:#000;width: 100px;height:23px;margin-top: 3px;border-radius: 5px;"
		    class="easyui-linkbutton" iconCls="icon-search">查询  
			</div>  
		</div>
	 </div> 
	
	<input type="hidden" id="proName" name="proName"/>
	<input type="hidden" id="cityName" name="cityName"/>
	<input type="hidden" id="shopTypeId" name="shopTyepId"/>
		
</div>	    
		<table id="grid" class="easyui-datagrid"  data-options="nowrap: false" style="width:100%;height:100%;display: none;" title="<img alt='帖子管理' style='width:17px;height:17px;' src='<%=basePath%>/img/titleImg.png'><div style='height:20px;line-height:22px;margin-left:25px;margin-top:-20;'>商户收入统计</div>"
			url="<%=basePath%>/income/shopShopIncomeListData.do" toolbar="#tb" data-options="nowrap: false"
			rownumbers="true" pagination="true" striped="true" singleSelect="false" loadMsg="正在努力为您加载数据">
			<thead>
				<tr>
					<th field="shopName" width="10%" align="center">商家名称</th>
					<th field="typeName" width="10%" align="center">商户类型</th>
					<th field="shopAddress" width="20%" align="center">地址</th>
					<th field="price" width="15%" align="center" >本店消费总金额/元</th>
					<th field="income" width="15%" align="center">收入总金额/元</th>
					<th field="num" width="15%" align="center">订单量</th>
					<th field="orderDetail" width="14%" align="center" formatter="formatOper" >操作</th>
				</tr>
			</thead>
			</table>
			
			
		<!------------------------------------ 搜索 ---------------------------------------->
		<div id="orderToolbar" style="height:auto;display: none;">   
			 <div style="text-align: list;background-color: #f4f4f4;width:100%;height:30px; border-top: 2px solid #D9D9D9;border-bottom: 0px solid #D9D9D9;">
				   <div style="float: left;height: 30px;line-height: 30px;"><label> &nbsp;&nbsp; 订单号: </label>
			   		 <input name="soOrderNum" id="soOrderNum" style="margin-top: 0px;border:1px #95B8E7 solid;width: 200px;height: 23px"/>
				   </div>
				    <div style="float: left;height: 30px;line-height: 30px;"><label> &nbsp;&nbsp; 开始日期: </label>
					 	<input  id="startDateOrder" name="startDateOrder" type= "text" class= "easyui-datebox" data-options="formatter:myformatter,parser:myparser"> </input>
					 </div>
					 <div style="float: left;height: 30px;line-height: 30px;"><label> &nbsp;&nbsp; 结束日期: </label>
					 	<input  id="endDateOrder" name="endDateOrder" type= "text" class= "easyui-datebox" data-options="formatter:myformatter,parser:myparser"> </input>
					 </div>
					<div onclick="sousuoOrder()" style=" float: left;border:1px solid #000;margin-left: 25px;cursor:pointer;text-align:center;line-height:24px;background-color: #FFF;color:#000;width: 100px;height:23px;margin-top: 3px;border-radius: 5px;"
				   			 class="easyui-linkbutton" iconCls="icon-search">查询  
					</div>  
			 </div>		 
		</div> 			
	<!-- ---------------------------------- 订单详情 ---------------------------------- -->
	<div id="shopOrderId" class="easyui-dialog" title="订单查看" style="top: 80px;left: 100px" modal="true" closed="true" >

		<table id="shopOrderTab" class="easyui-datagrid"  data-options="nowrap: false" style="width:1200px;height:700px;display: none;"
			 toolbar="#orderToolbar" rownumbers="true" pagination="true" singleSelect="true" loadMsg="正在努力为您加载数据">
			<thead>
				<tr>
					<th field="orderNum" width="20%" align="center">订单号</th>
					<th field="price" width="20%" align="center" >金额</th>
					<th field="payForUser" width="20%" align="center" >付款人</th>
					<th field="payFor" width="19%" align="center" formatter="forMatParFor">支付方式</th>
					<th field="createDate" width="19%" align="center">时间</th>
				</tr>
			</thead>
		</table>
		
		<input type="hidden" id="shopIdd" name="shopIdd"/>
		
	</div>
	
	<!-- ---------------------------------------------------- 线图 面板  ------------------------------------------->
	<div id="chaPanelH" class="easyui-dialog" title="订单查看" style="width: 1200px; height: 800px;  modal="true" closed="true" >
			<div style="float: left;">
				<label style="display:inline-block;width:80px;color:#000;font-size:15px;margin-left: 40px;margin-top: 10px; for="employee">年份</label>
				<select id="intoYear" name="intoYear" class="easyui-combobox" name="dept"  class="easyui-combobox" data-options="editable:false,valueField:'id', textField:'text'"
					style="width:100px;vertical-align:middle;font-size:15px;color:#000;margin-top: -6px">   
					
				</select> 
			</div>
			<div style="float: left;">
				<label style="display:inline-block;width:80px;color:#000;font-size:15px;margin-left: 40px;margin-top: 10px; for="employee">月份</label>
				<select id="yueFen" class="easyui-combobox" name="dept" style="width:100px; style="width:181px;vertical-align:middle;font-size:15px;color:#000;margin-top: -6px">   
					<option value=""></option>
				    <option value="1">1月</option>   
				    <option value="2">2月</option> 
				    <option value="3">3月</option> 
				    <option value="4">4月</option> 
				    <option value="5">5月</option> 
				    <option value="6">6月</option> 
				    <option value="7">7月</option> 
				    <option value="8">8月</option> 
				    <option value="9">9月</option> 
				    <option value="10">10月</option> 
				    <option value="11">11月</option> 
				    <option value="12">12月</option> 
				</select>  
			</div>
			<div onclick="soDate()" style=" float: left;border:1px solid #000;margin-left: 35px;cursor:pointer;text-align:center;line-height:24px;background-color: #FFF;color:#000;width: 100px;height:23px;margin-top: 10px;border-radius: 5px;"
		    class="easyui-linkbutton" iconCls="icon-search">查询  
			</div>
			
		<div style="float: left;width: 100%;height: 95%; margin-top: 10px">
			<div id="" class="easyui-panel" style="width:100%;height:100%;padding:10px;background:#fafafa;" data-options="">
				<div id="container" style="width:1000px; height: 70%; margin: 0 auto"></div>
			</div>
		</div>
		<input type="hidden" id="yearInp" name="yearInp"/>
		<input type="hidden" id="monthInp" name="monthInp"/>
		<input type="hidden" id="forShopId" name="forShopId"/>
		
	</div>
			
</body>
 
</html>
