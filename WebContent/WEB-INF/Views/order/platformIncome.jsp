<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/commons/taglibs.jsp" %>
 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>  
<base href="<%=basePath%>">
<title>平台收入统计</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">    
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<link href="<%=basePath%>/Public/css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/icon.css">
<script src="<%=basePath%>/Public/js/jquery-1.7.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/Public/js/jquery.easyui.min.js"></script>
<script src="<%=basePath%>/Public/layer/layer.js"></script>
<link href="<%=basePath%>/Public/css/pinglun.css" rel="stylesheet" type="text/css" />
<script src="<%=basePath%>/Public/js/shijian.js" type="text/javascript"></script>
<script src="Public/js/highcharts.js"></script>
<style type="text/css">
	.datagrid-row {
		height: 50px;
	}
</style>
<script>
var typeChar = '';

function getAllPlayMoney(startDate,endDate){
	$.post("<%=basePath%>income/platformStatisticAll.do",{startDate:startDate,endDate:endDate},function(result){
		data = eval("(" + result + ")");
		$("#playMoney").html(data.allSumPalyPrice);
	});	
}

function YearAndMonthSelectOrderNum(forYear,forMonth,type){
	$.post("<%=basePath%>/income/selectTuForPlay.do",{forYear:forYear,forMonth:forMonth},function(result){
		data = eval("(" + result + ")");
		var aa=new Array(); 
		var co=new Array(); 
		for(var i=0;i<data.length;i++){
			var month = data[i].month;
			var count = data[i].count;
			aa[i] = month;
			co[i] = count;
		}
		var sumaa = 0;
		for(var i=0;i<co.length;i++){
			var month = co[i];
			sumaa = sumaa + month;
		}
		
		$('#container').highcharts({
		chart : {
	      type: type
	   },
        title: {
            text: '平台收入统计',
            x: -20 //center
        },
       
        xAxis: {
            categories: aa
        },
        yAxis: {
            title: {
                text: '总计'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: '总计'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: '总额'+sumaa+'元',
            data: co
        }]
    });
	
	});	
}

$(document).ready(function(){
	var IsCheckFlag = true;
	$("#grid").datagrid({
	 	 onClickCell: function (rowIndex, field, value) {
	         IsCheckFlag = false;
	     },
	 	 onSelect: function (rowIndex, rowData) {
	         if (!IsCheckFlag) {
	             IsCheckFlag = true;
	             $("#grid").datagrid("unselectRow", rowIndex);
	         }
	     },    
	 	 onUnselect: function (rowIndex, rowData) {
	         if (!IsCheckFlag) {
	             IsCheckFlag = true;
	             $("#grid").datagrid("selectRow", rowIndex);
	         }
	     }
	})
	getAllPlayMoney('', '');
		var SS = new Date().getFullYear(); //获得当前时间
		var data,json;
		data = [];
	    data.push({ "text": SS-3, "id": SS-3},
				    { "text": SS-2, "id": SS-2},
				    { "text": SS-1, "id": SS-1},
				    { "text": SS, "id": SS},
				    { "text": SS+1, "id": SS+1},
				    { "text": SS+2, "id": SS+2},
				    { "text": SS+3, "id": SS+3}
			    );
	    $("#intoYear").combobox("loadData", data);
		$("#intoYear").combobox('setValue',SS);
		
	YearAndMonthSelectOrderNum('', '', typeChar);
});
//搜索
function sousuo()
{
	var beginDate = $('#startDate').datebox('getValue'); 
	var endDate = $('#endDate').datebox('getValue'); 
	$('#grid').datagrid('load', {beginDate:beginDate,endDate:endDate});
	getAllPlayMoney(beginDate, endDate);
}
/* 打开柱状图 */
function openHistogram(){
	 
	typeChar ='column';
	YearAndMonthSelectOrderNum('', '','column');
	
	$('#chaPanelH').dialog('open');
}
/* 打开折线图 */
function openChart(){
	typeChar ='';
	YearAndMonthSelectOrderNum('', '','');
	$('#chaPanelH').dialog('open');
}
/* 搜索 日期 折线图 */
function soDate(){
	var forYear = $("#intoYear").combobox('getValue');
	var forMonth = $("#yueFen").combobox('getValue');
	YearAndMonthSelectOrderNum(forYear, forMonth,typeChar);
	
}
/* 格式化总收入 */
function forAllPrice(value,row,index){
	var price = value*100;
	var ss = parseInt(price);
	return ss/100;
}
</script>
</head>
<body class="easyui-layout">
<!--------------------------------------- 功能按钮 -------------------------------------------->
<div id="tb" style="height:auto;display: none;">   
	<!-- <div style="width:110%; margin-bottom:3px;text-align: left;height:35px;"> 
	        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="openDels()" >删除</a>
	 </div>  -->
<!------------------------------------ 搜索 ---------------------------------------->
	 <div style="text-align: list;background-color: #f4f4f4;width:100%;height:30px; border-top: 2px solid #D9D9D9;border-bottom: 0px solid #D9D9D9;">
		   <div style="float: left;height: 30px;line-height: 30px;"><label> &nbsp;&nbsp; 开始日期: </label>
		 	<input  id="startDate" nastartDate" type= "text" class= "easyui-datebox" data-options="formatter:myformatter,parser:myparser"> </input>
			 </div>
			 <div style="float: left;height: 30px;line-height: 30px;"><label> &nbsp;&nbsp; 结束日期: </label>
			 	<input  id="endDate" name="endDate" type= "text" class= "easyui-datebox" data-options="formatter:myformatter,parser:myparser"> </input>
			 </div>
		   <div onclick="sousuo()" style=" float: left;border:1px solid #000;margin-left: 25px;cursor:pointer;text-align:center;line-height:24px;
		 	  background-color: #FFF;color:#000;width: 100px;height:23px;margin-top: 3px;border-radius: 5px;"
	    	  class="easyui-linkbutton" iconCls="icon-search">查询
	       </div>  
	      	 <div style="width: 80px;height: 35px;float: left;margin-left: 20px;"> 
			 	<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="graph" plain="true" onclick="openHistogram()" >柱状图</a>
			 </div>
			 <div style="width: 80px;height: 35px;float: left;"> 
			 	<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="chart_curve" plain="true" onclick="openChart()" >折线图</a>
			 </div>
      	 <div style="float: left;margin-left:5px; height: 30px;line-height: 30px">
      	 	<label> &nbsp;&nbsp; 平台收入总额: </label>
       		<span id="playMoney" style="display: inline;"></span>
		 </div>
	</div>
</div>	    
		<table id="grid" class="easyui-datagrid"  data-options="nowrap: false" style="width:100%;height:100%;display: none;" title="<img alt='帖子管理' style='width:17px;height:17px;' src='<%=basePath%>/img/titleImg.png'><div style='height:20px;line-height:22px;margin-left:25px;margin-top:-20;'>平台收入统计</div>"
			url="<%=basePath%>/income/platformIncomeData.do" toolbar="#tb" data-options="nowrap: false"
			rownumbers="true" pagination="true" singleSelect="false" loadMsg="正在努力为您加载数据">
			<thead>
				<tr>
					<th field="createDate" width="15%" align="center">时间</th>
					<th field="price" width="14%" align="center">本日进账金额</th>
					<th field="allPrice" width="14%" align="center" formatter="forAllPrice">本日平台总流水</th>
					<th field="num" width="14%" align="center">订单总数</th>
					<th field="shopSumPrice" width="14%" align="center">商家总分成</th>
					<th field="buyTopSum" width="14%" align="center">头条收入总额</th>
					<th field="takeMoney" width="14%" align="center">用户提现总金额</th>
				</tr>
			</thead>
		</table>
			
	<!-- ---------------------------------------------------- 线图 面板  ------------------------------------------->
	<div id="chaPanelH" class="easyui-dialog" title="订单查看" style="width: 1200px; height: 800px;  modal="true" closed="true" >
			<div style="float: left;">
				<label style="display:inline-block;width:80px;color:#000;font-size:15px;margin-left: 40px;margin-top: 10px; for="employee">年份</label>
				<select id="intoYear" name="intoYear" class="easyui-combobox" name="dept"  class="easyui-combobox" data-options="editable:false,valueField:'id', textField:'text'"
					style="width:100px;vertical-align:middle;font-size:15px;color:#000;margin-top: -6px">   
					
				</select> 
			</div>
			<div style="float: left;">
				<label style="display:inline-block;width:80px;color:#000;font-size:15px;margin-left: 40px;margin-top: 10px; for="employee">月份</label>
				<select id="yueFen" class="easyui-combobox" name="dept" style="width:100px; style="width:181px;vertical-align:middle;font-size:15px;color:#000;margin-top: -6px">   
					<option value=""></option>
				    <option value="1">1月</option>   
				    <option value="2">2月</option> 
				    <option value="3">3月</option> 
				    <option value="4">4月</option> 
				    <option value="5">5月</option> 
				    <option value="6">6月</option> 
				    <option value="7">7月</option> 
				    <option value="8">8月</option> 
				    <option value="9">9月</option> 
				    <option value="10">10月</option> 
				    <option value="11">11月</option> 
				    <option value="12">12月</option> 
				</select>  
			</div>
			<div onclick="soDate()" style=" float: left;border:1px solid #000;margin-left: 35px;cursor:pointer;text-align:center;line-height:24px;background-color: #FFF;color:#000;width: 100px;height:23px;margin-top: 10px;border-radius: 5px;"
		    class="easyui-linkbutton" iconCls="icon-search">查询  
			</div>
			
		<div style="float: left;width: 100%;height: 95%; margin-top: 10px">
			<div id="" class="easyui-panel" style="width:100%;height:100%;padding:10px;background:#fafafa;" data-options="">
				<div id="container" style="width:1000px; height: 70%; margin: 0 auto"></div>
			</div>
		</div>
		<input type="hidden" id="yearInp" name="yearInp"/>
		<input type="hidden" id="monthInp" name="monthInp"/>
	</div>
			
			
</body>
 
</html>
