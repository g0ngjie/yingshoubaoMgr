<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/commons/taglibs.jsp" %>
 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>  
<base href="<%=basePath%>">
<title>用户收入统计</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">    
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<link href="<%=basePath%>/Public/css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/icon.css">
<script src="<%=basePath%>/Public/js/jquery-1.7.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/Public/js/jquery.easyui.min.js"></script>
<script src="<%=basePath%>/Public/layer/layer.js"></script>
<link href="<%=basePath%>/Public/css/pinglun.css" rel="stylesheet" type="text/css" />
<script src="<%=basePath%>/Public/js/shijian.js" type="text/javascript"></script>
<style type="text/css">
	.datagrid-row {
		height: 50px;
	}
</style>
<script>
var yidu = 0;
$(document).ready(function(){

	var IsCheckFlag = true;
	$("#grid").datagrid({
	 	 onClickCell: function (rowIndex, field, value) {
	         IsCheckFlag = false;
	     },
	 	 onSelect: function (rowIndex, rowData) {
	         if (!IsCheckFlag) {
	             IsCheckFlag = true;
	             $("#grid").datagrid("unselectRow", rowIndex);
	         }
	     },    
	 	 onUnselect: function (rowIndex, rowData) {
	         if (!IsCheckFlag) {
	             IsCheckFlag = true;
	             $("#grid").datagrid("selectRow", rowIndex);
	         }
	     }
	})

	var date1 = new Date();
	$("#endDate").datetimebox('setValue',date1.getFullYear()+"-"+(date1.getMonth() + 1)+"-"+date1.getDate()+"-");
});


/* 操作 */

function formatOper(value,row,index){
	var userId = row.userId;
	return "<a href='javascript:openFans(\"" + userId + "\");'>查看粉丝</a>";
	/* + " | <a href='javascript:openQuxiao(\"" + orderId + "\");'>取消订单</a>"; */
} 


/**
 * 时间格式化
 * @param value
 * @returns {string}
 */
function dateFormatter (value) {
    var date = new Date(value);
    var year = date.getFullYear();
    var month = (date.getMonth() + 1);
    var day = date.getDate();
    var hour = date.getHours();
    var minutes = date.getMinutes();
    var seconds = date.getSeconds();
    if (month < 10) {
        month = "0" + month;
    }
    if (day < 10) {
        day = "0" + day;
    }
    if (hour < 10) {
        hour = "0" + hour;
    }
    if (minutes < 10) {
        minutes = "0" + minutes;
    }
    if (seconds < 10) {
        seconds = "0" + seconds;
    }
    return year + "-" + month + "-" + day + " " + hour + ":" + minutes + ":" + seconds;
}
//搜索
function sousuo()
{	var ssuser = $("#ssuser").val();
	var beginDate = $('#startDate').datebox('getValue'); 
	var endDate = $('#endDate').datebox('getValue'); 
	$('#grid').datagrid('load', {ssuser:ssuser,beginDate:beginDate,endDate:endDate});
}
$.fn.datebox.defaults.parser = function(s){
	var t = Date.parse(s);
	if (!isNaN(t)){
		return new Date(t);
	} else {
		return new Date();
	}
}

function formattIcon(value,row,index){
		var nn = value.indexOf('htt');
		if(nn == -1){
			return '<img width="60px" height="60px" src="<%=basePath3%>/RevenueTreasure/Public/headImg/'+ value +'">';
		}else{
			return '<img width="60px" height="60px" src="' + value + '">';
		}
	
}

function openFans(userId){
	$("#shopFansDVI").treegrid({    
	    url:'<%=basePath%>income/selectFansThree.do?userId='+userId,    
	    idField:'id',    
	    treeField:'text', 
	    animate:'true', 
	    columns:[[    
	        {title:'粉丝',field:'text',width:'25%'},    
	        {field:'icon',title:'头像',width:'10%',formatter:formattIcon},   
	        {field:'userType',title:'类型',width:'15%'},   
	        {field:'tel',title:'手机号',width:'15%'},    
	        {field:'address',title:'地址',width:'15%'},
	        {field:'fenPrice',title:'贡献金额',width:'15%'},    
	    ]]    
	});
	$("#shopFansId").dialog('open');
}

</script>
</head>
<body class="easyui-layout">
<!--------------------------------------- 功能按钮 -------------------------------------------->
<div id="tb" style="height:auto;display: none;">   
	<!-- <div style="width:110%; margin-bottom:3px;text-align: left;height:35px;"> 
	        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="openDels()" >删除</a>
	 </div> --> 
<!------------------------------------ 搜索 ---------------------------------------->
	 <div style="text-align: list;background-color: #f4f4f4;width:100%;height:30px; border-top: 2px solid #D9D9D9;border-bottom: 0px solid #D9D9D9;">
		   <div style="float: left;height: 30px;line-height: 30px;"><label> &nbsp;&nbsp; 用户查询: </label>
	   		  <input name="ssuser" id="ssuser" style="margin-top: 0px;border:1px #95B8E7 solid;width: 150px;height: 23px"  />
		   </div>
		   <div style="float: left;height: 30px;line-height: 30px;"><label> &nbsp;&nbsp; 开始日期: </label>
		 	<input  id="startDate" nastartDate" type= "text" class= "easyui-datebox" data-options="formatter:myformatter,parser:myparser"> </input>
		 </div>
		 <div style="float: left;height: 30px;line-height: 30px;"><label> &nbsp;&nbsp; 结束日期: </label>
		 	<input  id="endDate" name="endDate" type= "text" class= "easyui-datebox" data-options="formatter:myformatter,parser:myparser"> </input>
		 </div>
		   <div onclick="sousuo()" style=" float: left;border:1px solid #000;margin-left: 25px;cursor:pointer;text-align:center;line-height:24px;background-color: #FFF;color:#000;width: 100px;height:23px;margin-top: 3px;border-radius: 5px;"
	    class="easyui-linkbutton" iconCls="icon-search">查询
		</div>  
	</div>
		
</div>	    
		<table id="grid" class="easyui-datagrid"  data-options="nowrap: false" style="width:100%;height:100%;display: none;" title="<img alt='帖子管理' style='width:17px;height:17px;' src='<%=basePath%>/img/titleImg.png'><div style='height:20px;line-height:22px;margin-left:25px;margin-top:-20;'>用户收入统计</div>"
			url="<%=basePath%>/income/userIncomeData.do" toolbar="#tb" data-options="nowrap: false"
			rownumbers="true" pagination="true" striped="true" singleSelect="false" loadMsg="正在努力为您加载数据">
			<thead>
				<tr>
					<th field="userName" width="16%" align="center">用户</th>
					<th field="price" width="16%" align="center">消费总金额</th>
					<th field="sumMoney" width="16%" align="center">提现总金额</th>
					<th field="income" width="16%" align="center">分润收入总金额</th>
					<th field="num" width="16%" align="center">订单数</th>
					<th field="oper" width="19%" align="center" formatter="formatOper" >操作</th>
				</tr>
			</thead>
		</table>
	<!-- ---------------------- 粉丝查看 ----------------------- -->
	<div id="shopFansId" class="easyui-dialog" title="用户粉丝详情" style="top: 80px;left: 100px" modal="true" closed="true" >
		<table id="shopFansDVI" style="width:1200px;height:700px"></table>
	</div>		
			
</body>
 
</html>
