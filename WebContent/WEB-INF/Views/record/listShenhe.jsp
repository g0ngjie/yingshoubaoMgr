<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/commons/taglibs.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">    
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<link href="<%=basePath%>/Public/css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/icon.css">
<link href="<%=basePath%>/Public/uploadify/uploadify.css" type="text/css" rel="stylesheet" />
<!--装载文件-->

<script src="<%=basePath%>/Public/js/jquery-1.7.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/Public/js/jquery.easyui.min.js"></script>

<script type="text/javascript" src="<%=basePath%>/Public/uploadify/jquery.uploadify-3.1.js"></script>
<script type="text/javascript" src="<%=basePath%>/Public/uploadify/swfobject.js"></script>

<script src="<%=basePath%>/Public/js/shijian.js" type="text/javascript"></script>
<script src="<%=basePath%>/Public/layer/layer.js"></script>
<style type="text/css">
	.datagrid-btable tr{
		height: 50px;
	}
</style>
<script>
var sss = "";
$(document).ready(function(){
	var IsCheckFlag = true;
	$("#grid").datagrid({
	 	 onClickCell: function (rowIndex, field, value) {
	         IsCheckFlag = false;
	     },
	 	 onSelect: function (rowIndex, rowData) {
	         if (!IsCheckFlag) {
	             IsCheckFlag = true;
	             $("#grid").datagrid("unselectRow", rowIndex);
	         }
	     },    
	 	 onUnselect: function (rowIndex, rowData) {
	         if (!IsCheckFlag) {
	             IsCheckFlag = true;
	             $("#grid").datagrid("selectRow", rowIndex);
	         }
	     }
	})
});
/* 状态 */
function formatEnable(value,row,index){
	if(value == '0'){
		return "<a style='color:#008000;'>未审核</a>";
		}
}
/* 操作 */
function formatOper(value,row,index){
		return "<a href='javascript:openPasss(\"" + row['cashId'] +","+ row['openId']+","+ row['price'] + "\");'>通过</a>"
		+ " | <a href='javascript:openRefuses(\"" + row['cashId'] + "\");'>拒绝</a>"
	/* 	+ " | <a href='javascript:openLook(\"" + row['cashId'] + "\");'>查看</a>" */
		+ " | <a href='javascript:openDel(\"" + row['cashId'] + "\");'>删除</a>";
} 

/* 查看详情 */
function openLook(cashId)
{	

	$('#grids3').datagrid({
	url:"<%=basePath%>/record/chatixian.do?traState="+cashId,
	   });
	$('#grids3').datagrid('load',{});
	$('#adddlg3').panel({title: "查看详情"});
	$('#adddlg3').dialog('open');
		
	
	
}
/* 删除按钮 */
function openDels()
{	
	var select = $('#grid').datagrid('getSelected');
	if(select){
	if(window.confirm('你确定要删除吗？')){
		 var index = layer.load(1, {
	  shade: [0.1,'#fff'] //0.1透明度的白色背景
	});
	 var str = "";
	var rows  = $('#grid').datagrid("getRows");
	var ckArr = $("input[name='ck']");
	for(var i=0; i<ckArr.length; i++)
	{
		if(ckArr[i].checked)
		{
			if(str != "")
			{
				str += ",";
			}
			str += rows[i].cashId;
		}
	}
	
	$.post("<%=basePath%>/record/shachuRecord.do",{delKeys:str},function(result){
		layer.closeAll('loading');
		data = eval("(" + result + ")");
		if(data.code == 100){
			$('#grid').datagrid('reload');
			$.messager.show({
							title:'提示',
							msg:'<div class="msgs">删除成功</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
		}
	}); 
	}
	}else{
		$.messager.show({
							title:'提示',
							msg:'<div class="msgs">请选择您要删除的数据</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
	}
	
}

//单删
function openDel(cashId)
{	
	if(window.confirm('你确定要删除吗？')){
	$.post("<%=basePath%>/record/shachuRecord.do",{delKeys:cashId},function(result){
		layer.closeAll('loading');
		data = eval("(" + result + ")");
		if(data.code == 100){
			$('#grid').datagrid('reload');
			$.messager.show({
							title:'提示',
							msg:'<div class="msgs">删除成功</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
		}
		
	}); 
	}
}

/* 核审 */
function openPass()
{	
	var select = $('#grid').datagrid('getSelected');
	if(select){
		 var index = layer.load(1, {
	  shade: [0.1,'#fff'] //0.1透明度的白色背景
	});
	 var str = "";
	var rows  = $('#grid').datagrid("getRows");
	var ckArr = $("input[name='ck']");
	for(var i=0; i<ckArr.length; i++)
	{
		if(ckArr[i].checked)
		{
			if(str != "")
			{
				str += ",";
			}
			str += rows[i].cashId;
		}
	}
	
	$.post("<%=basePath%>/record/passRecord.do",{delKeys:str},function(result){
		layer.closeAll('loading');
		data = eval("(" + result + ")");
		//alert(result)
		if(data.code == 100){
			$('#grid').datagrid('reload');
			
			$.messager.show({
							title:'提示',
							msg:'<div class="msgs">已通过</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
		}
	}); 
	}else{
		$.messager.show({
							title:'提示',
							msg:'<div class="msgs">请选择您要通过的数据</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
	}
	
}
/* 单核审 */
function openPasss(res)
{
	var result = res.split(',');
	var cashId = result[0];
	var openId = result[1];
	var amount = result[2];
	$.post("http://123.56.238.98:3366/wxpay/transfer/pay.shtml",{openId:openId,amount:amount*100},function(result){
		data = eval("(" + result + ")");
			if(data.code == 1){
				$.post("<%=basePath%>/record/passRecord.do",{delKeys:cashId},function(result){
					layer.closeAll('loading');
					data = eval("(" + result + ")");
					if(data.code == 100){
						$.post("<%=basePath%>/send/sengManager.do",{userId:data.userId,mode:"1",state:"您申请的提现已经通过申请",price:data.price},function(result){
							layer.closeAll('loading');
						});
						$('#grid').datagrid('reload');
						$.messager.show({
										title:'提示',
										msg:'<div class="msgs">已通过</div>',
										timeout: 1000, 
										showType:'fade',
										style:{right:'', bottom:''}
						});
					}
				}); 
			}else{
				$.messager.show({
					title:'提示',
					msg:'<div class="msgs">'+data.msg+'</div>',
					timeout: 1000, 
					showType:'fade',
					style:{right:'', bottom:''}
				});
			}
		});
	
}

/* 拒绝 */
function openRefuses1()
{	
	var select = $('#grid').datagrid('getSelected');
	if(select){
		 var index = layer.load(1, {
	  shade: [0.1,'#fff'] //0.1透明度的白色背景
	});
	 var str = "";
	var rows  = $('#grid').datagrid("getRows");
	var ckArr = $("input[name='ck']");
	for(var i=0; i<ckArr.length; i++)
	{
		if(ckArr[i].checked)
		{
			if(str != "")
			{
				str += ",";
			}
			str += rows[i].cashId;
		}
	}
	
	$.post("<%=basePath%>/record/refuseRecord.do",{delKeys:str},function(result){
		layer.closeAll('loading');
		data = eval("(" + result + ")");
		//alert(result)
		if(data.code == 100){
			$('#grid').datagrid('reload');
			$.messager.show({
							title:'提示',
							msg:'<div class="msgs">已拒绝</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
		}
	}); 
	}else{
		$.messager.show({
							title:'提示',
							msg:'<div class="msgs">请选择您要拒绝的数据</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
	}
	
}

function openRefuses(cashId){
	$("#reject_dialog").dialog('open');
	$("#cashIdd").val(cashId);
}

/* 单拒绝 */
function openRefuse1()
{	
	var reason = $("#reasonId").val();
	var cashId = $("#cashIdd").val();
	$.post("<%=basePath%>/record/refuseRecord.do",{delKeys:cashId,reason:reason},function(result){
		layer.closeAll('loading');
		data = eval("(" + result + ")");
		//alert(result)
		if(data.code == 100){
			$.post("<%=basePath%>/send/sengManager.do",{userId:data.userId,mode:"2",state:"你的申请提现失败",price:data.price,reason:reason},function(result){
				layer.closeAll('loading');
			});
			$('#grid').datagrid('reload');
			$("#reject_dialog").dialog('close');
			$.messager.show({
							title:'提示',
							msg:'<div class="msgs">已拒绝</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
		}
	}); 
	
}


//搜索
function sousuo()
{
	var Ssuser = $('#Ssuser').val();
	$('#grid').datagrid('load', {Ssuser:Ssuser});
}

</script>
</head>
<body class="easyui-layout">
<!----------------------- 功能按钮 ----------------------->
<div id="tb" style="height:auto;display: none;">   
		<div style="width:110%; margin-bottom:3px;text-align: left;height:35px;"> 
	    <!--    <!-- <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-ok" plain="true" onclick="openPass()" >通过</a>
	        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-lock" plain="true" onclick="openRefuses()" >拒绝</a>
	        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="openDels()" >删除</a> 
	 </div> 

	  <div style="text-align: list;background-color: #f4f4f4;width:100%;height:30px; border-top: 2px solid #D9D9D9;border-bottom: 0px solid #D9D9D9;"> -->
		    <div style="float: left;height: 30px;line-height: 30px;"><label> &nbsp;&nbsp;提现用户: </label>
	   		<input name="Ssuser" id="Ssuser" style="margin-top: 0px;border:1px #95B8E7 solid;width: 150px;height: 23px"  />
		   </div>
		   
		   <div onclick="sousuo()" style=" float: left;border:1px solid #000;margin-left: 25px;cursor:pointer;text-align:center;line-height:24px;background-color: #FFF;color:#000;width: 100px;height:23px;margin-top: 3px;border-radius: 5px;"
	    class="easyui-linkbutton" iconCls="icon-search">搜索	   
		</div>  
	</div>
</div>
		<table id="grid" class="easyui-datagrid" data-options="nowrap: false" style="width:100%;height:100%;display: none;" title="<img alt='交易记录' style='width:17px;height:17px;' src='<%=basePath%>/img/titleImg.png'><div style='height:20px;line-height:22px;margin-left:25px;margin-top:-20;'>提现审核</div>"
			url="<%=basePath%>/record/shenheListData.do" toolbar="#tb"
			rownumbers="true" pagination="true" singleSelect="false" loadMsg="正在努力为您加载数据">
			<thead>
				<tr>
					<th field="ck" checkbox="true" id="ck" name="ck"></th>
					<th field="userName" width="20%" align="center">提现用户</th>
					<th field="price" width="20%" align="center">提现金额</th>
					<th field="introduce" width="20%" align="center">说明</th>
					<th field="isThrough" width="17%" align="center" formatter="formatEnable">状态</th>
					<th field="caozuo" width="20%" align="center" formatter="formatOper">操作</th>
				</tr>
			</thead>
		</table>
<div id="adddlg3" class="easyui-dialog"  closed="true" >
		<table id="grids3" class="easyui-datagrid" data-options="nowrap: false" style="width:700px;height:200px;display: none;"
			toolbar="#tt3" rownumbers="true" pagination="true" singleSelect="false" loadMsg="正在努力为您加载数据">
			<thead>
				<tr>
					<th field="cq" checkbox="true" id="cq3" name="cq3"></th>
					<th field="yinhang" width="30%" align="center">类型</th>
					<th field="kahao" width="30%" align="center">卡号/支付宝账号</th>
					<th field="chikaren" width="30%" align="center">持卡人</th>
					<!-- <th field="kaihu" width="25%" align="center">开户行</th> -->
				</tr>
			</thead>
		</table>
</div>

<div id="erjiButtonxx" style="height: 30px;display: none;">
	<div style="width:140px;height:35px;float: right;">
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="saveErjixx()" iconcls="icon-save">确定</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="javascript:$('#tanerjixx').dialog('close')"
		iconcls="icon-cancel">取消</a>	
	</div>
</div>

<!-- ----------------------------拒绝原因------------------------- -->
<div id="reject_dialog" class="easyui-dialog" title="拒绝原因" style="width:500px;height:300px;" closed="true" buttons="#adlg_btn" data-options="resizable:true,modal:true">   
    <textarea id="reasonId" style="width: 100%;height: 100%;padding: 10px"></textarea>   
    <input type="hidden" id="cashIdd" name="cashIdd"/> 
</div> 

<div id="adlg_btn" style="height: 30px;display: none">
	<div style="width:140px;height:35px;float: right;">
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="openRefuse1()" iconcls="icon-save">提交</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="javascript:$('#reject_dialog').dialog('close')"
		iconcls="icon-cancel">取消</a>
	</div>
</div> 

</body>

<style>
.uploadify-queue{ display: none;}
</style>
</html>