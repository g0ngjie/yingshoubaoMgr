<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/commons/taglibs.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">    
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<link href="<%=basePath%>/Public/css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/icon.css">
<link href="<%=basePath%>/Public/uploadify/uploadify.css" type="text/css" rel="stylesheet" />
<!--装载文件-->

<script src="<%=basePath%>/Public/js/jquery-1.7.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/Public/js/jquery.easyui.min.js"></script>

<script type="text/javascript" src="<%=basePath%>/Public/uploadify/jquery.uploadify-3.1.js"></script>
<script type="text/javascript" src="<%=basePath%>/Public/uploadify/swfobject.js"></script>

<script src="<%=basePath%>/Public/js/shijian.js" type="text/javascript"></script>
<script src="<%=basePath%>/Public/layer/layer.js"></script>
<style type="text/css">
	.datagrid-btable tr{
		height: 50px;
	}
</style>
<script>
var sss = "";
$(document).ready(function(){
	var IsCheckFlag = true;
	$("#grid").datagrid({
	 	 onClickCell: function (rowIndex, field, value) {
	         IsCheckFlag = false;
	     },
	 	 onSelect: function (rowIndex, rowData) {
	         if (!IsCheckFlag) {
	             IsCheckFlag = true;
	             $("#grid").datagrid("unselectRow", rowIndex);
	         }
	     },    
	 	 onUnselect: function (rowIndex, rowData) {
	         if (!IsCheckFlag) {
	             IsCheckFlag = true;
	             $("#grid").datagrid("selectRow", rowIndex);
	         }
	     }
	})
});
/* 操作 */
function formatOper(value,row,index){
		return "<a href='javascript:openDel(\"" + row['cashId'] + "\");'>删除</a>";
} 
/* 状态 */
function formatEnable(value,row,index){
	if(value == '2')
		return "<a style='color:#FF0000;'>已拒绝</a>";
	else
		return "<a style='color:#008000;'>已通过</a>";
}
/* 删除按钮 */
function openDels()
{	
	var select = $('#grid').datagrid('getSelected');
	if(select){
	if(window.confirm('你确定要删除吗？')){
		 var index = layer.load(1, {
	  shade: [0.1,'#fff'] //0.1透明度的白色背景
	});
	 var str = "";
	var rows  = $('#grid').datagrid("getRows");
	var ckArr = $("input[name='ck']");
	for(var i=0; i<ckArr.length; i++)
	{
		if(ckArr[i].checked)
		{
			if(str != "")
			{
				str += ",";
			}
			str += rows[i].cashId;
		}
	}
	
	$.post("<%=basePath%>/record/shachuRecord.do",{delKeys:str},function(result){
		layer.closeAll('loading');
		data = eval("(" + result + ")");
		if(data.code == 100){
			$('#grid').datagrid('reload');
			$.messager.show({
							title:'提示',
							msg:'<div class="msgs">删除成功</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
		}
	}); 
	}
	}else{
		$.messager.show({
							title:'提示',
							msg:'<div class="msgs">请选择您要删除的数据</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
	}
	
}

//单删
function openDel(cashId)
{	
	if(window.confirm('你确定要删除吗？')){
	$.post("<%=basePath%>/record/shachuRecord.do",{delKeys:cashId},function(result){
		layer.closeAll('loading');
		data = eval("(" + result + ")");
		if(data.code == 100){
			$('#grid').datagrid('reload');
			$.messager.show({
							title:'提示',
							msg:'<div class="msgs">删除成功</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
		}
		
	}); 
	}
}

//搜索
function sousuo()
{
	var Ssuser = $('#Ssuser').val();
	var SsJia = document.getElementById("SsJia").value;
	$('#grid').datagrid('load', {Ssuser:Ssuser,SsJia:SsJia});
}

</script>
</head>
<body class="easyui-layout">
<!----------------------- 功能按钮 ----------------------->
<div id="tb" style="height:auto;display: none;">   
		<div style="width:110%; margin-bottom:3px;text-align: left;height:35px;"> 
	        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="openDels()" >删除</a>
	 </div> 

	  <div style="text-align: list;background-color: #f4f4f4;width:100%;height:30px; border-top: 2px solid #D9D9D9;border-bottom: 0px solid #D9D9D9;">
		    <div style="float: left;height: 30px;line-height: 30px;"><label> &nbsp;&nbsp;提现用户: </label>
	   		<input name="Ssuser" id="Ssuser" style="margin-top: 0px;border:1px #95B8E7 solid;width: 150px;height: 23px"  />
		   </div>
		    <div style="float: left;height: 30px;line-height: 30px;"><label> &nbsp;&nbsp; 状态: </label>
	   		<select name="SsJia" id="SsJia" style="width:100px;height:22px;border:1px #95B8E7 solid;">
		   		<option  value="">全部</option>
		   		<option  value="1">已通过</option>
		   		<option  value="2">已拒绝</option>
			   </select>    
	     </div> 
		   <div onclick="sousuo()" style=" float: left;border:1px solid #000;margin-left: 25px;cursor:pointer;text-align:center;line-height:24px;background-color: #FFF;color:#000;width: 100px;height:23px;margin-top: 3px;border-radius: 5px;"
	    class="easyui-linkbutton" iconCls="icon-search">搜索	   
		</div>  
	</div>
</div>
		<table id="grid" class="easyui-datagrid" data-options="nowrap: false" style="width:100%;height:100%;display: none;" title="<img alt='交易记录' style='width:17px;height:17px;' src='<%=basePath%>/img/titleImg.png'><div style='height:20px;line-height:22px;margin-left:25px;margin-top:-20;'>提现记录</div>"
			url="<%=basePath%>/record/recordListData.do" toolbar="#tb"
			rownumbers="true" pagination="true" singleSelect="false" loadMsg="正在努力为您加载数据">
			<thead>
				<tr>
					<th field="ck" checkbox="true" id="ck" name="ck"></th>
					<th field="userName" width="20%" align="center">提现用户</th>
					<th field="price" width="20%" align="center">提现金额</th>
					<th field="introduce" width="20%" align="center">说明</th>
					<th field="isThrough" width="17%" align="center" formatter="formatEnable">状态</th>
					<th field="caozuo" width="20%" align="center" formatter="formatOper">操作</th>
				</tr>
			</thead>
		</table>


</body>

<div id="erjiButtonxx" style="height: 30px;display: none;">
	<div style="width:140px;height:35px;float: right;">
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="saveErjixx()" iconcls="icon-save">确定</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="javascript:$('#tanerjixx').dialog('close')"
		iconcls="icon-cancel">取消</a>	
	</div>
</div>
<style>
.uploadify-queue{ display: none;}
</style>
</html>