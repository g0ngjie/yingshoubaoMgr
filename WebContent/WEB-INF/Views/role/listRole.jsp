<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/commons/taglibs.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>角色管理</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">    
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<link href="<%=basePath%>/Public/css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/icon.css">
<script src="<%=basePath%>/Public/js/jquery-1.7.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/Public/js/jquery.easyui.min.js"></script>
<script src="<%=basePath%>/Public/layer/layer.js"></script>
<style type="text/css">
	.datagrid-btable tr{
		height: 50px;
	}
</style>
<script>
$(document).ready(function(){
//回车点击事件--搜索功能
	$(document).keydown(function(event){
		if(event.keyCode==13){
			//$("#login").click();
			sousuo();
		}
	}) ;
	var IsCheckFlag = true;
$("#grid").datagrid({
 	 onClickCell: function (rowIndex, field, value) {
         IsCheckFlag = false;
     },
 	 onSelect: function (rowIndex, rowData) {
         if (!IsCheckFlag) {
             IsCheckFlag = true;
             $("#grid").datagrid("unselectRow", rowIndex);
         }
     },    
 	 onUnselect: function (rowIndex, rowData) {
         if (!IsCheckFlag) {
             IsCheckFlag = true;
             $("#grid").datagrid("selectRow", rowIndex);
         }
     }
     
})

});
/* 操作 */
function formatOper(value,row,index){
	return "<a href='javascript:openquanxian(\"" + row['roleId'] + "\");'>权限设置</a>"
} 
/* 添加 保存 角色 */
function saveAddRole()
{	
	var userName = $('#userName').val();
	if(userName != "" && userName != null){
	$('#formAddRole').form('submit', {
		url : '<%=basePath%>/role/saveRole.do',
		onSubmit : function() {
			
			return $(this).form('validate');
			var index = layer.load(1, {
				  shade: [0.1,'#fff'] //0.1透明度的白色背景
			})
			
		},
		success : function(result) {
			data = eval("(" + result + ")");
			if(data.code == 100)
			{
				layer.closeAll('loading');
				$('#adddlg').dialog('close');
				$('#grid').datagrid('reload');
				$.messager.show({
					title:'提示',
					msg:'<div class="msgs">保存成功</div>',
					timeout: 1000, 
					showType:'fade',
					style:{right:'', bottom:''}
				});
			}
			else
			{
				/* $.messager.show({
					title:'提示',
					msg:data.msg,
					timeout: 1000,  
					showType:'fade',
					style:{right:'', bottom:''}
				}); */
			}
		}
	}); 
	}
	else
		layer.msg("请输入角色名称");
}

/* 添加 权限 */
function saveAddRolePower()
{	
    //var nodes = $('#orgTree').tree('getChecked');  
    var nodes = $('#orgTree').tree('getChecked',['checked','indeterminate']);  
    var data="";
    for(var i=0;i<nodes.length;i++)
    {
   	 	var string = nodes[i].id;
   	 	data+=string+",";
    }
    var index = layer.load(1, {
				  shade: [0.1,'#fff'] //0.1透明度的白色背景
			})
    data.substring(0, data.length-2);
    var hiddenRoleId = $('#roleIds').val();
	$.post("<%=basePath%>/role/saveRolePower.do",{roleId:hiddenRoleId,data:data},function(result){
		layer.closeAll('loading');
		data = eval("(" + result + ")");
		//alert(result)
		if(data.code == 100){
			layer.closeAll('loading');
			$('#grid').datagrid('reload');
			$('#treedlg').dialog('close');
			$.messager.show({
							title:'提示',
							msg:'<div class="msgs">设置成功</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
		}
	}); 
	
	
}

function openquanxian(roleId)
{	
	 $('#orgTree').tree({    
	    url:'tree_data.json'   
	});  

	 
	 $.post("<%=basePath%>/role/editRolePower.do",{roleIdsss:roleId},function(result){
		data = eval("(" + result + ")");
			$('#orgTree').tree({
			data: data
		});
	//$('#treedlg').tree('reload',{roleId:roleId});


			
	
	$('#roleIds').val(roleId);
	$('#treedlg').panel({title: "权限设置"});
	$('#treedlg').dialog('open');
	});  
}
</script>
</head>
<body class="easyui-layout">
<!----------------------------- 功能按钮 --------------------------------->
<div id="tb" style="height:auto;display: none;">   
</div>
		<table id="grid" class="easyui-datagrid" style="width:100%;height:100%;display: none;" title="<img alt='后台用户管理' style='width:17px;height:17px;' src='<%=basePath%>/img/titleImg.png'><div style='height:20px;line-height:22px;margin-left:25px;margin-top:-20;'>角色管理</div>"
			url="<%=basePath%>/role/roleListData.do" toolbar="#tb"
			rownumbers="true" pagination="true" singleSelect="false" loadMsg="正在努力为您加载数据">
			<thead>
				<tr>
					<th field="userName" width="50%" align="center">角色名称</th>
					<th field="oper" width="49%" align="center" formatter="formatOper" align="center">操作</th>
				</tr>
			</thead>
		</table>
		
<!-- 权限 -->
<div id="treedlg" class="easyui-dialog" style="width: 400px; height: 500px; padding: 10px 20px;" closed="true"  buttons="#powerdlg-buttons" >
	<form id="formAddRolePower" name="formAddRole" method="post">
	<div style="width:60%;height:100%;border:0px solid #ccc;">
		<ul id="orgTree" name="orgTree" target="leftEmp" class="easyui-tree" url="<%=basePath%>/role/editRolePower.do" data-options="method:'get',animate:true,checkbox:true">
		</ul>
	</div>
	
	<input type="hidden" name="roleIds" id="roleIds" value=""/>
	</form>
</div>
<!------------------------------ 确认取消按钮 ----------------------------------->
<div id="powerdlg-buttons" style="height: 30px;">
	<div style="width:140px;height:35px;float: right;">
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="saveAddRolePower()" iconcls="icon-save">保存</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="javascript:$('#treedlg').dialog('close')"
		iconcls="icon-cancel">取消</a>	
	</div>
</div> 
</body>
</html>
