<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/commons/taglibs.jsp" %>
 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>  
<base href="<%=basePath%>">
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">    
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<link href="<%=basePath%>/Public/css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/icon.css">
<script src="<%=basePath%>/Public/js/jquery-1.7.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/Public/js/jquery.easyui.min.js"></script>
<script src="<%=basePath%>/Public/layer/layer.js"></script>
<link href="<%=basePath%>/Public/css/pinglun.css" rel="stylesheet" type="text/css" />
<script src="<%=basePath%>/Public/js/shijian.js" type="text/javascript"></script>
<script src="Public/js/highcharts.js"></script>
<script>
$(document).ready(function(){
	loadRatio();
});

function loadRatio(){
	$.post("<%=basePath%>/saleRatio/ratioList.do",function(result){
		data = eval("(" + result + ")");
		var res = data.rows[0].result;
		var playBili = data.rows[0].palyBili;
		var fans = 100 - res;
		$("#panelBili").html(res+"%");
		$("#playBili").html(playBili+"%");
		$("#ratioId").val(data.rows[0].ratioId);
		 var chart = {
	      type: 'pie'
	   };
	   var title = {
	      text: '佣金比例'   
	   };      
	   var tooltip = {
	      //pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
	   };
	   var plotOptions = {
	      pie: {
	         allowPointSelect: true,
	         cursor: 'pointer',
	         dataLabels: {
	            enabled: true,
	            //format: '<b>{point.name}</b>: {point.percentage:.1f} %',
	            style: {
	               color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
	            }
	         }
	      }
	   };
	   var series= [{
	      type: 'pie',
	      name: '比例',
	      data: [
	         ['商家', res],
	         {
	            name: '粉丝',
	            y: fans,
	            sliced: true,
	            selected: true
	         },
	         ['平台',playBili]
	      ]
	   }];     
	   var json = {};   
	   json.chart = chart; 
	   json.title = title;     
	   json.tooltip = tooltip;  
	   json.series = series;
	   json.plotOptions = plotOptions;
	   $('#container').highcharts(json);  		
	});	
}

function openEdit(){
 	var ratioId = $("#ratioId").val();
	$.post("<%=basePath%>/saleRatio/shopRatioBj.do",{ratioId:ratioId},function(result){
		data = eval("(" + result + ")");
		$("#bili").numberbox({value:data.once});
		$("#playbili").numberbox({value:data.playbili});
		$("#ratioId").val(data.ratioId);
		$('#mode').val("edit");
		$('#adddlg').panel({title: "修改"});
		$('#adddlg').dialog('open');
	}); 
}

/* 添加 保存 角色 */
function saveShopRatio()
{	
	var bili = $('#bili').val();
	if(bili==""||bili==null){
		layer.msg("比例不能为空")
		return 0;
	}
	var play_bili = 10000 - bili*100;
	$("#result_playBili").val(play_bili/100);
	$('#formShopRatio').form('submit', {
		url : '<%=basePath%>/saleRatio/shopRatioEdit.do',
		onSubmit : function() {
			var index = layer.load(1, {
				  shade: [0.1,'#fff'] //0.1透明度的白色背景
			})
			return $(this).form('validate');
		},
		success : function(result) {
			data = eval("(" + result + ")");
			if(data.code == 100)
			{
				layer.closeAll('loading');
				$('#adddlg').dialog('close');
				$('#grid').datagrid('reload');
				loadRatio();
				$.messager.show({
					title:'提示',
					msg:'<div class="msgs">保存成功</div>',
					timeout: 1000, 
					showType:'fade',
					style:{right:'', bottom:''}
				});
			}
			else
			{
				$.messager.show({
					title:'提示',
					msg:data.msg,
					timeout: 1000,  
					showType:'fade',
					style:{right:'', bottom:''}
				});
			}
		}
	}); 
}
</script>
<style type="text/css">
.datagrid-btable tr{
height:50px; 
}
button:HOVER {
	background-color: #95CEFF;
	color: #FFF;
}
button:ACTIVE{
	background-color: #6383B8;
	color: #FFF;
}
button{
	color: #5E7EB4;
}
</style>
</head>
<body class="easyui-layout">
	<div id="p" class="easyui-panel" title="<img style='width:17px;height:17px;' src='<%=basePath%>/img/titleImg.png'><div style='height:20px;line-height:22px;margin-left:25px;margin-top:-20;'>佣金比例</div>" style="width:100%;height:100%;padding:20px 10px;background:#fafafa;" data-options="">
	
		<div style="float: left;">
			<div id="" class="easyui-panel" style="width:300px;height:500px;padding:10px;background:#fafafa;">
				<!-- <span id="panelBili" style="font-size: 80;text-align: center;margin-top: 50"></span> -->
				<span style="font-size: 23;margin-top: 57;float: left;">平台：</span>
				<span id="playBili" style="font-size: 35;text-align: center;margin-top: 50"></span>
				<span style="font-size: 23;margin-top: 57;float: left;">店铺：</span>
				<span id="panelBili" style="font-size: 35;text-align: center;margin-top: 50"></span>
				<button type="button" onclick="openEdit()" style="width: 150;height: 50;font-size: 20px;margin: 60px;border-radius:6px">设置</button>
			</div>
		</div>
		<div style="float: left;margin-left: 50;width: 70%;height: 80%">
			<div id="" class="easyui-panel" style="width:100%;height:100%;padding:10px;background:#fafafa;" data-options="">
				<div id="container" style="width:100%; height: 100%; margin: 0 auto"></div>
			</div>
		</div>
	</div>
	
	<!---------------------------------- 添加界面 ----------------------------->
	<div id="adddlg" class="easyui-dialog" style="width: 350px; height: 220px; padding: 10px 20px;display: none;" closed="true" modal="true" buttons="#adddlg-buttons" singleSelect="false" loadMsg="正在努力为您加载数据">
		<form id="formShopRatio" name="formShopRatio" method="post">
			<div style="float:left;width:100%;height:35px;margin-top: 5px;">
				<span style="color: #D9534F">(注:设置成功后,三级分销比例需要从新设置)</span>
				<div style="height: 10px;width: 50%;"></div>
				<div style="float:left;width:100%;height:21px;margin-top: 15px;">
					<div style="float: left;"><label style="display:inline-block;width:100px;color:#000;font-size:15px;">平台:</label></div>
					<div style="float: left;"><input class="easyui-numberbox" style="vertical-align:middle;font-size:15px;width:80px; color:#000;border:1px solid #95B8E7;padding-bottom: 5px;" type="text" value="" id="playbili" name="playbili" precision="2" min="0" /></div>
					<div style="float: left;"><label style="display:inline-block;width:50px;color:#000;font-size:15px;">%</label></div>
				</div>
				<div style="float:left;width:100%;height:21px;margin-top: 15px;">
					<div style="float: left;"><label style="display:inline-block;width:100px;color:#000;font-size:15px;">店铺:</label></div>
					<div style="float: left;"><input class="easyui-numberbox" style="vertical-align:middle;font-size:15px;width:80px; color:#000;border:1px solid #95B8E7;padding-bottom: 5px;" type="text" value="" id="bili" name="bili" precision="2" min="0" /></div>
					<div style="float: left;"><label style="display:inline-block;width:50px;color:#000;font-size:15px;">%</label></div>
				</div>
			</div>
			<input type="hidden" name="ratioId" id="ratioId" value="" />
			<input type="hidden" name="result_playBili" id="result_playBili" value=""/>
		</form>
	
	</div>
	
	<!-------------------------------------------- 确认取消按钮 -------------------------------------------------->
	<div id="adddlg-buttons" style="height: 30px;display: none;">
		<div style="width:140px;height:35px;float: right;">
			<a href="javascript:void(0)" class="easyui-linkbutton" onclick="saveShopRatio()" iconcls="icon-save">保存</a>
			<a href="javascript:void(0)" class="easyui-linkbutton" onclick="javascript:$('#adddlg').dialog('close')" iconcls="icon-cancel">取消</a>
		</div>
	</div>
			
</body>
</html>
