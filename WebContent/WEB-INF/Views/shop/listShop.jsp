<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/commons/taglibs.jsp" %>
 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>  
<base href="<%=basePath%>">
<title>商家用户管理</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">    
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<link href="<%=basePath%>/Public/css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/icon.css">
<link href="<%=basePath%>/Public/uploadify/uploadify.css" type="text/css" rel="stylesheet" />
<script src="<%=basePath%>/Public/js/jquery-1.7.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/Public/js/jquery.easyui.min.js"></script>
<script type="text/javascript" src="<%=basePath%>/Public/js/jquery.qrcode.min.js"></script>
<script src="<%=basePath%>/Public/layer/layer.js"></script>
<style type="text/css">
	#locationDVI input{
		display: inline;
		width: 100px;
		height:30px;
		border: 1px solid #CDCDCD;
		border-radius:5px;
		text-align: center;
		margin-top: 8px;
	}
	#locationDVI span{
		font-size: 15px;
		display: inline;
		margin-top: 8px;
	}
	#addressDVI{
		margin-top:25px;
	}
	#addressDVI span{
		font-size: 15px;
		display: inline;
	}
	#addressDVI input{
		border-radius:5px;
		height:30px;
		border: 1px solid #CDCDCD;
		width: 234px;
		display: inline;
		text-align: center;
	}
	.datagrid-row {
		height: 50px;
	}
</style>
<script>
var id = "";
var editmima = 0;
var tou = 0;
var bei = 0;
$(document).ready(function(){
	var IsCheckFlag = true;
	$("#grid").datagrid({
	 	 onClickCell: function (rowIndex, field, value) {
	         IsCheckFlag = false;
	     },
	 	 onSelect: function (rowIndex, rowData) {
	         if (!IsCheckFlag) {
	             IsCheckFlag = true;
	             $("#grid").datagrid("unselectRow", rowIndex);
	         }
	     },    
	 	 onUnselect: function (rowIndex, rowData) {
	         if (!IsCheckFlag) {
	             IsCheckFlag = true;
	             $("#grid").datagrid("selectRow", rowIndex);
	         }
	     }
	})
});
/* 显示启用按钮 */
function formatEnable(value,row,index){
	if(value == '0')
		return "<a style='color:#FF0000'>禁用</a>";
	return "<a style='color:#008000'>启用</a>";
}
/* 操作 */
function formatOper(value,row,index){
	return "<a href='javascript:openThreeBili(\"" + row['shopId'] + "\");'>比例设置</a>" 
	+ " | <a href='javascript:openFan(\"" + row['shopId'] + "\");'>粉丝</a>" 
	+ " | <a href='javascript:openQrCode(\"" + row['shopId'] + "\");'>二维码</a>"
	+ " | <a href='javascript:openActivity(\"" + row['shopId'] + "\");'>活动</a>"
	+ " | <a href='javascript:openOrder(\"" + row['shopId'] + "\");'>订单评价</a>"
	+ " | <a href='javascript:openModif(\"" + row['shopId'] + "\");'>查看图片</a>"
	+ " | <a href='javascript:openLocation(\"" + row['shopId'] + "\");'>定位</a>"
} 

function openQrCode(shopId){
	$("#shopQrDVI").empty();
	$("#shopQrId").dialog('open');
	$.post("<%=basePath%>/shop/shopCode.do",{shopId:shopId},function(result){
		data = eval("(" + result + ")")
		var shopCode = data.shopCode;
		$("#shopQrDVI").qrcode({ 
		    render: "canvas", //table方式 
		    width: 400, //宽度 
		    height:400, //高度 
		    text: "http://ysb.cosinwx.com/RevenueTreasure/appweb/order_pay.do?shopCode="+shopCode//任意内容 
		});
	});
}

function openLocation(shopId){
	$("#shopIdINP").val(shopId);
	$.post("<%=basePath%>/shop/chaLocation.do",{shopId:shopId},function(result){
		data = eval("(" + result + ")");
		$("#lngId").val(data.lng);
		$("#latId").val(data.lat);
		$("#addresId").val(data.address);
		$("#locationId").dialog('open');
	});
}

function ding(){
	window.open("<%=basePath%>shop/map.do","newindow","width=700,height=500,toolbar=no,scrollbars=no"); 
}

function saveLocation(){
	var lngId = $("#lngId").val();
	if(lngId == null || lngId == ""){
		alert("定位不能为空！");
		return;
	}
	$('#formLocation').form('submit', {
		url : '<%=basePath%>/shop/saveLocation.do',
		onSubmit : function() {
			var index = layer.load(1, {
	  shade: [0.1,'#fff'] //0.1透明度的白色背景
	})
			return $(this).form('validate');
		},
		success : function(result) {
			data = eval("(" + result + ")");
			if(data.code == 100)
			{
				layer.closeAll('loading');
				$('#locationId').dialog('close');
				$('#grid').datagrid('reload');
				$.messager.show({
					title:'提示',
					msg:'<div class="msgs">保存成功</div>',
					timeout: 1000, 
					showType:'fade',
					style:{right:'', bottom:''}
				});
			}
			else
			{
					layer.closeAll('loading');
			}
		}
	});
}

function getNewLinkValue(value){
  $("#lngId").val(value.split(",")[0]);
  $("#latId").val(value.split(",")[1]);
  $("#addresId").val(value.split(",")[2] + value.split(",")[3] + value.split(",")[4] + value.split(",")[5]);
}

/* 查看图片 */
function openModif(shopId)
{	
	$.post("<%=basePath%>/shop/Modif.do",{shopId:shopId},function(result){
		data = eval("(" + result + ")");
		var bei1 = data.img;
		if(bei1 == null || bei1.trim() == "")
			$('#img').attr("src", '<%=basePath2%>img/null.png');
		else{
			$('#img').attr("src", '<%=basePath3%>/RevenueTreasure/Public/licenseImg/'+bei1);
			$("#licenseImg").attr("href", '<%=basePath3%>/RevenueTreasure/Public/licenseImg/'+bei1);
			}
		var bei2 = data.icon;
		if(bei2 == null || bei2.trim() == "")
			$('#icon').attr("src", '<%=basePath3%>/RevenueTreasure/Public/headImg/null.png');
		else{
			$('#icon').attr("src", '<%=basePath3%>/RevenueTreasure/Public/licenseImg/'+bei2);
			$('#idCard_a').attr("href", '<%=basePath3%>/RevenueTreasure/Public/licenseImg/'+bei2);
			}
		var bei3 = data.backImg;
		if(bei3 == null || bei3.trim() == "")
			$('#backImg').attr("src", '<%=basePath2%>img/null.png');
		else{
			$('#backImg').attr("src", '<%=basePath3%>/RevenueTreasure/Public/shopBackImg/'+bei3);
			}
		$('#adddlg').panel({title: "查看图片"});
		$('#adddlg').dialog('open');
	}); 
}

/* 多启用按钮 */
function openAbles()
{	
	var select = $('#grid').datagrid('getSelected');
	if(select){
		 var index = layer.load(1, {
	  shade: [0.1,'#fff'] //0.1透明度的白色背景
	});
	 var str = "";
	var rows  = $('#grid').datagrid("getRows");
	var ckArr = $("input[name='ck']");
	for(var i=0; i<ckArr.length; i++)
	{
		if(ckArr[i].checked)
		{
			if(str != "")
			{
				str += ",";
			}
			str += rows[i].shopId;
		}
	}
	
	$.post("<%=basePath%>/shop/shopAbles.do",{delKeys:str},function(result){
		layer.closeAll('loading');
		data = eval("(" + result + ")");
		//alert(result)
		if(data.code == 100){
			$('#grid').datagrid('reload');
			$.messager.show({
							title:'提示',
							msg:'<div class="msgs">已启用</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
		}
	}); 
	}else{
		$.messager.show({
							title:'提示',
							msg:'<div class="msgs">请选择你要启用的数据</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
	}
	
}

/* 多禁用按钮 */
function openDisables()
{	
	var select = $('#grid').datagrid('getSelected');
	if(select){
		 var index = layer.load(1, {
	  shade: [0.1,'#fff'] //0.1透明度的白色背景
	});
	 var str = "";
	var rows  = $('#grid').datagrid("getRows");
	var ckArr = $("input[name='ck']");
	for(var i=0; i<ckArr.length; i++)
	{
		if(ckArr[i].checked)
		{
			if(str != "")
			{
				str += ",";
			}
			str += rows[i].shopId;
		}
	}
	
	$.post("<%=basePath%>/shop/disablesShops.do",{delKeys:str},function(result){
		layer.closeAll('loading');
		data = eval("(" + result + ")");
		//alert(result)
		if(data.code == 100){
			$('#grid').datagrid('reload');
			$.messager.show({
							title:'提示',
							msg:'<div class="msgs">已禁用</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
		}
	}); 
	}else{
		$.messager.show({
							title:'提示',
							msg:'<div class="msgs">请选择您要禁用的数据</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
	}
	
}

//搜索
function sousuo()
{
	var Name = $('#Name').val();
	var tel = $('#tel').numberbox('getValue');
	var SsEnable = $('#SsEnable').val();
	$('#grid').datagrid('load', {Name:Name,tel:tel,SsEnable:SsEnable});
}

function formattIcon(value,row,index){
		var nn = value.indexOf('htt');
		if(nn == -1){
			return '<img width="60px" height="60px" src="<%=basePath3%>/RevenueTreasure/Public/headImg/'+ value +'">';
		}else{
			return '<img width="60px" height="60px" src="' + value + '">';
		}
	
}
//查看粉丝
function openFan(shopId){
	$("#shopFansDVI").treegrid({    
	    url:'<%=basePath%>shopFans/getFans.do?shopId='+shopId,    
	    idField:'id',    
	    treeField:'text', 
	    animate:'true', 
	    columns:[[    
	        {title:'粉丝',field:'text',width:'25%'},    
	        {field:'icon',title:'头像',width:'5%',formatter:formattIcon},   
	        {field:'userType',title:'类型',width:'15%'},   
	        {field:'tel',title:'手机号',width:'15%'},    
	        {field:'address',title:'地址',width:'15%'},    
	        {field:'orderNum',title:'订单数',width:'10%'},
	        {field:'orderAmount',title:'订单总额',width:'10%'}
	    ]]    
	});
	$("#shopFansId").dialog('open');
}
//打开订单
function openOrder(shopId){
	$('#shopOrderId').dialog('open');
	$("#shopOrderTab").datagrid({
		url:"<%=basePath%>/order/CommentListData.do?shopId="+shopId
	});
}
//搜索
function orderSO()
{
	var ssuser = document.getElementById("ssuser").value;
	var ssOrderNum = document.getElementById("ssOrderNum").value;
	$('#shopOrderTab').datagrid('load', {ssuser:ssuser,ssOrderNum:ssOrderNum});
}
function openActivity(shopId){
	$("#contentDVI").html('');
	$("#shopActivityId").dialog('open');
	$.post("<%=basePath%>/shop/chaActivity.do",{shopId:shopId},function(result){
		data = eval("(" + result + ")");
		$("#contentDVI").html(data.content);
	});	
}

//打开比例设置
function openThreeBili(shopId){
	$("#shopId").val(shopId);
	$.post("<%=basePath%>/shop/chaShopBili.do",{shopId:shopId},function(result){
		data = eval("(" + result + ")");
		$("#onebili").numberbox({value:data.oneBili});
		$("#twobili").numberbox({value:data.twoBili});
		$("#threebili").numberbox({value:data.threeBili});
		$("#shopbili").numberbox({value:data.shopBili});
		$("#platbili").html(data.platBili+"%");
	});	
	$("#addBili").dialog('open');
}
//保存 比例
function saveShopRatio(){
	var onebili = $('#onebili').val();
	var twobili = $('#twobili').val();
	var threebili = $('#threebili').val();
	var shopbili = $('#shopbili').val();
	
	var zongBili = onebili*100 + twobili*100 + threebili*100 + shopbili*100;
	var resultBi = zongBili/100;
	if(resultBi > 100){
		layer.msg("比例设置不正确");
		return ;
	}
	//平台比例
	var residue_paly = 10000 - zongBili;
	/* $("#result_playBili").val(residue_paly/100); */
	$("#result_playBili").numberbox({value:residue_paly/100});
	$('#formShopRatio').form('submit', {
		url : '<%=basePath%>/shop/saveShopBili.do',
		onSubmit : function() {
			var index = layer.load(1, {
				  shade: [0.1,'#fff'] //0.1透明度的白色背景
			})
			return $(this).form('validate');
		},
		success : function(result) {
			data = eval("(" + result + ")");
			if(data.code == 100)
			{
				layer.closeAll('loading');
				$('#addBili').dialog('close');
				$('#grid').datagrid('reload');
				loadRatio();
				residueBili();
				$.messager.show({
					title:'提示',
					msg:'<div class="msgs">保存成功</div>',
					timeout: 1000, 
					showType:'fade',
					style:{right:'', bottom:''}
				});
			}
			else
			{
				$.messager.show({
					title:'提示',
					msg:data.msg,
					timeout: 1000,  
					showType:'fade',
					style:{right:'', bottom:''}
				});
			}
		}
	});
	
}
</script>
</head>
<body class="easyui-layout">
<!--------------------------------------- 功能按钮 -------------------------------------------->
<div id="tb" style="height:auto;display: none;">   
	 <div style="width:110%; margin-bottom:3px;text-align: left;height:35px;"> 
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-ok" plain="true" onclick="openAbles()" >启用</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-lock" plain="true" onclick="openDisables()" >禁用</a>
	 </div> 

	<!------------------------------------ 搜索 ---------------------------------------->
	 <div style="text-align: list;background-color: #f4f4f4;width:100%;height:30px; border-top: 2px solid #D9D9D9;border-bottom: 0px solid #D9D9D9;">
		   <div style="float: left;height: 30px;line-height: 30px;"><label> &nbsp;&nbsp; 姓名: </label>
	   		 <input name="Name" id="Name" style="margin-top: 0px;border:1px #95B8E7 solid;width: 200px;height: 23px"/>
		   </div>
		   <div style="float: left;height: 30px;line-height: 30px;"><label> &nbsp;&nbsp; 手机号: </label>
	   		 <input name="tel" id="tel" class="easyui-numberbox"  style="margin-top: 0px;border:1px #95B8E7 solid;width: 200px;height: 23px"/>
		   </div>
		   <div style="float: left;height: 30px;line-height: 30px;"><label> &nbsp;&nbsp; 状态: </label>
	   		<select name="SsEnable" id="SsEnable" style="width:60px;height:22px;border:1px #95B8E7 solid;">
	   			<option  value="">全部</option>
		   		<option  value="1">启用</option>
		   		<option  value="0">禁用</option>
			 </select>    
	    </div> 
	     <div onclick="sousuo()" style=" float: left;border:1px solid #000;margin-left: 25px;cursor:pointer;text-align:center;line-height:24px;background-color: #FFF;color:#000;width: 100px;height:23px;margin-top: 3px;border-radius: 5px;"
	     class="easyui-linkbutton" iconCls="icon-search">搜索</div>	   
</div>  
</div> 
		<table id="grid" class="easyui-datagrid" style="width:100%;height:100%;display: none;" title="<img alt='后待核审商家' style='width:17px;height:17px;' src='<%=basePath%>/img/titleImg.png'><div style='height:20px;line-height:22px;margin-left:25px;margin-top:-20;'>商家用户管理</div>"
			url="<%=basePath%>/shop/shopListDatas.do" toolbar="#tb" data-options="nowrap: false" 
			rownumbers="true" pagination="true" striped="true" singleSelect="false" loadMsg="正在努力为您加载数据">
			<thead>
				<tr>
					<th field="ck" checkbox="true" id="ck" name="ck"></th>
					<th field="shopName" width="6%" align="center">商家名称</th>
					<th field="mobile" width="8%" align="center">手机号</th>
					<th field="idCard" width="10%" align="center">身份证号</th>
					<th field="typeName" width="5%" align="center">服务类型</th>
					<th field="lngLat" width="8%" align="center">经纬度</th>
					<th field="address" width="12%" align="center">地址</th>
					<th field="topTime" width="8%" align="center">头条时间</th>
					<th field="commentNum" width="4%" align="center">评价次数</th>
					<th field="commScore" width="4%" align="center">评价分数</th>
					<th field="createDate" width="8%" align="center">创建日期</th>
					<th field="enable" width="3%" align="center" formatter="formatEnable">状态</th>
					<th field="oper" width="21%" align="center" formatter="formatOper" >操作</th>
				</tr>
			</thead>
		</table>
<!--------------------------------- 查看图片 --------------------------->
<div id="adddlg" class="easyui-dialog" style="width: 600px; height: 400px; padding: 10px 20px;display: none;" modal="true" closed="true"   >
	<form id="formAddRole" name="formAddRole" method="post">
	 	<div style="float:left;width:45%;height:21px;margin-top: 20px;">
			<div style="width: 130px;height: 140px;float: left;">
				<label style="float:left;display:inline-block;width:84px;color:#000;font-size:15px;margin-top: 70px;">身份证:</label>
				<div style="width:100%;height:120px;floay:left;margin-left: 85px;margin-top: 10px;">
					<img id="icon" alt="" src="<%=basePath%>/Public/images/null.png" style="width:100%;height:100%;">
				</div>
			</div>
		</div>
	 	<div style="float:left;width:45%;height:21px;margin-top:20px;">
			<div style="width: 130px;height: 140px;float: left;">
				<label style="float:left;display:inline-block;width:84px;color:#000;font-size:15px;margin-top: 70px;">营业执照:</label>
				<div style="width:100%;height:120px;floay:left;margin-left: 85px;margin-top: 10px;">
					<img id="img" alt="" src="<%=basePath%>/Public/images/null.png" style="width:100%;height:100%;">
				</div>
			</div>
		</div>
		<div style="float:left;width:100%;height:21px;margin-top: 130px;">
			<div style="width: 130px;height: 140px;float: left;">
				<label style="float:left;display:inline-block;width:84px;color:#000;font-size:15px;margin-top: 70px;">背景图:</label>
				<div style="width:370px;height:150px;floay:left;margin-left: 85px;margin-top: 10px;">
					<img id="backImg" alt="" src="<%=basePath%>/Public/images/null.png" style="width:100%;height:100%;">
				</div>
			</div>
		</div>
	 </form>
</div>

<!-- 定位 -->
<div id="locationId" class="easyui-dialog" title="商家定位" modal="true" style="width: 350px; height: 200px; padding: 10px 20px;display: none;background-color: #FFF" buttons="#location-button" closed="true" >
	<form id="formLocation">
		<div id="locationDVI">
			<span>经度</span><input id="lngId" name="lngId" readOnly="true"/>
			<span>纬度</span><input id="latId" name="latId" readOnly="true"/>
		</div>
		<div id="addressDVI">
			<span>地址</span><input id="addresId" name="addresId" readOnly="true"/>
		</div>
		<input type="hidden" id="shopIdINP" name="shopIdINP"/>
	</form>
</div>

<div id="location-button" style="height: 30px;display: none">
	<div style="width:140px;height:35px;float: right;">
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="ding()">定位</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="saveLocation()">保存</a>
	</div>
</div>

	<!-- ---------------------- 粉丝查看 ----------------------- -->
	<div id="shopFansId" class="easyui-dialog" title="商户粉丝详情" style="top: 80px;left: 100px" modal="true" closed="true" >
		<table id="shopFansDVI" style="width:1200px;height:700px"></table>
	</div>
	
	<!-- ---------------------- 商家收款 二维码  ----------------------- -->
	<div id="shopQrId" class="easyui-dialog" title="商户二维码"  modal="true" closed="true" >
		<div style="padding: 15px">
			<div id="shopQrDVI" style="width:400px;height:400px"></div>
		</div>
	</div>
	
	<!-- ---------------------- 订单查询 ----------------------- -->
<div id="shopOrderSO" style="height:auto;display: none;">   
<!------------------------------------ 搜索 ---------------------------------------->
	 <div style="text-align: list;background-color: #f4f4f4;width:100%;height:30px; border-top: 2px solid #D9D9D9;border-bottom: 0px solid #D9D9D9;">
		    <div style="float: left;height: 30px;line-height: 30px;"><label> &nbsp;&nbsp; 评论用户: </label>
	   		<input name="ssuser" id="ssuser" style="margin-top: 0px;border:1px #95B8E7 solid;width: 150px;height: 23px"  />
		   </div>
		   <div style="float: left;height: 30px;line-height: 30px;"><label> &nbsp;&nbsp; 订单号: </label>
	   		<input name="ssOrderNum" id="ssOrderNum" style="margin-top: 0px;border:1px #95B8E7 solid;width: 150px;height: 23px"  />
		   </div>
		   <div onclick="orderSO()" style=" float: left;border:1px solid #000;margin-left: 25px;cursor:pointer;text-align:center;line-height:24px;background-color: #FFF;color:#000;width: 100px;height:23px;margin-top: 3px;border-radius: 5px;"
	    class="easyui-linkbutton" iconCls="icon-search">搜索	   
		</div>  
	</div>
</div>	
	<div id="shopOrderId" class="easyui-dialog" title="订单查看" style="top: 80px;left: 100px" modal="true" closed="true" >
		<table id="shopOrderTab" class="easyui-datagrid"  data-options="nowrap: false" style="width:1200px;height:700px;display: none;"
			 toolbar="#shopOrderSO" rownumbers="true" pagination="true" singleSelect="true" loadMsg="正在努力为您加载数据">
			<thead>
				<tr>
					<th field="userName" width="10%" align="center">评论用户</th>
					<th field="orderNum" width="13%" align="center" >评论订单</th>
					<th field="shopName" width="10%" align="center" >所属商家</th>
					<th field="content" width="15%" align="center">内容</th>
					<th field="star" width="10%" align="center">店铺环境</th>
					<th field="starTwo" width="10%" align="center">服务态度</th>
					<th field="starThree" width="10%" align="center">消费体验</th>
					<th field="createDate" width="10%" align="center">评论时间</th>
					<th field="oper" width="10%" align="center" >操作</th>
				</tr>
			</thead>
		</table>
	</div>

	<!-- 商家 活动  -->
	<div id="shopActivityId" class="easyui-dialog" title="商家活动" modal="true" closed="true" >
	    <div style="width: 500px;height: 300px;padding: 20px">
	    	<span id="contentDVI" style="font-size: 15px"></span>
	    </div>  
	</div>
	
		<!---------------------------------- 添加界面 ----------------------------->
		<div id="addBili" title="比例设置" class="easyui-dialog" style="width: 350px; height: 300px; padding: 10px 20px;display: none;" closed="true" modal="true"  buttons="#addBili-buttons" singleSelect="false" loadMsg="正在努力为您加载数据">
			<form id="formShopRatio" name="formShopRatio" method="post" >
				<div style="float:left;width:100%;height:21px;margin-top: 5px;margin-bottom: 10px">
					<!-- <span style="color: #D9534F;float: left;">当前可设置比例:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span style="display: block;" id="formaBili"></span> -->
					<span style="color: #D9534F;float: left;">平台比例:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span id="platbili"></span>
				</div>
				<div style="float:left;width:100%;height:21px;margin-top: 15px;">
		        	<div style="float: left;"><label style="display:inline-block;width:100px;color:#000;font-size:15px;" >商家比例:</label></div>
		            <div style="float: left;"><input class="easyui-numberbox" style="vertical-align:middle;font-size:15px;width:80px; color:#000;border:1px solid #95B8E7;padding-bottom: 5px;" type="text" value=""  id="shopbili" name="shopbili" precision="2" min="0"/></div>
		            <div style="float: left;"><label style="display:inline-block;width:50px;color:#000;font-size:15px;" >%</label></div>
			  	</div>
				<div style="float:left;width:100%;height:21px;margin-top: 15px;">
		        	<div style="float: left;"><label style="display:inline-block;width:100px;color:#000;font-size:15px;" >一级比例:</label></div>
		            <div style="float: left;"><input class="easyui-numberbox" style="vertical-align:middle;font-size:15px;width:80px; color:#000;border:1px solid #95B8E7;padding-bottom: 5px;" type="text" value=""  id="onebili" name="onebili" precision="2" min="0"/></div>
		            <div style="float: left;"><label style="display:inline-block;width:50px;color:#000;font-size:15px;" >%</label></div>
			  	</div>
			  	<div style="float:left;width:100%;height:21px;margin-top: 15px;">
		        	<div style="float: left;"><label style="display:inline-block;width:100px;color:#000;font-size:15px;" >二级比例:</label></div>
		            <div style="float: left;"><input class="easyui-numberbox" style="vertical-align:middle;font-size:15px;width:80px; color:#000;border:1px solid #95B8E7;padding-bottom: 5px;" type="text" value=""  id="twobili" name="twobili" precision="2" min="0"/></div>
		            <div style="float: left;"><label style="display:inline-block;width:50px;color:#000;font-size:15px;" >%</label></div>
			  	</div>
			  	<div style="float:left;width:100%;height:21px;margin-top: 15px;">
		        	<div style="float: left;"><label style="display:inline-block;width:100px;color:#000;font-size:15px;" >三级比例:</label></div>
		            <div style="float: left;"><input class="easyui-numberbox" style="vertical-align:middle;font-size:15px;width:80px; color:#000;border:1px solid #95B8E7;padding-bottom: 5px;" type="text" value=""  id="threebili" name="threebili" precision="2" min="0"/></div>
		            <div style="float: left;"><label style="display:inline-block;width:50px;color:#000;font-size:15px;" >%</label></div>
			  	</div>
			    <input type="hidden" name="shopId" id="shopId" value=""/>
			    <!-- <input type="hidden" name="result_playBili" id="result_playBili" value=""/> -->
			    <div style="display: none">
				    <input class="easyui-numberbox" type="hidden" id="result_playBili" name="result_playBili" precision="2" min="0"/>
			    </div>
			 </form>
		</div>
		
		<!-------------------------------------------- 确认取消按钮 -------------------------------------------------->
		<div id="addBili-buttons" style="height: 30px;display: none;">
			<div style="width:140px;height:35px;float: right;">
				<a href="javascript:void(0)" class="easyui-linkbutton" onclick="saveShopRatio()" iconcls="icon-save">保存</a>
				<a href="javascript:void(0)" class="easyui-linkbutton" onclick="javascript:$('#addBili').dialog('close')"
				iconcls="icon-cancel">取消</a>	
			</div>
		</div>	
	

</body>
<script type="text/javascript">
</script>
</html>
