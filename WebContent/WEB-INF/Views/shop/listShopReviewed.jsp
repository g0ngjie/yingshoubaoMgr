<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/commons/taglibs.jsp" %>
 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>  
<base href="<%=basePath%>">
<title>商家用户审核</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">    
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<link href="<%=basePath%>/Public/css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/icon.css">
<link href="<%=basePath%>/Public/uploadify/uploadify.css" type="text/css" rel="stylesheet" />
<!--装载文件-->
<script src="<%=basePath%>/Public/js/jquery-1.7.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/Public/js/jquery.easyui.min.js"></script>

<script type="text/javascript" src="<%=basePath%>/Public/uploadify/jquery.uploadify-3.1.js"></script>
<script type="text/javascript" src="<%=basePath%>/Public/uploadify/swfobject.js"></script>
<link href="<%=basePath%>/Public/css/shopUser.css" rel="stylesheet" type="text/css" />
<script src="<%=basePath%>/Public/layer/layer.js"></script>
<script src="<%=basePath%>/Public/js/pro_common.js" type="text/javascript"></script>
<style type="text/css">
	.datagrid-btable tr{
		height: 50px;
	}
</style>
<script>
var id = "";
var pan = 0;
$(document).ready(function(){
//回车点击事件--搜索功能
	$(document).keydown(function(event){
		if(event.keyCode==13){
			//$("#login").click();
			sousuo();
		}
	}) ;
var IsCheckFlag = true;
$("#grid").datagrid({
 	 onClickCell: function (rowIndex, field, value) {
         IsCheckFlag = false;
     },
 	 onSelect: function (rowIndex, rowData) {
         if (!IsCheckFlag) {
             IsCheckFlag = true;
             $("#grid").datagrid("unselectRow", rowIndex);
         }
     },    
 	 onUnselect: function (rowIndex, rowData) {
         if (!IsCheckFlag) {
             IsCheckFlag = true;
             $("#grid").datagrid("selectRow", rowIndex);
         }
     }
})

});
/* 显示启用按钮 */
function formatEnable(value,row,index){
	if(value == '2'){
		pan = 1;
		return "<a style='color:#FF0000'>已拒绝</a>";
		}
	pan = 0;
	return "<a style='color:#008000'>未审核</a>";
}
/* 操作 */
function formatOper(value,row,index){
	if(pan==0)
		return "<a href='javascript:openShenhe(\"" + row['shopId'] + "\");'>审核</a>"
		/* + " | <a href='javascript:openDel(\"" + row['shopId'] + "\");'>删除</a>";
	return "<a href='javascript:openDel(\"" + row['shopId'] + "\");'>删除</a>"; */
} 

/* 审核商家 */
function openShenhe(shopId)
{	
	id = shopId;
	$.post("<%=basePath%>/shop/Modif.do",{shopId:shopId},function(result){
		data = eval("(" + result + ")");
		$('#shopId').val(shopId);
		var bei1 = data.img;
		if(bei1 == null || bei1.trim() == "")
			$('#img').attr("src", '<%=basePath2%>img/null.png');
		else{
			$('#img').attr("src", '<%=basePath3%>/RevenueTreasure/Public/licenseImg/'+bei1);
			$("#licenseImg").attr("href", '<%=basePath3%>/RevenueTreasure/Public/licenseImg/'+bei1);
			}
		var bei2 = data.idCardImg;
		if(bei2 == null || bei2.trim() == "")
			$('#icon').attr("src", '<%=basePath3%>/RevenueTreasure/Public/headImg/null.png');
		else{
			$('#icon').attr("src", '<%=basePath3%>/RevenueTreasure/Public/licenseImg/'+bei2);
			$('#idCard_a').attr("href", '<%=basePath3%>/RevenueTreasure/Public/licenseImg/'+bei2);
			}
		var bei3 = data.backImg;
		if(bei3 == null || bei3.trim() == "")
			$('#backImg').attr("src", '<%=basePath2%>img/null.png');
		else{
			$('#backImg').attr("src", '<%=basePath3%>/RevenueTreasure/Public/shopBackImg/'+bei3);
			}
		$('#adddlg').panel({title: "审核"});
		$('#adddlg').dialog('open');
	}); 
}

/* 多删除按钮 */
function openDels()
{	
	var select = $('#grid').datagrid('getSelected');
	if(select){
	if(window.confirm('你确定要删除吗？')){
		 var index = layer.load(1, {
	  shade: [0.1,'#fff'] //0.1透明度的白色背景
	});
	 var str = "";
	var rows  = $('#grid').datagrid("getRows");
	var ckArr = $("input[name='ck']");
	for(var i=0; i<ckArr.length; i++)
	{
		if(ckArr[i].checked)
		{
			if(str != "")
			{
				str += ",";
			}
			str += rows[i].shopId;
		}
	}
	
	$.post("<%=basePath%>/shop/delShop.do",{delKeys:str},function(result){
		layer.closeAll('loading');
		data = eval("(" + result + ")");
		//alert(result)
		if(data.code == 100){
			$('#grid').datagrid('reload');
			$.messager.show({
							title:'提示',
							msg:'<div class="msgs">删除成功</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
		}
	}); 
	}
	}else{
		$.messager.show({
							title:'提示',
							msg:'<div class="msgs">请选择您要删除的数据</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
	}
	
}

/* 单删除 */
function openDel(shopId)
{	
if(window.confirm('你确定要删除吗？')){
	$.post("<%=basePath%>/shop/delShop.do",{delKeys:shopId},function(result){
		layer.closeAll('loading');
		data = eval("(" + result + ")");
		//alert(result)
		if(data.code == 100){
			$('#grid').datagrid('reload');
			$.messager.show({
							title:'提示',
							msg:'<div class="msgs">删除成功</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
		}
	}); 
	}
}


/* 通过 */
function passShop()
{	
if(window.confirm('你确定要通过吗？')){
	$.post("<%=basePath%>/shop/passShop.do",{delKeys:id},function(result){
		layer.closeAll('loading');
		data = eval("(" + result + ")");
		//alert(result)
		if(data.code == 100){
			$.post("<%=basePath%>/send/sengManager.do",{userId:data.userId,mode:"3",state:"您申请的商铺已经通过申请啦！赶快去装饰一下吧！",shopId:data.shopId},function(result){
				layer.closeAll('loading');
			});
			$('#grid').datagrid('reload');
			$.messager.show({
							title:'提示',
							msg:'<div class="msgs">已通过</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
		}
	});
	$('#adddlg').dialog('close') 
	}
}

/* 拒绝 */
function refuseShop()
{	
if(window.confirm('你确定要拒绝吗？')){
	var reason = $("#reasonId").val();
	$.post("<%=basePath%>/shop/refuseShop.do",{delKeys:id,reason:reason},function(result){
		layer.closeAll('loading');
		data = eval("(" + result + ")");
		//alert(result)
		if(data.code == 100){
			$.post("<%=basePath%>/send/sengManager.do",{userId:data.userId,mode:"4",state:"很抱歉的通知您，您的开店申请没有通过。",reason:reason},function(result){
				layer.closeAll('loading');
			});
			$('#grid').datagrid('reload');
			$.messager.show({
							title:'提示',
							msg:'<div class="msgs">已拒绝</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
		}
	}); 
	$('#adddlg').dialog('close')
	$("#reject_dialog").dialog('close');
	}
}

/* 点击拒绝 显示 输入框 */
function rejectShop(){
	$("#reject_dialog").dialog('open');
	$("#adddlg").dialog('close');
}

/*显示图片*/
function xianshiImg(value,row,index){ 
	var lu = row.img;
	if(lu == null || lu.trim() == "")
		return '<img src="<%=basePath2%>img/null.png"  style="width:160px;height:100px;"/>';
	else
		return '<img src="<%=basePath2%>'+'img/'+lu+'"  style="width:160px;height:100px;"/>';
}
/*显示图片*/
function xianshiIcon(value,row,index){ 
	var lu = row.icon;
	if(lu == null || lu.trim() == "")
		return '<img src="<%=basePath2%>img/null.png"  style="width:100px;height:100px;"/>';
	else
		return '<img src="<%=basePath2%>'+'img/'+lu+'"  style="width:100px;height:100px;"/>';
}

//搜索
function sousuo()
{
	var Name = $('#Name').val();
	var tel = $('#tel').numberbox('getValue');
	var SsEnable = $('#SsEnable').val();
	$('#grid').datagrid('load', {Name:Name,tel:tel,SsEnable:SsEnable});
}


	

</script>
</head>
<body class="easyui-layout">
<!--------------------------------------- 功能按钮 -------------------------------------------->
<div id="tb" style="height:auto;display: none;">   
<!------------------------------------ 搜索 ---------------------------------------->
	 <div style="text-align: list;background-color: #f4f4f4;width:100%;height:30px; border-top: 2px solid #D9D9D9;border-bottom: 0px solid #D9D9D9;">
		   <div style="float: left;height: 30px;line-height: 30px;"><label> &nbsp;&nbsp; 姓名: </label>
	   		<input name="Name" id="Name" style="margin-top: 0px;border:1px #95B8E7 solid;width: 200px;height: 23px"/>
		   </div>
		   <div style="float: left;height: 30px;line-height: 30px;"><label> &nbsp;&nbsp; 手机号: </label>
	   		<input name="tel" id="tel" class="easyui-numberbox"  style="margin-top: 0px;border:1px #95B8E7 solid;width: 200px;height: 23px"/>
		   </div>
		   <div style="float: left;height: 30px;line-height: 30px;"><label> &nbsp;&nbsp; 状态: </label>
	   		<select name="SsEnable" id="SsEnable" style="width:80px;height:22px;border:1px #95B8E7 solid;">
	   			<option  value="">全部</option>
		   		<option  value="0">未审核</option>
		   		<option  value="2">已拒绝</option>
			 </select>    
	    </div> 
	     <div onclick="sousuo()" style=" float: left;border:1px solid #000;margin-left: 25px;cursor:pointer;text-align:center;line-height:24px;background-color: #FFF;color:#000;width: 100px;height:23px;margin-top: 3px;border-radius: 5px;"
	     class="easyui-linkbutton" iconCls="icon-search">搜索</div>	   
		</div>  
</div>  
		<table id="grid" class="easyui-datagrid" style="width:100%;height:100%;display: none;" title="<img alt='后待核审商家' style='width:17px;height:17px;' src='<%=basePath%>/img/titleImg.png'><div style='height:20px;line-height:22px;margin-left:25px;margin-top:-20;'>商家用户管理</div>"
			url="<%=basePath%>/shop/shopListData.do" toolbar="#tb" data-options="nowrap: false" 
			rownumbers="true" pagination="true" striped="true" singleSelect="false" loadMsg="正在努力为您加载数据">
			<thead>
				<tr>
					<th field="shopName" width="10%" align="center">商家名称</th>
					<th field="linkMan" width="10%" align="center">联系人</th>
					<th field="mobile" width="10%" align="center">联系电话</th>
					<th field="idCard" width="15%" align="center">身份证号</th>
					<th field="typeName" width="6%" align="center">服务类型</th>
					<th field="address" width="20%" align="center">地址</th>
					<th field="createDate" width="10%" align="center">创建日期</th>
					<th field="type" width="6%" align="center" formatter="formatEnable">状态</th>
					<th field="oper" width="12%" align="center" formatter="formatOper" >操作</th>
				</tr>
			</thead>
		</table>
<!--------------------------------- 添加界面 --------------------------->
<div id="adddlg" class="easyui-dialog" style="width: 600px; height: 450px; padding: 10px 20px;display: none;" closed="true"  buttons="#adddlg-buttons" >
	<form id="formAddRole" name="formAddRole" method="post">
	 	<div style="float:left;width:45%;height:21px;margin-top: 20px;">
               
			<div style="width: 130px;height: 140px;float: left;">
				<label style="float:left;display:inline-block;width:84px;color:#000;font-size:15px;margin-top: 70px;">身份证:</label>
				<div style="width:100%;height:120px;floay:left;margin-left: 85px;margin-top: 10px;">
					<a id="idCard_a" target="view_window"><img id="icon" alt="" src="<%=basePath%>/Public/images/null.png" style="width:100%;height:100%;"></a>
				</div>
			</div>
		</div>
	 	<div style="float:left;width:45%;height:21px;margin-top:20px;">
               
			<div style="width: 130px;height: 140px;float: left;">
				<label style="float:left;display:inline-block;width:84px;color:#000;font-size:15px;margin-top: 70px;">营业执照:</label>
				<div style="width:100%;height:120px;floay:left;margin-left: 85px;margin-top: 10px;">
					<a id="licenseImg" target="view_window"><img id="img" alt="" src="<%=basePath%>/Public/images/null.png" style="width:100%;height:100%;"></a>
				</div>
			</div>
		</div>
		<div style="float:left;width:100%;height:21px;margin-top: 130px;">
               
			<div style="width: 130px;height: 140px;float: left;">
				<label style="float:left;display:inline-block;width:84px;color:#000;font-size:15px;margin-top: 70px;">背景图:</label>
				<div style="width:370px;height:150px;floay:left;margin-left: 85px;margin-top: 10px;">
					<img id="backImg" alt="" src="<%=basePath%>/Public/images/null.png" style="width:100%;height:100%;">
				</div>
			</div>
		</div>
		<input type="hidden" name="shopId" id="shopId" value=""/>
	 </form>
</div>
<!-- ----------------------------拒绝原因------------------------- -->
<div id="reject_dialog" class="easyui-dialog" title="拒绝原因" style="width:500px;height:300px;" closed="true" buttons="#adlg_btn" data-options="resizable:true,modal:true">   
    <textarea id="reasonId" style="width: 100%;height: 100%;padding: 10px"></textarea>    
</div> 

<div id="adddlg-buttons" style="height: 30px;display: none">
	<div style="width:140px;height:35px;float: right;">
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="passShop()" iconcls="icon-save">通过</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="rejectShop()" iconcls="icon-cancel">拒绝</a>
	</div>
</div> 

<div id="adlg_btn" style="height: 30px;display: none">
	<div style="width:140px;height:35px;float: right;">
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="refuseShop()" iconcls="icon-save">提交</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="javascript:$('#reject_dialog').dialog('close')"
		iconcls="icon-cancel">取消</a>
	</div>
</div> 


</body>
  <style>
.uploadify-queue{ display: none;}
</style>

</html>
