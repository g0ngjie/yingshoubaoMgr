<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/commons/taglibs.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>服务类型</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">    
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<link href="<%=basePath%>/Public/css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/icon.css">
<link href="<%=basePath%>/Public/uploadify/uploadify.css" type="text/css" rel="stylesheet" />
<!--装载文件-->

<script src="<%=basePath%>/Public/js/jquery-1.7.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/Public/js/jquery.easyui.min.js"></script>

<script type="text/javascript" src="<%=basePath%>/Public/uploadify/jquery.uploadify-3.1.js"></script>
<script type="text/javascript" src="<%=basePath%>/Public/uploadify/swfobject.js"></script>

<script src="<%=basePath%>/Public/js/shijian.js" type="text/javascript"></script>
<script src="<%=basePath%>/Public/layer/layer.js"></script>
<style type="text/css">
	.datagrid-btable tr{
		height: 50px;
	}
</style>
<script>
$(document).ready(function(){
	var IsCheckFlag = true;
	$("#grid").datagrid({
	 	 onClickCell: function (rowIndex, field, value) {
	         IsCheckFlag = false;
	     },
	 	 onSelect: function (rowIndex, rowData) {
	         if (!IsCheckFlag) {
	             IsCheckFlag = true;
	             $("#grid").datagrid("unselectRow", rowIndex);
	         }
	     },    
	 	 onUnselect: function (rowIndex, rowData) {
	         if (!IsCheckFlag) {
	             IsCheckFlag = true;
	             $("#grid").datagrid("selectRow", rowIndex);
	         }
	     }
	})
});


/* 显示启用按钮 */
function formatEnabler(value,row,index){
	if(value == '0')
		return "<a style='color:#FF0000'>禁用</a>";
	return "<a style='color:#008000'>启用</a>";
}

/* 操作 */

function formatOper(value,row,index){
	var qrBackImgId = row.qrBackImgId;
	return "<a href='javascript:openchakan(\"" + qrBackImgId + "\");'>编辑</a>"
	+ " | <a href='javascript:openLook(\"" + qrBackImgId + "\");'>查看</a>"
	+ " | <a href='javascript:openshanchudan(\"" + qrBackImgId + "\");'>删除</a>";
} 
/* 添加 保存 角色 */
function saveAddRole()
{	
	var imgs = $("#imgCns").val();
	if(imgs==""||imgs==null){
		layer.msg("请上传图片")
		return 0;
	}
	$('#formAddRole').form('submit', {
		url : '<%=basePath%>/fans/upQrBackImg.do',
		onSubmit : function() {
			var index = layer.load(1, {
				  shade: [0.1,'#fff'] //0.1透明度的白色背景
			})
			return $(this).form('validate');
		},
		success : function(result) {
			data = eval("(" + result + ")");
			if(data.code == 100)
			{
				layer.closeAll('loading');
				$('#adddlg').dialog('close');
				$('#grid').datagrid('reload');
				$.serviceTyper.show({
					title:'提示',
					msg:'<div class="msgs">保存成功</div>',
					timeout: 1000, 
					showType:'fade',
					style:{right:'', bottom:''}
				});
			}
			else
			{
				layer.closeAll('loading');
				$('#adddlg').dialog('close');
				$.messager.show({
								title:'提示',
								msg:'<div class="msgs">'+ data.msg +'</div>',
								timeout: 1000, 
								showType:'fade',
								style:{right:'', bottom:''}
				});
			}
		}
	}); 
	
}


/* 添加按钮 */
function add()
{	
    $('#imgCn').attr("src", '<%=basePath%>Public/images/null.png');
    $('#mode').val("add");	
    $('#adddlg').panel({title: "添加"});
	$('#adddlg').dialog('open');
}
/* 编辑 */
function openchakan(qrBackImgId){
	$.post("<%=basePath%>/fans/qrBianji.do",{qrBackImgId:qrBackImgId},function(result){
		data = eval("(" + result + ")");
		$('#imgCns').val(data.backImg);
		var img0 = data.backImg;
		if(img0 == null || img0.trim() == "")
			$('#imgCn').attr("src", '<%=basePath2%>img/null.png');
		else{
			$('#imgCn').attr("src", '<%=basePath2%>'+'img/'+img0);
			$('#imgCns').attr("src", '<%=basePath2%>'+'img/'+img0);
			}
		$('#qrBackImgId').val(data.qrBackImgId);
		 if(data.enable ==0)	{ 
				var province_2 = 0;
				$("#enable   option[value='"+province_2+"']").attr("selected",true);
				}
			else{
				var province_1 = 1;
				$("#enable   option[value='"+province_1+"']").attr("selected",true);
				}
		$('#mode').val("edit");	
		$('#adddlg').panel({title: "编辑"});
		//$('#name').combobox('setValue', data.name);
		$('#adddlg').dialog('open');
	}); 
}

/* 查看图片 */
function openLook(qrBackImgId)
{	
	$.post("<%=basePath%>/fans/openLook.do",{qrBackImgId:qrBackImgId},function(result){
		data = eval("(" + result + ")");
		var bei3 = data.backImg;
		if(bei3 == null || bei3.trim() == "")
			$('#backImg').attr("src", '<%=basePath2%>img/null.png');
		else{
			$('#backImg').attr("src", '<%=basePath3%>/FileManagerService/img/'+bei3);
			}
		$('#adddlg_img').panel({title: "查看图片"});
		$('#adddlg_img').dialog('open');
	}); 
}

/* 多启用按钮 */
function openAbles()
{	
	var select = $('#grid').datagrid('getSelected');
	if(select){
		 var index = layer.load(1, {
	  shade: [0.1,'#fff'] //0.1透明度的白色背景
	});
	 var str = "";
	var rows  = $('#grid').datagrid("getRows");
	var ckArr = $("input[name='ck']");
	for(var i=0; i<ckArr.length; i++)
	{
		if(ckArr[i].checked)
		{
			if(str != "")
			{
				str += ",";
			}
			str += rows[i].qrBackImgId;
		}
	}
	
	$.post("<%=basePath%>/fans/shopAbles.do",{delKeys:str},function(result){
		layer.closeAll('loading');
		data = eval("(" + result + ")");
		//alert(result)
		if(data.code == 100){
			$('#grid').datagrid('reload');
			$.messager.show({
							title:'提示',
							msg:'<div class="msgs">已启用</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
		}
	}); 
	}else{
		$.messager.show({
							title:'提示',
							msg:'<div class="msgs">请选择你要启用的数据</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
	}
	
}

/* 多禁用按钮 */
function openDisables()
{	
	var select = $('#grid').datagrid('getSelected');
	if(select){
		 var index = layer.load(1, {
	  shade: [0.1,'#fff'] //0.1透明度的白色背景
	});
	 var str = "";
	var rows  = $('#grid').datagrid("getRows");
	var ckArr = $("input[name='ck']");
	for(var i=0; i<ckArr.length; i++)
	{
		if(ckArr[i].checked)
		{
			if(str != "")
			{
				str += ",";
			}
			str += rows[i].qrBackImgId;
		}
	}
	
	$.post("<%=basePath%>/fans/disablesShops.do",{delKeys:str},function(result){
		layer.closeAll('loading');
		data = eval("(" + result + ")");
		//alert(result)
		if(data.code == 100){
			$('#grid').datagrid('reload');
			$.messager.show({
							title:'提示',
							msg:'<div class="msgs">已禁用</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
		}
	}); 
	}else{
		$.messager.show({
							title:'提示',
							msg:'<div class="msgs">请选择您要禁用的数据</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
	}
	
}

/* 删除按钮 */
function openshanchu()
{	
	var select = $('#grid').datagrid('getSelected');
	if(select){
		if(window.confirm('你确定要删除吗？')){
		 var index = layer.load(1, {
	  shade: [0.1,'#fff'] //0.1透明度的白色背景
	});
	 var str = "";
	var rows  = $('#grid').datagrid("getRows");
	var ckArr = $("input[name='ck']");
	for(var i=0; i<ckArr.length; i++)
	{
		if(ckArr[i].checked)
		{
			if(str != "")
			{
				str += ",";
			}
			str += rows[i].qrBackImgId;
		}
	}
	
	$.post("<%=basePath%>/shopType/shachuMessage.do",{delKeys:str},function(result){
		layer.closeAll('loading');
		data = eval("(" + result + ")");
		if(data.code == 100){
			$('#grid').datagrid('reload');
			$.serviceTyper.show({
							title:'提示',
							msg:'<div class="msgs">删除成功</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
		}else{
			$.serviceTyper.show({
							title:'提示',
							msg:'<div class="msgs">'+ data.msg +'</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
		}
	}); 
}
	}else{
		$.serviceTyper.show({
							title:'提示',
							msg:'<div class="msgs">请选择您要删除的数据</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
	}
	
}
//单删
function openshanchudan(qrBackImgId)
{	
	if(window.confirm('你确定要删除吗？')){
	$.post("<%=basePath%>/fans/qrDel.do",{qrBackImgId:qrBackImgId},function(result){
		layer.closeAll('loading');
		data = eval("(" + result + ")");
		if(data.code == 100){
			$('#grid').datagrid('reload');
			$.serviceTyper.show({
							title:'提示',
							msg:'<div class="msgs">删除成功</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
		}else{
			$.messager.show({
							title:'提示',
							msg:'<div class="msgs">'+ data.msg + '</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
		}
	}); 
	}
}
/*显示图片*/
function xianshiIcon(value,row,index){ 
	var lu = row.backImg;
	if(lu == null || lu.trim() == "")
		return '<img src="<%=basePath2%>img/null.png"  style="width:50px;height:50px;"/>';
	else
		return '<img src="<%=basePath2%>img/'+lu+'"  style="width:50px;height:50px;"/>';
}

</script>
</head>
<body class="easyui-layout">
<!----------------------- 功能按钮 ----------------------->
<div id="tb" style="height:auto;display: none;">   
	<div style="width:110%; margin-bottom:3px;text-align: left;height:35px;" > 
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="add()" >添加</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-ok" plain="true" onclick="openAbles()" >启用</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-lock" plain="true" onclick="openDisables()" >禁用</a>
	        <!-- <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="openshanchu()" >删除</a> -->
	 </div> 
</div>
		<table id="grid" class="easyui-datagrid" style="width:100%;height:100%;display: none;" title="<img alt='推送消息管理' style='width:17px;height:17px;' src='<%=basePath%>/img/titleImg.png'><div style='height:20px;line-height:22px;margin-left:25px;margin-top:-20;'>服务类型管理</div>"
			url="<%=basePath%>/fans/selectQrBackImg.do" toolbar="#tb"
			rownumbers="true" pagination="true" singleSelect="false" loadMsg="正在努力为您加载数据">
			<thead>
				<tr>
					<th field="ck" checkbox="true" id="ck" name="ck"></th>
					<th field="img" width="48%" align="center"  formatter="xianshiIcon">图片</th>
					<th field="enable" width="25%" align="center" formatter="formatEnabler">状态</th>
					<th field="oper" width="24%" align="center" formatter="formatOper" >操作</th>

				</tr>
			</thead>
		</table>
<!---------------------------------- 添加界面 ----------------------------->
<div id="adddlg" class="easyui-dialog" style="width: 400px; height:400px; padding: 10px 20px;display: none;" closed="true"  buttons="#adddlg-buttons" singleSelect="false" loadMsg="正在努力为您加载数据">
	<form id="formAddRole" name="formAddRole" method="post" >
	
            <div class="fitem" style="width: 130px;height:140px;">
                
				<div style="width: 130px;height: 140px;float: left;">
					<label style="float:left;display:inline-block;width:84px;color:#000;font-size:15px;margin-top: 70px;">图片上传:&nbsp;&nbsp;&nbsp;&nbsp;</label>
					<div style="width:100%;height:220px;floay:left;margin-left: 85px;margin-top: 10px;">
						<img id="imgCn" alt="" src="<%=basePath%>/Public/images/null.png" style="width:214px;height:100%;">
					</div>
					<div style="width:100%;height:20px;floay:left;margin-left: 85px;margin-top: 5px;">
						<input id="file_upload_cn" name="file_upload_cn" type="file" >
					</div>
				</div>
			</div>
			
		    <input type="hidden" name="mode" id="mode" value=""/>
		   <input type="hidden" name="qrBackImgId" id="qrBackImgId" value=""/>
		    <input type="hidden" name="imgCns" id="imgCns" value=""/>
		   
	 </form>
</div>

<!--------------------------------- 查看图片 --------------------------->
<div id="adddlg_img" class="easyui-dialog" style="width: 600px; height: 550px; padding: 10px 20px;display: none;" modal="true" closed="true"   >
	<div style="float:left;width:100%;height:21px;margin-top: 30px;">
		<div style="width: 130px;height: 140px;float: left;">
			<label style="float:left;display:inline-block;width:84px;color:#000;font-size:15px;margin-top: 20px;">背景图:</label>
			<div style="width:370px;height:400px;floay:left;margin-left: 85px;">
				<img id="backImg" alt="" src="<%=basePath%>/Public/images/null.png" style="width:100%;height:100%;">
			</div>
		</div>
	</div>
</div>
 
<!-------------------------------------------- 确认取消按钮 -------------------------------------------------->
<div id="adddlg-buttons" style="height: 30px;display: none;">
	<div style="width:140px;height:35px;float: right;">
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="saveAddRole()" iconcls="icon-save">保存</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="javascript:$('#adddlg').dialog('close')"
		iconcls="icon-cancel">取消</a>	
	</div>
</div> 
</body>
 

<style>
.uploadify-queue{ display: none;}
</style>
<script type="text/javascript">
    $('#file_upload_cn').uploadify({
        'swf'      : '<%=basePath%>/Public/uploadify/uploadify.swf',
        'uploader' :  '<%=basePath3%>/FileManagerService/fileupload/uploadImg.do', 
        'buttonText' : '上传图片',
        'height'   : 20,
        'width':211,
        'fileTypeExts': '*.gif; *.jpg; *.png',
        'multi': false,
        'onUploadStart':function(file){
        	layer.msg("<b id='tests'>上传进度 0%</b>",{time: -1,shade: [0.4, '#393D49']});
        },
        'onUploadProgress': function(file, bytesUploaded, bytesTotal, totalBytesUploaded, totalBytesTotal){
       		var process = Math.floor(totalBytesUploaded/totalBytesTotal*100);
			$('#tests').html("上传进度  " + process + "%");
        },
        'onUploadSuccess': function (file, data, response) { 
        	layer.closeAll();     
        	var resObj = eval("(" + data + ")"); 
        	if(resObj.code == 100)
        	{
        		var lin ="<%=basePath2%>"+resObj.filePath;
        		$('#imgCn').attr("src", lin);
        		var reg = /\\img/;
        		var filePath = resObj.filePath;
        		var result = filePath.replace(reg,'');
        		$('#imgCns').val(result);
        		//alert(resObj.fullImgUrl);
        		//alert(resObj.filePath);
        		//alert(resObj.fileSize);
        		//alert(resObj.extName);
        	}
        	else
        	{
        		//上传失败，失败原因
        		alert(resObj.msg);
        	}
        }     
        
    });
    
     </script>
</html>