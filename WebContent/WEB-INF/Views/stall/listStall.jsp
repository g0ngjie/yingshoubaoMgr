<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/commons/taglibs.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>头条价格</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">    
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<link href="<%=basePath%>/Public/css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/icon.css">
<link href="<%=basePath%>/Public/uploadify/uploadify.css" type="text/css" rel="stylesheet" />
<!--装载文件-->

<script src="<%=basePath%>/Public/js/jquery-1.7.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/Public/js/jquery.easyui.min.js"></script>

<script type="text/javascript" src="<%=basePath%>/Public/uploadify/jquery.uploadify-3.1.js"></script>
<script type="text/javascript" src="<%=basePath%>/Public/uploadify/swfobject.js"></script>

<script src="<%=basePath%>/Public/js/shijian.js" type="text/javascript"></script>
<script src="<%=basePath%>/Public/layer/layer.js"></script>
<style type="text/css">
	#datagrid-row-r2-2-0{
		height: 50px;
	}
</style>
<script>
$(document).ready(function(){
	var IsCheckFlag = true;
	$("#grid").datagrid({
	 	 onClickCell: function (rowIndex, field, value) {
	         IsCheckFlag = false;
	     },
	 	 onSelect: function (rowIndex, rowData) {
	         if (!IsCheckFlag) {
	             IsCheckFlag = true;
	             $("#grid").datagrid("unselectRow", rowIndex);
	         }
	     },    
	 	 onUnselect: function (rowIndex, rowData) {
	         if (!IsCheckFlag) {
	             IsCheckFlag = true;
	             $("#grid").datagrid("selectRow", rowIndex);
	         }
	     }
	})
});

/* 操作 */

function formatOper(value,row,index){
	var stallId = row.stallId;
	return "<a href='javascript:openEdit(\"" + stallId + "\");'>修改</a>";
} 
/* 添加 保存 角色 */
function saveAddRole()
{	
	var bili = $('#bili').val();
	if(bili==""||bili==null){
		layer.msg("距离不能为空")
		return 0;
	}
	$('#formAddRole').form('submit', {
		url : '<%=basePath%>/stall/saveDistance.do',
		onSubmit : function() {
			var index = layer.load(1, {
				  shade: [0.1,'#fff'] //0.1透明度的白色背景
			})
			return $(this).form('validate');
			
		},
		success : function(result) {
			data = eval("(" + result + ")");
			if(data.code == 100)
			{
				layer.closeAll('loading');
				$('#adddlg').dialog('close');
				$('#grid').datagrid('reload');
				$.messager.show({
					title:'提示',
					msg:'<div class="msgs">保存成功</div>',
					timeout: 1000, 
					showType:'fade',
					style:{right:'', bottom:''}
				});
			}
			else
			{
				$.messager.show({
					title:'提示',
					msg:data.msg,
					timeout: 1000,  
					showType:'fade',
					style:{right:'', bottom:''}
				});
			}
		}
	}); 
	
}


/* 修改 */
function openEdit(stallId)
{	
	$.post("<%=basePath%>/stall/juli.do",{stallId:stallId},function(result){
		data = eval("(" + result + ")");
		$('#distanceNum').numberbox('setValue',data.distanceNum);
		$('#stallId').val(data.stallId);
		$('#adddlg').panel({title: "修改价格"});
		$('#adddlg').dialog('open');
	}); 
}

</script>
</head>
<body class="easyui-layout">
		<table id="grid" class="easyui-datagrid" style="width:100%;height:100%;display: none;" title="<img alt='推送消息管理' style='width:17px;height:17px;' src='<%=basePath%>/img/titleImg.png'><div style='height:20px;line-height:22px;margin-left:25px;margin-top:-20;'>头条价格设置</div>"
			url="<%=basePath%>/stall/listQiangDistance.do" toolbar="#tb"
			rownumbers="true" pagination="true" singleSelect="false" loadMsg="正在努力为您加载数据">
			<thead>
				<tr>
					<th field="ck" checkbox="true" id="ck" name="ck"></th>
					<th field="unitPrice" width="60%" align="center">头条价格</th>
					<th field="oper" width="37%" align="center" formatter="formatOper">操作</th>
				</tr>
			</thead>
		</table>
<!---------------------------------- 添加界面 ----------------------------->
<div id="adddlg" class="easyui-dialog" style="width: 350px; height: 150px; padding: 10px 20px;display: none;" closed="true"  buttons="#adddlg-buttons" singleSelect="false" loadMsg="正在努力为您加载数据">
	<form id="formAddRole" name="formAddRole" method="post" >
		<div style="float:left;width:100%;height:21px;margin-top: 15px;">
        	<div style="float: left;"><label style="display:inline-block;width:100px;color:#000;font-size:15px;" >价格:</label></div>
            <div style="float: left;"><input class="easyui-numberbox" style="vertical-align:middle;font-size:15px;width:80px; color:#000;border:1px solid #95B8E7;padding-bottom: 5px;" type="text"  id="bili" name="bili" precision="2" min="0"/></div>
            <div style="float: left;"><label style="display:inline-block;width:50px;color:#000;font-size:15px;" >元/天</label></div>
		  	</div>
		    <input type="hidden" name="stallId" id="stallId" value=""/>
	 </form>
	
</div>


<!-------------------------------------------- 确认取消按钮 -------------------------------------------------->
<div id="adddlg-buttons" style="height: 30px;display: none;">
	<div style="width:140px;height:35px;float: right;">
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="saveAddRole()" iconcls="icon-save">保存</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="javascript:$('#adddlg').dialog('close')"
		iconcls="icon-cancel">取消</a>	
	</div>
</div> 
</body>


<style>
.uploadify-queue{ display: none;}
</style>
</html>