<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/commons/taglibs.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>后台用户管理</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">    
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<link href="<%=basePath%>/Public/css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="<%=basePath%>/Public/themes/icon.css">
<script src="<%=basePath%>/Public/js/jquery-1.7.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/Public/js/jquery.easyui.min.js"></script>
<script src="<%=basePath%>/Public/layer/layer.js"></script>
<style type="text/css">
	.datagrid-btable tr{
		height: 50px;
	}
</style>
<script>
var id = "";
var mima = "";
var bian = 0;
$(document).ready(function(){
	var IsCheckFlag = true;
	$("#grid").datagrid({
	 	 onClickCell: function (rowIndex, field, value) {
	         IsCheckFlag = false;
	     },
	 	 onSelect: function (rowIndex, rowData) {
	         if (!IsCheckFlag) {
	             IsCheckFlag = true;
	             $("#grid").datagrid("unselectRow", rowIndex);
	         }
	     },    
	 	 onUnselect: function (rowIndex, rowData) {
	         if (!IsCheckFlag) {
	             IsCheckFlag = true;
	             $("#grid").datagrid("selectRow", rowIndex);
	         }
	     }
	})
	//回车
	$(document).keydown(function(event){
		if(event.keyCode==13){
			//$("#login").click();
			sousuo();
		}
	}) ;
});

//显示启用禁用
function formatEnable(value,row,index){
	if(value == '0')
		return "<a style='color:#FF0000'>禁用</a>";
	return "<a style='color:#008000'>启用</a>";
}
/* 操作 */

function formatOper(value,row,index){
	var userId = row.userId;
	return "<a href='javascript:openchakan(\"" + userId + "\");'>查看</a>"
	+ " | <a href='javascript:openbianji(\"" + userId + "\");'>编辑</a>";
} 
/* 添加 保存 角色 */
function saveAddRole()
{	
	var managerName = $('#managerName').val();
	var loginName = $('#loginName').val();
	var loginPwd = $('#loginPwd').val();
	var doublePwd = $('#doublePwd').val();
	
	if(managerName == null || managerName == ""){
		layer.msg("请输入用户名称");
		return;
	}
	if(loginName == null || loginName == ""){
		layer.msg("请输入登陆账号");
		return;
	}
	if(loginPwd == null || loginPwd == ""){
		layer.msg("请输入登陆密码");
		return;
	}
	if(doublePwd == null || doublePwd == ""){
		layer.msg("请确认登陆密码");
		return;
	}
	if(doublePwd != loginPwd){
		layer.msg("两次密码不一致");
		return;
	}
		
	$('#formAddRole').form('submit', {
		url : '<%=basePath%>/user/saveUser.do',
		onSubmit : function() {
			return $(this).form('validate');
			 var index = layer.load(1, {
				  shade: [0.1,'#fff'] //0.1透明度的白色背景
			})
		},
		success : function(result) {
			data = eval("(" + result + ")");
			if(data.code == 100)
			{
				layer.closeAll('loading');
				$('#adddlg').dialog('close');
				$('#grid').datagrid('reload');
				$.messager.show({
					title:'提示',
					msg:'<div class="msgs">保存成功</div>',
					timeout: 1000, 
					showType:'fade',
					style:{right:'', bottom:''}
				});
			}
			else
			{
				$.messager.show({
					title:'提示',
					msg:data.msg,
					timeout: 1000,  
					showType:'fade',
					style:{right:'', bottom:''}
				});
			}
		}
	}); 
}

/* 编辑角色 */
function openbianji(userId){
	$.post("<%=basePath%>/user/bjUser.do",{userId:userId},function(result){
		data = eval("(" + result + ")");
		$("#editLogin").val('');
		$("#editName").val('');
		$("#userIdDv2").val('');
		$("#userIdDv2").val(userId);
		$("#editLogin").val(data.loginName);
		$("#editName").val(data.userName);
		$('#edit_dialog').panel({title: "编辑用户"});
		$('#edit_dialog').dialog('open');
	}); 
}

//保存编辑
function saveEdit(){

	$('#formAddRoleEdit').form('submit', {
	url : '<%=basePath%>/user/saveEditUser.do',
	onSubmit : function() {
		return $(this).form('validate');
		 var index = layer.load(1, {
			  shade: [0.1,'#fff'] //0.1透明度的白色背景
		})
	},
	success : function(result) {
		data = eval("(" + result + ")");
		if(data.code == 100)
		{
			layer.closeAll('loading');
			$('#edit_dialog').dialog('close');
			$('#grid').datagrid('reload');
			$.messager.show({
				title:'提示',
				msg:'<div class="msgs">保存成功</div>',
				timeout: 1000, 
				showType:'fade',
				style:{right:'', bottom:''}
			});
		}
		else
		{
			$.messager.show({
				title:'提示',
				msg:data.msg,
				timeout: 1000,  
				showType:'fade',
				style:{right:'', bottom:''}
			});
		}
	}
});
}

/* 查看角色 */
function openchakan(userId){
	$.post("<%=basePath%>/user/bjUser.do",{userId:userId},function(result){
		data = eval("(" + result + ")");
		$('#userName1').val('');
		$('#pwdInp').val('');
		$('#userName1').val(data.loginName);
		$('#pwdInp').val(data.loginPwd);
		$('#adddlg1').dialog('open');
		
	}); 
}

/* 添加按钮 */
function add()
{	

    $("#managerName").val('');
    $("#loginName").val('');
    $("#loginPwd").val('');
    $("#doublePwd").val('');
    
    $('#mode').val("add");	
     $('#adddlg').panel({title: "添加管理员"});
	$('#adddlg').dialog('open');
}
/* 启用按钮 */
function qiyong()
{	
	var select = $('#grid').datagrid('getSelected');
	if(select){
		 var index = layer.load(1, {
	  shade: [0.1,'#fff'] //0.1透明度的白色背景
	});
	 var str = "";
	var rows  = $('#grid').datagrid("getRows");
	var ckArr = $("input[name='ck']");
	for(var i=0; i<ckArr.length; i++)
	{
		if(ckArr[i].checked)
		{
			if(str != "")
			{
				str += ",";
			}
			str += rows[i].userId;
		}
	}
	
	$.post("<%=basePath%>/user/saveqyUser.do",{qyKeys:str},function(result){
		layer.closeAll('loading');
		data = eval("(" + result + ")");
		//alert(result)
		if(data.code == 100){
			$('#grid').datagrid('reload');
			$.messager.show({
							title:'提示',
							msg:'<div class="msgs">已启用</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
		}
	}); 
	}else{
		$.messager.show({
						title:'提示',
						msg:'<div class="msgs">请选择您要启用的数据</div>',
						timeout: 1000, 
						showType:'fade',
						style:{right:'', bottom:''}
			});
	}
	
}
/* 禁用按钮 */
function jinyong()
{	
	var select = $('#grid').datagrid('getSelected');
	if(select){
		 var index = layer.load(1, {
	  shade: [0.1,'#fff'] //0.1透明度的白色背景
	});
	 var str = "";
	var rows  = $('#grid').datagrid("getRows");
	var ckArr = $("input[name='ck']");
	for(var i=0; i<ckArr.length; i++)
	{
		if(ckArr[i].checked)
		{
			if(str != "")
			{
				str += ",";
			}
			str += rows[i].userId;
		}
	}
	
	$.post("<%=basePath%>/user/savejyUser.do",{jyKeys:str},function(result){
		layer.closeAll('loading');
		data = eval("(" + result + ")");
		//alert(result)
		if(data.code == 100){
			$('#grid').datagrid('reload');
			$.messager.show({
							title:'提示',
							msg:'<div class="msgs">已禁用</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
		}
	}); 
	}else{
		$.messager.show({
						title:'提示',
						msg:'<div class="msgs">请选择您要禁用的数据</div>',
						timeout: 1000, 
						showType:'fade',
						style:{right:'', bottom:''}
			});
	}
	
}
/* 删除按钮 */
function openshanchu()
{	
	var select = $('#grid').datagrid('getSelected');
	if(select){
	if(window.confirm('你确定要删除吗？')){
		var index = layer.load(1, {
	    shade: [0.1,'#fff'] //0.1透明度的白色背景
	});
	 var str = "";
	var rows  = $('#grid').datagrid("getRows");
	var ckArr = $("input[name='ck']");
	for(var i=0; i<ckArr.length; i++)
	{
		if(ckArr[i].checked)
		{
			if(str != "")
			{
				str += ",";
			}
			str += rows[i].userId;
		}
	}
	
	$.post("<%=basePath%>/user/delUser.do",{delKeys:str},function(result){
		layer.closeAll('loading');
		data = eval("(" + result + ")");
		if(data.code == 100){
			$('#grid').datagrid('reload');
			$.messager.show({
							title:'提示',
							msg:'<div class="msgs">删除成功</div>',
							timeout: 1000, 
							showType:'fade',
							style:{right:'', bottom:''}
			});
		}
	}); 
	}
	}else{
		$.messager.show({
						title:'提示',
						msg:'<div class="msgs">请选择您要删除的数据</div>',
						timeout: 1000, 
						showType:'fade',
						style:{right:'', bottom:''}
			});
	}
	
}
//搜索
function sousuo()
{
	var SsName = $('#SsName').val();
	var roleName = $('#roleName').val();
	$('#grid').datagrid('load', {SsName:SsName,roleName:roleName});
	
}


</script>
</head>
<body class="easyui-layout">
<!-------------------- 按钮 ------------------------->
<div id="tb" style="height:auto;display: none;">   
	<div style="width:110%; margin-bottom:3px;text-align: left;height:35px;" > 
	        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="add()" >添加</a>
	        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="openshanchu()" >删除</a>
	        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-ok" plain="true" onclick="qiyong()" >启用</a>
	        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-lock" plain="true" onclick="jinyong()" >禁用</a>
	 </div> 

	 <div style="text-align: list;background-color: #f4f4f4;width:100%;height:30px; border-top: 2px solid #D9D9D9;border-bottom: 0px solid #D9D9D9;">
		   <div style="float: left;height: 30px;line-height: 30px;"><label>&nbsp;&nbsp;用户名称: </label>
	   		<input name="SsName" id="SsName"  style="margin-top: 0px;border:1px #95B8E7 solid;width: 200px;height: 23px"/>
		   </div>
		  <!--  <div style="float: left;height: 30px;line-height: 30px;"><label>&nbsp;&nbsp;角色名称: </label>
	   		<input name="roleName" id="roleName"  style="margin-top: 0px;border:1px #95B8E7 solid;width: 200px;height: 23px"/>
		   </div> -->
	     <div onclick="sousuo()" style=" float: left;border:1px solid #000;margin-left: 25px;cursor:pointer;text-align:center;line-height:24px;background-color: #FFF;color:#000;width: 100px;height:23px;margin-top: 3px;border-radius: 5px;"
	     class="easyui-linkbutton" iconCls="icon-search">搜索</div>	   
	 </div>
</div>
		
		<table id="grid" class="easyui-datagrid" style="width:100%;height:100%;display: none;" title="<img alt='后台用户管理' style='width:17px;height:17px;' src='<%=basePath%>/img/titleImg.png'><div style='height:20px;line-height:22px;margin-left:25px;margin-top:-20;'>后台用户管理</div>"
			url="<%=basePath%>/user/userListData.do" toolbar="#tb"
			rownumbers="true" pagination="true" singleSelect="false" loadMsg="正在努力为您加载数据">
			<thead>
				<tr>
					<th field="ck" checkbox="true" id="ck" name="ck"></th>
					<th field="userName" width="33%" align="center">用户名</th>
					<th field="enable" width="33%" align="center" formatter="formatEnable">是否启用</th>
					<th field="oper" width="31%" align="center" formatter="formatOper" align="center">操作</th>
					
				</tr>
			</thead>
		</table>
<!----------------------- 添加界面对话框 ----------------------------->
<div id="adddlg" class="easyui-dialog" style="width: 360px; height: 300px; padding: 10px 20px;display: none;" closed="true" buttons="#adddlg-buttons"  >
	<form id="formAddRole" name="formAddRole" method="post">
	
			 <div style="float:left;width:95%;height:21px;margin-top: 20px;">
        		<label style="display:inline-block;width:80px;color:#000;font-size:15px;">用户名称:</label>
            	<input class="vilid" style="vertical-align:middle;font-size:15px;color:#000;margin-top: -6px;border:1px solid #95B8E7;width: 181px" wtype="text"  id="managerName" name="managerName" value="" />
            </div>
            <div style="float:left;width:95%;height:21px;margin-top: 20px;">
        		<label style="display:inline-block;width:80px;color:#000;font-size:15px;">登陆账号:</label>
            	<input class="vilid" style="vertical-align:middle;font-size:15px;color:#000;margin-top: -6px;border:1px solid #95B8E7;width: 181px" wtype="text"  id="loginName" name="loginName" value="" />
            </div>
       		
        <div style="float:left;width:95%;height:21px;margin-top: 25px;"id="mima">
        	<label style="display:inline-block;width:80px;color:#000;font-size:15px">登陆密码:</label>
            <input class="vilid"  type="password" style="vertical-align:middle;font-size:15px;color:#000;margin-top: -6px;border:1px solid #95B8E7;width: 181px" type="text"  id="loginPwd" name="loginPwd" /></div>
        <div style="float:left;width:95%; height:21px;margin-top: 25px;"id="mimaque">
        	<label style="display:inline-block;width:80px;color:#000;font-size:15px">确认密码:</label>
            <input class="vilid" type="password" style="vertical-align:middle;font-size:15px;color:#000;margin-top: -6px;border:1px solid #95B8E7;width: 181px"  type="text"  id="doublePwd" name="doublePwd"/></div>
		
		    <input type="hidden" name="mode" id="mode" value=""/>
		   <input type="hidden" name="userId" id="userId" value=""/>
		    <input type="hidden" name="roleId" id="roleId" value=""/>
	 </form>
</div>

<!----------------------- 修改界面 ----------------------------->
<div id="edit_dialog" class="easyui-dialog" style="width: 360px; height: 200px; padding: 10px 20px;display: none;" closed="true" buttons="#edit-buttons"  >
	<form id="formAddRoleEdit" name="formAddRoleEdit" method="post">
	
			 <div style="float:left;width:95%;height:21px;margin-top: 20px;">
        		<label style="display:inline-block;width:80px;color:#000;font-size:15px;">用户名称:</label>
            	<input class="vilid" style="vertical-align:middle;font-size:15px;color:#000;margin-top: -6px;border:1px solid #95B8E7;width: 181px" wtype="text"  id="editName" name="editName" value="" />
            </div>
            <div style="float:left;width:95%;height:21px;margin-top: 20px;">
        		<label style="display:inline-block;width:80px;color:#000;font-size:15px;">登陆账号:</label>
            	<input class="vilid" style="vertical-align:middle;font-size:15px;color:#000;margin-top: -6px;border:1px solid #95B8E7;width: 181px" wtype="text"  id="editLogin" name="editLogin" value="" />
            </div>
            <input type="hidden" id="userIdDv2" name="userIdDv2"/>
	 </form>
</div>

<!-------------------------------- 查看界面对话框 ---------------------------------->
<div id="adddlg1" class="easyui-dialog" style="width: 360px; height: 200px; padding: 10px 20px;display: none;" closed="true" title="查看用户"  >
	<form id="formAddRole" name="formAddRole" method="post">
		<div style="float:left;width:95%;height:21px;margin-top: 20px;">
        	<label style="display:inline-block;width:80px;color:#666;font-size:15px;">账号:</label>
            <input style="vertical-align:middle;font-size:15px;color:#666666;width: 181px;margin-top: -6px;border:1px solid #95B8E7" type="text"  id="userName1" name="userName1" disabled="disabled"/>
        </div>
       	<div style="float:left;width:95%;height:21px;margin-top: 20px;">
        	<label style="display:inline-block;width:80px;color:#666;font-size:15px">密码:</label>
			<input style="vertical-align:middle;font-size:15px;width: 181px;color:#666666;margin-top: -6px;border:1px solid #95B8E7" type="text"  id="pwdInp" name="pwdInp" disabled="disabled"/>
		</div>

	 </form>
</div>


<!---------------------------------- 确认取消按钮 --------------------------------------->
<div id="adddlg-buttons" style="height: 30px;display: none;">
	<div style="width:140px;height:35px;float: right;">
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="saveAddRole()" iconcls="icon-save">保存</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="javascript:$('#adddlg').dialog('close')"
		iconcls="icon-cancel">取消</a>	
	</div>
</div> 
<div id="edit-buttons" style="height: 30px;display: none;">
	<div style="width:140px;height:35px;float: right;">
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="saveEdit()" iconcls="icon-save">保存</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="javascript:$('#edit_dialog').dialog('close')"
		iconcls="icon-cancel">取消</a>	
	</div>
</div>
</body>
</html>
