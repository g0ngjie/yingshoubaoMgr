package com.cosin.utils;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtils {

	public static String DEF_FORMAT = "yyyy-MM-dd HH:mm:ss";
	
	/**
	 * 字符串转日期
	 * @param format
	 * @param str
	 * @return
	 */
	public static Date parseStrToDate(String format, String str)
	{
		SimpleDateFormat sdf =   new SimpleDateFormat(format);
		try {
			return sdf.parse(str);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public static int getDaysOfMonth(Date date) {  
        Calendar calendar = Calendar.getInstance();  
        calendar.setTime(date);  
        return calendar.getActualMaximum(Calendar.DAY_OF_MONTH);  
    }  
	
	/**
	 * 解析日期到字符串
	 * @param format
	 * @param date
	 * @return
	 */
	public static String parseDateToStr(String format, Date date)
	{
		SimpleDateFormat sdf =   new SimpleDateFormat(format);
		String str = sdf.format(date);
		return str;
	}
	
	/**
	 * Date转换TimeStamp
	 * @param date
	 * @return
	 */
	public static Timestamp dateToTs(Date date)
	{
		return new java.sql.Timestamp(date.getTime());
	}
}
