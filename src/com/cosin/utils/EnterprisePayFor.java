package com.cosin.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cosin.web.dao.record.IRecordDao;
import com.cosin.web.entity.WithdrawCashRecord;

@Scope("prototype")
@Controller
public class EnterprisePayFor {

	@Autowired
	private IRecordDao recordDao;
	
	/**
	 * 2017年2月4日上午9:10:12
	 *  阳朔
	 *  注释:企业付款 调用接口
	 *  123.56.238.98/yingshoubaoMgr/pay/payFor.do?cashId=1
	 */
	@RequestMapping(value="/pay/payFor",produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String selectsubject(HttpServletRequest request,HttpServletResponse response) throws Exception{
//		UserSession userSession = (UserSession) request.getSession().getAttribute("userSession");
//		String jsapi_ticket = userSession.jsapi_ticket;
		
		String cashId = request.getParameter("cashId");
		WithdrawCashRecord cashRecord = recordDao.getWithdrawCashRecordById(cashId);
		Double price = cashRecord.getPrice()*100;
		String openId = cashRecord.getSysUser().getAppkey();
		String cashNo = cashRecord.getCashNo();
		
		
		String resultxml = WeChatToPay.enterprisePay( openId, price.toString(),cashNo);
		
		
		String url = "https://api.mch.weixin.qq.com/mmpaymkttransfers/promotion/transfers";
		
		String ss = HttpRequest.sendPost(url, resultxml);
		
		int num = 0;
		
		
		
		
		
		
		return null;
	}
}
