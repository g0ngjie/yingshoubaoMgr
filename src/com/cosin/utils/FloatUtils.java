package com.cosin.utils;

import java.math.BigDecimal;

public class FloatUtils {
	public static double add(double a, double b)
	{
		BigDecimal a1 = new BigDecimal(Double.toString(a));
		BigDecimal b1 = new BigDecimal(Double.toString(b));
		double c = a1.add(b1).doubleValue();
		return c;
	}
	
	public static double sub(double a, double b)
	{
		BigDecimal a1 = new BigDecimal(Double.toString(a));
		BigDecimal b1 = new BigDecimal(Double.toString(b));
		double c = a1.subtract(b1).doubleValue();
		return c;
	}
	
	public static double multiply(double a, double b)
	{
		BigDecimal a1 = new BigDecimal(Double.toString(a));
		BigDecimal b1 = new BigDecimal(Double.toString(b));
		double c = a1.multiply(b1).doubleValue();
		return c;
	}
	
	public static double divide(double a, double b)
	{
		BigDecimal a1 = new BigDecimal(Double.toString(a));
		BigDecimal b1 = new BigDecimal(Double.toString(b));
		double c = a1.divide(b1).doubleValue();
		return c;
	}
}
