package com.cosin.utils;

import java.util.HashMap;

import cn.jpush.api.JPushClient;
import cn.jpush.api.common.resp.APIConnectionException;
import cn.jpush.api.common.resp.APIRequestException;
import cn.jpush.api.push.PushResult;

public class JPushUtils {
	
	public static JPushClient jPushClient;
	public static JPushClient jPushClientCar;
	public static String masterSecret = "e27ef23a035f1817a2003b0b";
	public static String appKey = "34916497c239246d3c46c65d";
	/*public static String masterSecret1 = "eade518967915336c4f00f12";
	public static String appKey1 = "9cc69da48239d1635f917014";*/
	//初始化JPushDemo
	public static JPushClient initJPushClient() {
		jPushClient = new JPushClient(masterSecret,appKey);
		return jPushClient;
	}
	
	//初始化JPushDemo
	/*public static JPushClient initJPushClientCar() {
		jPushClientCar = new JPushClient(masterSecret1,appKey1);
			return jPushClientCar;
	}*/
	
	//对所有用户发送消息
	public static Boolean sendNotification(String notification)
            throws APIConnectionException,
                   APIRequestException{
		if(jPushClient==null)
		{
			initJPushClient();
		}
		PushResult result = jPushClient.sendNotificationAll(notification);
		if(result.isResultOK())
		{
			return true;
		}
		return false;			
	}
	//点对点推送消息to货主
	public static Boolean sendNotificationByalias(String title,String notification,String alias)
            throws APIConnectionException,
                   APIRequestException{
		if(jPushClient==null)
		{
			initJPushClient();
		}
		jPushClient.sendAndroidNotificationWithAlias(title, notification, new HashMap(),alias);
		
		jPushClient.sendIosNotificationWithAlias(title, new HashMap(), alias);
		
		return false;
	}
	
	//点对点推送消息to车主
/*	public static Boolean sendNotificationByaliasCar(String title,String notification,String alias)
            throws APIConnectionException,
                   APIRequestException{
		if(jPushClientCar==null)
		{
			initJPushClientCar();
		}
		jPushClientCar.sendAndroidNotificationWithAlias(title, notification, new HashMap(),alias);
		
		jPushClientCar.sendIosNotificationWithAlias(title, new HashMap(), alias);
		
		return false;
	}*/
}
