package com.cosin.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * json工具类   2016-02-19
 * @author zhb
 * @version 1.0
 */
public class JsonTwoUtils {
	
	public static JSONObject getRow(String data)
	{
		try {
			JSONObject jsonObj = new JSONObject(data);
			return jsonObj;
			//JSONArray jsonArray = new JSONArray(data);
			//if(jsonArray.length()>0)
			//	return jsonArray.getJSONObject(0);  
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public static Map parseJson(JSONObject jsobj)
	{
		Map map = new HashMap();
		Iterator it = jsobj.keys();
		while(it.hasNext())
		{
			String key = it.next().toString();
			try {
				Object obj = jsobj.get(key);
				if(obj instanceof JSONArray){
					JSONArray arr = (JSONArray)obj;
					List list = new ArrayList();
					for(int i=0; i<arr.length(); i++)
					{
						list.add(parseJson(arr.getJSONObject(i)));
					}
					map.put(key, list);
				}
				else if(obj instanceof JSONObject){
					map.put(key, parseJson((JSONObject)obj));//解map中的map
				}
				else
				{
					map.put(key, jsobj.getString(key));
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return map;
	}
	
}
