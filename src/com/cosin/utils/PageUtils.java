package com.cosin.utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.servlet.ModelAndView;

public class PageUtils {
	/*
	public static Map pageCreate(ModelAndView modelAndView, HttpServletRequest request, int count)
	{
		int pagelimit = 15;
  		int pagestart = 0;
  		String strpagelimit = request.getParameter("pagelimit");
		if(strpagelimit != null && !"".equals(strpagelimit))
			pagelimit = new Integer(pagelimit);
		String strpagestart = request.getParameter("pagestart");
		if(strpagestart != null && !"".equals(strpagestart))
			pagestart = new Integer(strpagestart);
		modelAndView.addObject("pagestart", pagestart);
		modelAndView.addObject("pagelimit", pagelimit);
		modelAndView.addObject("pagelen", count);
		
		Map map = new HashMap();
		map.put("pagestart", pagestart);
		map.put("pagelimit", pagelimit);
		return map;
	}*/
	
	/**
	 * 生成分页数据结果集map
	 * @param listResult
	 * @param totle
	 * @return
	 */
	public static Map getPageParam(List listResult, int totle)
	{
		Map mapRet = new HashMap();
		mapRet.put("rows", listResult);
		mapRet.put("total", totle);
		return mapRet;
	}
	
	/**
	 * 计算分页参数
	 * @param request
	 * @return
	 */
	public static Map getPageParam(HttpServletRequest request)
	{
		String strPage = request.getParameter("page");
		String strRows = request.getParameter("rows");
		int page = 1;
		int rows = 10;
		if(strPage != null && !"".equals(strPage))
		{
			page = new Integer(strPage);
		}
		if(strRows != null && !"".equals(strRows))
		{
			rows = new Integer(strRows);
		}
		int start = (page-1)*rows;
		int limit = rows;
		Map map = new HashMap();
		map.put("start", start);
		map.put("limit", limit);
		return map;
	}
	
	
}
