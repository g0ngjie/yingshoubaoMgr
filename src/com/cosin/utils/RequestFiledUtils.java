package com.cosin.utils;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class RequestFiledUtils {
	private HttpServletRequest request;
	private Map map = null;
	public RequestFiledUtils(HttpServletResponse response, HttpServletRequest request,String img)
	{
		this.request = request;
		try
		{
			map = FileUploadUtils.saveUploadFile(request, response);
		}
		catch(Exception e)
		{
			map = null;
		}
	}
	
	public String getParam(String paramName)
	{
		if(map != null)
		{
			return (String)map.get(paramName);
		}
		else
		{
			return (String)request.getParameter(paramName);
		}
	}
	
	public Object getFileArrParam(String paramName)
	{
		Object val = null;
		if(map != null)
		{
			val = (Object)map.get(paramName);
		}
		else
		{
			val = (Object)request.getParameter(paramName);
		}
		
		if(val == null)
			val = null;
		
		return val;
	}
	
	public String getFileParam(String paramName)
	{
		String val = null;
		if(map != null)
		{
			val = (String)map.get(paramName);
		}
		else
		{
			val = (String)request.getParameter(paramName);
		}
		
		if(val == null)
			val = "";
		
		return val;
	}
	
	public List getListFile()
	{
		return (List)map.get("filesArray");
	}
}
