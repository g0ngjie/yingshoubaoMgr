package com.cosin.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

public class WeChatToPay {
	public final static String APPID = "wx1c2fce17037e2556";
	public final static String SECRET = "fcc1a027fbff8e048fe8fe4e3eebd76f";
	public final static String MCH_ID = "1422683502";
	
	public static String WxJsApiCheck(String jsapi_ticket) {

		//String jsapi_ticket ="";//看清楚.这是ticket..用token在https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=ACCESS_TOKEN&type=jsapi里换的
		String nonce_str = Sha1Util.getNonceStr();// 随机字符串
		String timestamp = Sha1Util.getTimeStamp();// 时间戳
		String appid = APPID;//APPID,谁在问我为什么报没有APPID就去死吧
		String url="http://http://192.168.1.230:8081/yingshoubaoMgr/pay/payFor.do";//发起支付的前端页面的URL地址.而且...而且必须在微信支付里面配置才行!!!
		String sign = null;
		try {
			SortedMap<String, String> packageParams = new TreeMap<String, String>();
			packageParams.put("jsapi_ticket", jsapi_ticket);
			packageParams.put("noncestr", nonce_str);
			packageParams.put("timestamp", timestamp);
			packageParams.put("url", url);
			sign = Sha1Util.createSHA1Sign(packageParams);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String res="appId : \"" + appid + "\",timestamp : \"" + timestamp //微信个傻逼..这里的timestamp是小写~~
				+ "\", nonceStr : \"" + nonce_str
				+ "\", signature : \"" + sign + "\"";
		//return res;
		return sign;
	}
	
	
	public static String dopay(String openId, String orderNo ,String shopType ,String price) {

		// 商户相关资料
		String appid =  APPID;
		String appsecret = SECRET;
		String mch_id = MCH_ID;//邮件里的MCHID
		String partnerkey = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";//在微信商户平台pay.weixin.com里自己生成的那个key

		// 获取openId后调用统一支付接口https://api.mch.weixin.qq.com/pay/unifiedorder
		String currTime = TenpayUtil.getCurrTime();
		// 8位日期
		String strTime = currTime.substring(8, currTime.length());
		// 四位随机数
		String strRandom = TenpayUtil.buildRandom(4) + "";
		// 10位序列号,可以自行调整。
		String strReq = strTime + strRandom;
		// 设备号 非必输
		String device_info = "";
		// 随机数
		String nonce_str = strReq;
		// 商品描述根据情况修改
		String body = shopType;//"美食";
		// 商户订单号
		String out_trade_no = orderNo;
		String spbill_create_ip = "192.168.1.32";
		// 这里notify_url是 支付完成后微信发给该链接信息，可以判断会员是否支付成功，改变订单状态等。
		String notify_url = "http://ysb.cosinwx.com/RevenueTreasure/appweb/wxRedirect31.do";//send/sendMobel.do";//wxRedirect31.do";

		String trade_type = "JSAPI";
		String openid = openId;
		// 非必输
		// String product_id = "";
		SortedMap<String, String> packageParams = new TreeMap<String, String>();
		packageParams.put("appid", appid);
		packageParams.put("mch_id", mch_id);
		packageParams.put("nonce_str", nonce_str);
		packageParams.put("body", body);
		// packageParams.put("attach", attach);
		packageParams.put("out_trade_no", out_trade_no);

		// 这里写的金额为1 分到时修改
		packageParams.put("total_fee", price);
		// packageParams.put("total_fee", "finalmoney");
		packageParams.put("spbill_create_ip", spbill_create_ip);
		packageParams.put("notify_url", notify_url);

		packageParams.put("trade_type", trade_type);
		packageParams.put("openid", openid);

		RequestHandler reqHandler = new RequestHandler(null, null);
		reqHandler.init(appid, appsecret, partnerkey);

		String sign = reqHandler.createSign(packageParams);
		String xml = "<xml>" + "<appid>" + appid + "</appid>" + "<mch_id>"
				+ mch_id + "</mch_id>" + "<nonce_str>" + nonce_str
				+ "</nonce_str>" + "<sign><![CDATA[" + sign + "]]></sign>"
				+ "<body><![CDATA[" + body + "]]></body>"
				+ "<out_trade_no>"
				+ out_trade_no
				+ "</out_trade_no>"
				+

				// 金额，这里写的1 分到时修改
				"<total_fee>"
				+ price
				+ "</total_fee>"
				+
				// "<total_fee>"+finalmoney+"</total_fee>"+
				"<spbill_create_ip>" + spbill_create_ip + "</spbill_create_ip>"
				+ "<notify_url>" + notify_url + "</notify_url>"
				+ "<trade_type>" + trade_type + "</trade_type>" + "<openid>"
				+ openid + "</openid>" + "</xml>";
		System.out.println(xml);
		String allParameters = "";
		try {
			allParameters = reqHandler.genPackage(packageParams);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String createOrderURL = "https://api.mch.weixin.qq.com/pay/unifiedorder";
		Map<String, Object> dataMap2 = new HashMap<String, Object>();
		String prepay_id = "";
		try {
			prepay_id = new GetWxOrderno().getPayNo(createOrderURL, xml);
			if (prepay_id.equals("")) {
				System.out.println("统一支付接口获取预支付订单出错");
			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		System.out.println("prepay_id:"+prepay_id);
		SortedMap<String, String> finalpackage = new TreeMap<String, String>();
		String appid2 = appid;
		String timestamp = Sha1Util.getTimeStamp();
		String nonceStr2 = nonce_str;
		String prepay_id2 = "prepay_id=" + prepay_id;
		String packages = prepay_id2;
		finalpackage.put("appId", appid2);
		finalpackage.put("timeStamp", timestamp);
		finalpackage.put("nonceStr", nonceStr2);
		finalpackage.put("package", packages);
		finalpackage.put("signType", "MD5");
		String finalsign = reqHandler.createSign(finalpackage);
		return "timestamp:\"" + timestamp  //这里的也是小写~~
				+ "\",nonceStr:\"" + nonceStr2 + "\",package:\""
				+ packages + "\",signType: \"MD5" + "\",paySign:\""
				+ finalsign + "\"";
		
	}
	
	
	
	 public static String enterprisePay(String openId,String price,String cashNo){
		 	String partnerkey = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";//在微信商户平台pay.weixin.com里自己生成的那个key
		 	String currTime = TenpayUtil.getCurrTime();
			// 8位日期
			String strTime = currTime.substring(8, currTime.length());
			// 四位随机数
			String strRandom = TenpayUtil.buildRandom(4) + "";
			// 10位序列号,可以自行调整。
			String nonce_str = strTime + strRandom;
		 
			String casNo = strRandom + strTime;
			
			SortedMap<String, String> packageParams = new TreeMap<String, String>();
			packageParams.put("mch_appid", APPID);
			packageParams.put("mchid", MCH_ID);
			packageParams.put("nonce_str", nonce_str);
			packageParams.put("partner_trade_no", casNo);

			// 这里写的金额为1 分到时修改
			packageParams.put("amount", price);
			packageParams.put("spbill_create_ip", "123.56.238.98");
			packageParams.put("openid", openId);
			packageParams.put("desc", "提现");
			packageParams.put("check_name", "NO_CHECK");

			RequestHandler reqHandler = new RequestHandler(null, null);
			reqHandler.init(APPID, SECRET, partnerkey);

			String sign = reqHandler.createSign(packageParams);
			//amount=1.0, appid=wx1c2fce17037e2556, mch_appid=wx1c2fce17037e2556, mchid=1422683502, nonce_str=0953557286, openid=oAYvFwN16tQh9B7xMhwkq-5Z6Xeg, partner_trade_no=201654845123, spbill_create_ip=192.168.1.32
			String xml2 = "<xml>";
			xml2 +="<mch_appid>" +APPID+ "</mch_appid>";
			xml2 +="<mchid>1422683502</mchid>";
			xml2 +="<nonce_str>" + nonce_str +"</nonce_str>";
			xml2 +="<partner_trade_no>"+ casNo +"</partner_trade_no>";//商户订单号
			xml2 +="<openid>"+ openId +"</openid>";
			xml2 +="<check_name>NO_CHECK</check_name>";
			xml2 +="<amount>"+ price +"</amount>";
			xml2 +="<desc>"+"提现"+"</desc>";
			xml2 +="<spbill_create_ip>"+"123.56.238.98"+"</spbill_create_ip>";
			xml2 +="<sign>"+ sign +"</sign>";
			xml2 +="</xml>";

			return xml2;

	}
}
