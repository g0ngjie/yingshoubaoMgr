package com.cosin.web.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cosin.utils.JsonUtils;
import com.cosin.utils.RequestFiledUtils;
/**
 * 附文本图片上传
 * @author 王思明
 */
@Scope("prototype")
@Controller
public class FileUploadController
{
	/**
	 * 王思明 
	 * 2016年10月18日
	 */
	@RequestMapping(value={"/fileupload/uploadImg"}, produces={"text/html;charset=UTF-8"})
	@ResponseBody
	public String uploadImg(HttpServletRequest request, HttpServletResponse response)
	{
	     RequestFiledUtils rfu = new RequestFiledUtils(response, request, "img");
	     List listFile = rfu.getListFile();
	     if (listFile.size() == 0)
	     {
	      Map<String,Object> resMap = new HashMap<String,Object>();
	       resMap.put("code", Integer.valueOf(101));
	       String resStr = JsonUtils.fromObject(resMap);
	       return resStr;
	     }
	     Map<String,Object> mapFile = (Map)listFile.get(0);
	     Map<String,Object> resMap = new HashMap<String,Object>();
	     resMap.put("code", Integer.valueOf(100));
	     resMap.put("fullImgUrl", "http://localhost:8080/FileManagerService/img/" + mapFile.get("fileName").toString());
	     resMap.put("filePath", mapFile.get("fileName").toString());
	     resMap.put("fileSize", mapFile.get("size").toString());
	     resMap.put("extName", mapFile.get("extName").toString());
	 
	     String resStr = JsonUtils.fromObject(resMap);
	     return resStr;
	}
	 
	@RequestMapping(value={"/fileupload/uploadImgForKindEditor"}, produces={"text/html;charset=UTF-8"})
	@ResponseBody
	public String uploadImgForKindEditor(HttpServletRequest request, HttpServletResponse response)
	{
	    RequestFiledUtils rfu = new RequestFiledUtils(response, request, "img");
	    List<Object> listFile = rfu.getListFile();
	    if (listFile.size() == 0)
	    {
	       Map<String,Object> resMap = new HashMap<String,Object>();
	       resMap.put("error", Integer.valueOf(1));
	       resMap.put("message", "上传失败");
	       String resStr = JsonUtils.fromObject(resMap);
	       return resStr;
	    }
	    String path = request.getContextPath();
	    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + "/FileManagerService" + "/";
	    Map<String,Object> mapFile = (Map)listFile.get(0);
	    Map<String,Object> resMap = new HashMap<String,Object>();
	    resMap.put("error", Integer.valueOf(0));
	    resMap.put("url", basePath + "img/" + mapFile.get("fileName").toString());
	    String resStr = JsonUtils.fromObject(resMap);
	    return resStr;
	    
	}
	
	
	
	
	
}