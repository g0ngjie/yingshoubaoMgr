package com.cosin.web.controller.fans;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cosin.utils.FloatUtils;
import com.cosin.utils.JsonUtils;
import com.cosin.utils.PageUtils;
import com.cosin.web.entity.Fans;
import com.cosin.web.entity.SysOrder;
import com.cosin.web.entity.WithdrawCashRecord;
import com.cosin.web.service.fans.IFansManager;

@Scope("prototype")
@Controller
public class FansController {
	@Autowired
	private IFansManager fansManager;
	/**
	 * 跳转到商家粉丝
	 * @return
	 */
	@RequestMapping(value="/shopFans/listOrder.do")
    public String indexs(){
        return "fans/listFans";
    }
	
	
	/**
	 * 获取商家数据分页
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/shopFans/shopFansListData",  produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String engineerListData(HttpServletResponse response, HttpServletRequest request)
	{			
		String SsShopName = request.getParameter("SsShopName");
		//获取分页参数
		Map pageParam = PageUtils.getPageParam(request);
		List<Fans> listRes = fansManager.getUserList(SsShopName,new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));
		int totle =fansManager.getinfofenye(SsShopName);
		Map mapRet = PageUtils.getPageParam(listRes, totle);
		String resStr = JsonUtils.fromObject(mapRet);//过滤hiberte对象中不需要生成json的属性
		return resStr;
	}
	
	
	/**
	 * 查看粉丝
	 */
	@RequestMapping(value="/shopFans/chaXiangqing1", produces = "text/html;charset=UTF-8")
	@ResponseBody
    public String chaXiangqing1(HttpServletResponse response, HttpServletRequest request){
		String shopId = request.getParameter("shopId");
		String SsFensiName1 = request.getParameter("SsFensiName1");
		Map pageParam = PageUtils.getPageParam(request);
		List<Fans> listRes = fansManager.getFansList(shopId,SsFensiName1,new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));
		int totle =fansManager.getFansfenye(shopId,SsFensiName1);
		Map mapRet = PageUtils.getPageParam(listRes, totle);
		String resStr = JsonUtils.fromObject(mapRet);//过滤hiberte对象中不需要生成json的属性
		return resStr;
    }
	
	/**
	 * 粉丝统计1
	 * 崔青山
	 * 2016年12月5日
	 * @param response
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/shopFans/chaXiangqings1", produces = "text/html;charset=UTF-8")
	@ResponseBody
    public String chaXiangqings1(HttpServletResponse response, HttpServletRequest request){
		String shopId = request.getParameter("shopId");
		String SsFensiName1 = "";
		List<Fans> listStr = fansManager.getFans(shopId,SsFensiName1);
		double zong = 0;
		double zongTi = 0;
		int zongNum = 0;
		FloatUtils floatUtils = new FloatUtils();
		for (int i = 0; i < listStr.size(); i++) {
			Fans shop = listStr.get(i);
			List<SysOrder> list = fansManager.getOrder(shop.getSysUserByUserId().getUserId());
			for (int j = 0; j < list.size(); j++) {
				SysOrder order = list.get(j);
				zong = floatUtils.add(zong, order.getOrderPrice());
				zongNum++;
			}
			
			List<WithdrawCashRecord> lists = fansManager.getWithdrawCashRecord(shop.getSysUserByParentUserId().getUserId());
			for (int j = 0; j < lists.size(); j++) {
				WithdrawCashRecord cash = lists.get(j);
				zongTi = floatUtils.add(zongTi, cash.getPrice());
			}
		}
		
		Map map = new HashMap();
		map.put("zong", zong);
		map.put("zongTi", zongTi);
		map.put("zongNum", zongNum);
		map.put("code", "100");
		String resStr = JsonUtils.fromObject(map);
        return resStr;
    }
	
	/**
	 * 查看粉丝2
	 * 崔青山
	 * 2016年12月1日
	 * @param response
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/shopFans/chaXiangqing2", produces = "text/html;charset=UTF-8")
	@ResponseBody
    public String chaXiangqing2(HttpServletResponse response, HttpServletRequest request){
		String fansId = request.getParameter("fansId");
		String SsFensiName2 = request.getParameter("SsFensiName2");
		Map pageParam = PageUtils.getPageParam(request);
		List<Fans> listRes = fansManager.getFansList1(fansId,SsFensiName2,new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));
		int totle =fansManager.getFansfenye1(fansId,SsFensiName2);
		Map mapRet = PageUtils.getPageParam(listRes, totle);
		String resStr = JsonUtils.fromObject(mapRet);//过滤hiberte对象中不需要生成json的属性
		return resStr;
    }
	
	
	/**
	 * 粉丝统计2
	 * 崔青山
	 * 2016年12月5日
	 * @param response
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/shopFans/chaXiangqings2", produces = "text/html;charset=UTF-8")
	@ResponseBody
    public String chaXiangqings2(HttpServletResponse response, HttpServletRequest request){
		String fansId = request.getParameter("fansId");
		List<Fans> listStr = fansManager.getFans2(fansId);
		double zong = 0;
		double zongTi = 0;
		int zongNum = 0;
		FloatUtils floatUtils = new FloatUtils();
		for (int i = 0; i < listStr.size(); i++) {
			Fans shop = listStr.get(i);
			List<SysOrder> list = fansManager.getOrder(shop.getSysUserByUserId().getUserId());
			for (int j = 0; j < list.size(); j++) {
				SysOrder order = list.get(j);
				zong = floatUtils.add(zong, order.getOrderPrice());
				String aa = order.getOrderNum();
				zongNum++;
			}
			
			List<WithdrawCashRecord> lists = fansManager.getWithdrawCashRecord(shop.getSysUserByParentUserId().getUserId());
			for (int j = 0; j < lists.size(); j++) {
				WithdrawCashRecord cash = lists.get(j);
				zongTi = floatUtils.add(zongTi, cash.getPrice());
			}
		}
		
		Map map = new HashMap();
		map.put("zong2", zong);
		map.put("zongTi2", zongTi);
		map.put("zongNum2", zongNum);
		map.put("code", "100");
		String resStr = JsonUtils.fromObject(map);
        return resStr;
    }
	/**
	 * 查看粉丝3
	 * 崔青山
	 * 2016年12月1日
	 * @param response
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/shopFans/chaXiangqing3", produces = "text/html;charset=UTF-8")
	@ResponseBody
    public String chaXiangqing3(HttpServletResponse response, HttpServletRequest request){
		String fansId = request.getParameter("fansId");
		String SsFensiName3 = request.getParameter("SsFensiName3");
		Map pageParam = PageUtils.getPageParam(request);
		List<Fans> listRes = fansManager.getFansList2(fansId,SsFensiName3,new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));
		int totle =fansManager.getFansfenye2(fansId,SsFensiName3);
		Map mapRet = PageUtils.getPageParam(listRes, totle);
		String resStr = JsonUtils.fromObject(mapRet);//过滤hiberte对象中不需要生成json的属性
		return resStr;
    }
	

	/**
	 * 2016年12月27日下午2:52:31
	 *  阳朔
	 *  注释:粉丝 三级
	 */
	@RequestMapping(value="/shopFans/getFans", produces = "text/html;charset=UTF-8")
	@ResponseBody
    public String getFans(HttpServletResponse response, HttpServletRequest request){
		String shopId = request.getParameter("shopId");
		List<Object> list = fansManager.getFansByShopId(shopId);
		return JsonUtils.fromArrayObject(list);
    }
	
}

