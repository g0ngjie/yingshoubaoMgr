package com.cosin.web.controller.log;

import java.util.List;

import com.cosin.web.entity.About;
import com.cosin.web.entity.BankCard;
import com.cosin.web.entity.SysLog;
import com.cosin.web.entity.SysShop;
import com.cosin.web.entity.SysUser;


public interface ILogDao {

	/**
	 * 2017��1��10������9:44:48
	 *  ��˷
	 *  ע��:
	 */
	SysUser getSysUserByUserId(String userId);

	/**
	 * 2017��1��10������9:44:53
	 *  ��˷
	 *  ע��:
	 */
	void saveObj(Object obj);

	/**
	 * 2017��1��10������10:05:25
	 *  ��˷
	 *  ע��:
	 */
	List<SysLog> getSysLogList(String ssUser,String startDate,String endDate, Integer start, Integer limit);

	/**
	 * 2017��1��10������10:06:03
	 *  ��˷
	 *  ע��:
	 */
	int getLogCount(String ssUser,String startDate,String endDate);

	/**
	 * 2017��1��10������11:19:25
	 *  ��˷
	 *  ע��:
	 */
	SysShop getSysShopByShopId(String key);

	/**
	 * 2017��1��10������6:57:21
	 *  ��˷
	 *  ע��:
	 */
	List<BankCard> getbankList(Integer start, Integer limit);

	/**
	 * 2017��1��10������6:57:25
	 *  ��˷
	 *  ע��:
	 */
	int getbankCount();

	/**
	 * 2017��1��10������7:12:49
	 *  ��˷
	 *  ע��:
	 */
	BankCard getBankCardById(String bankId);

	/**
	 * 2017��1��10������7:29:48
	 *  ��˷
	 *  ע��:
	 */
	BankCard getBankCardByName(String bankName);

	/**
	 * 2017��1��15������3:27:53
	 *  ��˷
	 *  ע��:
	 */
	About getAbout();

	/**
	 * 2017��1��15������3:43:35
	 *  ��˷
	 *  ע��:
	 */
	About getAboutById(String aboutId);

	/**
	 * 2017��1��16������11:19:08
	 *  ��˷
	 *  ע��:
	 */
	SysUser getNoTypeSysUserByUserId(String userId);

	/**
	 * 2017��1��16������1:43:07
	 *  ��˷
	 *  ע��:
	 */
	SysShop getNoTypeSysShopByShopId(String shopId);




	
}
