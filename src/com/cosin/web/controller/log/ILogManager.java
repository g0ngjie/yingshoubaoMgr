package com.cosin.web.controller.log;

import java.util.List;
import java.util.Map;

import com.cosin.web.entity.BankCard;
import com.cosin.web.entity.SysLog;
import com.cosin.web.entity.SysShop;
import com.cosin.web.entity.SysUser;


public interface ILogManager {

	/**
	 * 2017年1月10日上午9:41:47
	 *  阳朔
	 *  注释:funName(功能模块名)/action(操作动作)/content(操作内容)
	 */
	void saveLog(String userId, String funName, String action, String content);

	/**
	 * 2017年1月10日上午9:52:44
	 *  阳朔
	 *  注释:
	 */
	List<SysLog> getLogList(String ssUser,String startDate,String endDate, Integer integer,Integer integer2);

	/**
	 * 2017年1月10日上午9:52:48
	 *  阳朔
	 *  注释:
	 */
	int getLogCount(String ssUser,String startDate,String endDate);

	/**
	 * 2017年1月10日上午11:19:09
	 *  阳朔
	 *  注释:
	 */
	SysShop getSysShopByShopId(String key);

	/**
	 * 2017年1月10日下午6:55:12
	 *  阳朔
	 *  注释:
	 */
	List<BankCard> getbankList(Integer integer, Integer integer2);

	/**
	 * 2017年1月10日下午6:55:23
	 *  阳朔
	 *  注释:
	 */
	int getbankCount();

	/**
	 * 2017年1月10日下午7:11:30
	 *  阳朔
	 *  注释:启用
	 */
	void startEnable(String loginUserId,String bankId);

	/**
	 * 2017年1月10日下午7:14:07
	 *  阳朔
	 *  注释:禁用
	 */
	void forbidden(String loginUserId,String bankId);

	/**
	 * 2017年1月10日下午7:28:30
	 *  阳朔
	 *  注释:
	 */
	Map saveBank(String loginUserId,String bankName);

	/**
	 * 2017年1月15日下午3:26:04
	 *  阳朔
	 *  注释:
	 */
	List chaAbout();

	/**
	 * 2017年1月15日下午3:43:02
	 *  阳朔
	 *  注释:
	 */
	Map aboutBj(String aboutId);

	/**
	 * 2017年1月15日下午4:07:32
	 *  阳朔
	 *  注释:
	 */
	void saveAbout(String loginUserId,String aboutId,String content,String img);

	/**
	 * 2017年1月16日上午11:18:02
	 *  阳朔
	 *  注释:
	 */
	SysUser getSysUserByUserId(String userId);

	/**
	 * 2017年1月16日下午1:42:52
	 *  阳朔
	 *  注释:
	 */
	SysShop getNoTypeSysShopByShopId(String shopId);

	/**
	 * 2017年1月16日下午5:18:48
	 *  阳朔
	 *  注释:
	 */
	Map bankDel(String delKeys);
	
	
	
	
	
}

