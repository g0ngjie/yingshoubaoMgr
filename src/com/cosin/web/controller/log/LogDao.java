package com.cosin.web.controller.log;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.cosin.web.dao.BaseDao;
import com.cosin.web.entity.About;
import com.cosin.web.entity.BankCard;
import com.cosin.web.entity.SysLog;
import com.cosin.web.entity.SysShop;
import com.cosin.web.entity.SysUser;
@Repository
public class LogDao extends BaseDao implements ILogDao{

	/**
	 * 2017��1��10������9:44:59
	 *  ��˷
	 *  ע��:
	 */
	@Override
	public SysUser getSysUserByUserId(String userId) {
		String sql = "from SysUser as a where a.type = 3 and a.isDel = 0 and a.userId = '" + userId + "'";
		List<SysUser> list = query(sql);
		if(list.size() > 0)
			return list.get(0);
		return null;
	}

	/**
	 * 2017��1��10������9:44:59
	 *  ��˷
	 *  ע��:
	 */
	@Override
	public void saveObj(Object obj) {
		super.save(obj);
	}

	/**
	 * 2017��1��10������10:06:09
	 *  ��˷
	 *  ע��:
	 */
	@Override
	public List<SysLog> getSysLogList(String ssUser,String startDate,String endDate, Integer start,
			Integer limit) {
		String sql = "from SysLog as a where 1 = 1";
		if(ssUser != null && !"".equals(ssUser)){
			sql += "and a.sysUser.userName = '" + ssUser + "'";
		}
		if(startDate != null && !"".equals(startDate)){
			sql += "and a.createDate >= '" + startDate + "00:00:00'";
		}
		if(endDate != null && !"".equals(endDate)){
			sql += "and a.createDate <= '" + endDate + "23:59:59'";
		}
		sql += "order by a.createDate desc";
		return query(sql,start,limit);
	}

	/**
	 * 2017��1��10������10:06:09
	 *  ��˷
	 *  ע��:
	 */
	@Override
	public int getLogCount(String ssUser,String startDate,String endDate) {
		String sql = "select count(*) from SysLog as a where 1 = 1";
		if(ssUser != null && !"".equals(ssUser)){
			sql += "and a.sysUser.userName = '" + ssUser + "'";
		}
		if(startDate != null && !"".equals(startDate)){
			sql += "and a.createDate >= '" + startDate + "00:00:00'";
		}
		if(endDate != null && !"".equals(endDate)){
			sql += "and a.createDate <= '" + endDate + "23:59:59'";
		}
		sql += "order by a.createDate desc";
		return queryCount(sql);
	}

	/**
	 * 2017��1��10������11:19:30
	 *  ��˷
	 *  ע��:
	 */
	@Override
	public SysShop getSysShopByShopId(String key) {
		String sql = "from SysShop as a where a.isDel = 0 and a.type = 1 and a.shopId = '" + key + "'";
		List<SysShop> list = query(sql);
		if(list.size() > 0)
			return list.get(0);
		return null;
	}

	/**
	 * 2017��1��10������6:57:30
	 *  ��˷
	 *  ע��:
	 */
	@Override
	public List<BankCard> getbankList(Integer start, Integer limit) {
		String sql = "from BankCard as a where 1 = 1";
		return query(sql);
	}

	/**
	 * 2017��1��10������6:57:30
	 *  ��˷
	 *  ע��:
	 */
	@Override
	public int getbankCount() {
		String sql = "select count(*) from BankCard as a where 1 = 1";
		return queryCount(sql);
	}

	/**
	 * 2017��1��10������7:12:54
	 *  ��˷
	 *  ע��:
	 */
	@Override
	public BankCard getBankCardById(String bankId) {
		String sql = "from BankCard as a where a.bankCardId = '" + bankId + "'";
		List<BankCard> list = query(sql);
		if(list.size() > 0)
			return list.get(0);
		return null;
	}

	/**
	 * 2017��1��10������7:29:53
	 *  ��˷
	 *  ע��:
	 */
	@Override
	public BankCard getBankCardByName(String bankName) {
		String sql = "from BankCard as a where a.bankCardName like '%" + bankName + "%'";
		List<BankCard> list = query(sql);
		if(list.size() > 0)
			return list.get(0);
		return null;
	}

	/**
	 * 2017��1��15������3:27:58
	 *  ��˷
	 *  ע��:
	 */
	@Override
	public About getAbout() {
		String sql = "from About as a where 1 = 1";
		List<About> list = query(sql);
		if(list.size() > 0)
			return list.get(0);
		return null;
	}

	/**
	 * 2017��1��15������3:43:41
	 *  ��˷
	 *  ע��:
	 */
	@Override
	public About getAboutById(String aboutId) {
		String sql = "from About as a where a.aboutId = '" + aboutId + "'";
		List<About> list = query(sql);
		if(list.size() > 0)
			return list.get(0);
		return null;
	}

	/**
	 * 2017��1��16������11:19:13
	 *  ��˷
	 *  ע��:
	 */
	@Override
	public SysUser getNoTypeSysUserByUserId(String userId) {
		String sql = "from SysUser as a where a.userId = '" + userId + "'";
		List<SysUser> list = query(sql);
		if(list.size() > 0)
			return list.get(0);
		return null;
	}

	/**
	 * 2017��1��16������1:43:25
	 *  ��˷
	 *  ע��:
	 */
	@Override
	public SysShop getNoTypeSysShopByShopId(String shopId) {
		String sql = "from SysShop as a where a.shopId = '" + shopId + "'";
		List<SysShop> list = query(sql);
		if(list.size() > 0)
			return list.get(0);
		return null;
	}

	



}
