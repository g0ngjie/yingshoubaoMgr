package com.cosin.web.controller.log;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cosin.web.entity.About;
import com.cosin.web.entity.BankCard;
import com.cosin.web.entity.SysLog;
import com.cosin.web.entity.SysShop;
import com.cosin.web.entity.SysUser;

@Service  
@Transactional 
public class LogManager implements ILogManager{
	@Autowired
	private ILogDao logDao;
	
	/**
	 * 2017年1月10日上午9:42:02
	 *  阳朔
	 *  注释:
	 */
	@Override
	public void saveLog(String userId, String funName, String action,String content) {
		SysLog sysLog = new SysLog();
		
		sysLog.setAction(action);
		sysLog.setContent(content);
		sysLog.setCreateDate(new Timestamp(new Date().getTime()));
		sysLog.setFunName(funName);
		SysUser sysUser = logDao.getSysUserByUserId(userId); 
		sysLog.setSysUser(sysUser);
		logDao.saveObj(sysLog);
		
	}

	/**
	 * 2017年1月10日上午9:52:55
	 *  阳朔
	 *  注释:
	 */
	@Override
	public List<SysLog> getLogList(String ssUser,String startDate,String endDate, Integer start,Integer limit) {
		List<SysLog> listRes = logDao.getSysLogList(ssUser,startDate,endDate,start,limit);
		List list = new ArrayList();
		for (int i = 0; i < listRes.size(); i++) {
			SysLog log = listRes.get(i);
			Map<Object,Object> map = new HashMap<Object, Object>();
			map.put("logId", log.getLogId());
			map.put("userName", log.getSysUser().getUserName());
			map.put("funName", log.getFunName());
			map.put("action", log.getAction());
			map.put("content", log.getContent());
			map.put("createDate", log.getCreateDate());
			list.add(map);
		}
		return list;
	}

	/**
	 * 2017年1月10日上午9:52:55
	 *  阳朔
	 *  注释:
	 */
	@Override
	public int getLogCount(String ssUser,String startDate,String endDate) {
		return logDao.getLogCount(ssUser,startDate,endDate);
	}

	/**
	 * 2017年1月10日上午11:19:15
	 *  阳朔
	 *  注释:
	 */
	@Override
	public SysShop getSysShopByShopId(String key) {
		return logDao.getSysShopByShopId(key);
	}

	/**
	 * 2017年1月10日下午6:55:28
	 *  阳朔
	 *  注释:
	 */
	@Override
	public List<BankCard> getbankList(Integer start, Integer limit) {
		 List<BankCard> listRes = logDao.getbankList(start,limit);
		 List list = new ArrayList();
		 for (int i = 0; i < listRes.size(); i++) {
			 BankCard bankCard = listRes.get(i);
			 Map<Object,Object> map = new HashMap<Object, Object>();
			 map.put("bankCardId", bankCard.getBankCardId());
			 map.put("bankName", bankCard.getBankCardName());
			 map.put("enable", bankCard.getEnable());
			 list.add(map);
		}
		return list;
	}

	/**
	 * 2017年1月10日下午6:55:28
	 *  阳朔
	 *  注释:
	 */
	@Override
	public int getbankCount() {
		return logDao.getbankCount();
	}

	/**
	 * 2017年1月10日下午7:11:41
	 *  阳朔
	 *  注释:
	 */
	@Override
	public void startEnable(String loginUserId,String bankId) {
		BankCard card = logDao.getBankCardById(bankId);
		card.setEnable(1);
		logDao.saveObj(card);
		saveLog(loginUserId, "银行管理", "启用", "启用");
	}

	/**
	 * 2017年1月10日下午7:14:16
	 *  阳朔
	 *  注释:
	 */
	@Override
	public void forbidden(String loginUserId,String bankId) {
		BankCard card = logDao.getBankCardById(bankId);
		card.setEnable(0);
		logDao.saveObj(card);
		saveLog(loginUserId, "银行管理", "禁用", "禁用");
	}

	/**
	 * 2017年1月10日下午7:28:36
	 *  阳朔
	 *  注释:
	 */
	@Override
	public Map saveBank(String loginUserId,String bankName) {
		Map map = new HashMap();
		BankCard chaBank = logDao.getBankCardByName(bankName);
		if(chaBank == null){
			BankCard card = new BankCard();
			card.setBankCardName(bankName);
			card.setEnable(1);
			logDao.saveObj(card);
			
			saveLog(loginUserId, "银行管理", "添加银行卡", "添加银行："+bankName);
			
			map.put("code", 100);
		}else{
			map.put("code", 101);
			map.put("msg", "改银行卡已存在");
		}
		return map;
	}

	/**
	 * 2017年1月15日下午3:26:12
	 *  阳朔
	 *  注释:
	 */
	@Override
	public List chaAbout() {
		List list = new ArrayList();
		About about = logDao.getAbout();
		Map<Object,Object> map = new HashMap<Object, Object>();
		map.put("aboutId", about.getAboutId());
		map.put("content", about.getContent());
		map.put("img", about.getImg());
		list.add(map);
		return list;
	}

	/**
	 * 2017年1月15日下午3:43:07
	 *  阳朔
	 *  注释:
	 */
	@Override
	public Map aboutBj(String aboutId) {
		About about = logDao.getAboutById(aboutId);
		Map<Object,Object> map = new HashMap<Object, Object>();
		map.put("aboutId", about.getAboutId());
		map.put("content", about.getContent());
		map.put("img", about.getImg());
		return map;
	}

	/**
	 * 2017年1月15日下午4:08:27
	 *  阳朔
	 *  注释:
	 */
	@Override
	public void saveAbout(String loginUserId,String aboutId, String content, String img) {
		About about = logDao.getAboutById(aboutId);
		about.setContent(content);
		about.setImg(img);
		logDao.saveObj(about);

		saveLog(loginUserId, "关于我们", "编辑操作", "编辑");
	}

	/**
	 * 2017年1月16日上午11:18:09
	 *  阳朔
	 *  注释:
	 */
	@Override
	public SysUser getSysUserByUserId(String userId) {
		return logDao.getNoTypeSysUserByUserId(userId);
	}

	/**
	 * 2017年1月16日下午1:42:56
	 *  阳朔
	 *  注释:
	 */
	@Override
	public SysShop getNoTypeSysShopByShopId(String shopId) {
		return logDao.getNoTypeSysShopByShopId(shopId);
	}

	/**
	 * 2017年1月16日下午5:18:54
	 *  阳朔
	 *  注释:
	 */
	@Override
	public Map bankDel(String delKeys) {
		BankCard bankCard = logDao.getBankCardById(delKeys);
		return null;
	}

	


	
}
