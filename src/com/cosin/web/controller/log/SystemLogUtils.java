package com.cosin.web.controller.log;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cosin.utils.JsonUtils;
import com.cosin.utils.PageUtils;
import com.cosin.utils.UserSession;
import com.cosin.web.entity.BankCard;
import com.cosin.web.entity.SysLog;

@Scope("prototype")
@Controller
public class SystemLogUtils {

	@Autowired
	private ILogManager logManager;
	
	/**
	 * 跳转到商家粉丝
	 * @return
	 */
	@RequestMapping(value="/system/systemLog")
    public String indexs(){
		int num = 0 ;
        return "message/systemlog";
    }
	
	/**
	 * 获取商家数据分页
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/system/logListData",  produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String engineerListData(HttpServletResponse response, HttpServletRequest request)
	{			
		String ssUser = request.getParameter("ssuser");
		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");
		//获取分页参数
		Map pageParam = PageUtils.getPageParam(request);
		List<SysLog> listRes = logManager.getLogList(ssUser,startDate,endDate,new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));
		int totle =logManager.getLogCount(ssUser,startDate,endDate);
		Map mapRet = PageUtils.getPageParam(listRes, totle);
		String resStr = JsonUtils.fromObject(mapRet);
		return resStr;
	}
	
	/**
	 * 跳转到银行管理
	 * @return
	 */
	@RequestMapping(value="/system/bankCard")
    public String bankList(){
        return "message/bankCard";
    }
	
	/**
	 * 获取银行卡列表
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/system/bankListData",  produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String bankListData(HttpServletResponse response, HttpServletRequest request)
	{			
		//获取分页参数
		Map pageParam = PageUtils.getPageParam(request);
		List<BankCard> listRes = logManager.getbankList(new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));
		int totle =logManager.getbankCount();
		Map mapRet = PageUtils.getPageParam(listRes, totle);
		String resStr = JsonUtils.fromObject(mapRet);
		return resStr;
	}
	
	/**
	 * 启用
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/system/startEnable",  produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String startEnable(HttpServletResponse response, HttpServletRequest request)
	{			
		UserSession userSession = (UserSession) request.getSession().getAttribute("userSession");
		String loginUserId = userSession.userId;
		String bankId = request.getParameter("delKeys");
		logManager.startEnable(loginUserId,bankId);
		Map map = new HashMap();
		map.put("code", 100);
		String resStr = JsonUtils.fromObject(map);
		return resStr;
	}
	
	/**
	 * 禁用
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/system/forbidden",  produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String forbidden(HttpServletResponse response, HttpServletRequest request)
	{			
		UserSession userSession = (UserSession) request.getSession().getAttribute("userSession");
		String loginUserId = userSession.userId;
		String bankId = request.getParameter("delKeys");
		logManager.forbidden(loginUserId,bankId);
		Map map = new HashMap();
		map.put("code", 100);
		String resStr = JsonUtils.fromObject(map);
		return resStr;
	}
	
	/**
	 * 添加银行卡
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/system/saveBank",  produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String saveBank(HttpServletResponse response, HttpServletRequest request)
	{			
		UserSession userSession = (UserSession) request.getSession().getAttribute("userSession");
		String loginUserId = userSession.userId;
		String bankName = request.getParameter("bankName");
		Map map = logManager.saveBank(loginUserId,bankName);
		
		String resStr = JsonUtils.fromObject(map);
		return resStr;
	}
	
	
	/**
	 * 跳转到关于我们
	 * @return
	 */
	@RequestMapping(value="/system/about_us")
    public String about_us(){
        return "about/about_us";
    }
	
	/**
	 * 查看 关于我们
	 */
	@RequestMapping(value="/about/chaAbout",  produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String chaAbout(HttpServletResponse response, HttpServletRequest request)
	{			
		List listRes = logManager.chaAbout();
		String resStr = JsonUtils.fromArrayObject(listRes);
		return resStr;
	}
	
	/**
	 * 关于我们编辑
	 */
	@RequestMapping(value="/about/aboutBj",  produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String aboutBj(HttpServletResponse response, HttpServletRequest request)
	{			
		String aboutId = request.getParameter("aboutId");
		Map map = logManager.aboutBj(aboutId);
		String resStr = JsonUtils.fromObject(map);
		return resStr;
	}
	
	/**
	 * 保存 关于我们 
	 */
	@RequestMapping(value="/about/saveAbout",  produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String saveAbout(HttpServletResponse response, HttpServletRequest request)
	{			
		UserSession userSession = (UserSession) request.getSession().getAttribute("userSession");
		String loginUserId = userSession.userId;
		String aboutId = request.getParameter("aboutId");
		String content = request.getParameter("content");
		String img = request.getParameter("imgCns");
		logManager.saveAbout(loginUserId,aboutId,content,img);
		Map map = new HashMap();
		map.put("code", 100);
		String resStr = JsonUtils.fromObject(map);
		return resStr;
	}
	
	/**
	 * 删除银行卡
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/system/bankDel",  produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String bankDel(HttpServletResponse response, HttpServletRequest request)
	{			
		String delKeys = request.getParameter("delKeys");
		Map map = logManager.bankDel(delKeys);
		
		String resStr = JsonUtils.fromObject(map);
		return resStr;
	}
	
}
