package com.cosin.web.controller.order;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFHeader;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cosin.utils.FloatUtils;
import com.cosin.utils.JsonUtils;
import com.cosin.utils.PageUtils;
import com.cosin.web.entity.CommentShop;
import com.cosin.web.entity.ShopStatistic;
import com.cosin.web.entity.SysOrder;
import com.cosin.web.entity.SysUser;
import com.cosin.web.service.order.IOrderManager;

@Scope("prototype")
@Controller
public class OrderController {
	@Autowired
	private IOrderManager orderManager;
	/**
	 * 跳转到订单管理
	 * @return
	 */
	@RequestMapping(value="/order/listOrder.do")
    public String indexs(){
        return "order/listOrder";
    }
	/**
	 * 跳转到用户评论
	 * @return
	 */
	@RequestMapping(value="/order/listComment")
    public String indexss(){
        return "order/listComment";
        
    }
	
	
	/**
	 * 获取订单数据分页
	 * 崔青山
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/order/orderListData",  produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String engineerListData(HttpServletResponse response, HttpServletRequest request)
	{			
		String proName = request.getParameter("proName");
		String cityName = request.getParameter("cityName");
		String SsUserName = request.getParameter("SsUserName");
		String SsShopName = request.getParameter("SsShopName");
		String SsOrderNum = request.getParameter("SsOrderNum");
		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");
		//获取分页参数
		Map pageParam = PageUtils.getPageParam(request);
		List<SysOrder> listRes = orderManager.getUserList(proName,cityName,SsUserName,SsShopName,SsOrderNum,startDate,endDate,new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));
		int totle =orderManager.getinfofenye(proName,cityName,SsUserName,SsShopName,SsOrderNum,startDate,endDate);
		Map mapRet = PageUtils.getPageParam(listRes, totle);
		String resStr = JsonUtils.fromObject(mapRet);//过滤hiberte对象中不需要生成json的属性
		return resStr;
	}


	/**
	 * 获取评论数据分页
	 * 崔青山
	 * @param response
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/order/CommentListData",  produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String shopCommentListData(HttpServletResponse response, HttpServletRequest request)
	{		
		String shopId = request.getParameter("shopId");
		String ssuser = request.getParameter("ssuser");	
		String ssOrderNum = request.getParameter("ssOrderNum");	
		String ssShop = request.getParameter("ssShop");	
		//获取分页参数
		Map pageParam = PageUtils.getPageParam(request);
		List<CommentShop> listRes = orderManager.getShopCommentList(shopId,ssuser,ssOrderNum,ssShop,new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));
		int totle =orderManager.getShopCommentfenye(shopId,ssuser,ssOrderNum,ssShop);
		Map mapRet = PageUtils.getPageParam(listRes, totle);
		String resStr = JsonUtils.fromObject(mapRet);
		return resStr;
	}

	
	/**
	 * 删除评论
	 * 崔青山
	 * 2016年7月26日
	 * @param response
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/order/delShopComment", produces = "text/html;charset=UTF-8")
	@ResponseBody
    public String delShopComment(HttpServletResponse response, HttpServletRequest request){
		String delKeys = request.getParameter("delKeys");
		String[] keyArr = delKeys.split(",");
			for(int i=0; i<keyArr.length; i++)
			{
				String key = keyArr[i];
				if(!"".equals(key) && key !=null){
					orderManager.delShopComment(key);
				}
			}
		Map map = new HashMap();
		map.put("code", "100");
		String resStr = JsonUtils.fromObject(map);
		return resStr;
    }
	
	/**
	 * 订单_导出
	 * @param response
	 * @param request
	 * @throws IOException 
	 */
	@RequestMapping(value="/order/exportWpsByOrder",  produces = "text/html;charset=UTF-8")
	@ResponseBody
	public void exportWpsByOrder(HttpServletResponse response, HttpServletRequest request) throws IOException{
		
		String proName = request.getParameter("proName");
		String cityName = request.getParameter("cityName");
		String SsUserName = request.getParameter("SsUserName");
		String SsShopName = request.getParameter("SsShopName");
		String SsOrderNum = request.getParameter("SsOrderNum");
		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");
		//获取分页参数
		Map pageParam = PageUtils.getPageParam(request);
		List<SysOrder> listRes = orderManager.getUserList(proName,cityName,SsUserName,SsShopName,SsOrderNum,startDate,endDate,new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));
		//创建一个新的 excel 
        HSSFWorkbook wb = new HSSFWorkbook();
        //创建 sheet 页
        HSSFSheet sheet = wb.createSheet("订单信息统计"); 
        HSSFRow row = sheet.createRow((int) 0);  
        //sheet.setColumnWidth((short) 0, (short) (35.7 * 150));
        //sheet.setColumnWidth((short)1, (short) 600);
       
        //设置水平居中
        HSSFCellStyle style = wb.createCellStyle();  
        
        //设置标题居中
        HSSFHeader header = sheet.getHeader();
        header.setCenter("订单信息统计");
        String[] excelHeader = new String[]{"商家名称","商户地址","订单号","用户名称","价格","支付方式","创建日期","状态"};
        for (int i = 0; i < excelHeader.length; i++) {  
            HSSFCell cell = row.createCell(i);  
            cell.setCellValue(excelHeader[i]);  
            cell.setCellStyle(style);  
            sheet.autoSizeColumn(i);  
        }  
        for (int i = 0; i < listRes.size(); i++) {  
            row = sheet.createRow(i + 1);  
            Map map = (Map)listRes.get(i);  
            row.createCell(0).setCellValue(map.get("shopName").toString());
            row.createCell(1).setCellValue(map.get("shopAddress").toString());
        	row.createCell(2).setCellValue(map.get("orderNum").toString());  
            row.createCell(3).setCellValue(map.get("userName").toString());
            row.createCell(4).setCellValue(map.get("orderPrice").toString());
            row.createCell(5).setCellValue(map.get("payFor").toString());
            row.createCell(6).setCellValue(map.get("createDate").toString());
            String isCommen = map.get("isCommen").toString();
            if(isCommen.equals("0")){
            	row.createCell(7).setCellValue("未评价");
            }else{
            	row.createCell(7).setCellValue("已评价");
            }
        }   
        sheet.autoSizeColumn((short)0);// 列表宽度自定义
        sheet.autoSizeColumn((short)1);// 列表宽度自定义
        sheet.autoSizeColumn((short)2);// 列表宽度自定义
        sheet.autoSizeColumn((short)3);// 列表宽度自定义
        sheet.autoSizeColumn((short)4);// 列表宽度自定义
        sheet.autoSizeColumn((short)5);// 列表宽度自定义
        sheet.autoSizeColumn((short)6);// 列表宽度自定义
        sheet.autoSizeColumn((short)7);// 列表宽度自定义
        
        style.setAlignment(HSSFCellStyle.ALIGN_CENTER);// 居中
        style.setAlignment(HSSFCellStyle.ALIGN_LEFT);// 居中
        style.setWrapText(true); 
        response.setContentType("application/vnd.ms-excel");    
        String excelName = "商户收入统计";
        response.setHeader("Content-disposition", "attachment;filename="+new String(excelName.getBytes(), "ISO8859-1")+".xls");    
        OutputStream ouputStream = response.getOutputStream();    
        wb.write(ouputStream);    
        ouputStream.flush();    
        ouputStream.close();  
	}
	
}

