package com.cosin.web.controller.order;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cosin.utils.JsonUtils;
import com.cosin.utils.PageUtils;
import com.cosin.web.entity.ShopActivity;
import com.cosin.web.entity.ShopType;
import com.cosin.web.entity.SysShop;
import com.cosin.web.service.order.IShopActivityManager;

@Scope("prototype")
@Controller
public class ShopActivityController {
	@Autowired
	private IShopActivityManager shopActivityManager;
	/**
	 * 跳转到意见反馈
	 * 崔青山
	 * 2016年7月22日
	 * @return
	 */
	@RequestMapping(value="/order/listShopActivity")
    public String index(){
        return "order/listShopActivity";
        
    }
	/**
	 * 获取帖子数据分页
	 * 崔青山
	 * 2016年7月22日
	 * @param response
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/order/shopActivityListData",  produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String postListData(HttpServletResponse response, HttpServletRequest request)
	{		
		String ssuser = request.getParameter("ssuser");	
		String ssksDate = request.getParameter("ssksDate");
		String ssjsDate = request.getParameter("ssjsDate");
		//获取分页参数
		Map pageParam = PageUtils.getPageParam(request);
		List<ShopActivity> listRes = shopActivityManager.getPriceList(ssuser,ssksDate,ssjsDate,new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));
		int totle =shopActivityManager.getPriceListFenye(ssuser,ssksDate,ssjsDate);
		Map mapRet = PageUtils.getPageParam(listRes, totle);
		String resStr = JsonUtils.fromObject(mapRet);
		return resStr;
	}
	
	
	/**
	 * 保存
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value="/order/issueShopActivity", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String issueShopActivity(HttpServletRequest request,HttpServletResponse response) throws Exception
	{	
		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");
		String content = request.getParameter("content");
		String shopActivityId=request.getParameter("shopActivityId");
		String shopId=request.getParameter("shopId");
		String mode=request.getParameter("mode");
		shopActivityManager.savepushMessage(startDate,endDate,content,shopActivityId,mode,shopId);
		Map map = new HashMap();
		map.put("code", 100);
		map.put("msg", "保存成功"); 
		String resStr = JsonUtils.fromObject(map);
		return resStr;
			
	}
	/**
	 * 删除
	 * 崔青山
	 * 2016年7月22日
	 * @param response
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/order/delshopActivity", produces = "text/html;charset=UTF-8")
	@ResponseBody
    public String delPost(HttpServletResponse response, HttpServletRequest request){
		String delKeys = request.getParameter("delKeys");
		String[] keyArr = delKeys.split(",");
		for(int i=0; i<keyArr.length; i++)
		{
			String key = keyArr[i];
			if(!"".equals(key) && key !=null)
				shopActivityManager.delPriceQiang(key);
		}
		Map map = new HashMap();
		map.put("code","100");
		String resStr = JsonUtils.fromObject(map);
		return resStr;
    }
	
	/**
	 * 查询商家
	 * @return
	 */
	@RequestMapping(value="/order/readShop", produces = "text/html;charset=UTF-8")
	@ResponseBody
    public String readFedback(HttpServletResponse response, HttpServletRequest request){
		List<SysShop> list = shopActivityManager.getSysShop();
		List lists = new ArrayList();
		for (int i = 0; i < list.size(); i++) {
			SysShop shop = list.get(i);
			Map map = new HashMap();
			map.put("userName",shop.getSysUser().getUserName());
			map.put("shopId",shop.getShopId());
			lists.add(map);
		}
		
		String resStr = JsonUtils.fromArrayObject(lists);
		return resStr;
    }
	

}