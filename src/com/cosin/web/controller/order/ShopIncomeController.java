package com.cosin.web.controller.order;

import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFHeader;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cosin.utils.JsonUtils;
import com.cosin.utils.PageUtils;
import com.cosin.web.entity.CommentShop;
import com.cosin.web.entity.PlatformStatistic;
import com.cosin.web.entity.ShopStatistic;
import com.cosin.web.entity.ShopType;
import com.cosin.web.entity.SysOrder;
import com.cosin.web.service.order.IShopIncomeManager;

@Scope("prototype")
@Controller
public class ShopIncomeController {
	@Autowired
	private IShopIncomeManager shopIncomeManager;
	/**
	 * 跳转到用户收入
	 * 崔青山
	 * 2016年7月22日
	 * @return
	 */
	@RequestMapping(value="/income/listShopIncome")
    public String index(){
        return "order/listShopIncome";
        
    }
	/**
	 * 获取商户收入
	 * 崔青山
	 * 2016年7月22日
	 * @param response
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/income/shopShopIncomeListData",  produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String postListData(HttpServletResponse response, HttpServletRequest request)
	{		
		String ssuser = request.getParameter("ssuser");	
		String beginDate = request.getParameter("beginDate");
		String endDate = request.getParameter("endDate");
		String proName = request.getParameter("proName");
		String cityName = request.getParameter("cityName");
		String shopTypeId = request.getParameter("shopTypeId");
		//获取分页参数
		Map pageParam = PageUtils.getPageParam(request);
		List<ShopStatistic> listRes = shopIncomeManager.getPriceList(ssuser,beginDate,endDate,proName,cityName,shopTypeId,new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));
		int totle =shopIncomeManager.getPriceListFenye(ssuser,beginDate,endDate,proName,cityName,shopTypeId);
		Map mapRet = PageUtils.getPageParam(listRes, totle);
		String resStr = JsonUtils.fromObject(mapRet);
		return resStr;
	}
	
	

	/**
	 * 删除
	 * 崔青山
	 * 2016年7月22日
	 * @param response
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/income/delshopIncome", produces = "text/html;charset=UTF-8")
	@ResponseBody
    public String delPost(HttpServletResponse response, HttpServletRequest request){
		String delKeys = request.getParameter("delKeys");
		String[] keyArr = delKeys.split(",");
		for(int i=0; i<keyArr.length; i++)
		{
			String key = keyArr[i];
			if(!"".equals(key) && key !=null)
				shopIncomeManager.delPriceQiang(key);
		}
		Map map = new HashMap();
		map.put("code","100");
		String resStr = JsonUtils.fromObject(map);
		return resStr;
    }
	
	
	/**
	 * 订单详情 查看 ——商户收入统计
	 * @param response
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/income/chaShopOrder",  produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String shopCommentListData(HttpServletResponse response, HttpServletRequest request)
	{		
		String orderNum = request.getParameter("soOrderNum");
		String startDate = request.getParameter("startDateOrder");	
		String endDate = request.getParameter("endDateOrder");	
		String shopId = request.getParameter("shopIdd");
		//获取分页参数
		Map pageParam = PageUtils.getPageParam(request);
		List<SysOrder> listRes = shopIncomeManager.getShopOrderList(orderNum,startDate,endDate,shopId,new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));
		int totle =shopIncomeManager.getShopOrderCount(orderNum,startDate,endDate,shopId);
		Map mapRet = PageUtils.getPageParam(listRes, totle);
		String resStr = JsonUtils.fromObject(mapRet);
		return resStr;
	}
	
	
	/**
	 * 2017年1月4日下午3:28:49
	 *  阳朔
	 *  注释: 商户 类型 查询
	 */
	@RequestMapping(value="/statistics/chaShopType",  produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String chaShopType(HttpServletResponse response, HttpServletRequest request)
	{		
		Map pageParam = PageUtils.getPageParam(request);
		List<ShopType> listRes = shopIncomeManager.getShopTypeList();
		String resStr = JsonUtils.fromArrayObject(listRes);
		return resStr;
	}
	
	
	/**
	 * 2017年1月4日下午3:28:27
	 *  阳朔
	 *  注释: 跳转到平台统计
	 */
	@RequestMapping(value="/income/platformIncome")
    public String platformIncome(){
        return "order/platformIncome";
        
    }
	
	/**
	 * 2017年1月4日下午3:28:49
	 *  阳朔
	 *  注释: 平台统计 分页查询
	 */
	@RequestMapping(value="/income/platformIncomeData",  produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String platformIncomeData(HttpServletResponse response, HttpServletRequest request)
	{		
		String beginDate = request.getParameter("beginDate");
		String endDate = request.getParameter("endDate");
		//获取分页参数
		Map pageParam = PageUtils.getPageParam(request);
		List<PlatformStatistic> listRes = shopIncomeManager.getPlatformStatisticList(beginDate,endDate,new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));
		int totle =shopIncomeManager.getPlatformStatisticCount(beginDate,endDate);
		Map mapRet = PageUtils.getPageParam(listRes, totle);
		String resStr = JsonUtils.fromObject(mapRet);
		return resStr;
	}
	
	/**
	 * 2017年1月4日下午3:28:49
	 *  阳朔
	 *  注释: 平台统计 收入 总数
	 */
	@RequestMapping(value="/income/platformStatisticAll",  produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String platformStatisticAll(HttpServletResponse response, HttpServletRequest request)
	{		
		String beginDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");
		
		Map map = shopIncomeManager.getPlatformStatisticAll(beginDate,endDate);
		
		String resStr = JsonUtils.fromObject(map);
		return resStr;
	}
	
	/**
	 * 2017年1月4日下午3:28:49
	 *  阳朔
	 *  注释: 折线图 接口 按年份 月份 数量
	 */
	@RequestMapping(value="/income/YearAndMonthSelectOrderNum",  produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String YearAndMonthSelectOrderNum(HttpServletResponse response, HttpServletRequest request)
	{		
		String shopId = request.getParameter("shopId");
		String forYear = request.getParameter("forYear");
		String forMonth = request.getParameter("forMonth");
		//获取分页参数
		List listRes = shopIncomeManager.getYearAndMonthSelectOrderNum(shopId,forYear,forMonth);
		String resStr = JsonUtils.fromArrayObject(listRes);
		return resStr;
	}
	
	/**
	 * 查询 平台 图形 数据
	 */
	@RequestMapping(value="/income/selectTuForPlay",  produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String selectTuForPlay(HttpServletResponse response, HttpServletRequest request)
	{		
		String forYear = request.getParameter("forYear");
		String forMonth = request.getParameter("forMonth");
		//获取分页参数
		List listRes = shopIncomeManager.getselectTuForPlay(forYear,forMonth);
		String resStr = JsonUtils.fromArrayObject(listRes);
		return resStr;
	}
	
	
	
	/**
	 * 商户_导出
	 * @param response
	 * @param request
	 * @throws IOException 
	 */
	@RequestMapping(value="/statistics/exportWpsByShop",  produces = "text/html;charset=UTF-8")
	@ResponseBody
	public void exportWpsByShop(HttpServletResponse response, HttpServletRequest request) throws IOException{
		
		String ssuser = request.getParameter("ssuser");	
		String beginDate = request.getParameter("beginDate");
		String endDate = request.getParameter("endDate");
		String proName = request.getParameter("proName");
		String cityName = request.getParameter("cityName");
		String shopTypeId = request.getParameter("shopTypeId");
		//获取分页参数
		Map pageParam = PageUtils.getPageParam(request);
		List<ShopStatistic> listRes = shopIncomeManager.getPriceList(ssuser,beginDate,endDate,proName,cityName,shopTypeId,new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));
		//创建一个新的 excel 
        HSSFWorkbook wb = new HSSFWorkbook();
        //创建 sheet 页
        HSSFSheet sheet = wb.createSheet("商户收入统计"); 
        HSSFRow row = sheet.createRow((int) 0);  
        //sheet.setColumnWidth((short) 0, (short) (35.7 * 150));
        //sheet.setColumnWidth((short)1, (short) 600);
       
        //设置水平居中
        HSSFCellStyle style = wb.createCellStyle();  
        
        
        //设置标题居中
        HSSFHeader header = sheet.getHeader();
        header.setCenter("商户收入统计");
        String[] excelHeader = new String[]{"商家名称","商户类型","地址","本店消费总金额/元","收入总金额/元","订单量"};
        for (int i = 0; i < excelHeader.length; i++) {  
            HSSFCell cell = row.createCell(i);  
            cell.setCellValue(excelHeader[i]);  
            cell.setCellStyle(style);  
            sheet.autoSizeColumn(i);  
        }  
        for (int i = 0; i < listRes.size(); i++) {  
            row = sheet.createRow(i + 1);  
            Map map = (Map)listRes.get(i);  
            row.createCell(0).setCellValue(map.get("shopName").toString());
            row.createCell(1).setCellValue(map.get("typeName").toString());
        	row.createCell(2).setCellValue(map.get("shopAddress").toString());  
            row.createCell(3).setCellValue(map.get("price").toString());
            row.createCell(4).setCellValue(map.get("income").toString());
            row.createCell(5).setCellValue(map.get("num").toString());
        }   
        sheet.autoSizeColumn((short)0);// 列表宽度自定义
        sheet.autoSizeColumn((short)1);// 列表宽度自定义
        sheet.autoSizeColumn((short)2);// 列表宽度自定义
        sheet.autoSizeColumn((short)3);// 列表宽度自定义
        sheet.autoSizeColumn((short)4);// 列表宽度自定义
        sheet.autoSizeColumn((short)5);// 列表宽度自定义
        style.setAlignment(HSSFCellStyle.ALIGN_CENTER);// 居中
        style.setAlignment(HSSFCellStyle.ALIGN_LEFT);// 居中
        style.setWrapText(true); 
        response.setContentType("application/vnd.ms-excel");    
        String excelName = "商户收入统计";
        response.setHeader("Content-disposition", "attachment;filename="+new String(excelName.getBytes(), "ISO8859-1")+".xls");    
        OutputStream ouputStream = response.getOutputStream();    
        wb.write(ouputStream);    
        ouputStream.flush();    
        ouputStream.close();  
	}
	
	
}