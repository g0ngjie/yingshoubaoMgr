package com.cosin.web.controller.order;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cosin.utils.JsonUtils;
import com.cosin.utils.PageUtils;
import com.cosin.utils.UserSession;
import com.cosin.web.entity.UserStatistic;
import com.cosin.web.service.order.IUserIncomeManager;

@Scope("prototype")
@Controller
public class UserIncomeController {
	@Autowired
	private IUserIncomeManager userIncomeManager;
	/**
	 * 跳转用户收入统计
	 * @return
	 */
	@RequestMapping(value="/income/userIncome")
    public String userIncome(){
        return "order/userIncome";
        
    }
	
	
	/**
	 * 用户收入统计
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/income/userIncomeData",  produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String userIncomeData(HttpServletResponse response, HttpServletRequest request)
	{			
		String ssuser = request.getParameter("ssuser");
		String beginDate = request.getParameter("beginDate");
		String endDate = request.getParameter("endDate");
		//获取分页参数
		Map pageParam = PageUtils.getPageParam(request);
		List<UserStatistic> listRes = userIncomeManager.getUserStatisticList(ssuser,beginDate,endDate,new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));
		int totle =userIncomeManager.getUserStatisticCount(ssuser,beginDate,endDate);
		Map mapRet = PageUtils.getPageParam(listRes, totle);
		String resStr = JsonUtils.fromObject(mapRet);//过滤hiberte对象中不需要生成json的属性
		return resStr;
	}

	/**
	 * 修改密码
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/income/passwordEdit",  produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String passwordEdit(HttpServletResponse response, HttpServletRequest request)
	{			
		UserSession userSession = (UserSession) request.getSession().getAttribute("userSession");
		String userId = userSession.userId;
		String beginPassword = request.getParameter("beginPassword");
		String endPassword = request.getParameter("endPassword");
		String doublePassword = request.getParameter("doublePassword");
		
		Map map = userIncomeManager.passwordEdit(userId,beginPassword,endPassword,doublePassword);
		String resStr = JsonUtils.fromObject(map);
		return resStr;
	}
	
	/**
	 * 用户 查看 三级粉丝 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/income/selectFansThree",  produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String selectFansThree(HttpServletResponse response, HttpServletRequest request)
	{			
		String userId = request.getParameter("userId");
		List list = userIncomeManager.selectFansThree(userId);
		String resStr = JsonUtils.fromArrayObject(list);
		return resStr;
	}
	
	
}

