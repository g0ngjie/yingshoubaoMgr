package com.cosin.web.controller.pushMessage;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cosin.utils.JsonUtils;
import com.cosin.utils.PageUtils;
import com.cosin.utils.ShortMessageUtils;
import com.cosin.utils.UserSession;
import com.cosin.web.entity.FeedBack;
import com.cosin.web.service.pushMessage.IFedbackManager;

@Scope("prototype")
@Controller
public class FedbackController {
	@Autowired
	private IFedbackManager fedbackManager;
	/**
	 * 跳转到意见反馈
	 * 崔青山
	 * 2016年7月22日
	 * @return
	 */
	@RequestMapping(value="/fedback/listFedback")
    public String index(){
        return "fedback/listFedback";
        
    }
	/**
	 * 获取帖子数据分页
	 * 崔青山
	 * 2016年7月22日
	 * @param response
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/fedback/fedbackListData",  produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String postListData(HttpServletResponse response, HttpServletRequest request)
	{		
		String ssuser = request.getParameter("ssuser");	
		String ssDate = request.getParameter("ssDate");
		//获取分页参数
		Map pageParam = PageUtils.getPageParam(request);
		List<FeedBack> listRes = fedbackManager.getPriceList(ssuser,ssDate,new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));
		int totle =fedbackManager.getPriceListFenye(ssuser,ssDate);
		Map mapRet = PageUtils.getPageParam(listRes, totle);
		String resStr = JsonUtils.fromObject(mapRet);
		return resStr;
	}
	
	/**
	 * 删除
	 * 崔青山
	 * 2016年7月22日
	 * @param response
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/fedback/delFedback", produces = "text/html;charset=UTF-8")
	@ResponseBody
    public String delPost(HttpServletResponse response, HttpServletRequest request){
		UserSession userSession = (UserSession) request.getSession().getAttribute("userSession");
		String loginUserId = userSession.userId;
		String delKeys = request.getParameter("delKeys");
		String[] keyArr = delKeys.split(",");
		for(int i=0; i<keyArr.length; i++)
		{
			String key = keyArr[i];
			if(!"".equals(key) && key !=null)
				fedbackManager.delPriceQiang(loginUserId,key);
		}
		Map map = new HashMap();
		map.put("code","100");
		String resStr = JsonUtils.fromObject(map);
		return resStr;
    }
	
	/**
	 * 标记已读
	 * 崔青山
	 * 2016年9月1日
	 * @param response
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/fedback/readFedback", produces = "text/html;charset=UTF-8")
	@ResponseBody
    public String readFedback(HttpServletResponse response, HttpServletRequest request){
		String delKeys = request.getParameter("delKeys");
		String[] keyArr = delKeys.split(",");
			for(int i=0; i<keyArr.length; i++)
			{
				String key = keyArr[i];
				if(!"".equals(key) && key !=null)
					fedbackManager.readPriceQiang(key);
			}
		Map map = new HashMap();
		map.put("code","100");
		String resStr = JsonUtils.fromObject(map);
		return resStr;
    }
	
	/**
	 * 2017年1月8日下午3:39:24
	 *  阳朔
	 *  注释:反馈 回复 用户 
	 */
	@RequestMapping(value="/fedback/sendMsg", produces = "text/html;charset=UTF-8")
	@ResponseBody
    public String sendMsg(HttpServletResponse response, HttpServletRequest request){
		UserSession userSession = (UserSession) request.getSession().getAttribute("userSession");
		String loginUserId = userSession.userId;
		String feedbackId = request.getParameter("feedbackId");
		String sendContent = request.getParameter("sendContent");
		Map map = fedbackManager.sendMsg(loginUserId,feedbackId,sendContent);
		String resStr = JsonUtils.fromObject(map);
		return resStr;
    }
	
	

}