package com.cosin.web.controller.record;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cosin.utils.JsonUtils;
import com.cosin.utils.PageUtils;
import com.cosin.utils.UserSession;
import com.cosin.web.entity.WithdrawCashRecord;
import com.cosin.web.service.record.IRecordManager;

@Scope("prototype")
@Controller
public class RecordController {
	@Autowired
	private IRecordManager recordManager;
	/**
	 * 跳转到提现记录
	 * @return
	 */
	@RequestMapping(value="/record/listRecord.do")
    public String indexs(){
        return "/record/listrecord";
    }
	
	/**
	 * 跳转到审核
	 * 崔青山
	 * 2016年8月21日
	 * @return
	 */
	@RequestMapping(value="/record/listShenhe.do")
    public String index(){
        return "/record/listShenhe";
    }
	
	
	
	/**
	 * 获取交易记录消息数据分页
	 * @param request
	 * @param responsess
	 * @return
	 */
	@RequestMapping(value="/record/recordListData",  produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String recordListData(HttpServletResponse response, HttpServletRequest request)
	{			
		String Ssuser = request.getParameter("Ssuser");
		String SsJia = request.getParameter("SsJia");
		//获取分页参数
		Map pageParam = PageUtils.getPageParam(request);
		List<WithdrawCashRecord> listRes = recordManager.getUserList(Ssuser,SsJia,new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));
		int totle =recordManager.getinfofenye(Ssuser,SsJia);
		Map mapRet = PageUtils.getPageParam(listRes, totle);
		String resStr = JsonUtils.fromObject(mapRet);//过滤hiberte对象中不需要生成json的属性
		return resStr;
	}
	
	/**
	 * 查询审核数据 
	 * 崔青山
	 * 2016年8月21日
	 * @param response
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/record/shenheListData",  produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String shenheListData(HttpServletResponse response, HttpServletRequest request)
	{			
		String Ssuser = request.getParameter("Ssuser");
		String SsJia = request.getParameter("SsJia");
		//获取分页参数
		Map pageParam = PageUtils.getPageParam(request);
		List<WithdrawCashRecord> listRes = recordManager.getShenheList(Ssuser,SsJia,new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));
		int totle =recordManager.getinfofenyess(Ssuser,SsJia);
		Map mapRet = PageUtils.getPageParam(listRes, totle);
		String resStr = JsonUtils.fromObject(mapRet);//过滤hiberte对象中不需要生成json的属性
		return resStr;
	}
	
	
	
	/**
	 * 通过
	 * 崔青山
	 * 2016年8月21日
	 * @param response
	 * @param request
	 * @return
	 */
	 @RequestMapping(value="/record/passRecord",  produces = "text/html;charset=UTF-8")
	 @ResponseBody
	 public String delMedia(HttpServletResponse response, HttpServletRequest request){
		UserSession userSession = (UserSession) request.getSession().getAttribute("userSession");
		String loginUserId = userSession.userId;
		String delKeys = request.getParameter("delKeys");
		String[] keyArr = delKeys.split(",");
		Map map = new HashMap();
		for(int i=0; i<keyArr.length; i++)
		{
			String key = keyArr[i];
			if(!"".equals(key) && key !=null){
				map = recordManager.tgShenhe(loginUserId,key);
			}
		}
		String resStr = JsonUtils.fromObject(map);
		return resStr;
	  }
	 
	 /**
	  * 拒绝
	  * 崔青山
	  * 2016年8月21日
	  * @param response
	  * @param request
	  * @return
	  */
	 @RequestMapping(value="/record/refuseRecord",  produces = "text/html;charset=UTF-8")
	 @ResponseBody
	 public String refuseRecord(HttpServletResponse response, HttpServletRequest request){
		 UserSession userSession = (UserSession) request.getSession().getAttribute("userSession");
		 String loginUserId = userSession.userId;
			String delKeys = request.getParameter("delKeys");
			String reason = request.getParameter("reason");
			String[] keyArr = delKeys.split(",");
			Map map = new HashMap();
			for(int i=0; i<keyArr.length; i++)
			{
				
				String key = keyArr[i];
				if(!"".equals(key) && key !=null){
					WithdrawCashRecord record = recordManager.fanQian(key);
					map = recordManager.jjShenhe(loginUserId,key,record.getPrice(),reason);
				}
			}
			
			String resStr = JsonUtils.fromObject(map);
			return resStr;
	    }
	 
	 /** 
		 * 删除信息
		 * @param response
		 * @param request
		 * @return
		 */
		@RequestMapping(value="/record/shachuRecord", produces = "text/html;charset=UTF-8")
		@ResponseBody
	    public String shachuMessage(HttpServletResponse response, HttpServletRequest request){
			UserSession userSession = (UserSession) request.getSession().getAttribute("userSession");
			 String loginUserId = userSession.userId;
			String delKeys = request.getParameter("delKeys");
			String[] keyArr = delKeys.split(",");
			for(int i=0; i<keyArr.length; i++)
			{
				String key = keyArr[i];
				if(!"".equals(key) && key !=null)
					recordManager.delPush(loginUserId,key);
			}
			Map map = new HashMap();
			map.put("code", 100);
			map.put("msg", "删除成功");
			String resStr = JsonUtils.fromObject(map);
			return resStr;
	    }
		
	
}

