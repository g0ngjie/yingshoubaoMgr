package com.cosin.web.controller.saleRatio;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cosin.utils.JsonUtils;
import com.cosin.utils.PageUtils;
import com.cosin.utils.UserSession;
import com.cosin.web.entity.Ratio;
import com.cosin.web.service.saleRatio.IRatioManager;

@Scope("prototype")
@Controller
public class RatioController {
	@Autowired
	private IRatioManager radioManager;
	/**
	 * 跳转到商家比例
	 * @return
	 */
	@RequestMapping(value="/saleRatio/shopRatio.do")
    public String shopRatio(){
        return "saleRatio/shopRatio";
    }
	
	/**
	 * 跳转到三级分销比例
	 * @return
	 */
	@RequestMapping(value="/saleRatio/threeRatio.do")
    public String threeRatio(){
        return "saleRatio/threeRatio";
    }
	
	/**
	 * 获取商家数据分页
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/saleRatio/ratioList",  produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String ratioList(HttpServletResponse response, HttpServletRequest request)
	{			
		//获取分页参数
		Map pageParam = PageUtils.getPageParam(request);
		List<Ratio> listRes = radioManager.getRatioListLimit(new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));
		List list = new ArrayList();
		for (int i = 0; i < listRes.size(); i++) {
			Map<Object,Object> map = new HashMap<Object, Object>();
			Ratio ratio = listRes.get(i);
			Double onece = ratio.getOnece();
			map.put("ratioId", ratio.getRatioId());
			map.put("result", onece);
			map.put("palyBili", ratio.getSecond());
			list.add(map);
		}
		int totle =radioManager.getRatioListCount();
		Map mapRet = PageUtils.getPageParam(list, totle);
		String resStr = JsonUtils.fromObject(mapRet);
		return resStr;
	}
	
	/**
	 * 修改-查询:店铺佣金比例
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/saleRatio/shopRatioBj",  produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String shopRatioBj(HttpServletResponse response, HttpServletRequest request)
	{			
		String ratioId = request.getParameter("ratioId");
		Ratio ratio = radioManager.getRatioById(ratioId);
		Map<Object,Object> map = new HashMap<Object, Object>();
		Double once = ratio.getOnece();
		map.put("once", once);
		map.put("playbili", ratio.getSecond());
		map.put("ratioId", ratioId);
		Double second = ratio.getSecond();
		Double third = ratio.getThird();
		if(second != null){
			map.put("second", second);
			map.put("third", third);
		}
		
		String resStr = JsonUtils.fromObject(map);
		return resStr;
	}
	
	/**
	 * 修改-实现:店铺佣金比例
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/saleRatio/shopRatioEdit",  produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String shopRatioEdit(HttpServletResponse response, HttpServletRequest request)
	{			
		UserSession userSession = (UserSession) request.getSession().getAttribute("userSession");
		String loginUserId = userSession.userId;
		String ratioId = request.getParameter("ratioId");
		String bili = request.getParameter("bili");
		String onebili = request.getParameter("onebili");
		String twobili = request.getParameter("twobili");
		String threebili = request.getParameter("threebili");
		//String result_playBili = request.getParameter("result_playBili");
		String result_playBili = request.getParameter("playbili");
		radioManager.shopRatioEdit(loginUserId,ratioId,bili,onebili,twobili,threebili,result_playBili);
		Map map = new HashMap();
		map.put("code", 100);
		map.put("msg", "保存成功");
		String resStr = JsonUtils.fromObject(map);
		return resStr;
	}
	
	
	/**
	 * 获取三级分销数据分页
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/saleRatio/ratioThreeData",  produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String ratioThreeData(HttpServletResponse response, HttpServletRequest request)
	{			
		//获取分页参数
		Map pageParam = PageUtils.getPageParam(request);
		List<Ratio> listRes = radioManager.getThreeDataLimit(new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));
		int totle =radioManager.getThreeDataCount();
		Map mapRet = PageUtils.getPageParam(listRes, totle);
		String resStr = JsonUtils.fromObject(mapRet);
		return resStr;
	}
	
	/**
	 * 获取可用比例额度
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/saleRatio/formaBili",  produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String formaBili(HttpServletResponse response, HttpServletRequest request)
	{			
		Ratio ratio = radioManager.getRatioFormaBili();
		Map<Object,Object> map = new HashMap<Object, Object>();
		map.put("busiBili", ratio.getOnece());
		map.put("playBili", ratio.getSecond());
		String resStr = JsonUtils.fromObject(map);
		return resStr;
	}
	
	
}

