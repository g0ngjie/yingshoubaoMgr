package com.cosin.web.controller.shop;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cosin.utils.JsonUtils;
import com.cosin.utils.PageUtils;
import com.cosin.utils.UserSession;
import com.cosin.web.entity.QrBackimg;
import com.cosin.web.entity.ShopType;
import com.cosin.web.entity.SysShop;
import com.cosin.web.service.shop.IServiceManager;

@Scope("prototype")
@Controller
public class ServiceController {
	@Autowired
	private IServiceManager serviceManager;
	/**
	 * 跳转到类型管理
	 * @return
	 */
	@RequestMapping(value="/shopType/listService.do")
    public String indexs(){
        return "/shop/listService";
    }
	
	/**
	 * 获取消息数据分页
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/shopType/serviceListData",  produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String engineerListData(HttpServletResponse response, HttpServletRequest request)
	{			
		//获取分页参数
		Map pageParam = PageUtils.getPageParam(request);
		List<ShopType> listRes = serviceManager.getUserList(new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));
		int totle =serviceManager.getinfofenye();
		Map mapRet = PageUtils.getPageParam(listRes, totle);
		String resStr = JsonUtils.fromObject(mapRet);//过滤hiberte对象中不需要生成json的属性
		return resStr;
	}

	/** 
	 * 删除信息
	 * @param response
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/shopType/shachuMessage", produces = "text/html;charset=UTF-8")
	@ResponseBody
    public String shachuMessage(HttpServletResponse response, HttpServletRequest request){
		UserSession userSession = (UserSession) request.getSession().getAttribute("userSession");
		String loginUserId = userSession.userId;
		String delKeys = request.getParameter("delKeys");
		Map map = new HashMap();
		int num = serviceManager.chaSysShopByShopTypeId(delKeys);
		if(num == 1)
		{
			map.put("code", 101);
			map.put("msg", "该类型已被使用,不可被删除!"); 
		}
		else
		{
			map.put("code", 100);
			map.put("msg", "保存成功"); 
			serviceManager.delPush(loginUserId,delKeys);
		}
		String resStr = JsonUtils.fromObject(map);
		return resStr;
    }

	/**
	 * 保存
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value="/shopType/saveType", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String saveUser(HttpServletRequest request,HttpServletResponse response) throws Exception
	{	
		UserSession userSession = (UserSession) request.getSession().getAttribute("userSession");
		String loginUserId = userSession.userId;
		String title = request.getParameter("title");
		String imgCns = request.getParameter("imgCns");
		String type = request.getParameter("type");
		String enable = request.getParameter("enable");
		String shopTypeId=request.getParameter("shopTypeId");
		String mode = request.getParameter("mode");
		List<ShopType> list = serviceManager.getShopType();
		if(list.size()<8 || "edit".equals(mode)){
			serviceManager.savepushMessage(loginUserId,title,imgCns,type,enable,shopTypeId,mode);
			Map map = new HashMap();
			map.put("code", 100);
			map.put("msg", "保存成功"); 
			String resStr = JsonUtils.fromObject(map);
			return resStr;
		}
		else
		{
			Map map = new HashMap();
			map.put("code", 101);
			map.put("msg", "保存失败，超出限制上限。"); 
			String resStr = JsonUtils.fromObject(map);
			return resStr;
		}
			
	}
	/**
	 * 查看
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/shopType/bianji",produces = "text/html;charset=UTF-8")
	@ResponseBody
    public String bianji(HttpServletRequest request)
	{
		String shopTypeId = request.getParameter("shopTypeId");
		ShopType push = serviceManager.getShopTypeId(shopTypeId);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("typeName", push.getTypeName());
		map.put("img", push.getImg());
		map.put("enable", push.getEnable());
		map.put("orderNum", push.getOrderNum());
		map.put("shopTypeId", push.getShopTypeId());
		map.put("code", "100");
		String resStr = JsonUtils.fromObject(map);
        return resStr;
	}
	
	/**
	 * 跳转到分享背景管理
	 * @return
	 */
	@RequestMapping(value="/fans/qrcode_back")
    public String qrcode_back(){
        return "/shop/qrCodeImg";
    }
	
	
	/**
	 * 获取二维码 数据分页
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/fans/selectQrBackImg",  produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String selectQrBackImg(HttpServletResponse response, HttpServletRequest request)
	{			
		//获取分页参数
		Map pageParam = PageUtils.getPageParam(request);
		List<QrBackimg> listRes = serviceManager.selectQrBackImgList(new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));
		int totle =serviceManager.getQrBackimgCount();
		Map mapRet = PageUtils.getPageParam(listRes, totle);
		String resStr = JsonUtils.fromObject(mapRet);
		return resStr;
	}
	
	/**
	 * 保存 二维码 背景图
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value="/fans/upQrBackImg", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String upQrBackImg(HttpServletRequest request,HttpServletResponse response) throws Exception
	{	
		UserSession userSession = (UserSession) request.getSession().getAttribute("userSession");
		String loginUserId = userSession.userId;
		String imgCns = request.getParameter("imgCns");
		String mode = request.getParameter("mode");
		String qrBackImgId = request.getParameter("qrBackImgId");
		serviceManager.saveQrBackImg(loginUserId,imgCns,mode,qrBackImgId);
		Map map = new HashMap();
		map.put("code", 100);
		map.put("msg", "保存成功"); 
		String resStr = JsonUtils.fromObject(map);
		return resStr;
	
			
	}
	
	/**
	 * 启用 二维码  背景
	 */
	@RequestMapping(value="/fans/shopAbles", produces = "text/html;charset=UTF-8")
	@ResponseBody
    public String shopAbles(HttpServletResponse response, HttpServletRequest request){
		UserSession userSession = (UserSession) request.getSession().getAttribute("userSession");
		String loginUserId = userSession.userId;
		String delKeys = request.getParameter("delKeys");
		String[] keyArr = delKeys.split(",");
		for(int i=0; i<keyArr.length; i++)
		{
			
			String key = keyArr[i];
			if(!"".equals(key) && key !=null){
				serviceManager.QrBackimgAbles(loginUserId,key);
			}
		}
		Map map = new HashMap();
		map.put("code", 100);
		map.put("msg", "启用成功");
		String resStr = JsonUtils.fromObject(map);
		return resStr;
    }

	/**
	 * 禁用 二维码 背景
	 */
	@RequestMapping(value="/fans/disablesShops", produces = "text/html;charset=UTF-8")
	@ResponseBody
    public String disablesShops(HttpServletResponse response, HttpServletRequest request){
		UserSession userSession = (UserSession) request.getSession().getAttribute("userSession");
		String loginUserId = userSession.userId;
		String delKeys = request.getParameter("delKeys");
		String[] keyArr = delKeys.split(",");
		for(int i=0; i<keyArr.length; i++)
		{
			
			String key = keyArr[i];
			if(!"".equals(key) && key !=null)
				serviceManager.disableQr(loginUserId,key);
		}
		Map map = new HashMap();
		map.put("code", 100);
		map.put("msg", "启用成功");
		String resStr = JsonUtils.fromObject(map);
		return resStr;
    }
	
	/**
	 *	二维码 背景 编辑 
	 */
	@RequestMapping(value="/fans/qrBianji", produces = "text/html;charset=UTF-8")
	@ResponseBody
    public String qrBianji(HttpServletResponse response, HttpServletRequest request){
		String qrBackImgId = request.getParameter("qrBackImgId");
		Map map = serviceManager.getQrBianjiById(qrBackImgId);
		String resStr = JsonUtils.fromObject(map);
		return resStr;
    }
	
	/**
	 *	二维码 查看
	 */
	@RequestMapping(value="/fans/openLook", produces = "text/html;charset=UTF-8")
	@ResponseBody
    public String openLook(HttpServletResponse response, HttpServletRequest request){
		String qrBackImgId = request.getParameter("qrBackImgId");
		Map map = serviceManager.getQrBianjiById(qrBackImgId);
		String resStr = JsonUtils.fromObject(map);
		return resStr;
    }
	
	/**
	 *	二维码 删除
	 */
	@RequestMapping(value="/fans/qrDel", produces = "text/html;charset=UTF-8")
	@ResponseBody
    public String qrDel(HttpServletResponse response, HttpServletRequest request){
		String qrBackImgId = request.getParameter("qrBackImgId");
		Map map = serviceManager.qrDel(qrBackImgId);
		String resStr = JsonUtils.fromObject(map);
		return resStr;
    }
	
}

