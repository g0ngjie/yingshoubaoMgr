package com.cosin.web.controller.shop;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cosin.utils.JsonUtils;
import com.cosin.utils.PageUtils;
import com.cosin.utils.UserSession;
import com.cosin.web.controller.log.ILogManager;
import com.cosin.web.controller.log.SystemLogUtils;
import com.cosin.web.dao.BaseDao;
import com.cosin.web.entity.CommentShop;
import com.cosin.web.entity.SysShop;
import com.cosin.web.entity.SysUser;
import com.cosin.web.service.shop.IShopManager;

@Scope("prototype")
@Controller
public class ShopController extends BaseDao{
	@Autowired
	private IShopManager shopManager;
	@Autowired
	private ILogManager logManager;
	
	/**
	 * 跳转到商户核审
	 * @return
	 */
	@RequestMapping(value="/shop/listShopReviewed")
    public String index(HttpServletRequest request){
        return "shop/listShopReviewed";
        
    }
	/**
	 * 跳转到商户
	 * 崔青山
	 * 2016年11月21日
	 * @return
	 */
	@RequestMapping(value="/shop/listShop")
    public String indexs(HttpServletRequest request){
        return "shop/listShop";
        
    }
	
	/**
	 * 跳转到用户
	 * 崔青山
	 * 2016年11月21日
	 * @return
	 */
	@RequestMapping(value="/shop/listUser")
    public String indes(HttpServletRequest request){
        return "shop/listUser";
        
    }
	/**
	 * 获取核审商家数据分页
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/shop/shopListData",  produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String shopListData(HttpServletResponse response, HttpServletRequest request)
	{		
		String Name = request.getParameter("Name");	
		String tel = request.getParameter("tel");	
		String SsEnable = request.getParameter("SsEnable");
		//获取分页参数
		Map pageParam = PageUtils.getPageParam(request);
		List<SysUser> listRes = shopManager.getSysUserList(Name,tel,SsEnable,new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));
		int totle =shopManager.getShopfenye(Name,tel,SsEnable);
		Map mapRet = PageUtils.getPageParam(listRes, totle);
		String resStr = JsonUtils.fromObject(mapRet);
		return resStr;
	}
	

	/**
	 * 获取商家数据分页
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/shop/shopListDatas",  produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String shopListDatas(HttpServletResponse response, HttpServletRequest request)
	{		
		String Name = request.getParameter("Name");	
		String tel = request.getParameter("tel");	
		String SsEnable = request.getParameter("SsEnable");
		//获取分页参数
		Map pageParam = PageUtils.getPageParam(request);
		List<SysUser> listRes = shopManager.getShopLists(Name,tel,SsEnable,new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));
		int totle =shopManager.getShopfenyes(Name,tel,SsEnable);
		Map mapRet = PageUtils.getPageParam(listRes, totle);
		String resStr = JsonUtils.fromObject(mapRet);
		return resStr;
	}
	
	
	/**
	 * 普通用户数据分页
	 * 崔青山
	 * 2016年11月21日
	 * @param response
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/shop/shopuserListDatas",  produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String shopuserListDatas(HttpServletResponse response, HttpServletRequest request)
	{		
		String SsName = request.getParameter("SsName");	
		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");
		//获取分页参数
		Map pageParam = PageUtils.getPageParam(request);
		List<SysUser> listRes = shopManager.getShopUserLists(SsName,startDate,endDate,new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));
		int totle =shopManager.getShopUserfenyes(SsName,startDate,endDate);
		Map mapRet = PageUtils.getPageParam(listRes, totle);
		String resStr = JsonUtils.fromObject(mapRet);
		return resStr;
	}
	
	/**
	 * 通过核审
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/shop/passShop", produces = "text/html;charset=UTF-8")
	@ResponseBody
    public String saveqyRole(HttpServletResponse response, HttpServletRequest request){
		UserSession userSession = (UserSession) request.getSession().getAttribute("userSession");
		String userId = userSession.userId;
		String delKeys = request.getParameter("delKeys");
		Map map = new HashMap();
		map = shopManager.passReviewed(userId,delKeys);
		String resStr = JsonUtils.fromObject(map);
		return resStr;
    }
	/**
	 * 驳回核审
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/shop/refuseShop", produces = "text/html;charset=UTF-8")
	@ResponseBody
    public String refuseShop(HttpServletResponse response, HttpServletRequest request){
		UserSession userSession = (UserSession) request.getSession().getAttribute("userSession");
		String userId = userSession.userId;
		String delKeys = request.getParameter("delKeys");
		String reason = request.getParameter("reason");
		Map map = shopManager.refuseReviewed(userId,delKeys,reason);
		
		String resStr = JsonUtils.fromObject(map);
		return resStr;
    }

	/** 
	 * 删除审核商家
	 * @param response
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/shop/delShopShen", produces = "text/html;charset=UTF-8")
	@ResponseBody
    public String delShopShen(HttpServletResponse response, HttpServletRequest request){
		String delKeys = request.getParameter("delKeys");
		String[] keyArr = delKeys.split(",");
		for(int i=0; i<keyArr.length; i++)
		{
			
			String key = keyArr[i];
			if(!"".equals(key) && key !=null){
				shopManager.delShop(key);
			}
		}
		Map map = new HashMap();
		map.put("code", 100);
		map.put("msg", "删除成功");
		String resStr = JsonUtils.fromObject(map);
		return resStr;
    }
	
	/** 
	 * 删除商家
	 * @param response
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/shop/delShop", produces = "text/html;charset=UTF-8")
	@ResponseBody
    public String delMedia(HttpServletResponse response, HttpServletRequest request){
		String delKeys = request.getParameter("delKeys");
		String[] keyArr = delKeys.split(",");
		for(int i=0; i<keyArr.length; i++)
		{
			
			String key = keyArr[i];
			if(!"".equals(key) && key !=null){
				shopManager.delShop(key);
			}
		}
		Map map = new HashMap();
		map.put("code", 100);
		map.put("msg", "删除成功");
		String resStr = JsonUtils.fromObject(map);
		return resStr;
    }
	
	
	/**
	 * 保存信息
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value="/shop/saveShop", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String saveShop(HttpServletRequest request,HttpServletResponse response) throws Exception
	{	
		String shopName = request.getParameter("shopName");
		String tel = request.getParameter("tels");
		String pwm=request.getParameter("pwm");
		//pwm = PwdMd5.pwdMD5(pwm);
		String introduction=request.getParameter("introduction");
		String mode=request.getParameter("mode");
		String userId=request.getParameter("shopId");
		String img=request.getParameter("bei");
		String icon=request.getParameter("tou");
		String touxiangs=request.getParameter("touxiangs");
		List<SysUser> sysUser = shopManager.chaXunHao(tel);
		if(!(mode.equals("edit")) && sysUser.size()>0){
			return "layer.msg('该手机号已存在')";
		}
		//pwd = PwdMd5.pwdMD5(pwd);
		shopManager.saveMedia(shopName,tel, pwm,introduction,mode,userId,img,icon,touxiangs);
		Map map = new HashMap();
		map.put("code", 100);
		map.put("msg", "保存成功"); 
		String resStr = JsonUtils.fromObject(map);
		return resStr;
	
	}

	/**
	 * 修改
	 * 崔青山
	 * 2016年7月11日
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/shop/Modif",produces = "text/html;charset=UTF-8")
	@ResponseBody
    public String xgmedia(HttpServletRequest request)
	{
		String shopId = request.getParameter("shopId");
		SysShop shop = shopManager.findModif(shopId);
		Map<String, Object> map = new HashMap<String, Object>();
		if(shop!=null){
			if(shop.getSysUser().getIcon()!=null)
				map.put("idCardImg", shop.getIdCardImg());
			else
				map.put("idCardImg", "");
			if(shop.getLicense()!=null)
				map.put("img", shop.getLicense());
			else
				map.put("img", "");
			if(shop.getBackImg()!=null)
				map.put("backImg", shop.getBackImg());
			else
				map.put("backImg", "");
			map.put("code", "100");
		}
		String resStr = JsonUtils.fromObject(map);
        return resStr;
	}

	/**
	 * shop启用
	 * 崔青山
	 * 2016年7月11日
	 * @param response
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/shop/shopAbles", produces = "text/html;charset=UTF-8")
	@ResponseBody
    public String shopAbles(HttpServletResponse response, HttpServletRequest request){
		UserSession userSession = (UserSession) request.getSession().getAttribute("userSession");
		String userId = userSession.userId;
		String delKeys = request.getParameter("delKeys");
		String[] keyArr = delKeys.split(",");
		for(int i=0; i<keyArr.length; i++)
		{
			
			String key = keyArr[i];
			if(!"".equals(key) && key !=null){
				SysShop sysShop = logManager.getSysShopByShopId(key);
				logManager.saveLog(userId, "商家用户管理", "启用", sysShop.getShopName()+"启用");
				shopManager.shopAbles(key);
			}
		}
		Map map = new HashMap();
		map.put("code", 100);
		map.put("msg", "启用成功");
		String resStr = JsonUtils.fromObject(map);
		return resStr;
    }

	/**
	 * 禁用Shop
	 * 崔青山
	 * 2016年7月11日
	 * @param response
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/shop/disablesShops", produces = "text/html;charset=UTF-8")
	@ResponseBody
    public String disablesShops(HttpServletResponse response, HttpServletRequest request){
		UserSession userSession = (UserSession) request.getSession().getAttribute("userSession");
		String userId = userSession.userId;
		String delKeys = request.getParameter("delKeys");
		String[] keyArr = delKeys.split(",");
		for(int i=0; i<keyArr.length; i++)
		{
			
			String key = keyArr[i];
			if(!"".equals(key) && key !=null)
				shopManager.disableShop(userId,key);
		}
		Map map = new HashMap();
		map.put("code", 100);
		map.put("msg", "启用成功");
		String resStr = JsonUtils.fromObject(map);
		return resStr;
    }
	
	/**
	 * 设置密码
	 * 崔青山
	 * 2016年9月1日
	 */
	@RequestMapping(value="/users/gaimiUser", produces = "text/html;charset=UTF-8")
	@ResponseBody
    public String gaimiUser(HttpServletResponse response, HttpServletRequest request){
		String delKeys = request.getParameter("delKeys");
		String pwd = request.getParameter("pwd");
		shopManager.savegaiRole(delKeys,pwd);
		Map map = new HashMap();
		map.put("code", 100);
		map.put("msg", "设置成功");
		String resStr = JsonUtils.fromObject(map);
		return resStr;
    }
	
	
	
	/**
	 * 编辑保存用户
	 * 崔青山
	 * 2016年11月22日
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/shop/saveUser", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String saveUser(HttpServletRequest request,HttpServletResponse response) throws Exception
	{	
		String userName = request.getParameter("userName");
		String userId = request.getParameter("userId");
		SysUser sysUser = shopManager.getUserId(userId);
		sysUser.setUserName(userName);
		shopManager.saveSysUser(sysUser);
		Map map = new HashMap();
		map.put("code", 100);
		map.put("msg", "保存成功"); 
		String resStr = JsonUtils.fromObject(map);
		return resStr;
	
	}
	
	/**
	 * 2016年12月26日上午11:58:13
	 *  阳朔
	 *  注释:百度地图
	 */
	@RequestMapping(value="/shop/map")
    public String locatioin(){
        return "shop/map";
    }
	
	/**
	 * 保存 经纬度 地址
	 */
	@RequestMapping(value="/shop/saveLocation", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String saveLocation(HttpServletRequest request,HttpServletResponse response) throws Exception
	{	
		String lng = request.getParameter("lngId");
		String lat = request.getParameter("latId");
		String addres = request.getParameter("addresId");
		String shopId = request.getParameter("shopIdINP");
		
		shopManager.saveLocation(shopId,lng,lat,addres);
		Map map = new HashMap();
		map.put("code", 100);
		map.put("msg", "保存成功"); 
		String resStr = JsonUtils.fromObject(map);
		return resStr;
	}
	
	/**
	 * 查看经纬度 地址
	 */
	@RequestMapping(value="/shop/chaLocation", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String chaLocation(HttpServletRequest request,HttpServletResponse response) throws Exception
	{	
		UserSession userSession = (UserSession) request.getSession().getAttribute("userSession");
		String userId = userSession.userId;
		String shopId = request.getParameter("shopId");
		
		Map map = shopManager.getSysShopByShopId(userId,shopId);
		
		String resStr = JsonUtils.fromObject(map);
		return resStr;
	}
	
	/**
	 * 查看商家活动
	 */
	@RequestMapping(value="/shop/chaActivity", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String chaActivity(HttpServletRequest request,HttpServletResponse response) throws Exception
	{	
		String shopId = request.getParameter("shopId");
		
		Map map = shopManager.getShopActivityById(shopId);
		
		String resStr = JsonUtils.fromObject(map);
		return resStr;
	}
	
	/**
	 * 查看商家shopCode
	 */
	@RequestMapping(value="/shop/shopCode", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String shopCode(HttpServletRequest request,HttpServletResponse response) throws Exception
	{	
		String shopId = request.getParameter("shopId");
		SysShop shop = shopManager.getShopByShopId(shopId);
		
		Map map = new HashMap();
		map.put("shopCode", shop.getShopCode());
		
		String resStr = JsonUtils.fromObject(map);
		return resStr;
	}
	
	/**
	 * 查看商家三级比例
	 */
	@RequestMapping(value="/shop/chaShopBili", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String chaShopBili(HttpServletRequest request,HttpServletResponse response) throws Exception
	{	
		String shopId = request.getParameter("shopId");
		
		Map map = shopManager.chaShopBili(shopId);
		
		String resStr = JsonUtils.fromObject(map);
		return resStr;
	}
	
	/**
	 * 保存 商家 三级 比例
	 */
	@RequestMapping(value="/shop/saveShopBili", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String saveShopBili(HttpServletRequest request,HttpServletResponse response) throws Exception
	{	
		String shopId = request.getParameter("shopId");
		String onebili = request.getParameter("onebili");
		String twobili = request.getParameter("twobili");
		String threebili = request.getParameter("threebili");
		String platbili = request.getParameter("result_playBili");
		String shopbili = request.getParameter("shopbili");
		
		shopManager.saveShopBili(shopId,onebili,twobili,threebili,shopbili,platbili);
		Map map = new HashMap();
		map.put("code", 100);
		map.put("msg", "保存成功"); 
		String resStr = JsonUtils.fromObject(map);
		return resStr;
	}
	
	
}