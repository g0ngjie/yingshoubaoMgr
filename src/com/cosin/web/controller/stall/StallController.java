package com.cosin.web.controller.stall;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cosin.utils.JsonUtils;
import com.cosin.utils.PageUtils;
import com.cosin.utils.UserSession;
import com.cosin.web.entity.StallPrice;
import com.cosin.web.service.stall.IStallManager;

@Scope("prototype")
@Controller
public class StallController {
	@Autowired
	private IStallManager stallManager;
	/**
	 * 跳转到头条价格
	 * @return
	 */
	@RequestMapping(value="/stall/listStall.do")
    public String indess(){
        return "stall/listStall";
    }
	
	/**
	 * 头条价格数据
	 * 崔青山
	 * 2016年11月15日
	 * @param response
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/stall/listQiangDistance",  produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String listDistance(HttpServletResponse response, HttpServletRequest request)
	{			
		Map pageParam = PageUtils.getPageParam(request);
		List<StallPrice> listRes = stallManager.getDistanceList(new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));
		Map mapRet = PageUtils.getPageParam(listRes, listRes.size());
		String resStr = JsonUtils.fromObject(mapRet);//过滤hiberte对象中不需要生成json的属性
		return resStr;
	}
	
	
	/**
	 * 查看距离
	 * 崔青山
	 * 2016年11月15日
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/stall/juli",produces = "text/html;charset=UTF-8")
	@ResponseBody
    public String juli(HttpServletRequest request)
	{
		String stallId = request.getParameter("stallId");
		StallPrice push = stallManager.findbjDistance(stallId);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("unitPrice", push.getUnitPrice());
		map.put("stallId", push.getStallId());
		map.put("code", "100");
		String resStr = JsonUtils.fromObject(map);
        return resStr;
	}
	
	
	/**
	 * 保存距离
	 * 崔青山
	 * 2016年11月15日
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/stall/saveDistance", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String saveDistance(HttpServletRequest request,HttpServletResponse response) throws Exception
	{	
		UserSession userSession = (UserSession) request.getSession().getAttribute("userSession");
		String loginUserId = userSession.userId;
		String title = request.getParameter("bili");
		stallManager.saveDistance(loginUserId,title);
		Map map = new HashMap();
		map.put("code", 100);
		map.put("msg", "保存成功"); 
		String resStr = JsonUtils.fromObject(map);
		return resStr;
		
	}
	
}

