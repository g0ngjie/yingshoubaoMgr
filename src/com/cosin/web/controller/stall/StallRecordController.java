package com.cosin.web.controller.stall;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cosin.utils.JsonUtils;
import com.cosin.utils.PageUtils;
import com.cosin.utils.UserSession;
import com.cosin.web.entity.StallRecord;
import com.cosin.web.service.stall.IStallRecordManager;

@Scope("prototype")
@Controller
public class StallRecordController {
	@Autowired
	private IStallRecordManager stallRecordManager;
	/**
	 * 跳转到购买记录
	 * @return
	 */
	@RequestMapping(value="/stallRecord/listStallRecord.do")
    public String indexs(){
        return "/stall/listStallRecord";
    }
	
	/**
	 * 获取交易记录消息数据分页
	 * @param request
	 * @param responsess
	 * @return
	 */
	@RequestMapping(value="/stallRecord/stallRecordListData",  produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String recordListData(HttpServletResponse response, HttpServletRequest request)
	{			
		String Ssuser = request.getParameter("Ssuser");
		String ssDate = request.getParameter("ssDate");
		//获取分页参数
		Map pageParam = PageUtils.getPageParam(request);
		List<StallRecord> listRes = stallRecordManager.getUserList(Ssuser,ssDate,new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));
		int totle =stallRecordManager.getinfofenye(Ssuser,ssDate);
		Map mapRet = PageUtils.getPageParam(listRes, totle);
		String resStr = JsonUtils.fromObject(mapRet);//过滤hiberte对象中不需要生成json的属性
		return resStr;
	}
	 
	 /** 
	 * 删除信息
	 * @param response
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/stallRecord/shachuRecord", produces = "text/html;charset=UTF-8")
	@ResponseBody
    public String shachuMessage(HttpServletResponse response, HttpServletRequest request){
		UserSession userSession = (UserSession) request.getSession().getAttribute("userSession");
		String loginUserId = userSession.userId;
		String delKeys = request.getParameter("delKeys");
		String[] keyArr = delKeys.split(",");
		for(int i=0; i<keyArr.length; i++)
		{
			String key = keyArr[i];
			if(!"".equals(key) && key !=null)
				stallRecordManager.delPush(loginUserId,key);
		}
		Map map = new HashMap();
		map.put("code", 100);
		map.put("msg", "删除成功");
		String resStr = JsonUtils.fromObject(map);
		return resStr;
    }
		
	
}

