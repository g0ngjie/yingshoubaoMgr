package com.cosin.web.controller.statistics;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFHeader;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cosin.utils.JsonUtils;
import com.cosin.utils.PageUtils;
import com.cosin.utils.UserSession;
import com.cosin.web.entity.SysUser;
import com.cosin.web.entity.SysUserRole;
import com.cosin.web.service.statistics.IUserLivenessManager;

@Scope("prototype")
@Controller
public class UserLivenessController {
	@Autowired
	private IUserLivenessManager userLivenessManager;
	/**
	 * 跳转到商家粉丝
	 * @return
	 */
	@RequestMapping(value="/statistics/userLiveness.do")
    public String indexs(){
        return "statistics/userLiveness";
    }
	
	
	/**
	 * 获取商家数据分页
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/statistics/userLivenessListData",  produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String userLivenessListData(HttpServletResponse response, HttpServletRequest request)
	{			
		
		String provinceId = request.getParameter("provinceId");
		String cityId = request.getParameter("cityId");
		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");
		Map pageParam = PageUtils.getPageParam(request);
		List listRes = userLivenessManager.getUserLivenessListDataNew(provinceId,cityId,startDate,endDate,new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));
		
		int totle =userLivenessManager.getUserLivenessListCountNew(provinceId,cityId,startDate,endDate);
		Map mapRet = PageUtils.getPageParam(listRes, totle);
		String resStr = JsonUtils.fromObject(mapRet);//过滤hiberte对象中不需要生成json的属性
		return resStr;
		/*String type = request.getParameter("type");
		String provinceId = request.getParameter("provinceId");
		String cityId = request.getParameter("cityId");
		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");
		//获取分页参数
		Map pageParam = PageUtils.getPageParam(request);
		List listRes = userLivenessManager.getUserLivenessListData(type,provinceId,cityId,startDate,endDate,new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));
		int totle =userLivenessManager.getUserLivenessListCount(type,provinceId,cityId,startDate,endDate);
		Map mapRet = PageUtils.getPageParam(listRes, totle);
		String resStr = JsonUtils.fromObject(mapRet);//过滤hiberte对象中不需要生成json的属性
		return resStr;*/
	}
	
	/**
	 * 用户活跃度——折线图 柱状图 接口
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/statistics/userFigure",  produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String userFigure(HttpServletResponse response, HttpServletRequest request)
	{			
		
		String forMonth = request.getParameter("forMonth");
		String forYear = request.getParameter("forYear");
		List listRes = userLivenessManager.getUserFigureData(forYear,forMonth);
		
		String resStr = JsonUtils.fromArrayObject(listRes);
		return resStr;
	}
	
	/**
	 * 查询总数
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/statistics/userTotal",  produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String userTotal(HttpServletResponse response, HttpServletRequest request)
	{			
		String type = request.getParameter("type");
		String provinceId = request.getParameter("provinceId");
		String cityId = request.getParameter("cityId");
		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");
		int totle =userLivenessManager.getUserLivenessListCount(type,provinceId,cityId,startDate,endDate);
		Map map = new HashMap();
		map.put("totle", totle);
		String resStr = JsonUtils.fromObject(map);
		return resStr;
	}
	
	/**
	 * 获取月初
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/statistics/takeMonth",  produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String takeMonth(HttpServletResponse response, HttpServletRequest request)
	{			
		Timestamp timestamp = new Timestamp(new Date().getTime());
		Map<Object, Object> map = new HashMap<Object, Object>();
		map.put("timDate", timestamp);
		String resStr = JsonUtils.fromObject(map);
		return resStr;
	}
	
	/**
	 * 省份 
	 */
	@RequestMapping(value="/statistics/chaProvince",  produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String chaProvince(HttpServletResponse response, HttpServletRequest request)
	{			
		List list = userLivenessManager.selectProList();
		String resStr = JsonUtils.fromArrayObject(list);
		return resStr;
	}
	
	/**
	 * 城市
	 */
	@RequestMapping(value="/statistics/chaCity",  produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String chaCity(HttpServletResponse response, HttpServletRequest request)
	{			
		String proId = request.getParameter("proId");
		List list = userLivenessManager.selectCityListByProId(proId);
		String resStr = JsonUtils.fromArrayObject(list);
		return resStr;
	}
	
	/**
	 * 导出
	 * @param response
	 * @param request
	 * @throws IOException 
	 */
	@RequestMapping(value="/statistics/exportWps",  produces = "text/html;charset=UTF-8")
	@ResponseBody
	public void exportWps(HttpServletResponse response, HttpServletRequest request) throws IOException{
		
/*		String type = request.getParameter("type");
		String provinceId = request.getParameter("provinceId");
		String cityId = request.getParameter("cityId");
		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");
		//获取分页参数
		Map pageParam = PageUtils.getPageParam(request);
		List listRes = userLivenessManager.getUserLivenessListData(type,provinceId,cityId,startDate,endDate,new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));
*/	
		String provinceId = request.getParameter("provinceId");
		String cityId = request.getParameter("cityId");
		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");
		Map pageParam = PageUtils.getPageParam(request);
		List listRes = userLivenessManager.getUserLivenessListDataNew(provinceId,cityId,startDate,endDate,new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));
		
		
		//创建一个新的 excel 
        HSSFWorkbook wb = new HSSFWorkbook();
        //创建 sheet 页
        HSSFSheet sheet = wb.createSheet("用户活跃度统计"); 
        HSSFRow row = sheet.createRow((int) 0);  
        //sheet.setColumnWidth((short) 0, (short) (35.7 * 150));
        //sheet.setColumnWidth((short)1, (short) 600);
       
        //设置水平居中
        HSSFCellStyle style = wb.createCellStyle();  
        
        
        //设置标题居中
        HSSFHeader header = sheet.getHeader();
        header.setCenter("用户活跃度统计");
        String[] excelHeader = new String[]{"时间","注册用户数（存量）","注册用户数（新增）","登录用户数","交易用户数"};
        for (int i = 0; i < excelHeader.length; i++) {  
            HSSFCell cell = row.createCell(i);  
            cell.setCellValue(excelHeader[i]);  
            cell.setCellStyle(style);  
            sheet.autoSizeColumn(i);  
        }  
        for (int i = 0; i < listRes.size(); i++) {  
            row = sheet.createRow(i + 1);  
            Map map = (Map)listRes.get(i);  
            row.createCell(0).setCellValue(map.get("date").toString());
        	row.createCell(1).setCellValue(map.get("chaUserBeforeByDate").toString());  
            row.createCell(2).setCellValue(map.get("count1").toString());
            row.createCell(3).setCellValue(map.get("count2").toString());
            row.createCell(4).setCellValue(map.get("count3").toString());
        }   
        sheet.autoSizeColumn((short)0);// 列表宽度自定义
        sheet.autoSizeColumn((short)1);// 列表宽度自定义
        sheet.autoSizeColumn((short)2);// 列表宽度自定义
        sheet.autoSizeColumn((short)3);// 列表宽度自定义
        sheet.autoSizeColumn((short)4);// 列表宽度自定义
        style.setAlignment(HSSFCellStyle.ALIGN_CENTER);// 居中
        style.setAlignment(HSSFCellStyle.ALIGN_LEFT);// 居中
        style.setWrapText(true); 
        response.setContentType("application/vnd.ms-excel");    
        String excelName = "用户活跃度统计";
        response.setHeader("Content-disposition", "attachment;filename="+new String(excelName.getBytes(), "ISO8859-1")+".xls");    
        OutputStream ouputStream = response.getOutputStream();    
        wb.write(ouputStream);    
        ouputStream.flush();    
        ouputStream.close();  
	}
	
	
}

