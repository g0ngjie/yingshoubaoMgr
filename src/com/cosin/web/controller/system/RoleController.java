/**
 * 　　　　　　　　┏┓　　　┏┓+ +
 * 　　　　　　　┏┛┻━━━┛┻┓ + +
 * 　　　　　　　┃　　　　　　　┃
 * 　　　　　　　┃　　　━　　　┃ ++ + + +
 * 　　　　　　 ████━████ ┃+
 * 　　　　　　　┃　　　　　　　┃ +
 * 　　　　　　　┃　　　┻　　　┃
 * 　　　　　　　┃　　　　　　　┃ + +
 * 　　　　　　　┗━┓　　　┏━┛
 * 　　　　　　　　　┃　　　┃
 * 　　　　　　　　　┃　　　┃ + + + +
 * 　　　　　　　　　┃　　　┃　　　　Code is far away from bug with the animal protecting
 * 　　　　　　　　　┃　　　┃ + 　　　　神兽保佑,代码无bug
 * 　　　　　　　　　┃　　　┃
 * 　　　　　　　　　┃　　　┃　　+
 * 　　　　　　　　　┃　 　　┗━━━┓ + +
 * 　　　　　　　　　┃ 　　　　　　　┣┓
 * 　　　　　　　　　┃ 　　　　　　　┏┛
 * 　　　　　　　　　┗┓┓┏━┳┓┏┛ + + + +
 * 　　　　　　　　　　┃┫┫　┃┫┫
 * 　　　　　　　　　　┗┻┛　┗┻┛+ + + +
 * <p>
 * Created by  on 16/9/21.
 */

package com.cosin.web.controller.system;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cosin.utils.JsonUtils;
import com.cosin.utils.PageUtils;
import com.cosin.utils.UserSession;
import com.cosin.web.controller.log.ILogManager;
import com.cosin.web.entity.SysMenu;
import com.cosin.web.entity.SysRole;
import com.cosin.web.entity.SysRolePower;
import com.cosin.web.entity.SysUser;
import com.cosin.web.entity.SysUserRole;
import com.cosin.web.service.system.IRoleManager;

@Scope("prototype")
@Controller
public class RoleController {
	@Autowired
	private IRoleManager roleManager;
	@Autowired
	private ILogManager logManager;
	
	/**
	 * 跳转到角色管理
	 * 崔青山
	 * 2016年7月7日
	 * @return
	 */
	@RequestMapping(value="/role/listRole")
    public String index(){
        return "role/listRole";
    }
	/**
	 * 跳转到用户管理
	 * 崔青山
	 * 2016年7月7日
	 * @return
	 */
	@RequestMapping(value="/user/listUser")
	@ResponseBody
    public ModelAndView indexs(HttpServletRequest request,HttpServletResponse ronse){
		ModelAndView mav = new ModelAndView();
/*		UserSession userSession = (UserSession) request.getSession().getAttribute("userSession");
		
		List<Object> listPw = userSession.list;
		
		for (int i = 0; i < listPw.size(); i++) {
			
			if("park_add".equals(listPw.get(i))){//添加权限
				
				mav.addObject("park_add", listPw.get(i));
				
			}
			if("park_xiugai".equals(listPw.get(i))){
				
				mav.addObject("park_xiugai", listPw.get(i));
				
			}
			if("park_del".equals(listPw.get(i))){
				
				mav.addObject("park_del", listPw.get(i));
				
			}
		}
*/		mav.setViewName("user/listUser");
		
		return mav;
    }
	
	/**
	 * 跳转到商家
	 * 崔青山
	 * 2016年7月7日
	 * @return
	 */
	@RequestMapping(value="/shopUser/listShopUser.do")
    public String indexs1(){
        return "user/listShopUser";
    }
	
	
	/**
	 * 跳转到会员
	 * 崔青山
	 * 2016年7月7日
	 * @return
	 */
	@RequestMapping(value="/huiYuanUser/listHuiYuanUser.do")
    public String indexs2(){
        return "user/listHuiYuanUser";
    }
	
	
	/**
	 * 获取角色数据分页
	 * 崔青山
	 * 2016年7月7日
	 * @param response
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/role/roleListData",  produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String shopListData(HttpServletResponse response, HttpServletRequest request)
	{			
		String SsName = request.getParameter("SsName");	
		//获取分页参数
		Map pageParam = PageUtils.getPageParam(request);
		//查询数据，使用分页参数start limit
		List<SysRole> listMainMenu = roleManager.getRoleList(SsName,new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));
		List listRes = new ArrayList();
		for (int i = 0; i < listMainMenu.size(); i++) {
			SysRole sysrole = listMainMenu.get(i);
			String roleId = sysrole.getRoleId();
			if(roleId.equals("1"))
				continue;
			Map map = new HashMap();
			map.put("roleId", sysrole.getRoleId());
			map.put("createDate", sysrole.getCreateDate());
			map.put("enable", sysrole.getEnable());
			map.put("userName", sysrole.getUserName());
			listRes.add(map);
		}
		int totle =roleManager.getRolefenye(SsName);
		Map mapRet = PageUtils.getPageParam(listRes, totle);
		String resStr = JsonUtils.fromObject(mapRet);//过滤hiberte对象中不需要生成json的属性
		return resStr;
	}
	
	/**
	 * 获取用户数据分页
	 * 崔青山
	 * 2016年7月7日
	 * @param response
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/user/userListData",  produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String userListData(HttpServletResponse response, HttpServletRequest request)
	{			
		String SsName = request.getParameter("SsName");	
		String roleName = request.getParameter("roleName");	
		//获取分页参数
		Map pageParam = PageUtils.getPageParam(request);
		List<SysUser> listRes = roleManager.getUserList(SsName,roleName,new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));
		int totle =roleManager.getUserfenye(SsName,roleName);
		Map mapRet = PageUtils.getPageParam(listRes, totle);
		String resStr = JsonUtils.fromObject(mapRet);
		return resStr;
	}
	
	
	/**
	 * 获取商家用户数据分页
	 * 崔青山
	 * 2016年7月7日
	 * @param response
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/user/shopUserListData",  produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String shopUserListData(HttpServletResponse response, HttpServletRequest request)
	{			
		String SsName = request.getParameter("SsName");	
		String roleName = request.getParameter("roleName");	
		//获取分页参数
		Map pageParam = PageUtils.getPageParam(request);
		List<SysUser> listRes = roleManager.getShopUserList(SsName,roleName,new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));
		int totle =roleManager.getUserfenyeShop(SsName,roleName);
		Map mapRet = PageUtils.getPageParam(listRes, totle);
		String resStr = JsonUtils.fromObject(mapRet);
		return resStr;
	}
	
	
	/**
	 * 获取会员用户数据分页
	 * 崔青山
	 * 2016年7月7日
	 * @param response
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/user/huiYuanUserListData",  produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String huiYuanUserListData(HttpServletResponse response, HttpServletRequest request)
	{			
		String SsName = request.getParameter("SsName");	
		String roleName = request.getParameter("roleName");	
		//获取分页参数
		Map pageParam = PageUtils.getPageParam(request);
		List<SysUser> listRes = roleManager.getHuiyuanUserList(SsName,roleName,new Integer(pageParam.get("start").toString()), new Integer(pageParam.get("limit").toString()));
		int totle =roleManager.getUserfenyeHuiyuan(SsName,roleName);
		Map mapRet = PageUtils.getPageParam(listRes, totle);
		String resStr = JsonUtils.fromObject(mapRet);
		return resStr;
	}
	/**
	 * 删除role
	 * 崔青山
	 * 2016年7月7日
	 * @param response
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/role/delRole", produces = "text/html;charset=UTF-8")
	@ResponseBody
    public String delRole(HttpServletResponse response, HttpServletRequest request){
		Map map = new HashMap();
		String delKeys = request.getParameter("delKeys");
		String[] keyArr = delKeys.split(",");
		for(int i=0; i<keyArr.length; i++)
		{
			String key = keyArr[i];
			if(!"".equals(key) && key !=null){
				List<SysUserRole> listUserRolr = roleManager.getRoleUserList(key);
				if(listUserRolr == null || listUserRolr.equals("0"))
					roleManager.delRole(key);
				else
					return "layer.msg('该角色正在使用，无法删除。')";
			}
		}
		String resStr = JsonUtils.fromObject(map);
		return resStr;
    }
	
	/**
	 * 删除user
	 * 崔青山
	 * 2016年7月7日
	 * @param response
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/user/delUser", produces = "text/html;charset=UTF-8")
	@ResponseBody
    public String delUser(HttpServletResponse response, HttpServletRequest request){
		UserSession userSession =(UserSession) request.getSession().getAttribute("userSession");
		String loginUserId = userSession.userId;
		Map map = new HashMap();
		String delKeys = request.getParameter("delKeys");
		String[] keyArr = delKeys.split(",");
		for(int i=0; i<keyArr.length; i++)
		{
			String key = keyArr[i];
			SysUser user = roleManager.getByUserId(key);
			if(user.getUserName().equals("admin"))
				return "layer.msg('该用户无法删除。')";
			if(!"".equals(key) && key !=null)
				roleManager.delUser(loginUserId,key);
		}
		map.put("code", 100);
		String resStr = JsonUtils.fromObject(map);
		return resStr;
    }

	/**
	 * 保存信息role
	 * 崔青山
	 * 2016年7月7日
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/role/saveRole", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String saveShop(HttpServletRequest request,HttpServletResponse response)
{		
		String userName = request.getParameter("userName");
		String enable = request.getParameter("enable");
		String roleid = request.getParameter("roleId");
		String createDate=request.getParameter("createDate");
		String del=request.getParameter("del");
		String mode=request.getParameter("mode");
		
		List<SysRole> list =  roleManager.chaRole(userName);
		if(!mode.equals("edit" ) && list.size() > 0){
			return "layer.msg('角色已存在')";
		}
		else{
			roleManager.saveRole(userName,enable, roleid, mode,del);
			Map map = new HashMap();
			map.put("code", 100);
			map.put("msg", "保存成功"); 
			String resStr = JsonUtils.fromObject(map);
			return resStr;
		}
	
	}
	
	/**
	 * 保存信息user
	 * 崔青山
	 * 2016年7月7日
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/user/saveUser", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String saveUser(HttpServletRequest request,HttpServletResponse response) throws Exception
	{	
		UserSession userSession = (UserSession) request.getSession().getAttribute("userSession");
		String loginUserId = userSession.userId;
		String managerName = request.getParameter("managerName");
		String loginName = request.getParameter("loginName");
		String doublePwd = request.getParameter("doublePwd");
		
		Map map = new HashMap();
		List<SysUser> list =  roleManager.chaUser(managerName);
		List<SysUser> chaUserList = roleManager.chaLoginName(loginName);
		if(list.size() > 0){
			map.put("code", 101);
			map.put("msg", "该用户名已存在"); 
		}else if(chaUserList.size() > 0){
			map.put("code", 101);
			map.put("msg", "该登陆账号已存在"); 
		}else{
			roleManager.saveUser(loginUserId,managerName, loginName, doublePwd);
			map.put("code", 100);
			map.put("msg", "保存成功"); 
			String resStr = JsonUtils.fromObject(map);
		}
		return JsonUtils.fromObject(map);
	}
	/**
	 * 编辑User
	 * 崔青山
	 * 2016年7月7日
	 * @param request
	 * @return
	 */
	@RequestMapping(value="user/bjUser",produces = "text/html;charset=UTF-8")
	@ResponseBody
    public String bjUser(HttpServletRequest request,HttpServletResponse response)
	{
		String userId = request.getParameter("userId");
		SysUser sysUser = roleManager.findbjUser(userId);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("userName", sysUser.getUserName());
		map.put("loginName", sysUser.getLoginName());
		map.put("loginPwd", sysUser.getPwd());
		String resStr = JsonUtils.fromObject(map);
        return resStr;
	}
	
	/**
	 * 编辑Role
	 * 崔青山
	 * 2016年7月7日
	 * @param request
	 * @return
	 */
	@RequestMapping(value="role/bjRole",produces = "text/html;charset=UTF-8")
	@ResponseBody
    public String bjRole(HttpServletRequest request)
	{
		String roleId = request.getParameter("roleId");
		SysRole userRole = roleManager.findbjRole(roleId);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("userName", userRole.getUserName());
		map.put("roleId", userRole.getRoleId());
		map.put("enable", userRole.getEnable());
		map.put("code", "100");
		String resStr = JsonUtils.fromObject(map);
        return resStr;
	}
	/**
	 * role启用
	 * 崔青山
	 * 2016年7月7日
	 * @param response
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/role/saveqyRole", produces = "text/html;charset=UTF-8")
	@ResponseBody
    public String saveqyRole(HttpServletResponse response, HttpServletRequest request){
		String delKeys = request.getParameter("qyKeys");
		String[] keyArr = delKeys.split(",");
		for(int i=0; i<keyArr.length; i++)
		{
			
			String key = keyArr[i];
			if(!"".equals(key) && key !=null)
				roleManager.saveqyRole(key);
		}
		Map map = new HashMap();
		map.put("code", 100);
		map.put("msg", "启用成功");
		String resStr = JsonUtils.fromObject(map);
		return resStr;
    }
	
	/**
	 * user启用
	 * 崔青山
	 * 2016年7月7日
	 * @param response
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/user/saveqyUser", produces = "text/html;charset=UTF-8")
	@ResponseBody
    public String saveqyUser(HttpServletResponse response, HttpServletRequest request){
		UserSession userSession =(UserSession) request.getSession().getAttribute("userSession");
		String loginUserId = userSession.userId;
		String delKeys = request.getParameter("qyKeys");
		String[] keyArr = delKeys.split(",");
		for(int i=0; i<keyArr.length; i++)
		{
			String key = keyArr[i];
			if(!"".equals(key) && key !=null)
				roleManager.saveqyUser(loginUserId,key);
		}
		Map map = new HashMap();
		map.put("code", 100);
		map.put("msg", "启用成功");
		String resStr = JsonUtils.fromObject(map);
		return resStr;
    }
	/**
	 * 禁用role
	 * 崔青山
	 * 2016年7月7日
	 * @param response
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/role/savejyRole", produces = "text/html;charset=UTF-8")
	@ResponseBody
    public String savejyRole(HttpServletResponse response, HttpServletRequest request){
		String delKeys = request.getParameter("jyKeys");
		String[] keyArr = delKeys.split(",");
		for(int i=0; i<keyArr.length; i++)
		{
			String key = keyArr[i];
			if(!"".equals(key) && key !=null)
				roleManager.savejyRole(key);
		}
		Map map = new HashMap();
		map.put("code", 100);
		map.put("msg", "禁用成功");
		String resStr = JsonUtils.fromObject(map);
		return resStr;
    }
	
	/**
	 * 禁用user
	 * 崔青山
	 * 2016年7月7日
	 * @param response
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/user/savejyUser", produces = "text/html;charset=UTF-8")
	@ResponseBody
    public String savejyUser(HttpServletResponse response, HttpServletRequest request){
		UserSession userSession =(UserSession) request.getSession().getAttribute("userSession");
		String loginUserId = userSession.userId;
		String delKeys = request.getParameter("jyKeys");
		String[] keyArr = delKeys.split(",");
		for(int i=0; i<keyArr.length; i++)
		{
			String key = keyArr[i];
			if(!"".equals(key) && key !=null)
				roleManager.savejyUser(loginUserId,key);
		}
		Map map = new HashMap();
		map.put("code", 100);
		map.put("msg", "禁用成功");
		String resStr = JsonUtils.fromObject(map);
		return resStr;
    }
	
	/**
	 * 角色选择
	 * 崔青山
	 * 2016年7月7日
	 * @return
	 */
	@RequestMapping(value="/role/listjuesexuanze", produces = "text/html;charset=UTF-8")
	@ResponseBody
    public String listjuesexuanze()
    {
		List listRes = new ArrayList();
		List<SysRole> listRole = roleManager.getSysRole();
		for(int i=0; i<listRole.size(); i++)
		{
			SysRole role = listRole.get(i);
			Map map = new HashMap();
			map.put("name", role.getUserName());
			map.put("roleId", role.getRoleId());
			listRes.add(map);
		}
		String str = JsonUtils.fromArrayObject(listRes);
		return str;
    }
	
	/**
	 * 设置密码
	 * 崔青山
	 * 2016年8月3日
	 * @param response
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/user/gaimiUser", produces = "text/html;charset=UTF-8")
	@ResponseBody
    public String gaimiUser(HttpServletResponse response, HttpServletRequest request){
		String delKeys = request.getParameter("delKeys");
		String pwd = request.getParameter("pwd");
		String[] keyArr = delKeys.split(",");
		//pwd = PwdMd5.pwdMD5(pwd);
		if(!"".equals(delKeys) && delKeys !=null)
			roleManager.savegaiRole(delKeys,pwd);
		Map map = new HashMap();
		map.put("code", 100);
		map.put("msg", "设置成功");
		String resStr = JsonUtils.fromObject(map);
		return resStr;
    }
	
	
	/**
	 * 角色权限
	 * 树
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/role/listOrgData", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String listOrgData(HttpServletRequest request, HttpServletResponse response)
	{	
		
		String orgTree = request.getParameter("id");
		String roleId = request.getParameter("roleId");
		List<SysRolePower> list = roleManager.findRolePowerById(roleId);
		
		Map mapPower = new HashMap();
		for(int i=0; i<list.size(); i++)
		{
			SysRolePower sysRolePower = (SysRolePower)list.get(i);
			mapPower.put(sysRolePower.getPowerCode(), "");
		}
		//if(orgTree == null)
			//orgTree = "0";
		List<SysMenu> listIdCompany = roleManager.getMenuList(orgTree);//查询公司的数据
		List listEveryCompanyData = new ArrayList();//新new 一个list集合,用来放map数据;
		for (int i = 0; i < listIdCompany.size(); i++) {
			SysMenu Data = listIdCompany.get(i);
			Map mapEveryData = new HashMap();
			mapEveryData.put("id", Data.getMenuId());
			boolean check = roleManager.roleCheck(list,Data.getMenuId());
			Map mapAttr = new HashMap();
			mapAttr.put("orgkey", Data.getMenuId());
			mapEveryData.put("attributes", mapAttr);
			mapEveryData.put("state", "closed");
			
			if(mapPower.containsKey(Data.getMenuId()))
			{
				mapEveryData.put("checked", true);
			}
		
			mapEveryData.put("text", Data.getMenuName());
			List listSubOrg = getSubOrg(Data.getMenuId(), mapPower);
			if(listSubOrg.size() > 0)
				mapEveryData.put("children", listSubOrg);
			listEveryCompanyData.add(mapEveryData);
		}
		String str = JsonUtils.fromArrayObject(listEveryCompanyData);
		return str;
	}
	/**
	 * 角色权限  tree
	 * 下一级查询
	 * @param orgkey
	 * @return
	 */
	public List getSubOrg(String orgkey, Map mapPower)
	{
		List listEveryCompanyData = new ArrayList();//新new 一个list集合,用来放map数据;
		List<SysMenu> listIdCompany = roleManager.getMenuList(orgkey);//查询公司的数据
		for (int i = 0; i < listIdCompany.size(); i++) {
			SysMenu orgData = listIdCompany.get(i);
			Map mapEveryData = new HashMap();
			mapEveryData.put("id", orgData.getMenuId());
			
			Map mapAttr = new HashMap();
			mapAttr.put("menuId", orgData.getMenuId());
			//mapEveryData.put("attributes", mapAttr);
			if(mapPower.containsKey(orgData.getMenuId()))
			{
				mapEveryData.put("checked", true);
			}
			//mapEveryData.put("checked", true);
			mapEveryData.put("text", orgData.getMenuName());
			List listSubOrg = getSubOrg(orgData.getMenuId(), mapPower);
			if(listSubOrg.size() > 0)
			{
				mapEveryData.put("children", listSubOrg);
				mapEveryData.put("state", "closed");
			}
			listEveryCompanyData.add(mapEveryData);
		}
			return listEveryCompanyData;
	}
	
	/**
	 * 角色权限设置
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/role/editRolePower",produces = "text/html;charset=UTF-8")
	@ResponseBody
    public String editRolePower(HttpServletRequest request)
	{
		String roleId = request.getParameter("roleIdsss");
		List<SysRolePower> list = roleManager.findRolePowerById(roleId);
		Map mapPower = new HashMap();
		for(int i=0; i<list.size(); i++)
		{
			SysRolePower sysRolePower = (SysRolePower)list.get(i);
			mapPower.put(sysRolePower.getPowerCode(), "");
		}
		//if(orgTree == null)
			//orgTree = "0";
		List<SysMenu> listIdCompany = roleManager.getMenuList(null);//查询公司的数据
		List listEveryCompanyData = new ArrayList();//新new 一个list集合,用来放map数据;
		for (int i = 0; i < listIdCompany.size(); i++) {
			SysMenu Data = listIdCompany.get(i);
			Map mapEveryData = new HashMap();
			mapEveryData.put("id", Data.getMenuId());
			boolean check = roleManager.roleCheck(list,Data.getMenuId());
			Map mapAttr = new HashMap();
			mapAttr.put("orgkey", Data.getMenuId());
			//mapEveryData.put("attributes", mapAttr);
			mapEveryData.put("state", "closed");
			if(mapPower.containsKey(Data.getMenuId()))
			{
				//mapEveryData.put("checked", true);
			}
		
			mapEveryData.put("text", Data.getMenuName());
			List listSubOrg = getSubOrg(Data.getMenuId(), mapPower);
			if(listSubOrg.size() > 0)
				mapEveryData.put("children", listSubOrg);
			listEveryCompanyData.add(mapEveryData);
		}
		String str = JsonUtils.fromArrayObject(listEveryCompanyData);
        return str;
	
	}	
	
	/**
	 * 保存权限
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/role/saveRolePower", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String saveRolePower(HttpServletRequest request,HttpServletResponse response)
	{		
		UserSession userSession =(UserSession) request.getSession().getAttribute("userSession");
		String loginUserId = userSession.userId;
		String data = request.getParameter("data");
		String roleId = request.getParameter("roleId");
		List<SysRolePower> powerList = roleManager.findRolePowerById(roleId);
			if (powerList.size()>0) {
				roleManager.delPower(roleId);
			}
			
			String[] strings = data.split(",");
			for(int i=0;i<strings.length;i++)
			{
				String powerCode = strings[i];
				roleManager.savePowerInRolePower(roleId,powerCode);
			}
			logManager.saveLog(loginUserId, "角色管理", "权限设置", "管理员权限设置");
			Map map = new HashMap();
			map.put("code", 100);
			map.put("msg", "保存成功"); 
			String resStr = JsonUtils.fromObject(map);
			return resStr;
		
	}
	
	/**
	 * 保存编辑
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/user/saveEditUser", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String saveEditUser(HttpServletRequest request,HttpServletResponse response)
	{		
		UserSession userSession =(UserSession) request.getSession().getAttribute("userSession");
		String loginUserId = userSession.userId;
		String userId = request.getParameter("userIdDv2");
		String userName = request.getParameter("editName");
		String loginName = request.getParameter("editLogin");
		Map map = roleManager.submitUser(loginUserId,userId,userName,loginName);
		
		String resStr = JsonUtils.fromObject(map);
		return resStr;
		
	}
}

