/**
 * 
 */
package com.cosin.web.controller.system;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.cosin.utils.HttpRequest;
import com.cosin.utils.JsonForMapUtils;
import com.cosin.utils.JsonUtils;
import com.cosin.utils.PwdMd5;
import com.cosin.utils.UserSession;
import com.cosin.web.entity.SysRolePower;
import com.cosin.web.service.system.ISystemManager;

/**
 * 用户登录验证以及权限控制
 * @author 王思明
 * 2016年7月19日
 * 下午2:01:34
 */
@Scope("prototype")
@Controller
@SessionAttributes("ip")
public class SystemController {

	@Autowired
	private ISystemManager systemManager;
	
	/**
	 * 跳转到登录页
	 * 王思明 
	 * 2016年7月19日
	 * 下午2:21:00
	 */
	@RequestMapping(value="/system/login")
	public String login(){
		return "main/login";
	}
	
	
	
	/**123.56.238.98
	 * http://192.168.1.230:8081/yingshoubaoMgr/system/login.do
	 * 百度地图API密钥：cRcALYI6iWyPZ1RAvbA4MFVBlIuFWX1p
	 * 登录验证
	 * 王思明 
	 * 2016年7月19日
	 * 下午2:17:53
	 * @throws Exception 
	 */
	@RequestMapping(value="/system/loginYZ",produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String selectsubject(HttpServletRequest request,HttpServletResponse response) throws Exception{
		String loginName = request.getParameter("loginName");
		String pwd = request.getParameter("pwd");//密码
		//pwd = PwdMd5.pwdMD5(pwd);
		
		Map<String, Object> map = systemManager.getSysUserYZ(loginName,pwd);
		String code = map.get("code").toString();
		if("100".equals(code)){
			String userId = map.get("userId").toString();
			String name = map.get("name").toString();
			String roleId = map.get("roleId").toString();
			
			//获取登录者的权限
			List<SysRolePower> list =  systemManager.getSysRolePower(roleId);
			List<Object> listObj = new ArrayList<Object>();
			for (int i = 0; i < list.size(); i++) {
				
				SysRolePower sysRolePower = list.get(i);
				
				listObj.add(sysRolePower.getPowerCode());
				
			}
			
			//session 存值
			UserSession userSession = new UserSession();
			userSession.userId = userId;
			userSession.name = name;
			userSession.roleId = roleId;
			userSession.list = listObj;
			String url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=wx1c2fce17037e2556&secret=fcc1a027fbff8e048fe8fe4e3eebd76f";
			String resultUrl = HttpRequest.sendGet(url, null);
			Map map1;
			String acesstk = "";
			try {
				map1 = JsonForMapUtils.parseMap(resultUrl);
				acesstk = (String) map1.get("access_token");
				
				userSession.access_token = acesstk;
				
			} catch (IOException e) {
				e.printStackTrace();
			}
			String str3 = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=" + acesstk + "&type=jsapi";
			String result1 = HttpRequest.sendGet(str3, null);
			Map map2 = JsonForMapUtils.parseMap(result1);
			String jsapi_ticket = (String) map2.get("ticket");
			userSession.jsapi_ticket = jsapi_ticket;
			
			request.getSession().setAttribute("userSession", userSession);
		}
		
		String strRes = JsonUtils.fromObject(map);
		return strRes;
	}
	/**
	 * 跳转到登录页面列表
	 * @return
	 */
	@RequestMapping(value="/system/loginerr")
    public String loginerr(){
        return "main/loginerr";
    }
	
}
