package com.cosin.web.controller.wetemplate;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.sql.Timestamp;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cosin.utils.HttpRequest;
import com.cosin.utils.JsonForMapUtils;
import com.cosin.utils.UserSession;
import com.cosin.web.controller.log.ILogManager;
import com.cosin.web.entity.SysShop;
import com.cosin.web.entity.SysUser;

@Scope("prototype")
@Controller

public class SendOrderPaySuccessMsg {
	
	@Autowired
	private ILogManager logManager;
    
	public static boolean sendTemplateMsg(String token,Template template){  
        
        boolean flag=false;  
          
        String requestUrl="https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=ACCESS_TOKEN";  
        requestUrl=requestUrl.replace("ACCESS_TOKEN", token);  
      
        String jsonResult=HttpRequest.sendPost(requestUrl, template.toJSON());
        if(jsonResult!=null){  
            /*int errorCode=jsonResult.getInt("errcode");  
            String errorMessage=jsonResult.getString("errmsg");  
            if(errorCode==0){  
                flag=true;  
            }else{  
                System.out.println("模板消息发送失败:"+errorCode+","+errorMessage);  
                flag=false;  
            } */ 
        	flag=true;  
        }  
        return flag;  
	}
          
	@RequestMapping(value = "/send/sengManager")
	public void send(HttpServletRequest request){
		String state = request.getParameter("state");
		String mode = request.getParameter("mode");
		String userId = request.getParameter("userId");
		SysUser sysUser = logManager.getSysUserByUserId(userId);
		String openId = sysUser.getAppkey();		
		
		UserSession userSession = (UserSession) request.getSession().getAttribute("userSession");
		String acesstk = userSession.access_token;
		/*if(acesstk.equals("")){
			String url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=wx1c2fce17037e2556&secret=fcc1a027fbff8e048fe8fe4e3eebd76f";
			String resultUrl = HttpRequest.sendGet(url, null);
			Map map;
			try {
				map = JsonForMapUtils.parseMap(resultUrl);
				acesstk = (String) map.get("access_token");
				
				userSession.access_token = acesstk;
				request.getSession().setAttribute("userSession", userSession);
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}*/
		
		List<TemplateParam> paras=new ArrayList<TemplateParam>();  
		Timestamp times = new Timestamp(new Date().getTime());
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-DD HH:mm:ss");
		String timestamp = format.format(times);
		String templateId = "";
		if(mode.equals("1")){//1提现成功通知
			/*{{first.DATA}}
			提现金额：{{keyword1.DATA}}
			到账时间：{{keyword2.DATA}}
			{{remark.DATA}}*/
			String price = request.getParameter("price");
			templateId = "p2IqJamE68-etf4S14Nk15-xbIzqIP1383dTI_m-_mA";//提现结果通知 模板Id
			paras.add(new TemplateParam("first",state,"#FF3333")); 
			paras.add(new TemplateParam("keyword1",price,"#0044BB"));
			paras.add(new TemplateParam("keyword2",timestamp.toString(),"#0044BB"));
			paras.add(new TemplateParam("remark","请及时查账","#AAAAAA"));
		}
		if(mode.equals("2")){
			/*{{first.DATA}}
			提现金额：{{keyword1.DATA}}
			提现时间：{{keyword2.DATA}}
			失败原因：{{keyword3.DATA}}
			{{remark.DATA}}*/
			String price = request.getParameter("price");
			String reason = request.getParameter("reason");
			templateId = "uEK5uQ-YYS2cSrK0UMw811L1Ku0eaY0tni1aidhBmOw";//提现申请失败模板Id
			paras.add(new TemplateParam("first",state,"#FF3333"));  
			paras.add(new TemplateParam("keyword1",sysUser.getUserName(),"#0044BB"));  
			paras.add(new TemplateParam("keyword2",timestamp.toString(),"#0044BB"));  
			paras.add(new TemplateParam("keyword3",reason,"#0044BB"));  
		}
		if(mode.equals("3")){//成功
			/*{{first.DATA}}
			姓名：{{keyword1.DATA}}
			联系方式：{{keyword2.DATA}}
			店铺类型：{{keyword3.DATA}}
			{{remark.DATA}}*/
			String shopId = request.getParameter("shopId");
			SysShop shop = logManager.getNoTypeSysShopByShopId(shopId);
			templateId = "Jrg5KuZUY6fmESopzEf9ysJJUWHAjJeeVewCT1rzTb4";//店铺申请模板Id
			paras.add(new TemplateParam("first",state,"#FF3333"));  
			paras.add(new TemplateParam("keyword1",shop.getLinkman(),"#0044BB"));  
			paras.add(new TemplateParam("keyword2",shop.getCallTel(),"#0044BB"));  
			paras.add(new TemplateParam("keyword3",shop.getShopType().getTypeName(),"#0044BB"));  
			paras.add(new TemplateParam("remark","感谢你对我们的支持!!!!","#AAAAAA"));
		}
		if(mode.equals("4")){//失败
			/*申请人：{{keyword1.DATA}}
			审核状态：{{keyword2.DATA}}
			未通过原因：{{keyword3.DATA}}*/
			String reason = request.getParameter("reason");
			templateId = "1TgYSHwpgjpmLfzFxKMdVtj6wiHFkrhY4V5o9N-yJ2w";//店铺申请模板Id
			paras.add(new TemplateParam("first",state,"#FF3333"));  
			paras.add(new TemplateParam("keyword1",sysUser.getUserName(),"#0044BB"));  
			paras.add(new TemplateParam("keyword2","未通过","#0044BB"));  
			paras.add(new TemplateParam("keyword3",reason,"#0044BB")); 
		}
		
		Template tem=new Template();  
		tem.setTemplateId(templateId);  
		tem.setTopColor("#00DD00");  
		tem.setToUser(openId);  
		tem.setUrl("");  
		
		
		
		tem.setTemplateParamList(paras);  
		
		boolean result=sendTemplateMsg(acesstk,tem);  
	}
          
}