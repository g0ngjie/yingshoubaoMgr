package com.cosin.web.dao;

import java.util.List;






import org.hibernate.CacheMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.CriteriaSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;


/**
 * 数据库操作基类
 * @author zhb
 */
@Controller
public class BaseDao {
	@Autowired
	protected SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public  Object findObjectById(Class c, String fieldName, int id)
	{
		String hql = "from " + c.getName() + " where " + fieldName + "=" + id;
		List list = query(hql);
		if(list.size() > 0)
			return list.get(0);
		return null;
	}
	public int queryCount(String hql)
	{
		Session session = sessionFactory.getCurrentSession();
		//session.clear();
		Query query = session.createQuery(hql);
		int count = ((Number) query.iterate().next()).intValue();
		return count;
	}
	
	public  List query(String sql)
	{
		Session session = sessionFactory.getCurrentSession();
		//session.clear();
		//String hql="from Org as a where a.aname=:name";
		String hql=sql;
		Query query=session.createQuery(hql);
		query.setCacheMode(CacheMode.IGNORE);
		//query.setString("name", name); 
		List list = query.list();
		list = query.list();
		return list;
	}
	
	
	public  List query(String sql, int start, int limit)
	{
		Session session = sessionFactory.getCurrentSession();
		session.clear();
		//String hql="from Org as a where a.aname=:name";
		String hql=sql;
		Query query=session.createQuery(hql);
		query.setCacheMode(CacheMode.IGNORE);
		//start = (start-1)*limit;
		query.setFirstResult(start);
		query.setMaxResults(limit);
		//query.setString("name", name); 
		List list = query.list();

		return list;
	}
	
	public  List queryNavite(String sql, int start, int limit)
	{
		Session session = sessionFactory.getCurrentSession();
		//session.clear();
		//String hql="from Org as a where a.aname=:name";
		String hql=sql;
		Query query=session.createSQLQuery(hql);
		query.setCacheMode(CacheMode.IGNORE);
		query.setFirstResult(start);
		query.setMaxResults(limit);
		query.setResultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP);
		//query.setString("name", name); 
		List list = query.list();
		return list;
	}
	
	public  List queryNavite(String sql)
	{
		Session session = sessionFactory.getCurrentSession();
		//session.clear();
		Query query=session.createSQLQuery(sql);
		query.setCacheMode(CacheMode.IGNORE);
		query.setResultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP);
		List list = query.list();
		return list;
	}
	
//	public static void closeDb()
//	{
//		HibernateSessionFactory.closeSession();
//	}
	
	public void save(Object obj)
	{
		Session session = sessionFactory.getCurrentSession();
		//Transaction tx=session.beginTransaction();
		session.saveOrUpdate(obj);
		//tx.commit();
	}
	public void delete(Object obj)
	{
		Session session = sessionFactory.getCurrentSession();
		//Transaction tx=session.beginTransaction();
		session.delete(obj);
		//tx.commit();
	}
	
	//保存坐标
		public void executeNavite(String sql)
		{
			Session session = sessionFactory.getCurrentSession();
			//ssion.clear();
			Query query=session.createSQLQuery(sql);
			query.setCacheMode(CacheMode.IGNORE);
			query.setResultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP);
			query.executeUpdate();
		}

}
