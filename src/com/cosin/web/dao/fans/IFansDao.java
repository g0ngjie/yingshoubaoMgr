package com.cosin.web.dao.fans;

import java.util.List;

import com.cosin.web.entity.Fans;
import com.cosin.web.entity.SysOrder;
import com.cosin.web.entity.SysShop;
import com.cosin.web.entity.WithdrawCashRecord;

public interface IFansDao {
	
	/**
	 * 查询商家信息
	 * @param start
	 * @param limit
	 * @return
	 */
	public List<SysShop> getPushMessage(String SsShopName,Integer start, Integer limit);
	
	public int getinfofenye(String SsShopName);

	/**
	 * 崔青山
	 * 2016年12月1日
	 * @param shopId
	 * @param integer
	 * @param integer2
	 * @return
	 */
	public List<Fans> getFans(String shopId,String SsFensiName1, Integer integer, Integer integer2);

	/**
	 * 崔青山
	 * 2016年12月1日
	 * @param shopId
	 * @return
	 */
	public int getFansfenye(String shopId,String SsFensiName1);

	/**
	 * 崔青山
	 * 2016年12月1日
	 * @param fansId
	 * @param integer
	 * @param integer2
	 * @return
	 */
	public List<Fans> getFans1(String fansId,String SsFensiName2, Integer integer, Integer integer2);

	/**
	 * 崔青山
	 * 2016年12月1日
	 * @param fansId
	 * @return
	 */
	public int getFansfenye1(String fansId,String SsFensiName2);

	/**
	 * 崔青山
	 * 2016年12月2日
	 * @param userId
	 * @return
	 */
	public List<SysOrder> getOrder(String userId);

	/**
	 * 崔青山
	 * 2016年12月2日
	 * @param shopId
	 * @param ssFensiName1
	 * @return
	 */
	public List<Fans> getFans(String shopId, String ssFensiName1);


	/**
	 * 崔青山
	 * 2016年12月2日
	 * @param userId
	 * @return
	 */
	public List<WithdrawCashRecord> getWithdrawCashRecord(String userId);

	/**
	 * 崔青山
	 * 2016年12月5日
	 * @param fansId
	 * @return
	 */
	public List<Fans> getFans2(String fansId);

	/**
	 * 2016年12月27日下午3:05:38
	 *  阳朔
	 *  注释:查询 关联 shopId 的一级粉丝 parentUserId 为Null
	 */
	public List<Fans> getOneFansByShopId(String shopId,String userId);

	/**
	 * 2016年12月27日下午4:56:42
	 *  阳朔
	 *  注释:
	 */
	public List<Fans> getFansByParentUserId(String shopId, String userId);

	/**
	 * 2017年1月11日下午7:03:19
	 *  阳朔
	 *  注释:
	 */
	public List getOrderByShopId(String shopId);

	/**
	 * 2017年1月11日下午8:03:27
	 *  阳朔
	 *  注释:
	 */
	public SysShop getSysShopByShopId(String shopId);



	
}
