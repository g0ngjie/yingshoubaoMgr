package com.cosin.web.dao.order;

import java.util.List;

import com.cosin.web.entity.CommentShop;
import com.cosin.web.entity.SysOrder;

public interface IOrderDao {
	
	/**
	 * 查询订单信息
	 * @param start
	 * @param limit
	 * @return
	 */
	public List<SysOrder> getPushMessage(String proName ,String cityName, String SsUserName,String SsShopName,String SsOrderNum,String startDate,String endDate,Integer start, Integer limit);
	
	public int getinfofenye(String proName ,String cityName, String SsUserName,String SsShopName,String SsOrderNum,String startDate,String endDate);

	/**
	 * 崔青山
	 * 2016年11月23日
	 * @param sjName
	 * @param name
	 * @param date
	 * @param start
	 * @param limit
	 * @return
	 */
	public List<CommentShop> getCommentShop(String shopId, String sjName, String name,
			String date, Integer start, Integer limit);

	/**
	 * 崔青山
	 * 2016年11月23日
	 * @param sjName
	 * @param name
	 * @param date
	 * @return
	 */
	public int getShopCommentfenye(String shopId, String sjName, String name, String date);

	/**
	 * 崔青山
	 * 2016年11月23日
	 * @param key
	 */
	public void delShopComment(String key);

	
	
}
