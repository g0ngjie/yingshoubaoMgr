package com.cosin.web.dao.order;

import java.util.List;

import com.cosin.web.entity.ShopActivity;
import com.cosin.web.entity.SysShop;


public interface IShopActivityDao {

	/**
	 * 查询feedbackId数据
	 * 崔青山
	 * 2016年7月12日
	 */
	List<ShopActivity> getPrice(String ssuser,String ssksDate,String ssjsDate,Integer integer, Integer integer2);
	
	/**
	 * 查询分页数据
	 * 崔青山
	 * 2016年7月28日
	 */
	int getPriceListFenye(String ssuser,String ssksDate,String ssjsDate);

	/**
	 * 删除
	 * 崔青山
	 * 2016年7月25日
	 * @param key
	 */
	void delPriceQiang(String key);

	/**
	 * 崔青山
	 * 2016年9月1日
	 * @param key
	 */
	void readPriceQiang(String key);

	/**
	 * 崔青山
	 * 2016年11月28日
	 * @param shopId
	 * @return
	 */
	SysShop getShopId(String shopId);

	/**
	 * 崔青山
	 * 2016年11月28日
	 * @param shopActivityId
	 * @return
	 */
	ShopActivity getShopActivityId(String shopActivityId);

	/**
	 * 崔青山
	 * 2016年11月28日
	 * @param shopActivity
	 */
	void saveShopActivity(ShopActivity shopActivity);

	/**
	 * 崔青山
	 * 2016年11月28日
	 * @return
	 */
	List<SysShop> getSysShop();
	
}
