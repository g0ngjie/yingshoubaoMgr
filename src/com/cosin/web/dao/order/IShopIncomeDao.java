package com.cosin.web.dao.order;

import java.util.List;

import com.cosin.web.entity.PlatformStatistic;
import com.cosin.web.entity.ShopStatistic;
import com.cosin.web.entity.ShopType;
import com.cosin.web.entity.SysOrder;


public interface IShopIncomeDao {

	/**
	 * 查询feedbackId数据
	 * 崔青山
	 * 2016年7月12日
	 */
	List getPrice(String ssuser,String beginDate,String endDate,String proName, String cityName,String shopTypeId, Integer integer, Integer integer2);
	
	/**
	 * 查询分页数据
	 * 崔青山
	 * 2016年7月28日
	 */
	List getPriceListFenye(String ssuser,String beginDate,String endDate,String proName ,String cityName,String shopTypeId);

	/**
	 * 删除
	 * 崔青山
	 * 2016年7月25日
	 * @param key
	 */
	void delPriceQiang(String key);

	/**
	 * 2017年1月4日下午3:34:01
	 *  阳朔
	 *  注释:
	 */
	List<PlatformStatistic> getPlatformStatisticList(String beginDate, String endDate, Integer integer, Integer integer2);

	/**
	 * 2017年1月4日下午3:35:16
	 *  阳朔
	 *  注释:
	 */
	int getPlatformStatisticCount(String beginDate, String endDate);

	/**
	 * 2017年1月8日下午12:44:49
	 *  阳朔
	 *  注释:
	 */
	List<ShopType> getShopTypeList();

	/**
	 * 2017年1月12日上午9:57:56
	 *  阳朔
	 *  注释:
	 */
	List<SysOrder> getShopOrderList(String orderNum, String startDate,
			String endDate, String shopId, Integer integer, Integer integer2);

	/**
	 * 2017年1月12日上午9:58:01
	 *  阳朔
	 *  注释:
	 */
	int getShopOrderCount(String orderNum, String startDate, String endDate,
			String shopId);

	/**
	 * 2017年1月12日上午11:28:30
	 *  阳朔
	 *  注释:
	 */
	List getYearAndMonthSelectOrderNum(String shopId, String forYear,
			String forMonth);

	/**
	 * 2017年1月12日下午3:05:06
	 *  阳朔
	 *  注释:
	 */
	List getAllPrice(String toDay);

	/**
	 * 2017年1月12日下午3:55:35
	 *  阳朔
	 *  注释:
	 */
	List getAllUserTakePrice(String toDay);

	/**
	 * 2017年1月12日下午4:06:55
	 *  阳朔
	 *  注释:
	 */
	List<PlatformStatistic> getPlatformStatisticAll(String beginDate,
			String endDate);

	/**
	 * 2017年1月12日下午5:07:35
	 *  阳朔
	 *  注释:
	 */
	List getselectTuForPlay(String forYear, String forMonth);

	
}
