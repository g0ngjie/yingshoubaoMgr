package com.cosin.web.dao.order;

import java.util.List;
import java.util.Map;

import com.cosin.web.entity.Fans;
import com.cosin.web.entity.SysUser;
import com.cosin.web.entity.UserStatistic;


public interface IUserIncomeDao {

	/**
	 * 2017年1月4日下午4:10:15
	 *  阳朔
	 *  注释:
	 */
	List getUserStatisticList(String ssuser, String beginDate,String endDate ,
			Integer integer, Integer integer2);
	/**
	 * 2017年1月4日下午4:10:20
	 *  阳朔
	 *  注释:
	 */
	List getUserStatisticCount(String ssuser, String beginDate, String endDate);
	/**
	 * 2017年1月9日上午9:55:45
	 *  阳朔
	 *  注释:
	 */
	SysUser getSysUserById(String userId);
	/**
	 * 2017年1月9日上午9:57:47
	 *  阳朔
	 *  注释:
	 */
	void saveObj(Object obj);
	/**
	 * 2017年1月12日下午6:04:01
	 *  阳朔
	 *  注释:
	 */
	List getWithdrawCashRecordByUserId(String userId);
	/**
	 * 2017年1月12日下午6:44:09
	 *  阳朔
	 *  注释:
	 */
	List getFansByParentUserId(String userId);
	/**
	 * 2017年1月12日下午7:03:44
	 *  阳朔
	 *  注释:
	 */
	List getFenRunDetailByUserData(String userId, String topUserId);
	/**
	 * 2017年1月12日下午7:57:11
	 *  阳朔
	 *  注释:查询普通用户 type= 1
	 */
	SysUser getPTSysUserById(String fansUserId);
	

	
	
}
