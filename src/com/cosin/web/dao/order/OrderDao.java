package com.cosin.web.dao.order;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.cosin.web.dao.BaseDao;
import com.cosin.web.entity.CommentShop;
import com.cosin.web.entity.SysOrder;
import com.cosin.web.entity.SysUser;
@Repository
public class OrderDao extends BaseDao implements IOrderDao{

	@Override
	public List<SysOrder> getPushMessage(String proName ,String cityName, String SsUserName,String SsShopName,String SsOrderNum,String startDate,String endDate,Integer start, Integer limit) {
		String sql="from SysOrder as a where 1=1 ";//order by a.createDate desc";
		if(proName != null && !"".equals(proName))
		{
			sql += " and a.sysShop.shopAddress like '%" + proName + "%'";
		}
		if(cityName != null && !"".equals(cityName))
		{
			sql += " and a.sysShop.shopAddress like '%" + cityName + "%'";
		}
		if(SsUserName != null && !"".equals(SsUserName))
		{
			sql += " and a.sysUser.userName like '%" + SsUserName + "%'";
		}
		if(SsShopName != null && !"".equals(SsShopName))
		{
			sql += " and a.sysShop.shopName like '%" + SsShopName + "%'";
		}
		if(SsOrderNum != null && !"".equals(SsOrderNum))
		{
			sql += " and a.orderNum like '%" + SsOrderNum + "%'";
		}
		if(startDate != null && !"".equals(startDate))
		{
			sql += " and a.createDate >= '" + startDate + " 00:00:00'";
		}
		if(endDate != null && !"".equals(endDate))
		{
			sql += " and a.createDate <= '" + endDate + " 23:59:59'";
		}
		sql += " and a.state=1 order by a.createDate desc";
		return query(sql,start,limit);
	}

	
	@Override
	public int getinfofenye(String proName ,String cityName, String SsUserName,String SsShopName,String SsOrderNum,String startDate,String endDate) {
		String sql="select count(*) from SysOrder as a where 1=1";
		if(proName != null && !"".equals(proName))
		{
			sql += " and a.sysShop.shopAddress like '%" + proName + "%'";
		}
		if(cityName != null && !"".equals(cityName))
		{
			sql += " and a.sysShop.shopAddress like '%" + cityName + "%'";
		}
		if(SsUserName != null && !"".equals(SsUserName))
		{
			sql += " and a.sysUser.userName like '%" + SsUserName + "%'";
		}
		if(SsShopName != null && !"".equals(SsShopName))
		{
			sql += " and a.sysShop.shopName like '%" + SsShopName + "%'";
		}
		if(SsOrderNum != null && !"".equals(SsOrderNum))
		{
			sql += " and a.orderNum like '%" + SsOrderNum + "%'";
		}
		if(startDate != null && !"".equals(startDate))
		{
			sql += " and a.createDate >= '" + startDate + " 00:00:00'";
		}
		if(endDate != null && !"".equals(endDate))
		{
			sql += " and a.createDate <= '" + endDate + " 23:59:59'";
		}
		sql += " and a.state=1 order by a.createDate desc";
		return queryCount(sql);
	}

	@Override
	public List<CommentShop> getCommentShop(String shopId, String sjName, String SsOrderNum,
			String ssShop, Integer start, Integer limit) {
		// TODO Auto-generated method stub
		String sql="from CommentShop as a where 1=1 ";
		if(shopId != null && !"".equals(shopId))
		{
			sql += " and a.sysShop = '" + shopId + "'";
		}
		if(sjName != null && !"".equals(sjName))
		{
			sql += " and a.sysUser.userName like '%" + sjName + "%'";
		}
		if(SsOrderNum != null && !"".equals(SsOrderNum))
		{
			sql += " and a.sysOrder.orderNum like '%" + SsOrderNum + "%'";
		}
		if(ssShop != null && !"".equals(ssShop))
		{
			sql += " and a.sysShop.sysUser.userName like '%" + ssShop + "%'";
		}
		sql += " order by a.createDate desc";
		return query(sql,start,limit);
	}


	/* 
	 * 崔青山
	 * 2016年11月23日
	 * 上午9:27:02
	 */
	@Override
	public int getShopCommentfenye(String shopId, String sjName, String SsOrderNum, String ssShop) {
		// TODO Auto-generated method stub
		String sql="select count(*) from CommentShop as a where 1=1";
		if(shopId != null && !"".equals(shopId))
		{
			sql += " and a.sysShop = '" + shopId + "'";
		}
		if(sjName != null && !"".equals(sjName))
		{
			sql += " and a.sysUser.userName like '%" + sjName + "%'";
		}
		if(SsOrderNum != null && !"".equals(SsOrderNum))
		{
			sql += " and a.sysOrder.orderNum like '%" + SsOrderNum + "%'";
		}
		if(ssShop != null && !"".equals(ssShop))
		{
			sql += " and a.sysShop.sysUser.userName like '%" + ssShop + "%'";
		}
		sql += " order by a.createDate desc";
		return queryCount(sql);
	}

	/* 
	 * 崔青山
	 * 2016年11月23日
	 * 上午10:12:46
	 */
	@Override
	public void delShopComment(String key) {
		// TODO Auto-generated method stub\
		CommentShop comment = getCommentShopId(key);
		super.delete(comment);
	}


	/**
	 * 崔青山
	 * 2016年11月23日
	 * @param key
	 * @return
	 */
	public CommentShop getCommentShopId(String key) {
		// TODO Auto-generated method stub
		String sql="from CommentShop as a where a.commentId='"+ key +"'";
		List<CommentShop> list = query(sql);
		if (list.size()>0) 
			return list.get(0);
		return null;
	}
}
