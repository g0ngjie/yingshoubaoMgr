package com.cosin.web.dao.order;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.cosin.web.dao.BaseDao;
import com.cosin.web.entity.FeedBack;
import com.cosin.web.entity.ShopActivity;
import com.cosin.web.entity.SysShop;
@Repository
public class ShopActivityDao extends BaseDao implements IShopActivityDao{

	@Override
	public List<ShopActivity> getPrice(String ssuser,String ssksDate,String ssjsDate,Integer integer, Integer integer2) {
		// TODO Auto-generated method stub
		
		String sql="from ShopActivity as a where 1=1";
		
		if(ssuser != null && !"".equals(ssuser))
		{
			sql += " and a.sysShop.sysUser.userName like '%" + ssuser + "%'";
		}
		if(ssksDate != null && !"".equals(ssksDate))
		{
			sql += " and a.beginTime >= '" + ssksDate + " 00:00:00'";
		}
		if(ssjsDate != null && !"".equals(ssjsDate))
		{
			sql += " and a.endTime <= '" + ssjsDate + " 23:59:59'";
		}
		sql+=" order by a.createDate desc";
		return query(sql,integer,integer2);
	}


	/* 
	 * 查询分页数据
	 * 崔青山
	 * 2016年7月28日
	 * 下午5:26:57
	 */
	@Override
	public int getPriceListFenye(String ssuser,String ssksDate,String ssjsDate) {
		// TODO Auto-generated method stub
		String hql = "select count(*) from ShopActivity as a where 1=1";
		if(ssuser != null && !"".equals(ssuser))
		{
			hql += " and a.sysShop.sysUser.userName like '%" + ssuser + "%'";
		}
		if(ssksDate != null && !"".equals(ssksDate))
		{
			hql += " and a.beginTime >= '" + ssksDate + " 00:00:00'";
		}
		if(ssjsDate != null && !"".equals(ssjsDate))
		{
			hql += " and a.endTime <= '" + ssjsDate + " 23:59:59'";
		}
		hql+=" order by a.createDate desc";
		return queryCount(hql);
	}
	/* 
	 * 删除
	 * 崔青山
	 * 2016年7月25日
	 * 下午8:34:25
	 */
	@Override
	public void delPriceQiang(String key) {
		// TODO Auto-generated method stub
		ShopActivity countdown = getByShopActivityId(key);
		super.delete(countdown);
	}
	
	/**
	 * 崔青山
	 * 2016年7月25日
	 * @param key
	 * @return
	 */
	private ShopActivity getByShopActivityId(String key) {
		// TODO Auto-generated method stub
		String hql = "from ShopActivity as a where a.shopActivityId='" + key + "'";
		List<ShopActivity> list = query(hql);
		if(list.size()>0)
			return list.get(0);
		return null;
	}


	/* 
	 * 崔青山
	 * 2016年9月1日
	 * 上午11:54:35
	 */
	@Override
	public void readPriceQiang(String key) {
		// TODO Auto-generated method stub
		/*FeedBack countdown = getByFeedBackId(key);
	//	countdown.setIsRead(1);
		super.save(countdown);*/
	}


	/* 
	 * 崔青山
	 * 2016年11月28日
	 * 下午2:59:44
	 */
	@Override
	public SysShop getShopId(String shopId) {
		// TODO Auto-generated method stub
		String hql = "from SysShop as a where a.shopId='" + shopId + "'";
		List<SysShop> list = query(hql);
		if(list.size()>0)
			return list.get(0);
		return null;
	}


	/* 
	 * 崔青山
	 * 2016年11月28日
	 * 下午3:00:58
	 */
	@Override
	public ShopActivity getShopActivityId(String shopActivityId) {
		// TODO Auto-generated method stub
		String hql = "from ShopActivity as a where a.shopActivityId='" + shopActivityId + "'";
		List<ShopActivity> list = query(hql);
		if(list.size()>0)
			return list.get(0);
		return null;
	}


	/* 
	 * 崔青山
	 * 2016年11月28日
	 * 下午3:01:38
	 */
	@Override
	public void saveShopActivity(ShopActivity shopActivity) {
		// TODO Auto-generated method stub
		super.save(shopActivity);
	}


	/* 
	 * 崔青山
	 * 2016年11月28日
	 * 下午3:16:48
	 */
	@Override
	public List<SysShop> getSysShop() {
		// TODO Auto-generated method stub
		String hql = "from SysShop as a where a.sysUser.isDel=0 and a.sysUser.enable=1 and a.sysUser.type=2 ";
		List<SysShop> list = query(hql);
		return list;
	}
}

	


	


