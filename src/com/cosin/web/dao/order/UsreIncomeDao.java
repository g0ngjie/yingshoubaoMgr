package com.cosin.web.dao.order;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.cosin.web.dao.BaseDao;
import com.cosin.web.entity.Fans;
import com.cosin.web.entity.SysUser;
import com.cosin.web.entity.UserStatistic;
@Repository
public class UsreIncomeDao extends BaseDao implements IUserIncomeDao{

	/**
	 * 2017��1��4������4:10:24
	 *  ��˷
	 *  ע��:
	 */
	@Override
	public List getUserStatisticList(String ssuser,String beginDate, String endDate, Integer integer, Integer integer2) {
		String sql = "select sysUser.userId,sysUser.userName,SUM(price),SUM(income),SUM(num) from UserStatistic as a where 1 = 1 ";
		if(ssuser != null && !"".equals(ssuser))
		{
			sql += " and a.sysUser.userName like '%" + ssuser + "%'";
		}
		if(beginDate != null && !"".equals(beginDate))
		{
			sql += " and a.createDate >= '" + beginDate + "'";
		}
		if(endDate != null && !"".equals(endDate))
		{
			sql += "and a.createDate <='"+ endDate +"'";
		}
		sql+=" GROUP BY sysUser order by createDate desc";
		return query(sql,integer,integer2);
	}

	/**
	 * 2017��1��4������4:10:24
	 *  ��˷
	 *  ע��:
	 */
	@Override
	public List getUserStatisticCount(String ssuser, String beginDate ,String endDate) {
		//String hql = "select count(t.counts) from(select a.userId,count(a.userId) counts from user_statistic as a, sys_user as b where a.userId=b.userId and like a.userName= '" + ssuser + "' group by userId) t";
		String sql = "select count(t.counts) from(select a.userId,count(a.userId) counts from user_statistic as a, sys_user as b where a.userId=b.userId ";
		
		if(ssuser != null && !"".equals(ssuser))
		{
			sql +="AND b.userName like '%" + ssuser + "%'";
		}
		if(beginDate != null && !"".equals(beginDate))
		{
			sql +="AND a.createDate >= '" + beginDate + "'";
		}
		if(endDate != null && !"".equals(endDate))
		{
			sql +="AND a.createDate <= '" + endDate + "'";
		}
		sql +=" GROUP BY a.userId) t";
		return queryNavite(sql);
	}

	/**
	 * 2017��1��9������9:55:50
	 *  ��˷
	 *  ע��:
	 */
	@Override
	public SysUser getSysUserById(String userId) {
		String sql = "from SysUser as a where a.isDel = 0 and a.type = 3 and a.userId = '" + userId + "'";
		List<SysUser> list = query(sql);
		if(list.size() > 0)
			return list.get(0);
		return null;
	}

	/**
	 * 2017��1��9������9:58:01
	 *  ��˷
	 *  ע��:
	 */
	@Override
	public void saveObj(Object obj) {
		super.save(obj);
	}

	/**
	 * 2017��1��12������6:04:06
	 *  ��˷
	 *  ע��:
	 */
	@Override
	public List getWithdrawCashRecordByUserId(String userId) {
		String sql = "select sum(a.price) from withdraw_cash_record a where a.isThrough = 1 and a.userId = '" + userId + "'";
		return queryNavite(sql);
	}

	/**
	 * 2017��1��12������6:44:29
	 *  ��˷
	 *  ע��:
	 */
	@Override
	public List getFansByParentUserId(String userId) {
		String sql = "SELECT DISTINCT a.userId FROM fans a WHERE a.parentUserId = '" + userId + "' order by a.createDate asc";
		return queryNavite(sql);
	}

	/**
	 * 2017��1��12������7:03:55
	 *  ��˷
	 *  ע��:
	 */
	@Override
	public List getFenRunDetailByUserData(String userId, String topUserId) {
		String sql = "select sum(a.money) from fenrun_detail a where a.userId = '" + userId + "' and a.topUserId = '" + topUserId + "'";
		return queryNavite(sql);
	}

	/**
	 * 2017��1��12������7:57:25
	 *  ��˷
	 *  ע��:
	 */
	@Override
	public SysUser getPTSysUserById(String fansUserId) {
		String sql = "from SysUser as a where a.isDel = 0 and a.userId = '" + fansUserId + "'";
		List<SysUser> list = query(sql);
		if(list.size() > 0)
			return list.get(0);
		return null;
	}


}
