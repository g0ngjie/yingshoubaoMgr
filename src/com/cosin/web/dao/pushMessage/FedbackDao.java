package com.cosin.web.dao.pushMessage;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.cosin.web.dao.BaseDao;
import com.cosin.web.entity.FeedBack;
@Repository
public class FedbackDao extends BaseDao implements IFedbackDao{

	@Override
	public List<FeedBack> getPrice(String ssuser,String ssDate,Integer integer, Integer integer2) {
		// TODO Auto-generated method stub
		
		String sql="from FeedBack as a where 1=1";
		
		if(ssuser != null && !"".equals(ssuser))
		{
			sql += " and a.sysUser.userName like '%" + ssuser + "%'";
		}
		if(ssDate != null && !"".equals(ssDate))
		{
			sql += " and a.createDate >= '" + ssDate + " 00:00:00' and a.createDate <= '" + ssDate + " 23:59:59'";
		}
		sql+=" order by a.createDate desc";
		return query(sql,integer,integer2);
	}


	/* 
	 * 查询分页数据
	 * 崔青山
	 * 2016年7月28日
	 * 下午5:26:57
	 */
	@Override
	public int getPriceListFenye(String ssuser,String ssDate) {
		// TODO Auto-generated method stub
		Date date1 = new Date();       
		Timestamp shijian = new Timestamp(date1.getTime());
		String hql = "select count(*) from FeedBack as a where 1=1";
		if(ssuser != null && !"".equals(ssuser))
		{
			hql += " and a.sysUser.userName like '%" + ssuser + "%'";
		}
		if(ssDate != null && !"".equals(ssDate))
		{
			hql += " and a.createDate >= '" + ssDate + " 00:00:00' and a.createDate <= '" + ssDate + " 23:59:59'";
		}
		hql+=" order by a.createDate desc";
		return queryCount(hql);
	}
	/* 
	 * 删除
	 * 崔青山
	 * 2016年7月25日
	 * 下午8:34:25
	 */
	@Override
	public void delPriceQiang(String key) {
		// TODO Auto-generated method stub
		FeedBack countdown = getByFeedBackId(key);
		super.delete(countdown);
	}
	
	/**
	 * 崔青山
	 * 2016年7月25日
	 * @param key
	 * @return
	 */
	private FeedBack getByFeedBackId(String key) {
		// TODO Auto-generated method stub
		String hql = "from FeedBack as a where a.feedBackId='" + key + "'";
		List<FeedBack> list = query(hql);
		if(list.size()>0)
			return list.get(0);
		return null;
	}


	/* 
	 * 崔青山
	 * 2016年9月1日
	 * 上午11:54:35
	 */
	@Override
	public void readPriceQiang(String key) {
		// TODO Auto-generated method stub
		FeedBack countdown = getByFeedBackId(key);
	//	countdown.setIsRead(1);
		super.save(countdown);
	}


	/**
	 * 2017年1月8日下午4:04:34
	 *  阳朔
	 *  注释:
	 */
	@Override
	public FeedBack getFeedBackById(String feedbackId) {
		String sql = "from FeedBack as a where a.feedBackId = '" + feedbackId + "'";
		List<FeedBack> list = query(sql);
		if(list.size()>0)
			return list.get(0);
		return null;
	}
}

	


	


