package com.cosin.web.dao.pushMessage;

import java.util.List;

import com.cosin.web.entity.FeedBack;


public interface IFedbackDao {

	/**
	 * 查询feedbackId数据
	 * 崔青山
	 * 2016年7月12日
	 */
	List<FeedBack> getPrice(String ssuser,String ssDate,Integer integer, Integer integer2);
	
	/**
	 * 查询分页数据
	 * 崔青山
	 * 2016年7月28日
	 */
	int getPriceListFenye(String ssuser,String ssDate);

	/**
	 * 删除
	 * 崔青山
	 * 2016年7月25日
	 * @param key
	 */
	void delPriceQiang(String key);

	/**
	 * 崔青山
	 * 2016年9月1日
	 * @param key
	 */
	void readPriceQiang(String key);

	/**
	 * 2017年1月8日下午4:04:29
	 *  阳朔
	 *  注释:
	 */
	FeedBack getFeedBackById(String feedbackId);
	
}
