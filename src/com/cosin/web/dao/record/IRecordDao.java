package com.cosin.web.dao.record;

import java.util.List;

import com.cosin.web.entity.BindBank;
import com.cosin.web.entity.WithdrawCashRecord;
import com.cosin.web.entity.SysUser;

public interface IRecordDao {
	
	
	/**
	 * 崔青山
	 * 2016年8月21日
	 */
	public List<WithdrawCashRecord> getShenhe(String ssuser, String ssStartTime,
			Integer integer, Integer integer2);

	
	/**
	 * 审核分页
	 * 崔青山
	 * 2016年8月21日
	 */
	public int getinfofenyess(String ssuser, String ssStartTime);




	/**
	 * 崔青山
	 * 2016年8月23日
	 * @param key
	 * @return 
	 */
	public WithdrawCashRecord getTransactionRecordId(String key);

	
	public SysUser getSysUserId(String key);

	/**
	 * 崔青山
	 * 2016年8月23日
	 * @param wallet
	 */
	public void saveSysUser(SysUser wallet);

	
	/**
	 * 通过
	 * 崔青山
	 * 2016年8月21日
	 * @param key
	 * @param name 
	 */
	public void tgShenhe(String key);

	
	/**
	 * 拒绝
	 * 崔青山
	 * @param key
	 * @param name 
	 */
	public void jjShenhe(String key);


	
	/**
	 * 崔青山
	 * 2016年9月2日
	 * @param key
	 */
	public void delPush(String loginUserId,String key);

	/**
	 * 查询
	 * @param start
	 * @param limit
	 * @return
	 */
	public List<WithdrawCashRecord> getPushMessage(String ssuser,String SsJia,Integer start, Integer limit);

	/**
	 * 获取分页
	 * 崔青山
	 * 2016年7月29日
	 * @return 
	 */
	public int getinfofenye(String ssuser,String SsJia);


	/**
	 * 崔青山
	 * 2016年11月9日
	 * @param ssuser
	 * @param ssJia
	 * @param integer
	 * @param integer2
	 * @return
	 */
	public List<BindBank> getBankShenhe(String ssuser, String ssJia,
			Integer integer, Integer integer2);


	/**
	 * 崔青山
	 * 2016年11月9日
	 * @param bindBank
	 */
	public void saveBindBank(BindBank bindBank);


	/**
	 * 崔青山
	 * 2016年11月9日
	 * @param key
	 * @return
	 */
	public BindBank getBindBankId(String key);


	/**
	 * 2017年1月16日下午12:16:09
	 *  阳朔
	 *  注释:
	 */
	public void saveObj(Object obj);


	/**
	 * 2017年2月3日下午2:29:00
	 *  阳朔
	 *  注释:
	 */
	public WithdrawCashRecord getWithdrawCashRecordById(String cashId);



	
}
