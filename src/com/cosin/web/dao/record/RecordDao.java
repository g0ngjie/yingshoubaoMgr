package com.cosin.web.dao.record;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cosin.utils.UserSession;
import com.cosin.web.controller.log.ILogManager;
import com.cosin.web.dao.BaseDao;
import com.cosin.web.entity.BindBank;
import com.cosin.web.entity.WithdrawCashRecord;
import com.cosin.web.entity.SysUser;
@Repository
public class RecordDao extends BaseDao implements IRecordDao{

	@Autowired
	private ILogManager logManager;
	
	/**
	 * 崔青山
	 * 2016年8月21日
	 * 下午2:28:13
	 */
	@Override
	public List<WithdrawCashRecord> getShenhe(String ssuser,String ssJia, Integer integer, Integer integer2) {
		// TODO Auto-generated method stub
		String sql="from WithdrawCashRecord as a where 1=1";
		if(ssuser != null && !"".equals(ssuser))
		{
			sql += " and a.sysUser.userName like '%" + ssuser + "%'";
		}
		if(ssJia != null && !"".equals(ssJia))
		{
			if(ssJia.equals("0"))
				sql += " and a.enable=0";
			else
				sql += " and a.enable=2 ";
		}
		sql+=" and a.isThrough=0 order by createDate desc";
		return query(sql,integer,integer2);
	}


	/**
	 * 崔青山
	 * 2016年8月21日
	 * 下午3:34:12
	 */
	@Override
	public int getinfofenyess(String ssuser,String ssJia) {
		// TODO Auto-generated method stub
		String sql="select count(*) from WithdrawCashRecord as a where 1=1";
		if(ssuser != null && !"".equals(ssuser))
		{
			sql += " and a.sysUser.userName like '%" + ssuser + "%'";
		}
		if(ssJia != null && !"".equals(ssJia))
		{
			if(ssJia.equals("0"))
				sql += " and a.enable=0";
			else
				sql += " and a.enable=2 ";
		}
		sql+=" and a.isThrough=0 order by createDate desc";
		return queryCount(sql);
	}

	/**
	 * 崔青山
	 * 2016年8月23日
	 * 上午11:07:37
	 */
	@Override
	public WithdrawCashRecord getTransactionRecordId(String key) {
		// TODO Auto-generated method stub
		String hql = "from WithdrawCashRecord as a where a.cashId = '" + key + "'";
		List<WithdrawCashRecord> list = query(hql);
		if(list.size()>0)
			return list.get(0);
		return null;
	}
	
	/**
	 * 崔青山
	 * 2016年8月22日
	 * 下午10:22:13
	 */
	@Override
	public SysUser getSysUserId(String key) {
		String hql = "from SysUser as a where a.userId = '" + key + "'";
		List<SysUser> list = query(hql);
		if(list.size()>0)
			return list.get(0);
		return null;
	}

	/**
	 * 崔青山
	 * 2016年8月23日
	 * 上午1:38:53
	 */
	@Override
	public void saveSysUser(SysUser wallet) {
		// TODO Auto-generated method stub
		super.save(wallet);
	}
	
	/**
	 * 崔青山
	 * 2016年8月21日
	 * 下午3:10:01
	 */
	@Override
	public void tgShenhe(String key) {
		// TODO Auto-generated method stub
		WithdrawCashRecord record = getTransactionRecordId(key);
		record.setIsThrough(1);;
		super.save(record);
	}
	
	/**
	 *崔青山
	 * 2016年8月21日
	 * 下午3:15:44
	 */
	@Override
	public void jjShenhe(String key) {
		// TODO Auto-generated method stub
		WithdrawCashRecord record = getTransactionRecordId(key);
		record.setIsThrough(2);
		super.save(record);
	}

	/**
	 * 崔青山
	 * 2016年9月2日
	 * 上午10:51:22
	 */
	@Override
	public void delPush(String loginUserId,String key) {
		// TODO Auto-generated method stub
		WithdrawCashRecord message = getTransactionRecordId(key);
		super.delete(message);

		logManager.saveLog(loginUserId, "提现记录", "删除操作", message.getSysUser().getUserName()+"记录删除");
	}

	@Override
	public List<WithdrawCashRecord> getPushMessage(String ssuser,String ssJia,Integer start, Integer limit) {
		// TODO Auto-generated method stub
		String sql="from WithdrawCashRecord as a where 1=1";
		if(ssuser != null && !"".equals(ssuser))
		{
			sql += " and a.sysUser.userName like '%" + ssuser + "%'";
		}
		if(ssJia != null && !"".equals(ssJia))
		{
			if(ssJia.equals("1"))
				sql += " and a.isThrough=1";
			else
				sql += " and a.isThrough=2 ";
		}
		sql+=" and a.isThrough!=0 order by createDate desc";
		return query(sql,start,limit);
	}

	
	/** 
	 * 崔青山
	 * 2016年7月29日
	 * 上午10:28:48
	 */
	@Override
	public int getinfofenye(String ssuser,String ssJia) {
		// TODO Auto-generated method stub
		String sql="select count(*) from WithdrawCashRecord as a where 1=1";
		if(ssuser != null && !"".equals(ssuser))
		{
			sql += " and a.sysUser.userName like '%" + ssuser + "%'";
		}
		if(ssJia != null && !"".equals(ssJia))
		{
			if(ssJia.equals("1"))
				sql += " and a.isThrough=1";
			else
				sql += " and a.isThrough=2 ";
		}
		sql+=" and a.isThrough!=0 order by createDate desc";
		return queryCount(sql);
	}


	/* 
	 * 崔青山
	 * 2016年11月9日
	 * 上午9:47:46
	 */
	@Override
	public List<BindBank> getBankShenhe(String ssuser, String ssJia,
			Integer integer, Integer integer2) {
		// TODO Auto-generated method stub
		String sql="from BindBank as a where 1=1";
		if(ssuser != null && !"".equals(ssuser))
		{
			sql += " and a.sysUser.userName like '%" + ssuser + "%'";
		}
		if(ssJia != null && !"".equals(ssJia))
		{
			if(ssJia.equals("0"))
				sql += " and a.enable=0";
			else
				sql += " and a.enable=2 ";
		}
		sql+=" and a.isDel=0 and a.isReviewed!=1 order by createDate desc";
		return query(sql,integer,integer2);
	}


	/* 
	 * 崔青山
	 * 2016年11月9日
	 * 上午9:58:01
	 */
	@Override
	public void saveBindBank(BindBank bindBank) {
		// TODO Auto-generated method stub
		super.save(bindBank);
	}


	/* 
	 * 崔青山
	 * 2016年11月9日
	 * 上午9:58:20
	 */
	@Override
	public BindBank getBindBankId(String key) {
		// TODO Auto-generated method stub
		String hql = "from BindBank as a where a.bindBankId = '" + key + "'";
		List<BindBank> list = query(hql);
		if(list.size()>0)
			return list.get(0);
		return null;
	}


	/**
	 * 2017年1月16日下午12:16:20
	 *  阳朔
	 *  注释:
	 */
	@Override
	public void saveObj(Object obj) {
		super.save(obj);
	}


	/**
	 * 2017年2月3日下午2:29:04
	 *  阳朔
	 *  注释:
	 */
	@Override
	public WithdrawCashRecord getWithdrawCashRecordById(String cashId) {
		String sql = "from WithdrawCashRecord as a where a.cashId = '" + cashId + "'";
		List<WithdrawCashRecord> list = query(sql);
		if(list.size()>0)
			return list.get(0);
		return null;
	}

	

	
}
