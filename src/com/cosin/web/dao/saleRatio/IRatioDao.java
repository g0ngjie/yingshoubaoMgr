package com.cosin.web.dao.saleRatio;

import java.util.List;

import com.cosin.web.entity.Ratio;
import com.cosin.web.entity.SysShop;


public interface IRatioDao {

	/**
	 * 2016��12��19������3:41:27
	 *  ��˷
	 *  ע��:
	 */
	List<Ratio> getRatioListLimit(Integer start, Integer limit);

	/**
	 * 2016��12��19������3:41:30
	 *  ��˷
	 *  ע��:
	 */
	int getRatioListCount();

	/**
	 * 2016��12��21������10:11:17
	 *  ��˷
	 *  ע��:
	 */
	Ratio getRatioById(String ratioId);

	/**
	 * 2016��12��21������11:04:22
	 *  ��˷
	 *  ע��:
	 */
	void saveObj(Object obj);

	/**
	 * 2016��12��21������11:47:18
	 *  ��˷
	 *  ע��:
	 */
	List<Ratio> getThreeDataLimit(Integer start, Integer limit);

	/**
	 * 2016��12��21������12:24:29
	 *  ��˷
	 *  ע��:
	 */
	int getThreeDataCount();

	/**
	 * 2017��1��9������4:20:12
	 *  ��˷
	 *  ע��:
	 */
	Ratio getRatioFormaBili();

	/**
	 * 2017��1��16������4:02:04
	 *  ��˷
	 *  ע��:type = 1 isDel = 0
	 */
	List<SysShop> getSysShopList();
	


	
}
