package com.cosin.web.dao.saleRatio;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.cosin.web.dao.BaseDao;
import com.cosin.web.entity.Ratio;
import com.cosin.web.entity.SysShop;
@Repository
public class RatioDao extends BaseDao implements IRatioDao{

	/**
	 * 2016��12��19������3:41:37
	 *  ��˷
	 *  ע��:
	 */
	@Override
	public List<Ratio> getRatioListLimit(Integer start, Integer limit) {
		String sql = "from Ratio as a where a.third = null";
		return query(sql,start,limit);
	}

	/**
	 * 2016��12��19������3:41:37
	 *  ��˷
	 *  ע��:
	 */
	@Override
	public int getRatioListCount() {
		String sql="select count(*) from Ratio as a where a.second = null and a.third = null";
		return queryCount(sql);
	}

	/**
	 * 2016��12��21������10:11:25
	 *  ��˷
	 *  ע��:
	 */
	@Override
	public Ratio getRatioById(String ratioId) {
		String sql = "from Ratio as a where a.ratioId = '" + ratioId + "'";
		List<Ratio> list = query(sql);
		if(list.size() > 0) 
			return list.get(0);
		return null;
	}

	/**
	 * 2016��12��21������11:04:34
	 *  ��˷
	 *  ע��:
	 */
	@Override
	public void saveObj(Object obj) {
		super.save(obj);
	}

	/**
	 * 2016��12��21������11:47:24
	 *  ��˷
	 *  ע��:
	 */
	@Override
	public List<Ratio> getThreeDataLimit(Integer start, Integer limit) {
		String sql = "from Ratio as a where a.ratioId = 1";
		return query(sql,start,start);
	}

	/**
	 * 2016��12��21������12:24:35
	 *  ��˷
	 *  ע��:
	 */
	@Override
	public int getThreeDataCount() {
		String sql="select count(*) from Ratio as a where a.ratioId = 1";
		return queryCount(sql);
	}

	/**
	 * 2017��1��9������4:20:20
	 *  ��˷
	 *  ע��:
	 */
	@Override
	public Ratio getRatioFormaBili() {
		String sql = "from Ratio as a where a.ratioId = 2";
		return (Ratio) query(sql).get(0);
	}

	/**
	 * 2017��1��16������4:02:16
	 *  ��˷
	 *  ע��:
	 */
	@Override
	public List<SysShop> getSysShopList() {
		String sql = "from SysShop as a where a.type = 1 and a.isDel = 0";
		return query(sql);
	}




}
