package com.cosin.web.dao.shop;

import java.util.List;

import com.cosin.web.entity.QrBackimg;
import com.cosin.web.entity.ShopType;
import com.cosin.web.entity.SysShop;

public interface IServiceDao {
	
	/**
	 * 查询工程师信息
	 * @param start
	 * @param limit
	 * @return
	 */
	public List<ShopType> getPushMessage(Integer start, Integer limit);

	/**
	 * 崔青山
	 * 2016年11月23日
	 * @return
	 */
	public int getinfofenye();

	/**
	 * 崔青山
	 * 2016年11月23日
	 * @param shopTypeId
	 * @return
	 */
	public ShopType getShopTypeId(String shopTypeId);

	/**
	 * 崔青山
	 * 2016年11月23日
	 * @param push
	 */
	public void setPushMessage(ShopType push);

	/**
	 * 崔青山
	 * 2016年11月23日
	 * @return
	 */
	public ShopType getShopTypeSuan();

	/**
	 * 崔青山
	 * 2016年11月23日
	 * @param key
	 */
	public void delPush(String key);

	/**
	 * 崔青山
	 * 2016年11月23日
	 * @return
	 */
	public List<ShopType> getShopType();

	/**
	 * 2016年12月26日下午4:27:11
	 *  阳朔
	 *  注释:
	 */
	public List<SysShop> chaSysShopByShopTypeId(String shopTypeId);

	/**
	 * 2017年1月13日下午2:41:14
	 *  阳朔
	 *  注释:
	 */
	public List<QrBackimg> getQrBackimgList(Integer start, Integer limit);

	/**
	 * 2017年1月13日下午2:41:17
	 *  阳朔
	 *  注释:
	 */
	public int getQrBackimgCount();

	/**
	 * 2017年1月13日下午3:05:34
	 *  阳朔
	 *  注释:
	 */
	public void saveObj(Object obj);

	/**
	 * 2017年1月13日下午3:05:38
	 *  阳朔
	 *  注释:
	 */
	public QrBackimg getQrBackimgById(String qrBackImgId);
	
	

}
