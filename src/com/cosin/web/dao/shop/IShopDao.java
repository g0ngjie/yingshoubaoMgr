package com.cosin.web.dao.shop;

import java.util.List;
import java.util.Map;

import com.cosin.web.entity.ShopActivity;
import com.cosin.web.entity.SysShop;
import com.cosin.web.entity.SysUser;


public interface IShopDao {

	/**
	 * 查询shop表中的数据
	 * 崔青山
	 * 2016年11月21日
	 * @param ssenable
	 * @param ssName
	 * @param integer
	 * @param integer2
	 * @return
	 */
	List<SysShop> getSysUser(String SjName, String Name,String tel, Integer integer, Integer integer2);

	/**
	 * 查询分页数据
	 * 崔青山
	 * 2016年11月21日
	 * @param ssName
	 * @param ssenable
	 * @return
	 */
	int getShopFenye(String SjName, String Name,String tel);

	
	/**
	 * 通过核审
	 * 崔青山
	 * 2016年11月21日
	 * @param key
	 */
	Map passShop(String userId,String key);

	/**
	 * 拒绝核审
	 * 崔青山
	 * 2016年11月21日
	 * @param key
	 * @param reason 
	 */
	Map refuseShop(String userId,String key, String reason);
	
	/**
	 * 查询要删除的数据
	 * 崔青山
	 * 2016年11月21日
	 * @param key
	 * @return
	 */
	void delShop(String key);

	
	/**
	 * 查询要修改的数据
	 * 崔青山
	 * @param shopId
	 * @return
	 */
	SysShop findModif(String shopId);

	
/**
	 * 查询shop表中的数据
	 * 崔青山
	 * @param ssEnable 
	 */
	List<SysShop> getShops(String name, String tel,String ssEnable, Integer integer, Integer integer2);

	/**
	 * 获取商家分页的数据
	 * 崔青山
	 * @param enable 
	 */
	int getShopFenyes(String name, String tel, String enable);

	
	/**
	 * 查询手机号是否重复
	 * 崔青山
	 */
	List<SysUser> chaXunHao(String tels);

	/**
	 * 崔青山
	 * 2016年9月26日
	 * @param user
	 */
	void saveUser(SysUser user);

	
	/**
	 * 根据shopName查询商家
	 * 崔青山
	 * 2016年7月9日
	 * @param shopName
	 * @return
	 *//*
	List<Shop> chaShop(String shopName);

	*//**
	 * 根据省市查id
	 * 崔青山
	 * 2016年7月9日
	 * @param provincess
	 * @return
	 *//*
	Area getByNameArea(String provincess);

	*//**
	 * 保存
	 * 崔青山
	 * 2016年7月9日
	 * @param shop
	 *//*
	void saveShop(Shop shop);
	void setSysUser(SysUser sysUser);

	*//**
	 * 根据手机号密码查询数据
	 * 崔青山
	 * 2016年7月9日
	 * @param tel
	 * @param pwm
	 * @return
	 *//*
	SysUser getIdUser(String tel);

	*//**
	 * 根据id查询市
	 * 崔青山
	 * 2016年7月11日
	 * @param string
	 * @return
	 *//*
	Area findArea(String string);

	*//**
	 * 根据id查询市区
	 * 崔青山
	 * 2016年7月11日
	 * @param areaByAreaId
	 * @return
	 *//*
	Area findAreas(String string);

	*//**
	 * 查询省区去表
	 * 崔青山
	 * 2016年7月11日
	 * @param areaId
	 * @return
	 *//*
	List<Area> getAreas(String areaId);

	*//**
	 * 启用shop
	 * 崔青山
	 * 2016年7月11日
	 * @param key
	 */
	void shopAbles(String key);

	/**
	 * 禁用shop
	 * 崔青山
	 * 2016年7月11日
	 * @param key
	 */
	void disableShop(String key);


	/**
	 * 崔青山
	 * 2016年9月1日
	 * @param delKeys
	 * @param pwd
	 * @param zhipwds
	 */
	void savegaiRole(String delKeys, String pwd);

	/**
	 * 崔青山
	 * 2016年9月27日
	 * @param userId
	 * @return
	 */
	SysUser getSysUserId(String userId);

	/**
	 * 崔青山
	 * 2016年11月21日
	 * @param name
	 * @param tel
	 * @param ssEnable
	 * @param integer
	 * @param integer2
	 * @return
	 */
	List<SysUser> getSysUserputong(String name,String startDate,String endDate,Integer integer, Integer integer2);

	/**
	 * 崔青山
	 * 2016年11月21日
	 * @param name
	 * @param tel
	 * @param ssEnable
	 * @return
	 */
	int getShopUserfenyes(String name,String startDate,String endDate);

	/**
	 * 崔青山
	 * 2016年11月22日
	 * @param userId
	 * @return
	 */
	SysUser getUserId(String userId);

	/**
	 * 崔青山
	 * 2016年11月22日
	 * @param sysUser
	 */
	void saveSysUser(SysUser sysUser);

	/**
	 * 2016年12月26日下午1:24:02
	 *  阳朔
	 *  注释:
	 */
	SysShop getSysShopByShopId(String shopId);

	/**
	 * 2016年12月26日下午1:26:16
	 *  阳朔
	 *  注释:
	 */
	void saveObj(Object obj);

	/**
	 * 2016年12月29日下午4:48:02
	 *  阳朔
	 *  注释:
	 */
	ShopActivity getShopActivityByShopId(String shopId);


	
	
}
