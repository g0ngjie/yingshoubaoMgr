package com.cosin.web.dao.shop;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cosin.web.controller.log.ILogManager;
import com.cosin.web.dao.BaseDao;
import com.cosin.web.entity.Message;
import com.cosin.web.entity.ShopActivity;
import com.cosin.web.entity.SysShop;
import com.cosin.web.entity.SysUser;
@Repository
public class ShopDao extends BaseDao implements IShopDao{
	@Autowired
	private ILogManager logManager;
	@Override
	public List<SysShop> getSysUser(String Name,String tel,String SsEnable,Integer integer, Integer integer2) {
		String sql="from SysShop as a where 1=1";
		if(Name != null && !"".equals(Name))
		{
			sql += " and a.sysUser.userName like '%" + Name + "%'";
		}
		if(tel != null && !"".equals(tel))
		{
			sql += " and a.sysUser.mobile like '%" + tel + "%'";
		}
		if(SsEnable != null && !"".equals(SsEnable))
		{
			sql += " and a.type= '" + SsEnable + "'";
		}
		sql+=" and a.sysUser.type=2 and a.sysUser.isDel=0 and a.sysUser.enable=1 and a.type!=1 and a.isDel=0 order by a.createDate desc";
		return query(sql,integer,integer2);
	}

	@Override
	public int getShopFenye(String Name,String tel,String SsEnable) {
		String hql = "select count(*) from SysShop as a where 1=1";
		if(Name != null && !"".equals(Name))
		{
			hql += " and a.sysUser.userName like '%" + Name + "%'";
		}
		if(tel != null && !"".equals(tel))
		{
			hql += " and a.sysUser.mobile like '%" + tel + "%'";
		}
		if(SsEnable != null && !"".equals(SsEnable))
		{
			hql += " and a.type='" + SsEnable + "'";
		}
		hql+=" and a.sysUser.type=2 and a.sysUser.isDel=0 and a.sysUser.enable=1 and a.type!=1 and a.isDel=0 order by a.createDate desc";
		return queryCount(hql);
	}

	/**
	 * 崔青山
	 * 2016年9月26日
	 * @param key
	 * @return
	 */
	public SysUser getUserId(String key) {
		String hql = "from SysUser as a where a.userId = '" + key + "'";
		List<SysUser> list = query(hql);
		if(list.size()>0)
			return list.get(0);
		return null;
	}
	
	@Override
	public Map passShop(String userId,String key) {
		SysShop shop = getShopId(key);
		logManager.saveLog(userId, "商家用户审核", "审核操作", "认证商家"+shop.getShopName()+"通过审核");
		shop.setType(1);
		super.save(shop);
		Map map = new HashMap();
		map.put("code", 100);
		map.put("shopId", shop.getShopId());
		map.put("userId", shop.getSysUser().getUserId());
		return map;
	}
	
	@Override
	public Map refuseShop(String userId,String key, String reason) {
		
		SysShop shop = getShopId(key);
		SysUser sysUser = shop.getSysUser();
		sysUser.setType(1);
		super.save(sysUser);
		shop.setType(2);
		shop.setIsDel(1);
		shop.setCreateDate(new Timestamp(new Date().getTime()));
		super.save(shop);
		
		Message message = new Message();
		message.setCreateDate(new Timestamp(new Date().getTime()));
		message.setIsDel(0);
		message.setReason(reason);
		message.setReasonType(0);
		message.setSysUser(sysUser);
		
		logManager.saveLog(userId, "商家用户审核", "审核操作", "认证商家"+shop.getShopName()+"拒绝审核");
		
		super.save(message);
		Map map = new HashMap();
		map.put("code", 100);
		map.put("userId", shop.getSysUser().getUserId());
		return map;
	}


	@Override
	public void delShop(String key) {
		SysShop shop = getShopId(key);
		shop.setIsDel(1);
		super.save(shop);
		SysUser user = getUserId(shop.getSysUser().getUserId());
		user.setIsDel(1);
		super.save(user);
	}
	
	/**
	 * 崔青山
	 * 2016年11月22日
	 * @param key
	 * @return
	 */
	private SysShop getShopId(String key) {
		String hql = "from SysShop as a where a.shopId= '" + key + "'and a.isDel=0";
		List<SysShop> list = query(hql);
		if(list.size()>0)
			return list.get(0);
		return null;
	}

	@Override
	public List<SysShop> getShops(String name, String tel,String SsEnable,Integer integer, Integer integer2) {
		String sql="from SysShop as a where 1=1";
		if(name != null && !"".equals(name))
		{
			sql += " and a.sysUser.userName like '%" + name + "%'";
		}
		if(tel != null && !"".equals(tel))
		{
			sql += " and a.sysUser.mobile like '%" + tel + "%'";
		}
		if(SsEnable != null && !"".equals(SsEnable))
		{
			if(SsEnable.equals("1"))
				sql += " and a.sysUser.enable=1";
			else
				sql += " and a.sysUser.enable=0";
		}
		sql+=" and a.sysUser.type=2 and a.sysUser.isDel=0 and a.type=1 order by a.sysUser.createDate desc";
		return query(sql,integer,integer2);
	}

	
	@Override
	public int getShopFenyes(String name, String tel,String SsEnable) {
		String hql = "select count(*) from SysShop as a where 1=1";
		if(name != null && !"".equals(name))
		{
			hql += " and a.sysUser.userName like '%" + name + "%'";
		}
		if(tel != null && !"".equals(tel))
		{
			hql += " and a.sysUser.mobile like '%" + tel + "%'";
		}
		if(SsEnable != null && !"".equals(SsEnable))
		{
			if(SsEnable.equals("1"))
				hql += " and a.sysUser.enable=1";
			else
				hql += " and a.sysUser.enable=0";
		}
		hql+=" and a.sysUser.type=2 and a.sysUser.isDel=0 and a.type=1 order by a.sysUser.createDate desc";
		return queryCount(hql);
	}
	
	/**
	 * 崔青山
	 * 2016年8月1日
	 * 下午12:28:16
	 */
	@Override
	public List<SysUser> chaXunHao(String tels) {
		String hql = "from SysUser as a where a.mobile= '" + tels + "' and a.isDel=0 and a.type!=3";
		List<SysUser> list = query(hql);
		return list;
	}

	@Override
	public void saveUser(SysUser user) {
		super.save(user);
	}
	
	@Override
	public void shopAbles(String key) {
		SysUser shop = getShopId(key).getSysUser(); 
		shop.setEnable(1);
		super.save(shop);
	}

	@Override
	public void disableShop(String key) {
		SysUser shop = getShopId(key).getSysUser();
		shop.setEnable(0);
		super.save(shop);
	}
	
	@Override
	public SysShop findModif(String shopId) {
		String hql = "from SysShop as a where a.shopId = '" + shopId + "'";
		List<SysShop> list = query(hql);
		if(list.size()>0)
			return list.get(0);
		return null;
	}
	
	
	/* 
	 * 崔青山
	 * 2016年9月27日
	 * 下午5:13:34
	 */
	@Override
	public SysUser getSysUserId(String userId) {
		String hql = "from SysUser as a where a.userId = '" + userId + "'";
		List<SysUser> list = query(hql);
		if(list.size()>0)
			return list.get(0);
		return null;
	}
	/* 
	 * 崔青山
	 * 2016年9月1日
	 * 下午6:35:12
	 */
	@Override
	public void savegaiRole(String delKeys, String pwd) {
		SysUser user = getUserId(delKeys);
		user.setPwd(pwd);
		super.save(user);
	}

	/* 
	 * 崔青山
	 * 2016年11月21日
	 * 下午3:52:07
	 */
	@Override
	public List<SysUser> getSysUserputong(String Name,String startDate,String endDate, Integer integer, Integer integer2) {
		String sql="from SysUser as a where 1=1";
		if(Name != null && !"".equals(Name))
		{
			sql += " and a.userName like '%" + Name + "%'";
		}
		if(startDate != null && !"".equals(startDate))
		{
			sql += " and a.createDate >= '" + startDate + "'";
		}
		if(endDate != null && !"".equals(endDate))
		{
			sql += " and a.createDate <= '" + endDate + "'";
		}
		sql+=" and a.type=1 and a.isDel=0 order by a.createDate desc";
		return query(sql,integer,integer2);
	}

	/* 
	 * 崔青山
	 * 2016年11月21日
	 * 下午3:54:27
	 */
	@Override
	public int getShopUserfenyes(String Name,String startDate,String endDate) {
		String hql = "select count(*) from SysUser as a where 1=1";
		if(Name != null && !"".equals(Name))
		{
			hql += " and a.userName like '%" + Name + "%'";
		}
		if(startDate != null && !"".equals(startDate))
		{
			hql += " and a.createDate >= '" + startDate + "'";
		}
		if(endDate != null && !"".equals(endDate))
		{
			hql += " and a.createDate <= '" + endDate + "'";
		}
		hql+=" and a.type=1 and a.isDel=0 order by a.createDate desc";
		return queryCount(hql);
	}

	/* 
	 * 崔青山
	 * 2016年11月22日
	 * 上午10:46:06
	 */
	@Override
	public void saveSysUser(SysUser sysUser) {
		super.save(sysUser);
	}

	/**
	 * 2016年12月26日下午1:24:10
	 *  阳朔
	 *  注释:
	 */
	@Override
	public SysShop getSysShopByShopId(String shopId) {
		String sql = "from SysShop as a where a.isDel = 0 and a.shopId = '" + shopId + "'";
		List<SysShop> list = query(sql);
		if(list.size()>0)
			return list.get(0);
		return null;
	}

	/**
	 * 2016年12月26日下午1:26:31
	 *  阳朔
	 *  注释:
	 */
	@Override
	public void saveObj(Object obj) {
		super.save(obj);
	}

	/**
	 * 2016年12月29日下午4:48:07
	 *  阳朔
	 *  注释:
	 */
	@Override
	public ShopActivity getShopActivityByShopId(String shopId) {
		String sql = "from ShopActivity as a where a.sysShop = '" + shopId + "'";
		List<ShopActivity> list = query(sql);
		if(list.size() > 0)
			return list.get(0);
		return null;
	}
	

	

}

	


	


