package com.cosin.web.dao.stall;

import java.util.List;

import com.cosin.web.entity.StallPrice;

public interface IStallDao {
	

	/**
	 * 崔青山
	 * 2016年11月15日
	 * @param integer
	 * @param integer2
	 * @return
	 */
	public List<StallPrice> getDistanceList(Integer integer, Integer integer2);



	/**
	 * 崔青山
	 * 2016年11月15日
	 * @param distanceId
	 * @return
	 */
	public StallPrice findbjDistance(String distanceId);



	/**
	 * 崔青山
	 * 2016年11月15日
	 * @return
	 */
	public StallPrice getDistance();



	/**
	 * 崔青山
	 * 2016年11月15日
	 * @param distance
	 */
	public void getsaveDistance(StallPrice distance);


}
