package com.cosin.web.dao.stall;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.cosin.web.dao.BaseDao;
import com.cosin.web.entity.StallPrice;
@Repository
public class StallDao extends BaseDao implements IStallDao{


	/* 
	 * 崔青山
	 * 2016年11月15日
	 * 下午3:30:57
	 */
	@Override
	public List<StallPrice> getDistanceList(Integer integer, Integer integer2) {
		// TODO Auto-generated method stub
		String sql="from StallPrice as a ";
		return query(sql,integer,integer2);
	}

	/* 
	 * 崔青山
	 * 2016年11月15日
	 * 下午3:45:03
	 */
	@Override
	public StallPrice findbjDistance(String stallId) {
		// TODO Auto-generated method stub
		String hql = "from StallPrice as a where a.stallId = '" + stallId + "'";
		List<StallPrice> list = query(hql);
		return list.get(0);
	}

	/* 
	 * 崔青山
	 * 2016年11月15日
	 * 下午4:58:18
	 */
	@Override
	public StallPrice getDistance() {
		// TODO Auto-generated method stub
		String hql = "from StallPrice as a";
		List<StallPrice> list = query(hql);
		return list.get(0);
	}

	/* 
	 * 崔青山
	 * 2016年11月15日
	 * 下午4:58:45
	 */
	@Override
	public void getsaveDistance(StallPrice distance) {
		// TODO Auto-generated method stub
		super.save(distance);
	}



}
