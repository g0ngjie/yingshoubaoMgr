package com.cosin.web.dao.stall;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.cosin.web.dao.BaseDao;
import com.cosin.web.entity.StallPrice;
import com.cosin.web.entity.StallRecord;
import com.cosin.web.entity.WithdrawCashRecord;
@Repository
public class StallRecordDao extends BaseDao implements IStallRecordDao{

	
	@Override
	public void delPush(String key) {
		// TODO Auto-generated method stub
		StallRecord message = getTransactionRecordId(key);
		super.delete(message);
	}

	/**
	 * 崔青山
	 * @param key
	 * @return
	 */
	private StallRecord getTransactionRecordId(String key) {
		// TODO Auto-generated method stub
		String hql = "from StallRecord as a where a.stallRecordId = '" + key + "'";
		List<StallRecord> list = query(hql);
		return list.get(0);
	}

	@Override
	public List<StallRecord> getPushMessage(String ssuser,String ssJia,Integer start, Integer limit) {
		// TODO Auto-generated method stub
		String sql="from StallRecord as a where 1=1";
		if(ssuser != null && !"".equals(ssuser))
		{
			sql += " and a.sysShop.sysUser.userName like '%" + ssuser + "%'";
		}
		if(ssJia != null && !"".equals(ssJia))
		{
			sql += " and a.createDate>= '" + ssJia + " 00:00:00' and a.createDate<='" + ssJia + " 23:59:59'";
		}
		sql+=" order by createDate desc";
		return query(sql,start,limit);
	}

	
	/** 
	 * 崔青山
	 * 2016年7月29日
	 * 上午10:28:48
	 */
	@Override
	public int getinfofenye(String ssuser,String ssJia) {
		// TODO Auto-generated method stub
		String sql="select count(*) from StallRecord as a where 1=1";
		if(ssuser != null && !"".equals(ssuser))
		{
			sql += " and a.sysShop.sysUser.userName like '%" + ssuser + "%'";
		}
		if(ssJia != null && !"".equals(ssJia))
		{
			sql += " and a.createDate>= '" + ssJia + " 00:00:00' and a.createDate<='" + ssJia + " 23:59:59'";
		}
		sql+=" order by createDate desc";
		return queryCount(sql);
	}

	/**
	 * 2017年1月10日上午11:41:00
	 *  阳朔
	 *  注释:
	 */
	@Override
	public StallRecord getStallRecordById(String key) {
		StallRecord record = getTransactionRecordId(key);
		return record;
	}


	

	
}
