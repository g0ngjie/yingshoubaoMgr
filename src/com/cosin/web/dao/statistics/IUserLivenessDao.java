package com.cosin.web.dao.statistics;

import java.util.List;

import com.cosin.web.entity.ActivDetail;
import com.cosin.web.entity.SysArea;
import com.cosin.web.entity.SysUser;


public interface IUserLivenessDao {

	/**
	 * 2017��1��6������12:08:39
	 *  ��˷
	 *  ע��:
	 */
	List<SysUser> getUserLivenessListData(String provinceId,String cityId,String startDate,String endDate, Integer start,
			Integer limit);

	/**
	 * 2017��1��6������12:09:04
	 *  ��˷
	 *  ע��:
	 */
	int getUserLivenessListCount(String provinceId,String cityId,String startDate,String endDate);

	/**
	 * 2017��1��6������5:19:00
	 *  ��˷
	 *  ע��:
	 */
	List<SysArea> getSysAreaList();

	/**
	 * 2017��1��6������5:23:35
	 *  ��˷
	 *  ע��:
	 */
	List<SysArea> getCityListByProId(String proId);

	/**
	 * 2017��1��11������6:08:03
	 *  ��˷
	 *  ע��:
	 */
	List<ActivDetail> getActivDetailListData(String type, String provinceId, String cityId,
			String startDate, String endDate, Integer start, Integer limit);

	/**
	 * 2017��1��11������6:16:30
	 *  ��˷
	 *  ע��:
	 */
	int getActivDetailListCount(String type, String provinceId, String cityId,
			String startDate, String endDate);

	/**
	 * 2017��1��12������8:41:43
	 *  ��˷
	 *  ע��:
	 */
	List getUserLivenessListDataNew(String provinceId, String cityId,
			String startDate, String endDate, Integer integer, Integer integer2);

	/**
	 * 2017��1��12������9:28:21
	 *  ��˷
	 *  ע��:
	 */
	int getUserLivenessListCountNew(String provinceId, String cityId,
			String startDate, String endDate);

	/**
	 * 2017��1��13������9:54:50
	 *  ��˷
	 *  ע��:
	 */
	int getUserBeforeByDate(String date);

	/**
	 * 2017��1��13������10:29:38
	 *  ��˷
	 *  ע��:
	 */
	List getUserFigureData(String forYear, String forMonth);
	



	
}
