package com.cosin.web.dao.statistics;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.cosin.web.dao.BaseDao;
import com.cosin.web.entity.ActivDetail;
import com.cosin.web.entity.SysArea;
import com.cosin.web.entity.SysUser;
@Repository
public class UserLivenessDao extends BaseDao implements IUserLivenessDao{

	/**
	 * 2017年1月6日下午12:09:08
	 *  阳朔
	 *  注释:
	 */
	@Override
	public List<SysUser> getUserLivenessListData(String provinceId,String cityId,String startDate,String endDate, Integer start,Integer limit) {
		String sql = "from SysUser as a where a.isDel = 0 and a.type = 1";
		if(provinceId != null && !provinceId.equals("")){
			sql +="and a.sysAreaByProvinceId = '" + provinceId + "'";
		}
		if(cityId != null && !cityId.equals("")){
			sql +="and a.sysAreaByCityId = '" + cityId + "'";
		}
		if(startDate != null && !startDate.equals("")){
			sql +="and a.createDate >= '" + startDate + "'";
		}
		if(endDate != null && !endDate.equals("")){
			sql +="and a.createDate <= '" + endDate + "'";
		}
		sql +="order by a.createDate desc";
		return query(sql, start, limit);
	}

	/**
	 * 2017年1月6日下午12:09:08
	 *  阳朔
	 *  注释:
	 */
	@Override
	public int getUserLivenessListCount(String provinceId,String cityId,String startDate,String endDate) {
		String sql = "select count(*) from SysUser as a where a.isDel = 0 and a.type = 1";
		if(provinceId != null && !provinceId.equals("")){
			sql +="and a.sysAreaByProvinceId = '" + provinceId + "'";
		}
		if(cityId != null && !cityId.equals("")){
			sql +="and a.sysAreaByCityId = '" + cityId + "'";
		}
		if(startDate != null && !startDate.equals("")){
			sql +="and a.createDate >= '" + startDate + "'";
		}
		if(endDate != null && !endDate.equals("")){
			sql +="and a.createDate <= '" + endDate + "'";
		}
		sql +="order by a.createDate desc";
		return queryCount(sql);
	}

	/**
	 * 2017年1月6日下午5:19:05
	 *  阳朔
	 *  注释:
	 */
	@Override
	public List<SysArea> getSysAreaList() {
		String sql = "from SysArea as a where a.type = 1";
		return query(sql);
	}

	/**
	 * 2017年1月6日下午5:23:48
	 *  阳朔
	 *  注释:
	 */
	@Override
	public List<SysArea> getCityListByProId(String proId) {
		String sql = "from SysArea as a where a.type = 2 and a.sysArea = '" + proId + "'";
		return query(sql);
	}

	/**
	 * 2017年1月11日下午6:08:33
	 *  阳朔
	 *  注释:
	 */
	@Override
	public List<ActivDetail> getActivDetailListData(String type,
			String provinceId, String cityId, String startDate, String endDate,
			Integer start, Integer limit) {
		
		String sql = "from ActivDetail as a where a.sysUser.isDel = 0 and a.type = '" + type + "'";
		if(provinceId != null && !provinceId.equals("")){
			sql +="and a.sysUser.sysAreaByProvinceId = '" + provinceId + "'";
		}
		if(cityId != null && !cityId.equals("")){
			sql +="and a.sysUser.sysAreaByCityId = '" + cityId + "'";
		}
		if(startDate != null && !startDate.equals("")){
			sql +="and a.createDate >= '" + startDate + "'";
		}
		if(endDate != null && !endDate.equals("")){
			sql +="and a.createDate <= '" + endDate + "'";
		}
		sql +="order by a.createDate desc";
		return query(sql, start, limit);
	}
	
	

	/**
	 * 2017年1月11日下午6:16:34
	 *  阳朔SELECT (SELECT t.counts FROM t b WHERE t.type=1 )tt1,t.createDate from (select a.type,a.createDate, COUNT(*) counts from activ_detail a GROUP BY a.type)t GROUP BY YEAR(t.createDate),MONTH(t.createDate),DAY(t.createDate) ORDER BY t.createDate DESC
	 * select t.counts as t1 wh
	 */
	@Override
	public int getActivDetailListCount(String type, String provinceId,
			String cityId, String startDate, String endDate) {
		String sql = "select count(*) from ActivDetail as a where a.sysUser.isDel = 0 and a.type = '" + type + "'";
		if(provinceId != null && !provinceId.equals("")){
			sql +="and a.sysUser.sysAreaByProvinceId = '" + provinceId + "'";
		}
		if(cityId != null && !cityId.equals("")){
			sql +="and a.sysUser.sysAreaByCityId = '" + cityId + "'";
		}
		if(startDate != null && !startDate.equals("")){
			sql +="and a.createDate >= '" + startDate + "'";
		}
		if(endDate != null && !endDate.equals("")){
			sql +="and a.createDate <= '" + endDate + "'";
		}
		sql +="order by a.createDate desc";
		return queryCount(sql);
	}

	/**
	 * 2017年1月12日下午8:41:51
	 *  阳朔 SELECT SUM(case when t.type=1 then t.counts else 0 end)t1,SUM(case when t.type=2 then t.counts else 0 end)t2,SUM(case when t.type=3 then t.counts else 0 end)t3,t.createDate FROM (select a.type,a.createDate, COUNT(*)counts from activ_detail a GROUP BY a.type)t GROUP BY YEAR(t.createDate),MONTH(t.createDate),DAY(t.createDate) ORDER BY t.createDate DESC
	 *  注释:
	 */
	@Override
	public List getUserLivenessListDataNew(String provinceId, String cityId,
			String startDate, String endDate, Integer integer, Integer integer2) {
		// TODO Auto-generated method stub
		
		String sql = "";
		sql+="SELECT SUM(case when t.type=1 then t.counts else 0 end)t1,";
		sql+="SUM(case when t.type=2 then t.counts else 0 end)t2,";
		sql+="SUM(case when t.type=3 then t.counts else 0 end)t3,";
		sql+="t.createDate FROM (select a.type,a.createDate, COUNT(*)counts from activ_detail a , sys_user b WHERE a.userId = b.userId";
		
		if(provinceId != null && !provinceId.equals("")){
			sql +=" and b.provinceId = '" + provinceId + "'";
		}
		if(cityId != null && !cityId.equals("")){
			sql +=" and b.cityId = '" + cityId + "'";
		}
		if(startDate != null && !startDate.equals("")){
			sql +=" and a.createDate >= '" + startDate + "'";
		}
		if(endDate != null && !endDate.equals("")){
			sql +=" and a.createDate <= '" + endDate + "'";
		}
				
		sql+=" GROUP BY a.type)t";
		sql+=" GROUP BY YEAR(t.createDate),MONTH(t.createDate),DAY(t.createDate) ORDER BY t.createDate DESC";
				
		return queryNavite(sql, integer, integer2);		
				 
		/*String sql = "select a.createDate,COUNT(*) from activ_detail a, sys_user b WHERE a.userId = b.userId";
		if(provinceId != null && !provinceId.equals("")){
			sql +="and b.provinceId = '" + provinceId + "'";
		}
		if(cityId != null && !cityId.equals("")){
			sql +="and b.cityId = '" + cityId + "'";
		}
		if(startDate != null && !startDate.equals("")){
			sql +="and a.createDate >= '" + startDate + "'";
		}
		if(endDate != null && !endDate.equals("")){
			sql +="and a.createDate <= '" + endDate + "'";
		}
		sql +=" GROUP BY YEAR(a.createDate),MONTH(a.createDate),DAY(a.createDate) ORDER BY a.createDate DESC";
		return queryNavite(sql, integer, integer2);*/
	}
	

	/**
	 * 2017年1月12日下午9:28:30
	 *  阳朔
	 *  注释:
	 */
	@Override
	public int getUserLivenessListCountNew(String provinceId, String cityId,
			String startDate, String endDate) {
		// TODO Auto-generated method stub
		String sql = "";
		sql+="SELECT ";
		sql+="count(*) FROM (select a.type,a.createDate, COUNT(*) counts from activ_detail a , sys_user b WHERE a.userId = b.userId";
		
		if(provinceId != null && !provinceId.equals("")){
			sql +=" and b.provinceId = '" + provinceId + "'";
		}
		if(cityId != null && !cityId.equals("")){
			sql +=" and b.cityId = '" + cityId + "'";
		}
		if(startDate != null && !startDate.equals("")){
			sql +=" and a.createDate >= '" + startDate + "'";
		}
		if(endDate != null && !endDate.equals("")){
			sql +=" and a.createDate <= '" + endDate + "'";
		}
				
		sql+=" GROUP BY a.type)t";
		sql+=" GROUP BY YEAR(t.createDate),MONTH(t.createDate),DAY(t.createDate) ORDER BY t.createDate DESC";
				
		return queryNavite(sql).size();
	}

	/**
	 * 2017年1月13日上午9:55:05
	 *  阳朔
	 *  注释:
	 */
	@Override
	public int getUserBeforeByDate(String date) {
		String sql = "select count(*) from SysUser a where a.createDate <= '" + date + " 23:59:59'";
		return queryCount(sql);
	}

	/**
	 * 2017年1月13日上午10:29:50
	 *  阳朔
	 *  注释:
	 */
	@Override
	public List getUserFigureData(String forYear, String forMonth) {
		String sql = "";
		if("".equals(forMonth)){
			sql+="SELECT MONTH(t.createDate),SUM(case when t.type=1 then t.counts else 0 end)t1,";
			sql+="SUM(case when t.type=2 then t.counts else 0 end)t2,";
			sql+="SUM(case when t.type=3 then t.counts else 0 end)t3";
			sql+=" FROM (select a.type,a.createDate, COUNT(*)counts from activ_detail a , sys_user b WHERE a.userId = b.userId";
		}else{
			sql+="SELECT DAY(t.createDate),SUM(case when t.type=1 then t.counts else 0 end)t1,";
			sql+="SUM(case when t.type=2 then t.counts else 0 end)t2,";
			sql+="SUM(case when t.type=3 then t.counts else 0 end)t3";
			sql+=" FROM (select a.type,a.createDate, COUNT(*)counts from activ_detail a , sys_user b WHERE a.userId = b.userId";
		}
		
		
		if(forYear != null && !forYear.equals("")){
			sql +=" and YEAR(a.createDate) <= '" + forYear + "'";
		}
		if(forMonth != null && !forMonth.equals("")){
			sql +=" and MONTH(a.createDate) <= '" + forMonth + "'";
		}
				
		sql+=" GROUP BY a.type)t";
		if(!"".equals(forMonth)){
			sql+=" GROUP BY YEAR(t.createDate),MONTH(t.createDate),DAY(t.createDate) ORDER BY t.createDate DESC";
		}else{
			sql+=" GROUP BY YEAR(t.createDate),MONTH(t.createDate) ORDER BY t.createDate DESC";
		}
				
		return queryNavite(sql);
	}




}
