package com.cosin.web.dao.system;

import java.util.List;

import org.springframework.stereotype.Controller;

import com.cosin.web.entity.SysMenu;
import com.cosin.web.entity.SysUserRole;

public interface IMenuDao {
	public List getMainMenuList(int start, int limit);
	
	public SysMenu findMenuById(String id);
	
	public List findMenuByParentMenuId(String id);
	
	//查询  dao接口
	public List getSysMenu(String menuId);
	
	public void saveSysMenu(String menuName,String menuId); 
	
	public void saveAddSysMenu(String menuName,String menuId);
	
	public void delSysMenu(String sysMenu);
	//保存  dao接口
	public void saveSysMenu(SysMenu sysMenus);

	public void delSysMenu(SysMenu sysMenu);

	/**
	 * 2016年8月17日
	 * @param menuId
	 * @return
	 */
	public SysMenu getMainMenuById(String menuId);

	/**
	 * 崔青山
	 * 2016年8月17日
	 * @param menuId
	 * @return
	 */
	public SysMenu getSubMenuById(String menuId);

	/**
	 * 2017年1月9日上午11:43:34
	 *  阳朔
	 *  注释:
	 */
	public SysUserRole getSysUserRoleByUserId(String userId);

	/**
	 * 2017年1月9日下午2:57:40
	 *  阳朔
	 *  注释:
	 */
	public SysMenu getMainTopMenuById(String powerCode);

	/**
	 * 2017年1月9日下午3:12:55
	 *  阳朔
	 *  注释:
	 */
	public SysMenu getMainLeftMenuById(String powerCode);

	
	
}
