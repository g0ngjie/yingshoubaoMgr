package com.cosin.web.dao.system;

import java.util.List;

import com.cosin.web.entity.SysMenu;
import com.cosin.web.entity.SysPowerItem;
import com.cosin.web.entity.SysRole;
import com.cosin.web.entity.SysRolePower;
import com.cosin.web.entity.SysUser;
import com.cosin.web.entity.SysUserRole;

public interface IRoleDao {
	
	/**
	 * 查询Role表数据
	 * 崔青山
	 * 2016年7月7日
	 * @param SsNameint
	 * @param start
	 * @param limit
	 * @return
	 */
	public List getRole(String SsNameint,int start, int limit);
	
	/**
	 * 查询用户表数据
	 * 崔青山
	 * 2016年7月7日
	 * @param SsNameint
	 * @param start
	 * @param limit
	 * @return
	 */
	public List getUser(String SsNameint,String roleName,int start, int limit);
	
	/**
	 * 根据账号密码查询
	 * 崔青山
	 * 2016年7月7日
	 * @param userName
	 * @param pwd
	 * @return
	 */
	public SysUser getUsers(String userName, String pwd);
	
	
	
	public SysUser getByUserId(String userId);
	
	/**
	 * 保存Role
	 * 崔青山
	 * 2016年7月7日
	 * @param roleId
	 * @return
	 */
	public SysRole getByRoleId(String roleId);
	
	public void save(SysRole sysRole);
	
	/**
	 * 保存user
	 * 崔青山
	 * 2016年7月7日
	 * @param sysuser
	 */
	public void saveUser(SysUser sysuser);
	
	public void saveRole(String userName, String enable, String roleId, String mode);
	
	/**
	 * 删除role
	 * 崔青山
	 * 2016年7月7日
	 * @param roleId
	 */
	public void delRole(String roleId);
	
	/**
	 * 删除user
	 * 崔青山
	 * 2016年7月7日
	 * @param userId
	 */
	public void delUser(String userId);
	
	/**
	 * 启用Role
	 * 崔青山
	 * 2016年7月7日
	 * @param roleId
	 */
	public void saveqyRole(String roleId);
	
	/**
	 * 启用User
	 * 崔青山
	 * 2016年7月7日
	 * @param userId
	 */
	public void saveqyUser(String userId);
	
	/**
	 * 禁用Role
	 * 崔青山
	 * 2016年7月7日
	 * @param roleId
	 */
	public void savejyRole(String roleId);
	
	/**
	 * 禁用User
	 * 崔青山
	 * 2016年7月7日
	 * @param userId
	 */
	public void savejyUser(String userId);
	
	/**
	 * 获取Role数据
	 * 崔青山
	 * 2016年7月7日
	 * @param userRoleId
	 * @return
	 */
	public SysUserRole getByIdSysUserRole(String userRoleId);
	
	/**
	 * 选择角色
	 * 崔青山
	 * 2016年7月7日
	 * @return
	 */
	public List getSysRole();
	
	/**
	 * 保存SysUserRole
	 * 崔青山
	 * 2016年7月7日
	 * @param sysUserRole
	 */
	public void saveSysUserRole(SysUserRole sysUserRole);
	
	/**
	 * 查询SysUserRole数据
	 * 崔青山
	 * 2016年7月7日
	 * @param userId
	 * @return
	 */
	public SysUser getByUser(String userId);
	
	public SysUserRole getByUserRoleId(String userId);
	
	/**
	 * 查Role是否有重复
	 * 崔青山
	 * 2016年7月7日
	 * @param userName
	 * @return
	 */
	public List<SysRole> chaRole(String userName);
	
	/**
	 * 查User是否有重复
	 * 崔青山
	 * 2016年7月7日
	 * @param userName
	 * @return
	 */
	public List<SysUser> chaUser(String userName);

	public int getRolef();
	
	/**
	 * 查询Role数据
	 * 崔青山
	 * 2016年7月7日
	 * @param roleId
	 * @return
	 */
	public SysRole getByRole(String roleId);

	/**
	 * Role分页
	 * @return
	 */
	public int getRolefenye(String SsName);

	/**
	 * User分页
	 * @return
	 */
	public int getUserfenye(String SsName,String roleName);
	
	public List getByRoleIdList(String roleId);

	/**
	 * 查sysRoleUser表中数据的主键
	 * 崔青山
	 * 2016年7月7日
	 * @param userId
	 * @param sysRole
	 * @return
	 */
	public SysUserRole getUsersRole(String userId, String sysRole);

	/**
	 * 根据RoleId查询RoleUser表中的数据
	 * 崔青山
	 * 2016年7月15日
	 * @param key
	 * @return
	 */
	public List<SysUserRole> getRoleUserList(String key);

	/**
	 * 崔青山
	 * 2016年8月3日
	 * @param key
	 */
	public void savegaiRole(String delKeys,String pwd);

	
	public List<SysRolePower> findRolePowerById(String roleId);

	public List<SysMenu> getMenuList(String parentMenuId);

	/**
	 * 崔青山
	 * 2016年8月8日
	 * @param roleId
	 * @return
	 */
	public void delPower(String roleId);

	/**
	 * 崔青山
	 * 2016年8月8日
	 * @param roleId
	 * @return
	 */
	public SysRole findRoleById(String roleId);

	/**
	 * 崔青山
	 * 2016年8月8日
	 * @param rolePower
	 */
	public void savePowerInRolePower(SysRolePower rolePower);

	public List<SysUser> getShopUserList(String ssName, String roleName,
			Integer integer, Integer integer2);

	public List<SysUser> getHuiyuanUserList(String ssName, String roleName,
			Integer integer, Integer integer2);

	/**
	 * 崔青山
	 * 2016年8月17日
	 * @param userId
	 * @return
	 */
	public SysUserRole findUserRoleByUserId(String userId);

	/**
	 * 崔青山
	 * 2016年8月17日
	 * @param powerCode
	 * @return
	 */
	public SysPowerItem getSysPowerItemById(String powerCode);

	/**
	 * 崔青山
	 * 2016年8月21日
	 * @param ssName
	 * @param roleName
	 * @return
	 */
	public int getUserfenyeShop(String ssName, String roleName);

	/**
	 * 崔青山
	 * 2016年8月21日
	 * @param ssName
	 * @param roleName
	 * @return
	 */
	public int getUserfenyeHuiyuan(String ssName, String roleName);

	/**
	 * 崔青山
	 * 2016年9月21日
	 * @param userId
	 * @return
	 */
	public SysUserRole getSysUserRole(String userId);

	/**
	 * 2017年1月9日上午11:17:49
	 *  阳朔
	 *  注释:
	 */
	public List<SysUser> chaLoginName(String loginName);

	/**
	 * 2017年1月9日上午11:24:05
	 *  阳朔
	 *  注释:
	 */
	public SysRole getManagerRole();

	/**
	 * 2017年1月9日下午12:19:48
	 *  阳朔
	 *  注释:type为3后台管理员
	 */
	public SysUser getSysUserByUserId(String userId);


	
}
