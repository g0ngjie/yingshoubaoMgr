/**
 * 
 */
package com.cosin.web.dao.system;

import java.util.List;

import com.cosin.web.entity.SysRolePower;
import com.cosin.web.entity.SysUser;
import com.cosin.web.entity.SysUserRole;

/**
 * @author 王思明
 * 2016年7月19日
 * 下午2:03:48
 */
public interface ISystemDao {

	/**
	 * 验证用户登录信息
	 * @param loginName
	 * @param pwd
	 * @return
	 */
	public SysUser getByEmailNamePwd(String loginName, String pwd);

	/**
	 * 保存方法
	 * 王思明 
	 * 2016年7月19日
	 * 下午3:21:16
	 */
	public void saveObject(Object obj);

	/**
	 * 2017年1月9日下午2:19:47
	 *  阳朔
	 *  注释:
	 */
	public SysUserRole getSysRoleByUserId(String userId);

	/**
	 * 2017年1月9日下午2:25:09
	 *  阳朔
	 *  注释:
	 */
	public List<SysRolePower> getSysRolePower(String roleId);

	
}
