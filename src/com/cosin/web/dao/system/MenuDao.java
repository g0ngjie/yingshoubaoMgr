package com.cosin.web.dao.system;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Formatter;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;

import com.cosin.web.dao.BaseDao;
import com.cosin.web.entity.SysMenu;
import com.cosin.web.entity.SysUserRole;

/**
 * 菜单Dao
 * @author zhb 2016-03-30
 */
@Repository
public class MenuDao extends BaseDao implements IMenuDao{
	
	
	
	/**
	 * 获取一级菜单
	 * @return
	 */
	public List getMainMenuList(int start, int limit)
	{
		String hql = "from SysMenu as a where a.sysMenu=null order by a.orderNo desc";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		return query.list();
	}
		
	/**
	 * 根据主键获取菜单
	 * @param id
	 * @return
	 */
	public SysMenu findMenuById(String id)
	{
		Formatter fmt = new Formatter();
		String sql = fmt.format("from SysMenu as a where a.menuId='%s'", id).toString();
		List list = query(sql);
		if(list.size() > 0)
			return (SysMenu)list.get(0);
		return null;
	}

	/**
	 * 获取下级菜单
	 * @param id
	 * @return
	 */
	public List findMenuByParentMenuId(String id)
	{
		String sql = "from SysMenu as a where a.sysMenu.menuId='" + id +"' and a.enable=1 order by a.createDate,a.orderNo desc";
		List list = query(sql);
		return list;
	}
	
	//查询  dao
	@Override
	public List getSysMenu(String menuId) {
		String sql = "";
		if("0".equals(menuId))
			sql="from SysMenu as a where a.sysMenu.menuId is null  order by a.orderNo desc";
		else
			sql="from SysMenu as a where a.sysMenu.menuId = '" + menuId +"'  order by a.orderNo desc"; 
		List list =query(sql);
		return list;
	}

	@Override
	public void saveSysMenu(String menuName, String menuId) {
		// TODO Auto-generated method stub
	}

	@Override
	public void saveAddSysMenu(String menuName, String menuId) {
		// TODO Auto-generated method stub
	}

	@Override
	public void delSysMenu(String sysMenu) {
		
		super.delete(sysMenu);
	}
	
	@Override
	public void saveSysMenu(SysMenu sysMenus) {
		super.save(sysMenus); 
	}

	@Override
	public void delSysMenu(SysMenu sysMenu) {
		// TODO Auto-generated method stub
		super.delete(sysMenu);
	}

	/* 
	 * 崔青山
	 * 2016年8月17日
	 * 上午10:19:21
	 */
	@Override
	public SysMenu getMainMenuById(String menuId) {
		// TODO Auto-generated method stub
		String sql = "from SysMenu as a where a.menuId='"+menuId+"' and a.sysMenu.menuId is null";
		List<SysMenu> list = query(sql);
		if(list.size() > 0)
			return list.get(0);
		return null;
	}

	/* 
	 * 崔青山
	 * 2016年8月17日
	 * 上午10:40:07
	 */
	@Override
	public SysMenu getSubMenuById(String menuId) {
		// TODO Auto-generated method stub
		String sql = "from SysMenu as a where a.menuId='"+menuId+"' and a.sysMenu.menuId != null";
		List<SysMenu> list = query(sql);
		if(list.size() > 0)
			return list.get(0);
		return null;
	}

	/**
	 * 2017年1月9日上午11:43:39
	 *  阳朔
	 *  注释:
	 */
	@Override
	public SysUserRole getSysUserRoleByUserId(String userId) {
		String sql = "from SysUserRole as a where a.sysUser = '" + userId + "'";
		List<SysUserRole> list = query(sql);
		if(list.size() > 0)
			return list.get(0);
		return null;
	}

	/**
	 * 2017年1月9日下午2:57:45
	 *  阳朔
	 *  注释:
	 */
	@Override
	public SysMenu getMainTopMenuById(String powerCode) {
		String sql = "from SysMenu as a where a.enable = 1 and a.sysMenu = null and a.menuId = '" + powerCode + "'";
		List<SysMenu> list = query(sql);
		if(list.size() > 0)
			return list.get(0);
		return null;
	}

	/**
	 * 2017年1月9日下午3:13:00
	 *  阳朔
	 *  注释:
	 */
	@Override
	public SysMenu getMainLeftMenuById(String powerCode) {
		String sql = "from SysMenu as a where a.enable = 1 and a.sysMenu <> null and a.menuId = '" + powerCode + "'";
		List<SysMenu> list = query(sql);
		if(list.size() > 0)
			return list.get(0);
		return null;
	}

}
