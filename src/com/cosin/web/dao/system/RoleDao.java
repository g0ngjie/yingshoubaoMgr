package com.cosin.web.dao.system;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.cosin.web.dao.BaseDao;
import com.cosin.web.entity.SysMenu;
import com.cosin.web.entity.SysPowerItem;
import com.cosin.web.entity.SysRole;
import com.cosin.web.entity.SysRolePower;
import com.cosin.web.entity.SysUser;
import com.cosin.web.entity.SysUserRole;


@Repository
public class RoleDao extends BaseDao implements IRoleDao{

	@Override
	public List getRole(String SsNameint,int start, int limit) {
		String sql="from SysRole as a where 1=1";
		if(SsNameint != null && !"".equals(SsNameint))
		{
			sql += " and a.userName like '%" + SsNameint + "%'";
		}
		sql+=" and a.isDel=0 order by a.createDate asc";
		return query(sql,start,limit);
	}

	@Override
	public SysRole getByRoleId(String roleId) {
		String hql = "from SysRole as a where a.roleId = '" + roleId + "' and a.isDel=0";
		List<SysRole> list = query(hql);
		if(list.size()>0)
			return list.get(0);
		return null;
	}
	

	

	@Override
	public void delRole(String roleId) {
		// TODO Auto-generated method stub
		SysRole role = getByRoleId(roleId);
		role.setIsDel(1);
		super.save(role);
	}


	@Override
	public List getByRoleIdList(String roleId) {
		
		// TODO Auto-generated method stubsave
		return null;
	}


	@Override
	public void saveqyRole(String roleId) {
		// TODO Auto-generated method stub
		SysRole role = getByRoleId(roleId);
		role.setEnable(1);
		super.save(role);
	}

	@Override
	public void savejyRole(String roleId) {
		// TODO Auto-generated method stub
		SysRole role = getByRoleId(roleId);
		role.setEnable(0);
		super.save(role);
	}

	@Override
	public List getUser(String SsNameint,String roleName, int start, int limit) {
		// TODO Auto-generated method stub
		String sql="from SysUser as a where 1=1";
		if(SsNameint != null && !"".equals(SsNameint))
		{
			sql += " and a.userName like '%" + SsNameint + "%'";
		}
	/*	if(roleName != null && !"".equals(roleName))
		{
			sql += " and a.sysUserRole.sysRole.userName like '%" + roleName + "%'";
		}*/
		sql+=" and a.isDel=0 and a.type=3 order by a.createDate asc";
		return query(sql,start,limit);
	}
	
	

	@Override
	public void saveUser(SysUser sysuser) {
		// TODO Auto-generated method stub
		super.save(sysuser);
	}

	@Override
	public void saveRole(String userName, String enable, String roleId,
			String mode) {
		
		
	}

	@Override
	public void save(SysRole sysRole) {
		super.save(sysRole);
	}

	@Override
	public SysUser getByUserId(String userId) {
		String hql = "from SysUser as a where a.userId = '" + userId + "' and a.isDel=0";
		List<SysUser> list = query(hql);
		if(list.size()>0)
			return list.get(0);
		return null;
	}

	@Override
	public void delUser(String userId) {
			SysUser user = getByUserId(userId);
			user.setIsDel(1);
			super.save(user);
	}
	
	public SysUserRole getBySysUserRoleId(String userRoleId)
	{
		String sql = "from SysUserRole as a where a.userId='" + userRoleId + "'";
		List<SysUserRole> list = query(sql);
		if(list.size()>0)
			return list.get(0);
		return null;
	}	
	
	@Override
	public void saveqyUser(String userId) {
		// TODO Auto-generated method stub
		SysUser user = getByUserId(userId);
		user.setEnable(1);
		super.save(user);
	}

	@Override
	public void savejyUser(String userId) {
		// TODO Auto-generated method stub
		SysUser user = getByUserId(userId);
		user.setEnable(0);
		super.save(user);
	}

	@Override
	public SysUserRole getByIdSysUserRole(String userRoleId) {
		 
		String hql = "from SysUserRole as a where a.sysUser.userId='" + userRoleId + "'";
		List<SysUserRole> list = query(hql);
		if(list.size()>0)
			return list.get(0);
		return null;
	}

	@Override
	public List getSysRole() {
		// TODO Auto-generated method stub
		String sql = "from SysRole as a where a.isDel=0 and a.enable=1";
		return query(sql);
	}

	@Override
	public SysUser getUsers(String userName, String pwd) {
		// TODO Auto-generated method stub
		String sql="from SysUser as a where a.userName='" + userName +"' and a.pwd='" + pwd + "'";
		List<SysUser> list = query(sql);
		if(list.size()>0)
			return list.get(0);
		return null;
	}

	@Override
	public void saveSysUserRole(SysUserRole sysUserRole) {
		// TODO Auto-generated method stub
		super.save(sysUserRole);
	}

	@Override
	public SysUser getByUser(String userId) {
		// TODO Auto-generated method stub
		String hql = "from SysUser as a where a.isDel = 0 and a.type = 3 and a.userId='" + userId + "'";
		List<SysUser> list = query(hql);
		if(list.size()>0)
			return list.get(0);
		return null;
	}

	@Override
	public SysUserRole getByUserRoleId(String userId) {
		// TODO Auto-generated method stub
		String hql = "from SysUserRole as a where a.sysUser.userId='" + userId + "'";
		List<SysUserRole> list = query(hql);
		if(list.size()>0)
			return list.get(0);
		return null;
	}

	@Override
	public List<SysRole> chaRole(String userName) {
		// TODO Auto-generated method stub
		String sql = "from SysRole as a where a.isDel=0 and a.userName= '" + userName + "'";
		List<SysRole> list = query(sql);
		return list;
	}

	@Override
	public List<SysUser> chaUser(String userName) {
		// TODO Auto-generated method stub
		String sql = "from SysUser as a where a.type = 3 and a.userName='" + userName + "' and a.isDel=0";
		List<SysUser> list = query(sql);
		return list;
	}

	@Override
	public int getRolef() {
		String hql="select count(*) from SysRole as a";
		return queryCount(hql);
	}

	@Override
	public SysRole getByRole(String roleId) {
		// TODO Auto-generated method stub
		String hql = "from SysRole as a where a.roleId='" + roleId + "'";
		List<SysRole> list = query(hql);
		if(list.size()>0)
			return list.get(0);
		return null;
	}

	@Override
	public int getRolefenye(String SsName) {
		// TODO Auto-generated method stub
		String hql = "select count(*) from SysRole as a where 1=1";
		if(SsName != null && !"".equals(SsName))
		{
			hql += " and a.userName like '%" + SsName + "%'";
		}
		hql+=" and a.isDel=0 order by a.createDate asc";
		return queryCount(hql);
	}

	@Override
	public int getUserfenye(String SsName,String roleName) {
		// TODO Auto-generated method stub
		String hql = "select count(*) from SysUser as a where 1=1";
		if(SsName != null && !"".equals(SsName))
		{
			hql += " and a.userName like '%" + SsName + "%'";
		}
		if(roleName != null && !"".equals(roleName))
		{
			hql += " and a.sysUserRole.sysRole.userName like '%" + roleName + "%'";
		}
		hql+=" and a.isDel=0 and a.type=3 order by a.createDate asc";
		return queryCount(hql);
	}

	@Override
	public SysUserRole getUsersRole(String userId, String sysRole) {
		// TODO Auto-generated method stub
		String sql="from SysUserRole as a where a.sysUser.userId='" + userId +"' and a.sysRole.roleId='" + sysRole + "'";
		List<SysUserRole> list = query(sql);
		if(list.size()>0)
			return list.get(0);
		return null;
	}

	@Override
	public List<SysUserRole> getRoleUserList(String key) {
		// TODO Auto-generated method stub
		String sql="from SysUserRole as a where a.sysRole.roleId='" + key +"'";
		List<SysUserRole> list = query(sql);
		if(list.size()>0)
			return list;
		return null;
	}

	/* 
	 * 崔青山
	 * 2016年8月3日
	 * 上午12:16:06
	 */
	@Override
	public void savegaiRole(String delKeys,String pwd) {
		// TODO Auto-generated method stub
		SysUser user = getByUserId(delKeys);
			user.setPwd(pwd);
			super.save(user);
	}

	@Override
	public List<SysRolePower> findRolePowerById(String roleId) {
		// TODO Auto-generated method stub
		String sql = "from SysRolePower as a where a.sysRole.roleId='"+roleId+"' order by a.powerCode desc";
		List<SysRolePower> list = query(sql);
		return list;
	}

	@Override
	public List<SysMenu> getMenuList(String parentMenuId) {
		// TODO Auto-generated method stub
		String sql = "";
		if(parentMenuId == null)
			sql="from SysMenu as a where a.sysMenu.menuId is null and a.enable=1";
		else
			sql="from SysMenu as a where a.sysMenu.menuId = '" + parentMenuId +"' and a.enable=1"; 
		return super.query(sql);
	}

	/* 
	 * 崔青山
	 * 2016年8月8日
	 * 上午9:25:36
	 */
	@Override
	public void delPower(String roleId) {
		// TODO Auto-generated method stub
		String sql ="from SysRolePower as a where a.sysRole.roleId='"+roleId+"'";
		List<SysRolePower> list = query(sql);
		for (int i = 0; i < list.size(); i++) {
			SysRolePower delPower = list.get(i);
			super.delete(delPower);
		}
	}

	/* 
	 * 崔青山
	 * 2016年8月8日
	 * 上午9:25:36
	 */
	@Override
	public SysRole findRoleById(String roleId) {
		// TODO Auto-generated method stub
		String sql = "from SysRole as a where a.roleId='"+roleId+"'";
		List<SysRole> list = query(sql);
		if (list.size()>0) {
			return list.get(0);
		}else {
			return null;
		}
	}

	/* 
	 * 崔青山
	 * 2016年8月8日
	 * 上午9:25:36
	 */
	@Override
	public void savePowerInRolePower(SysRolePower rolePower) {
		// TODO Auto-generated method stub
		super.save(rolePower);
	}

	@Override
	public List<SysUser> getShopUserList(String ssName, String roleName,
			Integer integer, Integer integer2) {
		// TODO Auto-generated method stub
		String sql="from SysUser as a where 1=1";
		if(ssName != null && !"".equals(ssName))
		{
			sql += " and a.userName like '%" + ssName + "%'";
		}
		if(roleName != null && !"".equals(roleName))
		{
			sql += " and a.sysUserRole.sysRole.userName like '%" + roleName + "%'";
		}
		sql+=" and a.isDel=0 and a.type=2 order by a.createDate asc";
		return query(sql,integer,integer2);
	}

	@Override
	public List<SysUser> getHuiyuanUserList(String ssName, String roleName,
			Integer integer, Integer integer2) {
		// TODO Auto-generated method stub
		String sql="from SysUser as a where 1=1";
		if(ssName != null && !"".equals(ssName))
		{
			sql += " and a.userName like '%" + ssName + "%'";
		}
		if(roleName != null && !"".equals(roleName))
		{
			sql += " and a.sysUserRole.sysRole.userName like '%" + roleName + "%'";
		}
		sql+=" and a.isDel=0 and a.type=1 order by a.createDate asc";
		return query(sql,integer,integer2);
	}

	/* 
	 * 崔青山
	 * 2016年8月17日
	 * 上午10:15:04
	 */
	@Override
	public SysUserRole findUserRoleByUserId(String userId) {
		// TODO Auto-generated method stub
		String sql = "from SysUserRole as a where a.sysUser.userId='"+userId+"'";
		List<SysUserRole> list = query(sql);
		return list.get(0);
	}

	/* 
	 * 崔青山
	 * 2016年8月17日
	 * 上午10:16:20
	 */
	@Override
	public SysPowerItem getSysPowerItemById(String powerCode) {
		// TODO Auto-generated method stub
		String sql = "from SysPowerItem as a where a.sysMenu.menuId='"+powerCode+"'";
		List<SysPowerItem> list = query(sql);
		if(list.size() > 0)
			return list.get(0);
		return null;
	}

	/* 
	 * 崔青山
	 * 2016年8月21日
	 * 下午1:06:58
	 */
	@Override
	public int getUserfenyeShop(String ssName, String roleName) {
		// TODO Auto-generated method stub
		String sql="select count(*) from SysUser as a where 1=1";
		if(ssName != null && !"".equals(ssName))
		{
			sql += " and a.userName like '%" + ssName + "%'";
		}
		if(roleName != null && !"".equals(roleName))
		{
			sql += " and a.sysUserRole.sysRole.userName like '%" + roleName + "%'";
		}
		sql+=" and a.isDel=0 and a.type=2 order by a.createDate asc";
		return queryCount(sql);
	}

	/* 
	 * 崔青山
	 * 2016年8月21日
	 * 下午1:09:18
	 */
	@Override
	public int getUserfenyeHuiyuan(String ssName, String roleName) {
		// TODO Auto-generated method stub
		String sql="select count(*) from SysUser as a where 1=1";
		if(ssName != null && !"".equals(ssName))
		{
			sql += " and a.userName like '%" + ssName + "%'";
		}
		if(roleName != null && !"".equals(roleName))
		{
			sql += " and a.sysUserRole.sysRole.userName like '%" + roleName + "%'";
		}
		sql+=" and a.isDel=0 and a.type=1 order by a.createDate asc";
		return queryCount(sql);
	}

	/* 
	 * 崔青山
	 * 2016年9月21日
	 * 下午5:43:56
	 */
	@Override
	public SysUserRole getSysUserRole(String userId) {
		// TODO Auto-generated method stub
		String sql = "from SysUserRole as a where a.sysUser.userId='"+userId+"'";
		List<SysUserRole> list = query(sql);
		if(list.size() > 0)
			return list.get(0);
		return null;
	}

	/**
	 * 2017年1月9日上午11:17:55
	 *  阳朔
	 *  注释:
	 */
	@Override
	public List<SysUser> chaLoginName(String loginName) {
		String sql = "from SysUser as a where a.isDel = 0 and a.type = 3 and a.loginName = '" + loginName + "'";
		return query(sql);
	}

	/**
	 * 2017年1月9日上午11:24:10
	 *  阳朔
	 *  注释:
	 */
	@Override
	public SysRole getManagerRole() {
		String sql = "from SysRole as a where a.isDel = 0 and a.enable = 1 and a.roleId = 2";
		List<SysRole> rolelist = query(sql);
		return rolelist.get(0);
	}

	/**
	 * 2017年1月9日下午12:19:54
	 *  阳朔
	 *  注释:
	 */
	@Override
	public SysUser getSysUserByUserId(String userId) {
		String sql = "from SysUser as a where a.isDel = 0 and a.type = 3 and a.userId = '" + userId + "'";
		List<SysUser> list = query(sql);
		if(list.size() > 0)
			return list.get(0);
		return null;
	}


	

}
