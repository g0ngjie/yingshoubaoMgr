/**
 * 
 */
package com.cosin.web.dao.system;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.cosin.web.dao.BaseDao;
import com.cosin.web.entity.SysRolePower;
import com.cosin.web.entity.SysUser;
import com.cosin.web.entity.SysUserRole;

/**
 * @author 王思明
 * 2016年7月19日
 * 下午2:06:10
 */
@Repository  //将Dao类声明为Bean
public class systemDao extends BaseDao implements ISystemDao {

	/*
	 * 后台用户登录验证
	 * 王思明
	 * 2016年7月19日
	 * 下午3:19:30
	 */
	@Override
	public SysUser getByEmailNamePwd(String loginName, String pwd) {
		String hql = "from SysUser as a where a.isDel=0 and a.enable = 1 and a.pwd='" + pwd + "' and a.loginName='" + loginName + "'and a.type=3";
		List<SysUser> list = query(hql);
		if(list.size() > 0)
			return list.get(0);
		return null;
	}

	/* 
	 * 保存对象方法
	 * 王思明
	 * 2016年7月19日
	 * 下午3:21:33
	 */
	@Override
	public void saveObject(Object obj) {
		
		
		super.save(obj);
	}

	/**
	 * 2017年1月9日下午2:19:52
	 *  阳朔
	 *  注释:
	 */
	@Override
	public SysUserRole getSysRoleByUserId(String userId) {
		String sql = "from SysUserRole as a where a.sysUser = '" + userId + "'";
		List<SysUserRole> list = query(sql);
		if(list.size() > 0)
			return list.get(0);
		return null;
	}

	/**
	 * 2017年1月9日下午2:25:13
	 *  阳朔
	 *  注释:
	 */
	@Override
	public List<SysRolePower> getSysRolePower(String roleId) {
		String sql = "from SysRolePower as a where a.sysRole = '" + roleId + "'";
		return query(sql);
	}
	

	
	
}
