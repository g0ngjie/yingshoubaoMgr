package com.cosin.web.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 * About entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "about", catalog = "revenue")
public class About implements java.io.Serializable {

	// Fields

	private String aboutId;
	private String img;
	private String content;

	// Constructors

	/** default constructor */
	public About() {
	}

	/** full constructor */
	public About(String img, String content) {
		this.img = img;
		this.content = content;
	}

	// Property accessors
	@GenericGenerator(name = "generator", strategy = "uuid.hex")
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name = "aboutId", unique = true, nullable = false, length = 32)
	public String getAboutId() {
		return this.aboutId;
	}

	public void setAboutId(String aboutId) {
		this.aboutId = aboutId;
	}

	@Column(name = "img", length = 128)
	public String getImg() {
		return this.img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	@Column(name = "content", length = 16777215)
	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}