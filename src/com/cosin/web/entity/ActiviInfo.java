package com.cosin.web.entity;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 * ActiviInfo entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "activi_info", catalog = "revenue")
public class ActiviInfo implements java.io.Serializable {

	// Fields

	private String activId;
	private Integer loginNum;
	private Integer dealNum;
	private Integer registerNum;
	private Timestamp createDate;

	// Constructors

	/** default constructor */
	public ActiviInfo() {
	}

	/** full constructor */
	public ActiviInfo(Integer loginNum, Integer dealNum, Integer registerNum,
			Timestamp createDate) {
		this.loginNum = loginNum;
		this.dealNum = dealNum;
		this.registerNum = registerNum;
		this.createDate = createDate;
	}

	// Property accessors
	@GenericGenerator(name = "generator", strategy = "uuid.hex")
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name = "activId", unique = true, nullable = false, length = 32)
	public String getActivId() {
		return this.activId;
	}

	public void setActivId(String activId) {
		this.activId = activId;
	}

	@Column(name = "loginNum")
	public Integer getLoginNum() {
		return this.loginNum;
	}

	public void setLoginNum(Integer loginNum) {
		this.loginNum = loginNum;
	}

	@Column(name = "dealNum")
	public Integer getDealNum() {
		return this.dealNum;
	}

	public void setDealNum(Integer dealNum) {
		this.dealNum = dealNum;
	}

	@Column(name = "registerNum")
	public Integer getRegisterNum() {
		return this.registerNum;
	}

	public void setRegisterNum(Integer registerNum) {
		this.registerNum = registerNum;
	}

	@Column(name = "createDate", length = 0)
	public Timestamp getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

}