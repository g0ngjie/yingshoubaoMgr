package com.cosin.web.entity;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 * BankCard entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "bank_card", catalog = "revenue")
public class BankCard implements java.io.Serializable {

	// Fields

	private String bankCardId;
	private String bankCardName;
	private Integer enable;
	private Set<BindBank> bindBanks = new HashSet<BindBank>(0);

	// Constructors

	/** default constructor */
	public BankCard() {
	}

	/** full constructor */
	public BankCard(String bankCardName, Integer enable, Set<BindBank> bindBanks) {
		this.bankCardName = bankCardName;
		this.enable = enable;
		this.bindBanks = bindBanks;
	}

	// Property accessors
	@GenericGenerator(name = "generator", strategy = "uuid.hex")
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name = "bankCardId", unique = true, nullable = false, length = 32)
	public String getBankCardId() {
		return this.bankCardId;
	}

	public void setBankCardId(String bankCardId) {
		this.bankCardId = bankCardId;
	}

	@Column(name = "bankCardName", length = 64)
	public String getBankCardName() {
		return this.bankCardName;
	}

	public void setBankCardName(String bankCardName) {
		this.bankCardName = bankCardName;
	}

	@Column(name = "enable")
	public Integer getEnable() {
		return this.enable;
	}

	public void setEnable(Integer enable) {
		this.enable = enable;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "bankCard")
	public Set<BindBank> getBindBanks() {
		return this.bindBanks;
	}

	public void setBindBanks(Set<BindBank> bindBanks) {
		this.bindBanks = bindBanks;
	}

}