package com.cosin.web.entity;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 * BindBank entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "bind_bank", catalog = "revenue")
public class BindBank implements java.io.Serializable {

	// Fields

	private String bindBankId;
	private BankCard bankCard;
	private SysUser sysUser;
	private String openBank;
	private String userName;
	private String bankCard_1;
	private Timestamp createDate;

	// Constructors

	/** default constructor */
	public BindBank() {
	}

	/** full constructor */
	public BindBank(BankCard bankCard, SysUser sysUser, String openBank,
			String userName, String bankCard_1, Timestamp createDate) {
		this.bankCard = bankCard;
		this.sysUser = sysUser;
		this.openBank = openBank;
		this.userName = userName;
		this.bankCard_1 = bankCard_1;
		this.createDate = createDate;
	}

	// Property accessors
	@GenericGenerator(name = "generator", strategy = "uuid.hex")
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name = "bindBankId", unique = true, nullable = false, length = 32)
	public String getBindBankId() {
		return this.bindBankId;
	}

	public void setBindBankId(String bindBankId) {
		this.bindBankId = bindBankId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "bankCardId")
	public BankCard getBankCard() {
		return this.bankCard;
	}

	public void setBankCard(BankCard bankCard) {
		this.bankCard = bankCard;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userId")
	public SysUser getSysUser() {
		return this.sysUser;
	}

	public void setSysUser(SysUser sysUser) {
		this.sysUser = sysUser;
	}

	@Column(name = "openBank", length = 64)
	public String getOpenBank() {
		return this.openBank;
	}

	public void setOpenBank(String openBank) {
		this.openBank = openBank;
	}

	@Column(name = "userName", length = 64)
	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Column(name = "bankCard", length = 32)
	public String getBankCard_1() {
		return this.bankCard_1;
	}

	public void setBankCard_1(String bankCard_1) {
		this.bankCard_1 = bankCard_1;
	}

	@Column(name = "createDate", length = 0)
	public Timestamp getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

}