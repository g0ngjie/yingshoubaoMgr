package com.cosin.web.entity;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 * CodeBackground entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "code_background", catalog = "revenue")
public class CodeBackground implements java.io.Serializable {

	// Fields

	private String codeBackgroundId;
	private String img;
	private Timestamp createDate;

	// Constructors

	/** default constructor */
	public CodeBackground() {
	}

	/** full constructor */
	public CodeBackground(String img, Timestamp createDate) {
		this.img = img;
		this.createDate = createDate;
	}

	// Property accessors
	@GenericGenerator(name = "generator", strategy = "uuid.hex")
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name = "codeBackgroundId", unique = true, nullable = false, length = 32)
	public String getCodeBackgroundId() {
		return this.codeBackgroundId;
	}

	public void setCodeBackgroundId(String codeBackgroundId) {
		this.codeBackgroundId = codeBackgroundId;
	}

	@Column(name = "img", length = 128)
	public String getImg() {
		return this.img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	@Column(name = "createDate", length = 0)
	public Timestamp getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

}