package com.cosin.web.entity;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 * CollectShop entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "collect_shop", catalog = "revenue")
public class CollectShop implements java.io.Serializable {

	// Fields

	private String collectShopId;
	private SysShop sysShop;
	private SysUser sysUser;
	private Timestamp createDate;

	// Constructors

	/** default constructor */
	public CollectShop() {
	}

	/** full constructor */
	public CollectShop(SysShop sysShop, SysUser sysUser, Timestamp createDate) {
		this.sysShop = sysShop;
		this.sysUser = sysUser;
		this.createDate = createDate;
	}

	// Property accessors
	@GenericGenerator(name = "generator", strategy = "uuid.hex")
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name = "collectShopId", unique = true, nullable = false, length = 32)
	public String getCollectShopId() {
		return this.collectShopId;
	}

	public void setCollectShopId(String collectShopId) {
		this.collectShopId = collectShopId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "shopId")
	public SysShop getSysShop() {
		return this.sysShop;
	}

	public void setSysShop(SysShop sysShop) {
		this.sysShop = sysShop;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userId")
	public SysUser getSysUser() {
		return this.sysUser;
	}

	public void setSysUser(SysUser sysUser) {
		this.sysUser = sysUser;
	}

	@Column(name = "createDate", length = 0)
	public Timestamp getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

}