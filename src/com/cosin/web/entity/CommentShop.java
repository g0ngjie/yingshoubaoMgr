package com.cosin.web.entity;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 * CommentShop entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "comment_shop", catalog = "revenue")
public class CommentShop implements java.io.Serializable {

	// Fields

	private String commentId;
	private SysShop sysShop;
	private SysUser sysUser;
	private SysOrder sysOrder;
	private String content;
	private Integer star;
	private Integer starTwo;
	private Integer starThree;
	private Timestamp createDate;

	// Constructors

	/** default constructor */
	public CommentShop() {
	}

	/** full constructor */
	public CommentShop(SysShop sysShop, SysUser sysUser, SysOrder sysOrder,
			String content, Integer star, Integer starTwo, Integer starThree,
			Timestamp createDate) {
		this.sysShop = sysShop;
		this.sysUser = sysUser;
		this.sysOrder = sysOrder;
		this.content = content;
		this.star = star;
		this.starTwo = starTwo;
		this.starThree = starThree;
		this.createDate = createDate;
	}

	// Property accessors
	@GenericGenerator(name = "generator", strategy = "uuid.hex")
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name = "commentId", unique = true, nullable = false, length = 32)
	public String getCommentId() {
		return this.commentId;
	}

	public void setCommentId(String commentId) {
		this.commentId = commentId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "shopId")
	public SysShop getSysShop() {
		return this.sysShop;
	}

	public void setSysShop(SysShop sysShop) {
		this.sysShop = sysShop;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "createUser")
	public SysUser getSysUser() {
		return this.sysUser;
	}

	public void setSysUser(SysUser sysUser) {
		this.sysUser = sysUser;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "orderId")
	public SysOrder getSysOrder() {
		return this.sysOrder;
	}

	public void setSysOrder(SysOrder sysOrder) {
		this.sysOrder = sysOrder;
	}

	@Column(name = "content", length = 1024)
	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Column(name = "star")
	public Integer getStar() {
		return this.star;
	}

	public void setStar(Integer star) {
		this.star = star;
	}

	@Column(name = "starTwo")
	public Integer getStarTwo() {
		return this.starTwo;
	}

	public void setStarTwo(Integer starTwo) {
		this.starTwo = starTwo;
	}

	@Column(name = "starThree")
	public Integer getStarThree() {
		return this.starThree;
	}

	public void setStarThree(Integer starThree) {
		this.starThree = starThree;
	}

	@Column(name = "createDate", length = 0)
	public Timestamp getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

}