package com.cosin.web.entity;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 * Fans entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "fans", catalog = "revenue")
public class Fans implements java.io.Serializable {

	// Fields

	private String fansId;
	private SysUser sysUserByParentUserId;
	private SysShop sysShop;
	private SysUser sysUserByUserId;
	private Double contribute;
	private Timestamp createDate;

	// Constructors

	/** default constructor */
	public Fans() {
	}

	/** full constructor */
	public Fans(SysUser sysUserByParentUserId, SysShop sysShop,
			SysUser sysUserByUserId, Double contribute, Timestamp createDate) {
		this.sysUserByParentUserId = sysUserByParentUserId;
		this.sysShop = sysShop;
		this.sysUserByUserId = sysUserByUserId;
		this.contribute = contribute;
		this.createDate = createDate;
	}

	// Property accessors
	@GenericGenerator(name = "generator", strategy = "uuid.hex")
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name = "fansId", unique = true, nullable = false, length = 32)
	public String getFansId() {
		return this.fansId;
	}

	public void setFansId(String fansId) {
		this.fansId = fansId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "parentUserId")
	public SysUser getSysUserByParentUserId() {
		return this.sysUserByParentUserId;
	}

	public void setSysUserByParentUserId(SysUser sysUserByParentUserId) {
		this.sysUserByParentUserId = sysUserByParentUserId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "shopId")
	public SysShop getSysShop() {
		return this.sysShop;
	}

	public void setSysShop(SysShop sysShop) {
		this.sysShop = sysShop;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userId")
	public SysUser getSysUserByUserId() {
		return this.sysUserByUserId;
	}

	public void setSysUserByUserId(SysUser sysUserByUserId) {
		this.sysUserByUserId = sysUserByUserId;
	}

	@Column(name = "contribute", precision = 11)
	public Double getContribute() {
		return this.contribute;
	}

	public void setContribute(Double contribute) {
		this.contribute = contribute;
	}

	@Column(name = "createDate", length = 0)
	public Timestamp getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

}