package com.cosin.web.entity;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 * FeedBack entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "feed_back", catalog = "revenue")
public class FeedBack implements java.io.Serializable {

	// Fields

	private String feedBackId;
	private SysUser sysUser;
	private String content;
	private Integer type;
	private Timestamp createDate;

	// Constructors

	/** default constructor */
	public FeedBack() {
	}

	/** full constructor */
	public FeedBack(SysUser sysUser, String content, Integer type,
			Timestamp createDate) {
		this.sysUser = sysUser;
		this.content = content;
		this.type = type;
		this.createDate = createDate;
	}

	// Property accessors
	@GenericGenerator(name = "generator", strategy = "uuid.hex")
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name = "feedBackId", unique = true, nullable = false, length = 32)
	public String getFeedBackId() {
		return this.feedBackId;
	}

	public void setFeedBackId(String feedBackId) {
		this.feedBackId = feedBackId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userId")
	public SysUser getSysUser() {
		return this.sysUser;
	}

	public void setSysUser(SysUser sysUser) {
		this.sysUser = sysUser;
	}

	@Column(name = "content", length = 2048)
	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Column(name = "type")
	public Integer getType() {
		return this.type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	@Column(name = "createDate", length = 0)
	public Timestamp getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

}