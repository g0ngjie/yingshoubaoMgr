package com.cosin.web.entity;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 * FenrunDetail entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "fenrun_detail", catalog = "revenue")
public class FenrunDetail implements java.io.Serializable {

	// Fields

	private String frDetailId;
	private SysUser sysUserByTopUserId;
	private SysUser sysUserByUserId;
	private String orderNum;
	private Double money;
	private Timestamp createDate;
	private String shopId;

	// Constructors

	/** default constructor */
	public FenrunDetail() {
	}

	/** full constructor */
	public FenrunDetail(SysUser sysUserByTopUserId, SysUser sysUserByUserId,
			String orderNum, Double money, Timestamp createDate, String shopId) {
		this.sysUserByTopUserId = sysUserByTopUserId;
		this.sysUserByUserId = sysUserByUserId;
		this.orderNum = orderNum;
		this.money = money;
		this.createDate = createDate;
		this.shopId = shopId;
	}

	// Property accessors
	@GenericGenerator(name = "generator", strategy = "uuid.hex")
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name = "frDetailId", unique = true, nullable = false, length = 32)
	public String getFrDetailId() {
		return this.frDetailId;
	}

	public void setFrDetailId(String frDetailId) {
		this.frDetailId = frDetailId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "topUserId")
	public SysUser getSysUserByTopUserId() {
		return this.sysUserByTopUserId;
	}

	public void setSysUserByTopUserId(SysUser sysUserByTopUserId) {
		this.sysUserByTopUserId = sysUserByTopUserId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userId")
	public SysUser getSysUserByUserId() {
		return this.sysUserByUserId;
	}

	public void setSysUserByUserId(SysUser sysUserByUserId) {
		this.sysUserByUserId = sysUserByUserId;
	}

	@Column(name = "orderNum", length = 32)
	public String getOrderNum() {
		return this.orderNum;
	}

	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}

	@Column(name = "money", precision = 11)
	public Double getMoney() {
		return this.money;
	}

	public void setMoney(Double money) {
		this.money = money;
	}

	@Column(name = "createDate", length = 0)
	public Timestamp getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	@Column(name = "shopId", length = 32)
	public String getShopId() {
		return this.shopId;
	}

	public void setShopId(String shopId) {
		this.shopId = shopId;
	}

}