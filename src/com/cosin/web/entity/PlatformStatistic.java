package com.cosin.web.entity;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 * PlatformStatistic entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "platform_statistic", catalog = "revenue")
public class PlatformStatistic implements java.io.Serializable {

	// Fields

	private String flatformStatisticId;
	private Double price;
	private Integer num;
	private Timestamp createDate;
	private Double shopSumPrice;
	private Double buyTopSum;

	// Constructors

	/** default constructor */
	public PlatformStatistic() {
	}

	/** full constructor */
	public PlatformStatistic(Double price, Integer num, Timestamp createDate,
			Double shopSumPrice, Double buyTopSum) {
		this.price = price;
		this.num = num;
		this.createDate = createDate;
		this.shopSumPrice = shopSumPrice;
		this.buyTopSum = buyTopSum;
	}

	// Property accessors
	@GenericGenerator(name = "generator", strategy = "uuid.hex")
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name = "flatformStatisticId", unique = true, nullable = false, length = 32)
	public String getFlatformStatisticId() {
		return this.flatformStatisticId;
	}

	public void setFlatformStatisticId(String flatformStatisticId) {
		this.flatformStatisticId = flatformStatisticId;
	}

	@Column(name = "price", precision = 11)
	public Double getPrice() {
		return this.price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	@Column(name = "num")
	public Integer getNum() {
		return this.num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	@Column(name = "createDate", length = 0)
	public Timestamp getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	@Column(name = "shopSumPrice", precision = 11)
	public Double getShopSumPrice() {
		return this.shopSumPrice;
	}

	public void setShopSumPrice(Double shopSumPrice) {
		this.shopSumPrice = shopSumPrice;
	}

	@Column(name = "buyTopSum", precision = 11)
	public Double getBuyTopSum() {
		return this.buyTopSum;
	}

	public void setBuyTopSum(Double buyTopSum) {
		this.buyTopSum = buyTopSum;
	}

}