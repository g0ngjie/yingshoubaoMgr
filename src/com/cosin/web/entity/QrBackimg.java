package com.cosin.web.entity;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 * QrBackimg entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "qr_backimg", catalog = "revenue")
public class QrBackimg implements java.io.Serializable {

	// Fields

	private String qrBackImgId;
	private String img;
	private Integer enable;
	private Integer isDel;
	private Timestamp createDate;

	// Constructors

	/** default constructor */
	public QrBackimg() {
	}

	/** full constructor */
	public QrBackimg(String img, Integer enable, Integer isDel,
			Timestamp createDate) {
		this.img = img;
		this.enable = enable;
		this.isDel = isDel;
		this.createDate = createDate;
	}

	// Property accessors
	@GenericGenerator(name = "generator", strategy = "uuid.hex")
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name = "qrBackImgId", unique = true, nullable = false, length = 32)
	public String getQrBackImgId() {
		return this.qrBackImgId;
	}

	public void setQrBackImgId(String qrBackImgId) {
		this.qrBackImgId = qrBackImgId;
	}

	@Column(name = "img")
	public String getImg() {
		return this.img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	@Column(name = "enable")
	public Integer getEnable() {
		return this.enable;
	}

	public void setEnable(Integer enable) {
		this.enable = enable;
	}

	@Column(name = "isDel")
	public Integer getIsDel() {
		return this.isDel;
	}

	public void setIsDel(Integer isDel) {
		this.isDel = isDel;
	}

	@Column(name = "createDate", length = 0)
	public Timestamp getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

}