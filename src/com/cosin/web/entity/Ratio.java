package com.cosin.web.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 * Ratio entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "ratio", catalog = "revenue")
public class Ratio implements java.io.Serializable {

	// Fields

	private String ratioId;
	private Double onece;
	private Double second;
	private Double third;

	// Constructors

	/** default constructor */
	public Ratio() {
	}

	/** full constructor */
	public Ratio(Double onece, Double second, Double third) {
		this.onece = onece;
		this.second = second;
		this.third = third;
	}

	// Property accessors
	@GenericGenerator(name = "generator", strategy = "uuid.hex")
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name = "ratioId", unique = true, nullable = false, length = 32)
	public String getRatioId() {
		return this.ratioId;
	}

	public void setRatioId(String ratioId) {
		this.ratioId = ratioId;
	}

	@Column(name = "onece", precision = 22, scale = 0)
	public Double getOnece() {
		return this.onece;
	}

	public void setOnece(Double onece) {
		this.onece = onece;
	}

	@Column(name = "second", precision = 22, scale = 0)
	public Double getSecond() {
		return this.second;
	}

	public void setSecond(Double second) {
		this.second = second;
	}

	@Column(name = "third", precision = 22, scale = 0)
	public Double getThird() {
		return this.third;
	}

	public void setThird(Double third) {
		this.third = third;
	}

}