package com.cosin.web.entity;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 * ShopActivity entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "shop_activity", catalog = "revenue")
public class ShopActivity implements java.io.Serializable {

	// Fields

	private String shopActivityId;
	private SysShop sysShop;
	private String content;
	private Timestamp createDate;
	private Timestamp beginTime;
	private Timestamp endTime;

	// Constructors

	/** default constructor */
	public ShopActivity() {
	}

	/** full constructor */
	public ShopActivity(SysShop sysShop, String content, Timestamp createDate,
			Timestamp beginTime, Timestamp endTime) {
		this.sysShop = sysShop;
		this.content = content;
		this.createDate = createDate;
		this.beginTime = beginTime;
		this.endTime = endTime;
	}

	// Property accessors
	@GenericGenerator(name = "generator", strategy = "uuid.hex")
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name = "shopActivityId", unique = true, nullable = false, length = 32)
	public String getShopActivityId() {
		return this.shopActivityId;
	}

	public void setShopActivityId(String shopActivityId) {
		this.shopActivityId = shopActivityId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "shopId")
	public SysShop getSysShop() {
		return this.sysShop;
	}

	public void setSysShop(SysShop sysShop) {
		this.sysShop = sysShop;
	}

	@Column(name = "content", length = 4096)
	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Column(name = "createDate", length = 0)
	public Timestamp getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	@Column(name = "beginTime", length = 0)
	public Timestamp getBeginTime() {
		return this.beginTime;
	}

	public void setBeginTime(Timestamp beginTime) {
		this.beginTime = beginTime;
	}

	@Column(name = "endTime", length = 0)
	public Timestamp getEndTime() {
		return this.endTime;
	}

	public void setEndTime(Timestamp endTime) {
		this.endTime = endTime;
	}

}