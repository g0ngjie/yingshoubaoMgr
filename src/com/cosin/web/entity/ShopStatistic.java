package com.cosin.web.entity;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 * ShopStatistic entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "shop_statistic", catalog = "revenue")
public class ShopStatistic implements java.io.Serializable {

	// Fields

	private String shopStatisticId;
	private SysShop sysShop;
	private Double price;
	private Double income;
	private Integer num;
	private Timestamp createDate;

	// Constructors

	/** default constructor */
	public ShopStatistic() {
	}

	/** full constructor */
	public ShopStatistic(SysShop sysShop, Double price, Double income,
			Integer num, Timestamp createDate) {
		this.sysShop = sysShop;
		this.price = price;
		this.income = income;
		this.num = num;
		this.createDate = createDate;
	}

	// Property accessors
	@GenericGenerator(name = "generator", strategy = "uuid.hex")
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name = "shopStatisticId", unique = true, nullable = false, length = 32)
	public String getShopStatisticId() {
		return this.shopStatisticId;
	}

	public void setShopStatisticId(String shopStatisticId) {
		this.shopStatisticId = shopStatisticId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "shopId")
	public SysShop getSysShop() {
		return this.sysShop;
	}

	public void setSysShop(SysShop sysShop) {
		this.sysShop = sysShop;
	}

	@Column(name = "price", precision = 11)
	public Double getPrice() {
		return this.price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	@Column(name = "income", precision = 11)
	public Double getIncome() {
		return this.income;
	}

	public void setIncome(Double income) {
		this.income = income;
	}

	@Column(name = "num")
	public Integer getNum() {
		return this.num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	@Column(name = "createDate", length = 0)
	public Timestamp getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

}