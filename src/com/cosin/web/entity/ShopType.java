package com.cosin.web.entity;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 * ShopType entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "shop_type", catalog = "revenue")
public class ShopType implements java.io.Serializable {

	// Fields

	private String shopTypeId;
	private String typeName;
	private Integer enable;
	private Integer isDel;
	private String img;
	private String orderNum;
	private Set<SysShop> sysShops = new HashSet<SysShop>(0);

	// Constructors

	/** default constructor */
	public ShopType() {
	}

	/** full constructor */
	public ShopType(String typeName, Integer enable, Integer isDel, String img,
			String orderNum, Set<SysShop> sysShops) {
		this.typeName = typeName;
		this.enable = enable;
		this.isDel = isDel;
		this.img = img;
		this.orderNum = orderNum;
		this.sysShops = sysShops;
	}

	// Property accessors
	@GenericGenerator(name = "generator", strategy = "uuid.hex")
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name = "shopTypeId", unique = true, nullable = false, length = 32)
	public String getShopTypeId() {
		return this.shopTypeId;
	}

	public void setShopTypeId(String shopTypeId) {
		this.shopTypeId = shopTypeId;
	}

	@Column(name = "typeName", length = 32)
	public String getTypeName() {
		return this.typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	@Column(name = "enable")
	public Integer getEnable() {
		return this.enable;
	}

	public void setEnable(Integer enable) {
		this.enable = enable;
	}

	@Column(name = "isDel")
	public Integer getIsDel() {
		return this.isDel;
	}

	public void setIsDel(Integer isDel) {
		this.isDel = isDel;
	}

	@Column(name = "img", length = 128)
	public String getImg() {
		return this.img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	@Column(name = "orderNum", length = 32)
	public String getOrderNum() {
		return this.orderNum;
	}

	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "shopType")
	public Set<SysShop> getSysShops() {
		return this.sysShops;
	}

	public void setSysShops(Set<SysShop> sysShops) {
		this.sysShops = sysShops;
	}

}