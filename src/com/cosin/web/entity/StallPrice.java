package com.cosin.web.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 * StallPrice entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "stall_price", catalog = "revenue")
public class StallPrice implements java.io.Serializable {

	// Fields

	private String stallId;
	private Double unitPrice;

	// Constructors

	/** default constructor */
	public StallPrice() {
	}

	/** full constructor */
	public StallPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}

	// Property accessors
	@GenericGenerator(name = "generator", strategy = "uuid.hex")
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name = "stallId", unique = true, nullable = false, length = 32)
	public String getStallId() {
		return this.stallId;
	}

	public void setStallId(String stallId) {
		this.stallId = stallId;
	}

	@Column(name = "unitPrice", precision = 11)
	public Double getUnitPrice() {
		return this.unitPrice;
	}

	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}

}