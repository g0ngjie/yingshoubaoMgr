package com.cosin.web.entity;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 * StallRecord entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "stall_record", catalog = "revenue")
public class StallRecord implements java.io.Serializable {

	// Fields

	private String stallRecordId;
	private SysShop sysShop;
	private Integer priceToDay;
	private Double unitPrice;
	private Timestamp createDate;
	private Double price;

	// Constructors

	/** default constructor */
	public StallRecord() {
	}

	/** full constructor */
	public StallRecord(SysShop sysShop, Integer priceToDay, Double unitPrice,
			Timestamp createDate, Double price) {
		this.sysShop = sysShop;
		this.priceToDay = priceToDay;
		this.unitPrice = unitPrice;
		this.createDate = createDate;
		this.price = price;
	}

	// Property accessors
	@GenericGenerator(name = "generator", strategy = "uuid.hex")
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name = "stallRecordId", unique = true, nullable = false, length = 32)
	public String getStallRecordId() {
		return this.stallRecordId;
	}

	public void setStallRecordId(String stallRecordId) {
		this.stallRecordId = stallRecordId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "shopId")
	public SysShop getSysShop() {
		return this.sysShop;
	}

	public void setSysShop(SysShop sysShop) {
		this.sysShop = sysShop;
	}

	@Column(name = "priceToDay")
	public Integer getPriceToDay() {
		return this.priceToDay;
	}

	public void setPriceToDay(Integer priceToDay) {
		this.priceToDay = priceToDay;
	}

	@Column(name = "unitPrice", precision = 11)
	public Double getUnitPrice() {
		return this.unitPrice;
	}

	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}

	@Column(name = "createDate", length = 0)
	public Timestamp getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	@Column(name = "price", precision = 22, scale = 0)
	public Double getPrice() {
		return this.price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

}