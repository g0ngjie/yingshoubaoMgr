package com.cosin.web.entity;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 * SysOrder entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "sys_order", catalog = "revenue")
public class SysOrder implements java.io.Serializable {

	// Fields

	private String orderId;
	private SysShop sysShop;
	private SysUser sysUser;
	private String orderNum;
	private Double orderPrice;
	private Integer state;
	private Integer isComment;
	private Timestamp createDate;
	private Timestamp payDate;
	private String remark;
	private Integer payFor;
	private Double shopIncome;
	private Double userPrice;
	private Set<CommentShop> commentShops = new HashSet<CommentShop>(0);

	// Constructors

	/** default constructor */
	public SysOrder() {
	}

	/** full constructor */
	public SysOrder(SysShop sysShop, SysUser sysUser, String orderNum,
			Double orderPrice, Integer state, Integer isComment,
			Timestamp createDate, Timestamp payDate, String remark,
			Integer payFor, Double shopIncome, Double userPrice,
			Set<CommentShop> commentShops) {
		this.sysShop = sysShop;
		this.sysUser = sysUser;
		this.orderNum = orderNum;
		this.orderPrice = orderPrice;
		this.state = state;
		this.isComment = isComment;
		this.createDate = createDate;
		this.payDate = payDate;
		this.remark = remark;
		this.payFor = payFor;
		this.shopIncome = shopIncome;
		this.userPrice = userPrice;
		this.commentShops = commentShops;
	}

	// Property accessors
	@GenericGenerator(name = "generator", strategy = "uuid.hex")
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name = "orderId", unique = true, nullable = false, length = 32)
	public String getOrderId() {
		return this.orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "shopId")
	public SysShop getSysShop() {
		return this.sysShop;
	}

	public void setSysShop(SysShop sysShop) {
		this.sysShop = sysShop;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userId")
	public SysUser getSysUser() {
		return this.sysUser;
	}

	public void setSysUser(SysUser sysUser) {
		this.sysUser = sysUser;
	}

	@Column(name = "orderNum", length = 32)
	public String getOrderNum() {
		return this.orderNum;
	}

	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}

	@Column(name = "orderPrice", precision = 22, scale = 0)
	public Double getOrderPrice() {
		return this.orderPrice;
	}

	public void setOrderPrice(Double orderPrice) {
		this.orderPrice = orderPrice;
	}

	@Column(name = "state")
	public Integer getState() {
		return this.state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	@Column(name = "isComment")
	public Integer getIsComment() {
		return this.isComment;
	}

	public void setIsComment(Integer isComment) {
		this.isComment = isComment;
	}

	@Column(name = "createDate", length = 0)
	public Timestamp getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	@Column(name = "payDate", length = 0)
	public Timestamp getPayDate() {
		return this.payDate;
	}

	public void setPayDate(Timestamp payDate) {
		this.payDate = payDate;
	}

	@Column(name = "remark", length = 16777215)
	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Column(name = "payFor")
	public Integer getPayFor() {
		return this.payFor;
	}

	public void setPayFor(Integer payFor) {
		this.payFor = payFor;
	}

	@Column(name = "shopIncome", precision = 11)
	public Double getShopIncome() {
		return this.shopIncome;
	}

	public void setShopIncome(Double shopIncome) {
		this.shopIncome = shopIncome;
	}

	@Column(name = "userPrice", precision = 11)
	public Double getUserPrice() {
		return this.userPrice;
	}

	public void setUserPrice(Double userPrice) {
		this.userPrice = userPrice;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "sysOrder")
	public Set<CommentShop> getCommentShops() {
		return this.commentShops;
	}

	public void setCommentShops(Set<CommentShop> commentShops) {
		this.commentShops = commentShops;
	}

}