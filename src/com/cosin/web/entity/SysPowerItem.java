package com.cosin.web.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 * SysPowerItem entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "sys_power_item", catalog = "revenue")
public class SysPowerItem implements java.io.Serializable {

	// Fields

	private String powerCode;
	private SysMenu sysMenu;
	private String powerName;

	// Constructors

	/** default constructor */
	public SysPowerItem() {
	}

	/** full constructor */
	public SysPowerItem(SysMenu sysMenu, String powerName) {
		this.sysMenu = sysMenu;
		this.powerName = powerName;
	}

	// Property accessors
	@GenericGenerator(name = "generator", strategy = "uuid.hex")
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name = "powerCode", unique = true, nullable = false, length = 32)
	public String getPowerCode() {
		return this.powerCode;
	}

	public void setPowerCode(String powerCode) {
		this.powerCode = powerCode;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "menuId")
	public SysMenu getSysMenu() {
		return this.sysMenu;
	}

	public void setSysMenu(SysMenu sysMenu) {
		this.sysMenu = sysMenu;
	}

	@Column(name = "powerName", length = 32)
	public String getPowerName() {
		return this.powerName;
	}

	public void setPowerName(String powerName) {
		this.powerName = powerName;
	}

}