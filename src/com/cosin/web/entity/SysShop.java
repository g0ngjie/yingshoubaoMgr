package com.cosin.web.entity;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 * SysShop entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "sys_shop", catalog = "revenue")
public class SysShop implements java.io.Serializable {

	// Fields

	private String shopId;
	private ShopType shopType;
	private SysUser sysUser;
	private String shopName;
	private String linkman;
	private String callTel;
	private String shopAddress;
	private String shopCode;
	private String idCard;
	private String idCardImg;
	private String license;
	private Integer type;
	private Timestamp topTime;
	private String backImg;
	private Double lat;
	private Double lng;
	private Integer commentNum;
	private Integer commScore;
	private Timestamp createDate;
	private Integer isDel;
	private String reason;
	private Integer reasonType;
	private Double oneBili;
	private Double twoBili;
	private Double threeBili;
	private Double shopBili;
	private Double platBili;
	private Set<Fans> fanses = new HashSet<Fans>(0);
	private Set<ShopActivity> shopActivities = new HashSet<ShopActivity>(0);
	private Set<StallRecord> stallRecords = new HashSet<StallRecord>(0);
	private Set<CommentShop> commentShops = new HashSet<CommentShop>(0);
	private Set<ShopStatistic> shopStatistics = new HashSet<ShopStatistic>(0);
	private Set<CollectShop> collectShops = new HashSet<CollectShop>(0);
	private Set<SysOrder> sysOrders = new HashSet<SysOrder>(0);
	private Set<OrderDetail> orderDetails = new HashSet<OrderDetail>(0);

	// Constructors

	/** default constructor */
	public SysShop() {
	}

	/** full constructor */
	public SysShop(ShopType shopType, SysUser sysUser, String shopName,
			String linkman, String callTel, String shopAddress,
			String shopCode, String idCard, String idCardImg, String license,
			Integer type, Timestamp topTime, String backImg, Double lat,
			Double lng, Integer commentNum, Integer commScore,
			Timestamp createDate, Integer isDel, String reason,
			Integer reasonType, Double oneBili, Double twoBili,
			Double threeBili, Double shopBili, Double platBili,
			Set<Fans> fanses, Set<ShopActivity> shopActivities,
			Set<StallRecord> stallRecords, Set<CommentShop> commentShops,
			Set<ShopStatistic> shopStatistics, Set<CollectShop> collectShops,
			Set<SysOrder> sysOrders, Set<OrderDetail> orderDetails) {
		this.shopType = shopType;
		this.sysUser = sysUser;
		this.shopName = shopName;
		this.linkman = linkman;
		this.callTel = callTel;
		this.shopAddress = shopAddress;
		this.shopCode = shopCode;
		this.idCard = idCard;
		this.idCardImg = idCardImg;
		this.license = license;
		this.type = type;
		this.topTime = topTime;
		this.backImg = backImg;
		this.lat = lat;
		this.lng = lng;
		this.commentNum = commentNum;
		this.commScore = commScore;
		this.createDate = createDate;
		this.isDel = isDel;
		this.reason = reason;
		this.reasonType = reasonType;
		this.oneBili = oneBili;
		this.twoBili = twoBili;
		this.threeBili = threeBili;
		this.shopBili = shopBili;
		this.platBili = platBili;
		this.fanses = fanses;
		this.shopActivities = shopActivities;
		this.stallRecords = stallRecords;
		this.commentShops = commentShops;
		this.shopStatistics = shopStatistics;
		this.collectShops = collectShops;
		this.sysOrders = sysOrders;
		this.orderDetails = orderDetails;
	}

	// Property accessors
	@GenericGenerator(name = "generator", strategy = "uuid.hex")
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name = "shopId", unique = true, nullable = false, length = 32)
	public String getShopId() {
		return this.shopId;
	}

	public void setShopId(String shopId) {
		this.shopId = shopId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "shopTypeId")
	public ShopType getShopType() {
		return this.shopType;
	}

	public void setShopType(ShopType shopType) {
		this.shopType = shopType;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userId")
	public SysUser getSysUser() {
		return this.sysUser;
	}

	public void setSysUser(SysUser sysUser) {
		this.sysUser = sysUser;
	}

	@Column(name = "shopName", length = 32)
	public String getShopName() {
		return this.shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	@Column(name = "linkman", length = 32)
	public String getLinkman() {
		return this.linkman;
	}

	public void setLinkman(String linkman) {
		this.linkman = linkman;
	}

	@Column(name = "callTel", length = 32)
	public String getCallTel() {
		return this.callTel;
	}

	public void setCallTel(String callTel) {
		this.callTel = callTel;
	}

	@Column(name = "shopAddress", length = 32)
	public String getShopAddress() {
		return this.shopAddress;
	}

	public void setShopAddress(String shopAddress) {
		this.shopAddress = shopAddress;
	}

	@Column(name = "shopCode", length = 32)
	public String getShopCode() {
		return this.shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	@Column(name = "idCard", length = 64)
	public String getIdCard() {
		return this.idCard;
	}

	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}

	@Column(name = "idCardImg", length = 256)
	public String getIdCardImg() {
		return this.idCardImg;
	}

	public void setIdCardImg(String idCardImg) {
		this.idCardImg = idCardImg;
	}

	@Column(name = "license", length = 256)
	public String getLicense() {
		return this.license;
	}

	public void setLicense(String license) {
		this.license = license;
	}

	@Column(name = "type")
	public Integer getType() {
		return this.type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	@Column(name = "topTime", length = 0)
	public Timestamp getTopTime() {
		return this.topTime;
	}

	public void setTopTime(Timestamp topTime) {
		this.topTime = topTime;
	}

	@Column(name = "backImg", length = 256)
	public String getBackImg() {
		return this.backImg;
	}

	public void setBackImg(String backImg) {
		this.backImg = backImg;
	}

	@Column(name = "lat", precision = 22, scale = 0)
	public Double getLat() {
		return this.lat;
	}

	public void setLat(Double lat) {
		this.lat = lat;
	}

	@Column(name = "lng", precision = 22, scale = 0)
	public Double getLng() {
		return this.lng;
	}

	public void setLng(Double lng) {
		this.lng = lng;
	}

	@Column(name = "commentNum")
	public Integer getCommentNum() {
		return this.commentNum;
	}

	public void setCommentNum(Integer commentNum) {
		this.commentNum = commentNum;
	}

	@Column(name = "commScore")
	public Integer getCommScore() {
		return this.commScore;
	}

	public void setCommScore(Integer commScore) {
		this.commScore = commScore;
	}

	@Column(name = "createDate", length = 0)
	public Timestamp getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	@Column(name = "isDel")
	public Integer getIsDel() {
		return this.isDel;
	}

	public void setIsDel(Integer isDel) {
		this.isDel = isDel;
	}

	@Column(name = "reason")
	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	@Column(name = "reasonType")
	public Integer getReasonType() {
		return this.reasonType;
	}

	public void setReasonType(Integer reasonType) {
		this.reasonType = reasonType;
	}

	@Column(name = "oneBili", precision = 22, scale = 0)
	public Double getOneBili() {
		return this.oneBili;
	}

	public void setOneBili(Double oneBili) {
		this.oneBili = oneBili;
	}

	@Column(name = "twoBili", precision = 22, scale = 0)
	public Double getTwoBili() {
		return this.twoBili;
	}

	public void setTwoBili(Double twoBili) {
		this.twoBili = twoBili;
	}

	@Column(name = "threeBili", precision = 22, scale = 0)
	public Double getThreeBili() {
		return this.threeBili;
	}

	public void setThreeBili(Double threeBili) {
		this.threeBili = threeBili;
	}

	@Column(name = "shopBili", precision = 22, scale = 0)
	public Double getShopBili() {
		return this.shopBili;
	}

	public void setShopBili(Double shopBili) {
		this.shopBili = shopBili;
	}

	@Column(name = "platBili", precision = 22, scale = 0)
	public Double getPlatBili() {
		return this.platBili;
	}

	public void setPlatBili(Double platBili) {
		this.platBili = platBili;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "sysShop")
	public Set<Fans> getFanses() {
		return this.fanses;
	}

	public void setFanses(Set<Fans> fanses) {
		this.fanses = fanses;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "sysShop")
	public Set<ShopActivity> getShopActivities() {
		return this.shopActivities;
	}

	public void setShopActivities(Set<ShopActivity> shopActivities) {
		this.shopActivities = shopActivities;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "sysShop")
	public Set<StallRecord> getStallRecords() {
		return this.stallRecords;
	}

	public void setStallRecords(Set<StallRecord> stallRecords) {
		this.stallRecords = stallRecords;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "sysShop")
	public Set<CommentShop> getCommentShops() {
		return this.commentShops;
	}

	public void setCommentShops(Set<CommentShop> commentShops) {
		this.commentShops = commentShops;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "sysShop")
	public Set<ShopStatistic> getShopStatistics() {
		return this.shopStatistics;
	}

	public void setShopStatistics(Set<ShopStatistic> shopStatistics) {
		this.shopStatistics = shopStatistics;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "sysShop")
	public Set<CollectShop> getCollectShops() {
		return this.collectShops;
	}

	public void setCollectShops(Set<CollectShop> collectShops) {
		this.collectShops = collectShops;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "sysShop")
	public Set<SysOrder> getSysOrders() {
		return this.sysOrders;
	}

	public void setSysOrders(Set<SysOrder> sysOrders) {
		this.sysOrders = sysOrders;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "sysShop")
	public Set<OrderDetail> getOrderDetails() {
		return this.orderDetails;
	}

	public void setOrderDetails(Set<OrderDetail> orderDetails) {
		this.orderDetails = orderDetails;
	}

}