package com.cosin.web.entity;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 * SysUser entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "sys_user", catalog = "revenue")
public class SysUser implements java.io.Serializable {

	// Fields

	private String userId;
	private SysArea sysAreaByAreaId;
	private SysArea sysAreaByProvinceId;
	private SysArea sysAreaByCityId;
	private String userName;
	private String appkey;
	private Integer type;
	private String loginName;
	private String pwd;
	private String mobile;
	private String email;
	private Integer sex;
	private String icon;
	private Double price;
	private String address;
	private Integer enable;
	private Integer isDel;
	private Timestamp createDate;
	private Set<Message> messages = new HashSet<Message>(0);
	private Set<Fans> fansesForUserId = new HashSet<Fans>(0);
	private Set<CollectShop> collectShops = new HashSet<CollectShop>(0);
	private Set<SysShop> sysShops = new HashSet<SysShop>(0);
	private Set<FeedBack> feedBacks = new HashSet<FeedBack>(0);
	private Set<UserStatistic> userStatistics = new HashSet<UserStatistic>(0);
	private Set<WithdrawCashRecord> withdrawCashRecords = new HashSet<WithdrawCashRecord>(
			0);
	private Set<SysOrder> sysOrders = new HashSet<SysOrder>(0);
	private Set<BindBank> bindBanks = new HashSet<BindBank>(0);
	private Set<Fans> fansesForParentUserId = new HashSet<Fans>(0);
	private Set<ActivDetail> activDetails = new HashSet<ActivDetail>(0);
	private Set<SysLog> sysLogs = new HashSet<SysLog>(0);
	private Set<CommentShop> commentShops = new HashSet<CommentShop>(0);
	private Set<SysUserRole> sysUserRoles = new HashSet<SysUserRole>(0);
	private Set<FenrunDetail> fenrunDetailsForUserId = new HashSet<FenrunDetail>(
			0);
	private Set<FenrunDetail> fenrunDetailsForTopUserId = new HashSet<FenrunDetail>(
			0);
	private Set<ShopIncome> shopIncomes = new HashSet<ShopIncome>(0);

	// Constructors

	/** default constructor */
	public SysUser() {
	}

	/** full constructor */
	public SysUser(SysArea sysAreaByAreaId, SysArea sysAreaByProvinceId,
			SysArea sysAreaByCityId, String userName, String appkey,
			Integer type, String loginName, String pwd, String mobile,
			String email, Integer sex, String icon, Double price,
			String address, Integer enable, Integer isDel,
			Timestamp createDate, Set<Message> messages,
			Set<Fans> fansesForUserId, Set<CollectShop> collectShops,
			Set<SysShop> sysShops, Set<FeedBack> feedBacks,
			Set<UserStatistic> userStatistics,
			Set<WithdrawCashRecord> withdrawCashRecords,
			Set<SysOrder> sysOrders, Set<BindBank> bindBanks,
			Set<Fans> fansesForParentUserId, Set<ActivDetail> activDetails,
			Set<SysLog> sysLogs, Set<CommentShop> commentShops,
			Set<SysUserRole> sysUserRoles,
			Set<FenrunDetail> fenrunDetailsForUserId,
			Set<FenrunDetail> fenrunDetailsForTopUserId,
			Set<ShopIncome> shopIncomes) {
		this.sysAreaByAreaId = sysAreaByAreaId;
		this.sysAreaByProvinceId = sysAreaByProvinceId;
		this.sysAreaByCityId = sysAreaByCityId;
		this.userName = userName;
		this.appkey = appkey;
		this.type = type;
		this.loginName = loginName;
		this.pwd = pwd;
		this.mobile = mobile;
		this.email = email;
		this.sex = sex;
		this.icon = icon;
		this.price = price;
		this.address = address;
		this.enable = enable;
		this.isDel = isDel;
		this.createDate = createDate;
		this.messages = messages;
		this.fansesForUserId = fansesForUserId;
		this.collectShops = collectShops;
		this.sysShops = sysShops;
		this.feedBacks = feedBacks;
		this.userStatistics = userStatistics;
		this.withdrawCashRecords = withdrawCashRecords;
		this.sysOrders = sysOrders;
		this.bindBanks = bindBanks;
		this.fansesForParentUserId = fansesForParentUserId;
		this.activDetails = activDetails;
		this.sysLogs = sysLogs;
		this.commentShops = commentShops;
		this.sysUserRoles = sysUserRoles;
		this.fenrunDetailsForUserId = fenrunDetailsForUserId;
		this.fenrunDetailsForTopUserId = fenrunDetailsForTopUserId;
		this.shopIncomes = shopIncomes;
	}

	// Property accessors
	@GenericGenerator(name = "generator", strategy = "uuid.hex")
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name = "userId", unique = true, nullable = false, length = 32)
	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "areaId")
	public SysArea getSysAreaByAreaId() {
		return this.sysAreaByAreaId;
	}

	public void setSysAreaByAreaId(SysArea sysAreaByAreaId) {
		this.sysAreaByAreaId = sysAreaByAreaId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "provinceId")
	public SysArea getSysAreaByProvinceId() {
		return this.sysAreaByProvinceId;
	}

	public void setSysAreaByProvinceId(SysArea sysAreaByProvinceId) {
		this.sysAreaByProvinceId = sysAreaByProvinceId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cityId")
	public SysArea getSysAreaByCityId() {
		return this.sysAreaByCityId;
	}

	public void setSysAreaByCityId(SysArea sysAreaByCityId) {
		this.sysAreaByCityId = sysAreaByCityId;
	}

	@Column(name = "userName", length = 32)
	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Column(name = "appkey", length = 64)
	public String getAppkey() {
		return this.appkey;
	}

	public void setAppkey(String appkey) {
		this.appkey = appkey;
	}

	@Column(name = "type")
	public Integer getType() {
		return this.type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	@Column(name = "loginName", length = 32)
	public String getLoginName() {
		return this.loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	@Column(name = "pwd", length = 32)
	public String getPwd() {
		return this.pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	@Column(name = "mobile", length = 16)
	public String getMobile() {
		return this.mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	@Column(name = "email", length = 32)
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "sex")
	public Integer getSex() {
		return this.sex;
	}

	public void setSex(Integer sex) {
		this.sex = sex;
	}

	@Column(name = "icon", length = 1024)
	public String getIcon() {
		return this.icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	@Column(name = "price", precision = 11)
	public Double getPrice() {
		return this.price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	@Column(name = "address", length = 521)
	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Column(name = "enable")
	public Integer getEnable() {
		return this.enable;
	}

	public void setEnable(Integer enable) {
		this.enable = enable;
	}

	@Column(name = "isDel")
	public Integer getIsDel() {
		return this.isDel;
	}

	public void setIsDel(Integer isDel) {
		this.isDel = isDel;
	}

	@Column(name = "createDate", length = 0)
	public Timestamp getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "sysUser")
	public Set<Message> getMessages() {
		return this.messages;
	}

	public void setMessages(Set<Message> messages) {
		this.messages = messages;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "sysUserByUserId")
	public Set<Fans> getFansesForUserId() {
		return this.fansesForUserId;
	}

	public void setFansesForUserId(Set<Fans> fansesForUserId) {
		this.fansesForUserId = fansesForUserId;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "sysUser")
	public Set<CollectShop> getCollectShops() {
		return this.collectShops;
	}

	public void setCollectShops(Set<CollectShop> collectShops) {
		this.collectShops = collectShops;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "sysUser")
	public Set<SysShop> getSysShops() {
		return this.sysShops;
	}

	public void setSysShops(Set<SysShop> sysShops) {
		this.sysShops = sysShops;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "sysUser")
	public Set<FeedBack> getFeedBacks() {
		return this.feedBacks;
	}

	public void setFeedBacks(Set<FeedBack> feedBacks) {
		this.feedBacks = feedBacks;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "sysUser")
	public Set<UserStatistic> getUserStatistics() {
		return this.userStatistics;
	}

	public void setUserStatistics(Set<UserStatistic> userStatistics) {
		this.userStatistics = userStatistics;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "sysUser")
	public Set<WithdrawCashRecord> getWithdrawCashRecords() {
		return this.withdrawCashRecords;
	}

	public void setWithdrawCashRecords(
			Set<WithdrawCashRecord> withdrawCashRecords) {
		this.withdrawCashRecords = withdrawCashRecords;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "sysUser")
	public Set<SysOrder> getSysOrders() {
		return this.sysOrders;
	}

	public void setSysOrders(Set<SysOrder> sysOrders) {
		this.sysOrders = sysOrders;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "sysUser")
	public Set<BindBank> getBindBanks() {
		return this.bindBanks;
	}

	public void setBindBanks(Set<BindBank> bindBanks) {
		this.bindBanks = bindBanks;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "sysUserByParentUserId")
	public Set<Fans> getFansesForParentUserId() {
		return this.fansesForParentUserId;
	}

	public void setFansesForParentUserId(Set<Fans> fansesForParentUserId) {
		this.fansesForParentUserId = fansesForParentUserId;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "sysUser")
	public Set<ActivDetail> getActivDetails() {
		return this.activDetails;
	}

	public void setActivDetails(Set<ActivDetail> activDetails) {
		this.activDetails = activDetails;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "sysUser")
	public Set<SysLog> getSysLogs() {
		return this.sysLogs;
	}

	public void setSysLogs(Set<SysLog> sysLogs) {
		this.sysLogs = sysLogs;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "sysUser")
	public Set<CommentShop> getCommentShops() {
		return this.commentShops;
	}

	public void setCommentShops(Set<CommentShop> commentShops) {
		this.commentShops = commentShops;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "sysUser")
	public Set<SysUserRole> getSysUserRoles() {
		return this.sysUserRoles;
	}

	public void setSysUserRoles(Set<SysUserRole> sysUserRoles) {
		this.sysUserRoles = sysUserRoles;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "sysUserByUserId")
	public Set<FenrunDetail> getFenrunDetailsForUserId() {
		return this.fenrunDetailsForUserId;
	}

	public void setFenrunDetailsForUserId(
			Set<FenrunDetail> fenrunDetailsForUserId) {
		this.fenrunDetailsForUserId = fenrunDetailsForUserId;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "sysUserByTopUserId")
	public Set<FenrunDetail> getFenrunDetailsForTopUserId() {
		return this.fenrunDetailsForTopUserId;
	}

	public void setFenrunDetailsForTopUserId(
			Set<FenrunDetail> fenrunDetailsForTopUserId) {
		this.fenrunDetailsForTopUserId = fenrunDetailsForTopUserId;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "sysUser")
	public Set<ShopIncome> getShopIncomes() {
		return this.shopIncomes;
	}

	public void setShopIncomes(Set<ShopIncome> shopIncomes) {
		this.shopIncomes = shopIncomes;
	}

}