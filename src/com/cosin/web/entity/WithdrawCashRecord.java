package com.cosin.web.entity;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 * WithdrawCashRecord entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "withdraw_cash_record", catalog = "revenue")
public class WithdrawCashRecord implements java.io.Serializable {

	// Fields

	private String cashId;
	private SysUser sysUser;
	private Integer isThrough;
	private Integer cashType;
	private String introduce;
	private String reason;
	private Timestamp createDate;
	private Double price;
	private String cashNo;
	private String bankDesc;

	// Constructors

	/** default constructor */
	public WithdrawCashRecord() {
	}

	/** full constructor */
	public WithdrawCashRecord(SysUser sysUser, Integer isThrough,
			Integer cashType, String introduce, String reason,
			Timestamp createDate, Double price, String cashNo, String bankDesc) {
		this.sysUser = sysUser;
		this.isThrough = isThrough;
		this.cashType = cashType;
		this.introduce = introduce;
		this.reason = reason;
		this.createDate = createDate;
		this.price = price;
		this.cashNo = cashNo;
		this.bankDesc = bankDesc;
	}

	// Property accessors
	@GenericGenerator(name = "generator", strategy = "uuid.hex")
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name = "cashId", unique = true, nullable = false, length = 32)
	public String getCashId() {
		return this.cashId;
	}

	public void setCashId(String cashId) {
		this.cashId = cashId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userId")
	public SysUser getSysUser() {
		return this.sysUser;
	}

	public void setSysUser(SysUser sysUser) {
		this.sysUser = sysUser;
	}

	@Column(name = "isThrough")
	public Integer getIsThrough() {
		return this.isThrough;
	}

	public void setIsThrough(Integer isThrough) {
		this.isThrough = isThrough;
	}

	@Column(name = "cashType")
	public Integer getCashType() {
		return this.cashType;
	}

	public void setCashType(Integer cashType) {
		this.cashType = cashType;
	}

	@Column(name = "introduce", length = 512)
	public String getIntroduce() {
		return this.introduce;
	}

	public void setIntroduce(String introduce) {
		this.introduce = introduce;
	}

	@Column(name = "reason", length = 1024)
	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	@Column(name = "createDate", length = 0)
	public Timestamp getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	@Column(name = "price", precision = 11)
	public Double getPrice() {
		return this.price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	@Column(name = "cashNo", length = 32)
	public String getCashNo() {
		return this.cashNo;
	}

	public void setCashNo(String cashNo) {
		this.cashNo = cashNo;
	}

	@Column(name = "bankDesc", length = 128)
	public String getBankDesc() {
		return this.bankDesc;
	}

	public void setBankDesc(String bankDesc) {
		this.bankDesc = bankDesc;
	}

}