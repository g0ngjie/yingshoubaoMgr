package com.cosin.web.service.fans;

import java.util.List;
import java.util.Map;

import com.cosin.web.entity.Fans;
import com.cosin.web.entity.SysOrder;
import com.cosin.web.entity.WithdrawCashRecord;

public interface IFansManager {
	
	
	/**
	 * 获取粉丝数据
	 * @param start
	 * @param limit
	 * @return
	 */
	List<Fans> getUserList(String SsShopName,Integer start, Integer limit);
	
	
	int getinfofenye(String SsShopName);


	/**
	 * 崔青山
	 * 2016年12月1日
	 * @param shopId
	 * @param integer
	 * @param integer2
	 * @return
	 */
	List<Fans> getFansList(String shopId,String SsFensiName1, Integer integer, Integer integer2);


	/**
	 * 崔青山
	 * 2016年12月1日
	 * @param shopId
	 * @return
	 */
	int getFansfenye(String shopId,String SsFensiName1);


	/**
	 * 崔青山
	 * 2016年12月1日
	 * @param fansId
	 * @param integer
	 * @param integer2
	 * @return
	 */
	List<Fans> getFansList1(String fansId,String SsFensiName2, Integer integer, Integer integer2);


	/**
	 * 崔青山
	 * 2016年12月1日
	 * @param fansId
	 * @return
	 */
	int getFansfenye1(String fansId,String SsFensiName2);


	/**
	 * 崔青山
	 * 2016年12月1日
	 * @param fansId
	 * @param integer
	 * @param integer2
	 * @return
	 */
	List<Fans> getFansList2(String fansId,String SsFensiName1, Integer integer, Integer integer2);


	/**
	 * 崔青山
	 * 2016年12月1日
	 * @param fansId
	 * @return
	 */
	int getFansfenye2(String fansId,String SsFensiName3);


	/**
	 * 崔青山
	 * 2016年12月2日
	 * @param userId
	 * @return
	 */
	List<SysOrder> getOrder(String userId);


	/**
	 * 崔青山
	 * 2016年12月2日
	 * @param shopId
	 * @param ssFensiName1
	 * @return
	 */
	List<Fans> getFans(String shopId, String ssFensiName1);


	/**
	 * 崔青山
	 * 2016年12月2日
	 * @param userId
	 * @return
	 */
	List<WithdrawCashRecord> getWithdrawCashRecord(String userId);


	/**
	 * 崔青山
	 * 2016年12月5日
	 * @param fansId
	 * @return
	 */
	List<Fans> getFans2(String fansId);


	/**
	 * 2016年12月27日下午3:04:50
	 *  阳朔
	 *  注释:查询 关联 shopId 的一级粉丝 parentUserId 为Null
	 */
	List getFansByShopId(String shopId);




	
	
	
	
	
}

