package com.cosin.web.service.order;

import java.util.List;

import com.cosin.web.entity.CommentShop;
import com.cosin.web.entity.SysOrder;
import com.cosin.web.entity.SysUser;

public interface IOrderManager {
	
	
	/**
	 * 获取订单数据
	 * @param start
	 * @param limit
	 * @return
	 */
	List<SysOrder> getUserList(String proName ,String cityName, String SsUserName,String SsShopName,String SsOrderNum,String startDate,String endDate, Integer start, Integer limit);
	
	
	int getinfofenye(String proName ,String cityName, String SsUserName,String SsShopName,String SsOrderNum,String startDate,String endDate);
	


	/**
	 * 崔青山
	 * @return
	 */
	List<CommentShop> getShopCommentList(String shopId, String sjName, String name,
			String date, Integer integer, Integer integer2);


	/**
	 * 崔青山
	 * 2016年11月23日
	 * @return
	 */
	int getShopCommentfenye(String shopId, String sjName, String name, String date);


	/**
	 * 崔青山
	 * 2016年11月23日
	 * @param key
	 */
	void delShopComment(String key);
	
	
	
	
	
	
	
}

