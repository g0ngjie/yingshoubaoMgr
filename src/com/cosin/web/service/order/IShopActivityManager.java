package com.cosin.web.service.order;

import java.util.List;

import com.cosin.web.entity.ShopActivity;
import com.cosin.web.entity.SysShop;

public interface IShopActivityManager {

	/**
	 * 获取微卡数据
	 * 崔青山
	 * 2016年7月12日
	 * @return
	 */
	List<ShopActivity> getPriceList(String ssuser,String ssksDate,String ssjsDate,Integer integer, Integer integer2);

	/**
	 * 获取分页
	 * 崔青山
	 */
	int getPriceListFenye(String ssuser,String ssksDate,String ssjsDate);

	/**
	 * 删除
	 * 崔青山
	 * 2016年7月25日
	 * @param key
	 */
	void delPriceQiang(String key);

	/**
	 * 崔青山
	 * 2016年9月1日
	 * @param key
	 */
	void readPriceQiang(String key);

	/**
	 * 崔青山
	 * @param shopActivityId
	 * @param mode
	 */
	void savepushMessage(String price, String prices, String content,
			String shopActivityId, String mode,String shopId);

	/**
	 * 崔青山
	 * 2016年11月28日
	 * @return
	 */
	List<SysShop> getSysShop();
}
