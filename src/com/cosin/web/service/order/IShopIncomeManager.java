package com.cosin.web.service.order;

import java.util.List;
import java.util.Map;

import com.cosin.web.entity.PlatformStatistic;
import com.cosin.web.entity.ShopStatistic;
import com.cosin.web.entity.ShopType;
import com.cosin.web.entity.SysOrder;

public interface IShopIncomeManager {

	/**
	 * 获取微卡数据
	 * 崔青山
	 * 2016年7月12日
	 * @return
	 */
	List<ShopStatistic> getPriceList(String ssuser,String beginDate,String endDate,String proName ,String cityName,String shopTypeId, Integer integer, Integer integer2);

	/**
	 * 获取分页
	 * 崔青山
	 */
	int getPriceListFenye(String ssuser,String beginDate, String endDate, String proName,String cityName,String shopTypeId);

	/**
	 * 删除
	 * 崔青山
	 * 2016年7月25日
	 * @param key
	 */
	void delPriceQiang(String key);

	/**
	 * 2017年1月4日下午3:33:33
	 *  阳朔
	 *  注释:
	 */
	List<PlatformStatistic> getPlatformStatisticList(String beginDate, String endDate,
			Integer integer, Integer integer2);

	/**
	 * 2017年1月4日下午3:34:51
	 *  阳朔
	 *  注释:
	 */
	int getPlatformStatisticCount(String beginDate, String endDate);

	/**
	 * 2017年1月8日下午12:43:59
	 *  阳朔
	 *  注释:查询 商户类型
	 */
	List<ShopType> getShopTypeList();

	/**
	 * 2017年1月12日上午9:51:34
	 *  阳朔
	 *  注释:
	 */
	List<SysOrder> getShopOrderList(String orderNum, String startDate,
			String endDate, String shopId, Integer integer, Integer integer2);

	/**
	 * 2017年1月12日上午9:51:38
	 *  阳朔
	 *  注释:
	 */
	int getShopOrderCount(String orderNum, String startDate, String endDate,
			String shopId);

	/**
	 * 2017年1月12日上午11:25:43
	 *  阳朔
	 *  注释:按条件查询订单量
	 */
	List getYearAndMonthSelectOrderNum(String shopId, String forYear, String forMonth);

	/**
	 * 2017年1月12日下午4:28:05
	 *  阳朔
	 *  注释:
	 */
	Map getPlatformStatisticAll(String beginDate, String endDate);

	/**
	 * 2017年1月12日下午5:06:54
	 *  阳朔
	 *  注释:
	 */
	List getselectTuForPlay(String forYear, String forMonth);

	
}
