package com.cosin.web.service.order;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cosin.web.dao.order.IOrderDao;
import com.cosin.web.entity.CommentShop;
import com.cosin.web.entity.SysOrder;

@Service  
@Transactional 
public class OrderManager implements IOrderManager{
	@Autowired
	private IOrderDao orderDao;

	@Override
	public List<SysOrder> getUserList(String proName ,String cityName, String SsUserName,String SsShopName,String SsOrderNum,String startDate,String endDate,Integer start, Integer limit) {
		// TODO Auto-generated method stub
		List<SysOrder> listStr = orderDao.getPushMessage(proName, cityName ,SsUserName,SsShopName,SsOrderNum,startDate,endDate,start, limit);
		List listRes = new ArrayList();
		for (int i = 0; i < listStr.size(); i++) {
			SysOrder order = listStr.get(i);
			Map map = new HashMap();
			map.put("userName", order.getSysUser().getUserName());
			map.put("shopName", order.getSysShop().getShopName());
			map.put("orderId", order.getOrderId());
			map.put("orderNum", order.getOrderNum());
			map.put("orderPrice", order.getOrderPrice());
			map.put("state", order.getState());
			map.put("isCommen", order.getIsComment());
			map.put("remark", order.getRemark());
			map.put("shopAddress", order.getSysShop().getShopAddress());
			map.put("payFor", order.getPayFor());
			map.put("createDate", order.getCreateDate());
			listRes.add(map);
		}
		return listRes;
	}
	
	
	@Override
	public int getinfofenye(String proName ,String cityName, String SsUserName,String SsShopName,String SsOrderNum,String startDate,String endDate) {
		// TODO Auto-generated method stub
		return orderDao.getinfofenye(proName , cityName, SsUserName,SsShopName,SsOrderNum,startDate,endDate);
	}


	/* 
	 * 崔青山
	 * 2016年11月22日
	 * 下午7:05:25
	 */
	@Override
	public List<CommentShop> getShopCommentList(String shopId, String sjName, String name,
			String date, Integer start, Integer limit) {
		// TODO Auto-generated method stub
		List<CommentShop> listStr = orderDao.getCommentShop(shopId,sjName,name,date,start, limit);
		List listRes = new ArrayList();
		for (int i = 0; i < listStr.size(); i++) {
			CommentShop order = listStr.get(i);
			Map map = new HashMap();
			map.put("userName", order.getSysUser().getUserName());
			map.put("orderNum", order.getSysOrder().getOrderNum());
			map.put("shopName", order.getSysShop().getSysUser().getUserName());
			map.put("commentId", order.getCommentId());
			map.put("content", order.getContent());
			map.put("star", order.getStar());
			map.put("starTwo", order.getStarTwo());
			map.put("starThree", order.getStarThree());
			map.put("createDate", order.getCreateDate());
			listRes.add(map);
		}
		return listRes;
	}


	/* 
	 * 崔青山
	 * 2016年11月23日
	 * 上午9:26:30
	 */
	@Override
	public int getShopCommentfenye(String shopId, String sjName, String name, String date) {
		// TODO Auto-generated method stub
		return orderDao.getShopCommentfenye(shopId,sjName,name,date);
	}


	/* 
	 * 崔青山
	 * 2016年11月23日
	 * 上午10:12:27
	 */
	@Override
	public void delShopComment(String key) {
		// TODO Auto-generated method stub
		orderDao.delShopComment(key);
	}

	
}
