package com.cosin.web.service.order;


import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cosin.web.dao.order.IShopActivityDao;
import com.cosin.web.entity.ShopActivity;
import com.cosin.web.entity.SysShop;


@Service  
@Transactional 
public class ShopActivityManager implements IShopActivityManager{
	@Autowired
	private IShopActivityDao shopActivityDao;


	@Override
	public List<ShopActivity> getPriceList(String ssuser,String ssksDate,String ssjsDate,Integer integer, Integer integer2) {
		// TODO Auto-generated method stub
		
		List<ShopActivity> listStr = shopActivityDao.getPrice(ssuser,ssksDate,ssjsDate,integer, integer2);
		List listRes = new ArrayList();
		for (int i = 0; i < listStr.size(); i++) {
			ShopActivity back = listStr.get(i);
			Map map = new HashMap();
			map.put("userName", back.getSysShop().getSysUser().getUserName());
			map.put("date", back.getCreateDate());
			map.put("content",back.getContent());
			map.put("startDate",back.getBeginTime());
			map.put("endDate",back.getEndTime());
			map.put("shopActivityId",back.getShopActivityId());
			listRes.add(map);
		}
		return listRes;
	}

	/* 
	 * 崔青山
	 * 2016年7月28日
	 * 下午5:31:38
	 */
	@Override
	public int getPriceListFenye(String ssuser,String ssksDate,String ssjsDate) {
		// TODO Auto-generated method stub
		return shopActivityDao.getPriceListFenye(ssuser,ssksDate,ssjsDate);
	}
	
	/* 
	 * 删除
	 * 崔青山
	 * 2016年7月22日
	 * 上午9:18:18
	 */
	@Override
	public void delPriceQiang(String key) {
		// TODO Auto-generated method stub
		//VaccaePaiDao.delSmallcardhour(key);
		shopActivityDao.delPriceQiang(key);
	}

	/* 
	 * 崔青山
	 * 2016年9月1日
	 * 上午11:50:06
	 */
	@Override
	public void readPriceQiang(String key) {
		// TODO Auto-generated method stub
		shopActivityDao.readPriceQiang(key);
	}

	/* 
	 * 崔青山
	 * 2016年11月28日
	 * 下午2:49:27
	 */
	@Override
	public void savepushMessage(String price, String prices, String content,
			String shopActivityId, String mode,String shopId) {
		// TODO Auto-generated method stub
		Timestamp ts = Timestamp.valueOf(price);
		Timestamp tt = Timestamp.valueOf(prices);
		SysShop shop = shopActivityDao.getShopId(shopId);
		if(mode.equals("edit")){
			ShopActivity shopActivity = shopActivityDao.getShopActivityId(shopActivityId);
			shopActivity.setBeginTime(ts);
			shopActivity.setEndTime(tt);
			shopActivity.setContent(content);
			shopActivity.setCreateDate(new Timestamp(new Date().getTime()));
			shopActivity.setSysShop(shop);
			shopActivityDao.saveShopActivity(shopActivity);
		}
		else{
			ShopActivity shopActivity = new ShopActivity();
			shopActivity.setBeginTime(ts);
			shopActivity.setEndTime(tt);
			shopActivity.setContent(content);
			shopActivity.setCreateDate(new Timestamp(new Date().getTime()));
			shopActivity.setSysShop(shop);
			shopActivityDao.saveShopActivity(shopActivity);
		}
	}

	/* 
	 * 崔青山
	 * 2016年11月28日
	 * 下午3:16:25
	 */
	@Override
	public List<SysShop> getSysShop() {
		// TODO Auto-generated method stub
		return shopActivityDao.getSysShop();
	}
	
}
