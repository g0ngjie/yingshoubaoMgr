package com.cosin.web.service.order;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cosin.web.dao.order.IUserIncomeDao;
import com.cosin.web.entity.Fans;
import com.cosin.web.entity.FenrunDetail;
import com.cosin.web.entity.SysShop;
import com.cosin.web.entity.SysUser;
import com.cosin.web.entity.UserStatistic;
import com.cosin.web.entity.WithdrawCashRecord;

@Service  
@Transactional 
public class UserIncomeManager implements IUserIncomeManager{
	@Autowired
	private IUserIncomeDao userIncomeDao;

	/**
	 * 2017年1月4日下午4:08:59
	 *  阳朔
	 *  注释:
	 */
	@Override
	public List<UserStatistic> getUserStatisticList(String ssuser,String beginDate, String endDate, Integer integer, Integer integer2) {
		List<Object[]> listStr = userIncomeDao.getUserStatisticList(ssuser, beginDate, endDate, integer, integer2);
		List list = new ArrayList();
		for (int i = 0; i < listStr.size(); i++) {
			Object[] object = (Object[])listStr.get(i);
			Map<Object, Object> map = new HashMap<Object, Object>();
			String userId = (String)object[0];
			//根据userId 查询 提现记录总和
			Map sunMoneyMap = (Map) userIncomeDao.getWithdrawCashRecordByUserId(userId).get(0);
			if(sunMoneyMap.get("sum(a.price)") != null){
				double sumMoney = new Double(sunMoneyMap.get("sum(a.price)").toString());
				map.put("sumMoney", sumMoney);
			}else{
				map.put("sumMoney", 0);
			}
			map.put("userId", userId);
			map.put("userName", (String)object[1]);
			map.put("price", (Double)object[2]);
			map.put("income", (Double)object[3]);
			map.put("num", (Long)object[4]);
			//map.put("date", statistic.getCreateDate());
			list.add(map);
		}
		return list;
	}

	/**
	 * 2017年1月4日下午4:08:59
	 *  阳朔
	 *  注释:[{count(t.counts)=3}]
	 */
	@Override
	public int getUserStatisticCount(String ssuser, String beginDate, String endDate) {
		List list = userIncomeDao.getUserStatisticCount(ssuser,beginDate,endDate);
		Map map = (Map)list.get(0);
		Object obj = map.get("count(t.counts)");
		int num = Integer.parseInt(String.valueOf(obj));  
		return num;
	}

	/**
	 * 2017年1月9日上午9:54:52
	 *  阳朔
	 *  注释:
	 */
	@Override
	public Map passwordEdit(String userId, String beginPassword,
			String endPassword, String doublePassword) {
		SysUser sysUser = userIncomeDao.getSysUserById(userId);
		String pwd = sysUser.getPwd();
		
		Map map = new HashMap();
		if(pwd.equals(beginPassword)){
			if(pwd.equals(endPassword)||pwd.equals(doublePassword)){
				map.put("code", 103);
				map.put("msg", "修改失败，与旧密码一致!");
				return map;
			}else{
				if(endPassword.equals(doublePassword)&&!"".equals(endPassword)&&!"null".equals(endPassword)){
					sysUser.setPwd(endPassword);
					userIncomeDao.saveObj(sysUser);
					map.put("code", 100);
					map.put("msg", "保存成功");
					return map;
				}else{
					map.put("code", 102);
					map.put("msg", "两次密码输入不一致");
					return map;
				}
			}
		}else{
			map.put("code", 101);
			map.put("msg", "原密码输入错误!");
		}
		
		return map;
	}

	/**
	 * 唯一标识符
	 */
	private int num = 0;
	
	/**
	 * 2016年12月27日下午3:05:00
	 *  阳朔
	 *  注释:一级粉丝
	 */
	@Override
	public List selectFansThree(String userId) {
		List fanList = userIncomeDao.getFansByParentUserId(userId);
		
		List<Object> list = new ArrayList<Object>();
		for (int i = 0; i < fanList.size(); i++) {
			Map map = new HashMap();
			List<Object> listOne = new ArrayList();
			Map fans = (Map) fanList.get(i);
			if(fans.get("userId") != null){
				String fansUserId = fans.get("userId").toString();
				map = getFansByParent(userId,fansUserId,num++);
				listOne = getTwoFans(fansUserId,userId);
			}
			map.put("iconCls","user_suit_black");
			if(listOne.size() > 0){
				map.put("children", listOne);
				map.put("state", "closed");
			}
			list.add(map);
		}
		return list;
	}
	
	/**
	 * 2016年12月27日下午5:02:22
	 *  阳朔
	 *  注释:二级粉丝
	 */
	public List<Object> getTwoFans(String userId,String topUserId){
		List<Object> list = new ArrayList<Object>();
		List fanList = userIncomeDao.getFansByParentUserId(userId);
		for (int i = 0; i < fanList.size(); i++) {
			Map map = new HashMap();
			List<Object> listTwo = new ArrayList();
			Map fans = (Map) fanList.get(i);
			if(fans.get("userId") != null){
				String fansUserId = fans.get("userId").toString();
				map = getFansByParent(topUserId,fansUserId,num++);
				listTwo = getThreeFans(fansUserId,topUserId);
			}
			map.put("iconCls","user_green");
			if(listTwo.size() > 0){
				map.put("children", listTwo);
				map.put("state", "closed");
			}
			list.add(map);
		}
		
		return list;
	}
	
	/**
	 * 2016年12月27日下午5:02:30
	 *  阳朔
	 *  注释:三级粉丝
	 */
	public List<Object> getThreeFans(String userId,String topUserId){
		List<Object> list = new ArrayList<Object>();
		List fanList = userIncomeDao.getFansByParentUserId(userId);
		for (int i = 0; i < fanList.size(); i++) {
			Map map = new HashMap();
			Map fans = (Map) fanList.get(i);
			if(fans.get("userId") != null){
				String fansUserId = fans.get("userId").toString();
				map = getFansByParent(topUserId,fansUserId,num++);
			}
			//List<Object> listInfinite = getTwoFans(shopId,fans.getSysUserByUserId().getUserId());
			map.put("iconCls","user_mature");
//			if(listInfinite.size() > 0){
//				map.put("children", listInfinite);
//				map.put("state", "closed");
//			}
			list.add(map);
		}
		
		return list;
	}
	
	public Map<Object, Object> getFansByParent(String topUserId,String fansUserId,int num){
		//手机号 地址 类型 头像 贡献值
		Map<Object,Object> map = new HashMap<Object,Object>();
		map.put("id", num);
		SysUser sysUser = userIncomeDao.getPTSysUserById(fansUserId);
		//map.put("id", fans.getSysUserByUserId().getUserId());
		map.put("text", sysUser.getUserName());
		//map.put("state", "closed");
		map.put("tel", sysUser.getMobile());
		map.put("address", sysUser.getAddress());
		if(sysUser.getType() == 1){
			map.put("userType", "普通用户");
		}else{
			map.put("userType", "商户");
		}
		map.put("icon", sysUser.getIcon());
		double fenPrice = getFenRun(fansUserId, topUserId);
		map.put("fenPrice", fenPrice);
		
		return map;
	}
	
	//分润明细
	public double getFenRun(String userId,String topUserId){
		double num = 0.0;
		List detailList = userIncomeDao.getFenRunDetailByUserData(userId,topUserId);
		Map map = (Map) detailList.get(0);
		if(map.get("sum(a.money)") != null){
			num = new Double(map.get("sum(a.money)").toString());
		}
		return num;
	}
	
	
}
