package com.cosin.web.service.pushMessage;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cosin.utils.ShortMessageUtils;
import com.cosin.web.controller.log.ILogManager;
import com.cosin.web.dao.pushMessage.IFedbackDao;
import com.cosin.web.entity.FeedBack;


@Service  
@Transactional 
public class FedbackManager implements IFedbackManager{
	@Autowired
	private IFedbackDao fedbackDao;
	@Autowired
	private ILogManager logManager;

	@Override
	public List<FeedBack> getPriceList(String ssuser,String ssDate,Integer integer, Integer integer2) {
		// TODO Auto-generated method stub
		
		List<FeedBack> listStr = fedbackDao.getPrice(ssuser,ssDate,integer, integer2);
		List listRes = new ArrayList();
		for (int i = 0; i < listStr.size(); i++) {
			FeedBack back = listStr.get(i);
			Map map = new HashMap();
			map.put("userName", back.getSysUser().getUserName());
			map.put("date", back.getCreateDate());
			map.put("content",back.getContent());
			map.put("type",back.getType());
			map.put("feedbackId",back.getFeedBackId());
			listRes.add(map);
		}
		return listRes;
	}

	/* 
	 * 崔青山
	 * 2016年7月28日
	 * 下午5:31:38
	 */
	@Override
	public int getPriceListFenye(String ssuser,String ssDate) {
		// TODO Auto-generated method stub
		return fedbackDao.getPriceListFenye(ssuser,ssDate);
	}
	
	/* 
	 * 删除
	 * 崔青山
	 * 2016年7月22日
	 * 上午9:18:18
	 */
	@Override
	public void delPriceQiang(String loginUserId,String key) {
		FeedBack feedBack = fedbackDao.getFeedBackById(key);
		logManager.saveLog(loginUserId, "意见反馈", "删除操作", "删除");
		fedbackDao.delPriceQiang(key);
	}

	/* 
	 * 崔青山
	 * 2016年9月1日
	 * 上午11:50:06
	 */
	@Override
	public void readPriceQiang(String key) {
		// TODO Auto-generated method stub
		fedbackDao.readPriceQiang(key);
	}

	/**
	 * 2017年1月8日下午4:03:07
	 *  阳朔
	 *  注释:
	 */
	@Override
	public Map sendMsg(String loginUserId,String feedbackId, String sendContent) {
		ShortMessageUtils messageUtils = new ShortMessageUtils();
		FeedBack feedBack = fedbackDao.getFeedBackById(feedbackId);
		logManager.saveLog(loginUserId, "意见反馈", "回复操作", "用户："+feedBack.getSysUser().getUserName());
		String tel = feedBack.getSysUser().getMobile();
		messageUtils.sendCode(tel, sendContent);
		Map map = new HashMap();
		map.put("code", 100);
		return map;
	}
	
}
