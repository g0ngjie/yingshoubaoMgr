package com.cosin.web.service.pushMessage;

import java.util.List;
import java.util.Map;

import com.cosin.web.entity.FeedBack;

public interface IFedbackManager {

	/**
	 * 获取微卡数据
	 * 崔青山
	 * 2016年7月12日
	 * @return
	 */
	List<FeedBack> getPriceList(String ssuser,String ssDate,Integer integer, Integer integer2);

	/**
	 * 获取分页
	 * 崔青山
	 */
	int getPriceListFenye(String ssuser,String ssDate);

	/**
	 * 删除
	 * 崔青山
	 * 2016年7月25日
	 * @param key
	 */
	void delPriceQiang(String loginUserId,String key);

	/**
	 * 崔青山
	 * 2016年9月1日
	 * @param key
	 */
	void readPriceQiang(String key);

	/**
	 * 2017年1月8日下午4:03:01
	 *  阳朔
	 *  注释:
	 */
	Map sendMsg(String loginUserId,String feedbackId, String sendContent);
}
