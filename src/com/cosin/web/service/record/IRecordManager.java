package com.cosin.web.service.record;

import java.util.List;
import java.util.Map;

import com.cosin.web.entity.WithdrawCashRecord;

public interface IRecordManager {
	
	
	/**
	 * 获取提现数据
	 * @param start
	 * @param limit
	 * @return
	 */
	List<WithdrawCashRecord> getUserList(String ssuser,String SsJia, Integer start, Integer limit);
	
	/**
	 * 获取提现分页
	 * 崔青山
	 * 2016年7月29日
	 * @return
	 */
	int getinfofenye(String ssuser,String SsJia);

	/**
	 * 审核
	 * 崔青山
	 * 2016年8月21日
	 * @return
	 */
	List<WithdrawCashRecord> getShenheList(String ssuser, String ssStartTime,
			Integer integer, Integer integer2);

	/**
	 * 审核分页
	 * 崔青山
	 * @return
	 */
	int getinfofenyess(String ssuser, String ssStartTime);

	/**
	 * 通过
	 * 崔青山
	 * 2016年8月21日
	 * @param key
	 * @param name 
	 */
	Map tgShenhe(String loginUserId,String key);

	/**
	 * 拒绝后把钱返回商家
	 * 崔青山
	 * 2016年8月22日
	 * @param key
	 * @return 
	 */
	WithdrawCashRecord fanQian(String key);
	/**
	 * 拒绝
	 * 崔青山
	 * 2016年8月21日
	 * @param key
	 * @param name 
	 */
	Map jjShenhe(String loginUserId,String key,Double double1,String reason);


	/**
	 * 崔青山
	 * @param key
	 */
	void delPush(String loginUserId,String key);

	/**
	 * 崔青山
	 * 2016年11月9日
	 * @param ssuser
	 * @param ssJia
	 * @param integer
	 * @param integer2
	 * @return
	 */
	List<WithdrawCashRecord> getBankShenheList(String ssuser, String ssJia,
			Integer integer, Integer integer2);

	/**
	 * 崔青山
	 * 2016年11月9日
	 * @param key
	 */
	void bankShenhe(String key);

	/**
	 * 崔青山
	 * 2016年11月9日
	 * @param key
	 */
	void bankJujue(String key);

	/**
	 * 崔青山
	 * 2016年11月9日
	 * @param key
	 */
	void delBank(String key);
	
	
}

