package com.cosin.web.service.record;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cosin.web.controller.log.ILogManager;
import com.cosin.web.dao.record.IRecordDao;
import com.cosin.web.entity.BindBank;
import com.cosin.web.entity.SysUser;
import com.cosin.web.entity.WithdrawCashRecord;

@Service  
@Transactional 
public class RecordManager implements IRecordManager{
	@Autowired
	private IRecordDao recordDao;
	@Autowired
	private ILogManager logManager;

	@Override
	public List<WithdrawCashRecord> getUserList(String ssuser,String SsJia,Integer start, Integer limit) {
		List<WithdrawCashRecord> listStr = recordDao.getPushMessage(ssuser,SsJia,start, limit);
		List listRes = new ArrayList();
		for (int i = 0; i < listStr.size(); i++) {
			WithdrawCashRecord message = listStr.get(i);
			Map map = new HashMap();
			map.put("createDate", message.getCreateDate());
			map.put("userName", message.getSysUser().getUserName());
			map.put("introduce", message.getIntroduce());
			map.put("price", message.getPrice());
			map.put("isThrough", message.getIsThrough());
			map.put("cashId", message.getCashId());
			listRes.add(map);
		}
		return listRes;
	}
	 
	/**
	 * 崔青山
	 * 2016年7月29日
	 * 上午10:27:48
	 */
	@Override
	public int getinfofenye(String ssuser,String SsJia) {
		// TODO Auto-generated method stub
		return recordDao.getinfofenye(ssuser,SsJia);
	}
	/* 
	 * 崔青山
	 * 2016年8月21日
	 * 下午2:25:56
	 */
	@Override
	public List<WithdrawCashRecord> getShenheList(String ssuser, String ssJia,Integer integer, Integer integer2) {
		// TODO Auto-generated method stub
		List<WithdrawCashRecord> listStr = recordDao.getShenhe(ssuser,ssJia,integer, integer2);
		List listRes = new ArrayList();
		for (int i = 0; i < listStr.size(); i++) {
			WithdrawCashRecord message = listStr.get(i);
			Map map = new HashMap();
			map.put("createDate", message.getCreateDate());
			map.put("userName", message.getSysUser().getUserName());
			map.put("introduce", message.getIntroduce());
			map.put("price", message.getPrice());
			map.put("isThrough", message.getIsThrough());
			map.put("cashId", message.getCashId());
			map.put("openId", message.getSysUser().getAppkey());
			listRes.add(map);
		}
		return listRes;
	}
	
	/* 
	 * 崔青山
	 * 2016年8月21日
	 * 下午3:33:25
	 */
	@Override
	public int getinfofenyess(String ssuser,String ssJia) {
		// TODO Auto-generated method stub
		return recordDao.getinfofenyess(ssuser,ssJia);
	}
	/* 
	 * 崔青山
	 * 2016年8月21日
	 * 下午3:09:11
	 */
	@Override
	public Map tgShenhe(String loginUserId,String key) {
		// TODO Auto-generated method stub
		WithdrawCashRecord record = recordDao.getTransactionRecordId(key);
		recordDao.tgShenhe(key);
		logManager.saveLog(loginUserId, "提现审核", "通过操作", record.getSysUser().getUserName()+":通过审核");
		Map map = new HashMap();
		map.put("code", 100);
		map.put("msg", "成功");
		String userId = record.getSysUser().getUserId();
		map.put("userId", userId);
		map.put("price", record.getPrice());
		return map;
	}
	/**
	 * 崔青山
	 * 2016年8月22日
	 * 下午10:12:03
	 */
	@Override
	public WithdrawCashRecord fanQian(String key) {
		// TODO Auto-generated method stub
		return recordDao.getTransactionRecordId(key);
	}
	
	
	/**
	 *  崔青山
	 * 下午3:15:20
	 */
	@Override
	public Map jjShenhe(String loginUserId,String key,Double jine,String reason) {
		// TODO Auto-generated method stub
		WithdrawCashRecord record = recordDao.getTransactionRecordId(key);
		record.setReason(reason);
		recordDao.saveObj(record);
		SysUser wallet = recordDao.getSysUserId(record.getSysUser().getUserId());
		wallet.setPrice(wallet.getPrice()+jine);
		recordDao.saveSysUser(wallet);
		recordDao.jjShenhe(key);
		logManager.saveLog(loginUserId, "提现审核", "拒绝操作", record.getSysUser().getUserName()+":未通过审核");
		Map map = new HashMap();
		map.put("code", 100);
		map.put("userId", record.getSysUser().getUserId());
		map.put("price", record.getPrice());
		return map;
	}
	
	/**
	 * 崔青山
	 * 2016年9月2日
	 * 上午10:50:52
	 */
	@Override
	public void delPush(String loginUserId,String key) {
		// TODO Auto-generated method stub
		recordDao.delPush(loginUserId,key);
	}


	/* 
	 * 崔青山
	 * 2016年11月9日
	 * 上午9:43:51
	 */
	@Override
	public List<WithdrawCashRecord> getBankShenheList(String ssuser, String ssJia,
			Integer integer, Integer integer2) {
		// TODO Auto-generated method stub
		List<BindBank> listStr = recordDao.getBankShenhe(ssuser,ssJia,integer, integer2);
		List listRes = new ArrayList();
		for (int i = 0; i < listStr.size(); i++) {
			BindBank message = listStr.get(i);
			Map map = new HashMap();
			map.put("createDate", message.getCreateDate());
			map.put("name", message.getSysUser().getUserName());
			map.put("bankCard", message.getBankCard());
		
			listRes.add(map);
		}
		return listRes;
	}

	/* 
	 * 崔青山
	 * 2016年11月9日
	 * 上午9:56:18
	 */
	@Override
	public void bankShenhe(String key) {
		// TODO Auto-generated method stub
		BindBank bindBank = recordDao.getBindBankId(key);
	//	bindBank.setIsReviewed(1);
		recordDao.saveBindBank(bindBank);
	}

	/* 
	 * 崔青山
	 * 2016年11月9日
	 * 上午10:00:35
	 */
	@Override
	public void bankJujue(String key) {
		// TODO Auto-generated method stub
		BindBank bindBank = recordDao.getBindBankId(key);
		//bindBank.setIsReviewed(2);
		recordDao.saveBindBank(bindBank);
	}

	/* 
	 * 崔青山
	 * 2016年11月9日
	 * 上午10:07:23
	 */
	@Override
	public void delBank(String key) {
		// TODO Auto-generated method stub
		BindBank bindBank = recordDao.getBindBankId(key);
	//	bindBank.setIsDel(1);
		recordDao.saveBindBank(bindBank);
	}

	
}
