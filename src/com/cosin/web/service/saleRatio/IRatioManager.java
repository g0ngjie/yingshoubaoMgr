package com.cosin.web.service.saleRatio;

import java.util.List;

import com.cosin.web.entity.Ratio;


public interface IRatioManager {

	/**
	 * 2016年12月19日下午3:39:27
	 *  阳朔
	 *  注释:
	 */
	List<Ratio> getRatioListLimit(Integer start, Integer limit);

	/**
	 * 2016年12月19日下午3:39:32
	 *  阳朔
	 *  注释:
	 */
	int getRatioListCount();

	/**
	 * 2016年12月21日上午10:10:34
	 *  阳朔
	 *  注释:根据Id 查询 店铺 比例
	 */
	Ratio getRatioById(String ratioId);

	/**
	 * 2016年12月21日上午10:57:21
	 *  阳朔
	 *  注释:
	 * @param threebili 
	 * @param twobili 
	 * @param onebili 
	 */
	void shopRatioEdit(String loginUserId,String ratioId, String bili, String onebili, String twobili, String threebili,String result_playBili);

	/**
	 * 2016年12月21日上午11:46:36
	 *  阳朔
	 *  注释:
	 */
	List<Ratio> getThreeDataLimit(Integer integer, Integer integer2);

	/**
	 * 2016年12月21日下午12:24:04
	 *  阳朔
	 *  注释:
	 */
	int getThreeDataCount();

	/**
	 * 2017年1月9日下午4:10:37
	 *  阳朔
	 *  注释:
	 */
	Ratio getRatioFormaBili();
	
	

	
	
	
	
	
}

