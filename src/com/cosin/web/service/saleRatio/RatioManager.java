package com.cosin.web.service.saleRatio;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cosin.web.controller.log.ILogManager;
import com.cosin.web.dao.saleRatio.IRatioDao;
import com.cosin.web.entity.Ratio;
import com.cosin.web.entity.SysShop;

@Service  
@Transactional 
public class RatioManager implements IRatioManager{
	@Autowired
	private IRatioDao ratioDao;
	@Autowired
	private ILogManager logManager;

	/**
	 * 2016年12月19日下午3:39:37
	 *  阳朔
	 *  注释:
	 */
	@Override
	public List<Ratio> getRatioListLimit(Integer start, Integer limit) {
		return ratioDao.getRatioListLimit(start,limit);
	}

	/**
	 * 2016年12月19日下午3:39:37
	 *  阳朔
	 *  注释:
	 */
	@Override
	public int getRatioListCount() {
		return ratioDao.getRatioListCount();
	}

	/**
	 * 2016年12月21日上午10:10:54
	 *  阳朔
	 *  注释:
	 */
	@Override
	public Ratio getRatioById(String ratioId) {
		return ratioDao.getRatioById(ratioId);
	}

	/**
	 * 2016年12月21日上午11:01:57
	 *  阳朔
	 *  注释:
	 */
	@Override
	public void shopRatioEdit(String loginUserId,String ratioId, String bili, String onebili, String twobili, String threebili,String result_playBili) {
		Ratio ratio = ratioDao.getRatioById(ratioId);
		if(bili != null){
			Double onece = new Double(bili);
			ratio.setOnece(onece);
			ratio.setSecond(new Double(result_playBili));
			Ratio userRatio = ratioDao.getRatioById("1");
			userRatio.setOnece(new Double(0));
			userRatio.setSecond(new Double(0));
			userRatio.setThird(new Double(0));

			logManager.saveLog(loginUserId, "店铺佣金比例", "设置", "佣金比例修改");
			
			ratioDao.saveObj(userRatio);
			
			//所有 商家 三级比例 全部归零
			List<SysShop> shopList = ratioDao.getSysShopList();
			for (int i = 0; i < shopList.size(); i++) {
				SysShop shop = shopList.get(i);
				shop.setOneBili(0.0);
				shop.setTwoBili(0.0);
				shop.setThreeBili(0.0);
				ratioDao.saveObj(shop);
			}
			
		}else{
			Double onece = new Double(onebili);
			Double second = new Double(twobili);
			Double third = new Double(threebili);
			ratio.setOnece(onece);
			ratio.setSecond(second);
			ratio.setThird(third);
			Ratio playRatio = ratioDao.getRatioById("2");
			playRatio.setSecond(new Double(result_playBili));
			
			logManager.saveLog(loginUserId, "三级分销比例", "设置", "三级分销比例修改");
			
			ratioDao.saveObj(playRatio);
		}
		ratioDao.saveObj(ratio);
	}

	/**
	 * 2016年12月21日上午11:46:48
	 *  阳朔
	 *  注释:
	 */
	@Override
	public List<Ratio> getThreeDataLimit(Integer start, Integer limit) {
		return ratioDao.getThreeDataLimit(start,limit);
	}

	/**
	 * 2016年12月21日下午12:24:10
	 *  阳朔
	 *  注释:
	 */
	@Override
	public int getThreeDataCount() {
		return ratioDao.getThreeDataCount();
	}

	/**
	 * 2017年1月9日下午4:09:14
	 *  阳朔
	 *  注释:
	 */
	@Override
	public Ratio getRatioFormaBili() {
		return ratioDao.getRatioFormaBili();
	}




	
}
