package com.cosin.web.service.shop;

import java.util.List;
import java.util.Map;

import com.cosin.web.entity.QrBackimg;
import com.cosin.web.entity.ShopType;

public interface IServiceManager {
	
	
	/**
	 * @param start
	 * @param limit
	 * @return
	 */
	List<ShopType> getUserList(Integer start, Integer limit);

	/**
	 * 崔青山
	 * 2016年11月23日
	 * @return
	 */
	int getinfofenye();

	/**
	 * 崔青山
	 * 2016年11月23日
	 * @param title
	 * @param imgCns
	 * @param type
	 * @param enable
	 * @param shopTypeId
	 * @param mode
	 */
	void savepushMessage(String loginUserId,String title, String imgCns, String type,
			String enable, String shopTypeId, String mode);

	/**
	 * 崔青山
	 * 2016年11月23日
	 * @param key
	 */
	void delPush(String loginUserId,String key);

	/**
	 * 崔青山
	 * 2016年11月23日
	 * @return
	 */
	List<ShopType> getShopType();

	/**
	 * 崔青山
	 * 2016年11月23日
	 * @param shopTypeId
	 * @return
	 */
	ShopType getShopTypeId(String shopTypeId);

	/**
	 * 2016年12月26日下午4:24:48
	 *  阳朔
	 *  注释:查询 是否 有商家 关联 是返回1 否 返回0
	 */
	int chaSysShopByShopTypeId(String shopTypeId);

	/**
	 * 2017年1月13日下午2:39:02
	 *  阳朔
	 *  注释:
	 */
	List<QrBackimg> selectQrBackImgList(Integer integer, Integer integer2);

	/**
	 * 2017年1月13日下午2:39:08
	 *  阳朔
	 *  注释:
	 */
	int getQrBackimgCount();

	/**
	 * 2017年1月13日下午3:01:11
	 *  阳朔
	 *  注释:
	 */
	void saveQrBackImg(String loginUserId,String imgCns, String mode,String qrBackImgId);

	/**
	 * 2017年1月13日下午3:13:04
	 *  阳朔
	 *  注释:
	 */
	void QrBackimgAbles(String loginUserId,String key);

	/**
	 * 2017年1月13日下午3:13:09
	 *  阳朔
	 *  注释:
	 */
	void disableQr(String loginUserId,String key);

	/**
	 * 2017年1月13日下午3:20:10
	 *  阳朔
	 *  注释:
	 */
	Map getQrBianjiById(String qrBackImgId);

	/**
	 * 2017年1月13日下午3:34:21
	 *  阳朔
	 *  注释:
	 */
	Map qrDel(String qrBackImgId);
	
	
	
}

