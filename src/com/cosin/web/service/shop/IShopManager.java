package com.cosin.web.service.shop;

import java.util.List;
import java.util.Map;

import com.cosin.web.entity.CommentShop;
import com.cosin.web.entity.SysShop;
import com.cosin.web.entity.SysUser;

public interface IShopManager {

	/**
	 * 获取shop数据
	 * 崔青山
	 * @param ssenable
	 * @param ssName
	 * @param integer
	 * @param integer2
	 * @return
	 */
	public List<SysUser> getSysUserList(String SjName, String Name,String tel, Integer integer, Integer integer2);

	/**
	 * 待核审商家分页
	 * 崔青山
	 * @param ssName
	 * @param ssenable
	 * @return
	 */
	public int getShopfenye(String SjName, String Name,String tel);

	

	/**
	 * 通过核审
	 * 崔青山
	 * 2016年7月8日
	 * @param delKeys
	 */
	public Map passReviewed(String userId,String delKeys);

	/**
	 * 拒绝核审
	 * 崔青山
	 * @param delKeys
	 * @param reason 
	 */
	public Map refuseReviewed(String userId,String delKeys, String reason);

	/**
	 * 删除待核审商户
	 * 崔青山
	 * @param key
	 */
	public void delShop(String key);
	/**
	 * 获取商家数据
	 * 崔青山
	 * @param ssEnable 
	 */
	public List<SysUser> getShopLists(String sjName, String name, String ssEnable, Integer integer, Integer integer2);

	/**
	 * 获取商家分页
	 * 崔青山
	 * @param ssEnable 
	 */
	public int getShopfenyes(String name, String tel, String ssEnable);

	
	/**
	 * 查询手机号是否重复
	 * 崔青山
	 * 2016年8月1日
	 * @param tels
	 * @return
	 */
	public List<SysUser> chaXunHao(String tel);

	
	
	public void saveMedia(String shopName, String tel, String pwm,String introduction,String mode,
			String userId,String img,String icon, String touxiangs);

	
	/**
	 * 修改
	 * 崔青山
	 * @param shopId
	 * @return
	 */
	public SysShop findModif(String shopId);
	/**
	 * 查询商家是否存在
	 * 崔青山
	 * 2016年7月9日
	 * @param shopName
	 * @return
	 *//*
	public List<Shop> chaShop(String shopName);

	*//**
	 * 保存商家
	 * 崔青山
	 * 2016年7月9日
	 * @return 
	 */

	/**
	 * 修改选择市
	 * 崔青山
	 * 2016年7月11日
	 * @param string
	 * @return
	 *//*
	public Area findArea(String string);

	*//**
	 * 修改选择区域
	 * 崔青山
	 * 2016年11月21日
	 * @param areaByAreaId
	 * @return
	 *//*
	public Area findAreas(String string);

	*//**
	 * 修改省市区
	 * 崔青山
	 * 2016年11月21日
	 * @param areaId
	 * @return
	 *//*
	public List<Area> getAreas(String areaId);

	*//**
	 * 启用shop
	 * 崔青山
	 * 2016年11月21日
	 * @param key
	 */
	public void shopAbles(String key);

	/**
	 * 禁用shop
	 * 崔青山
	 * 2016年11月21日
	 * @param key
	 */
	public void disableShop(String userId,String key);

	

	/**
	 * 查询评论数据
	 * 崔青山
	 * 2016年11月21日
	 * @return
	 *//*
	public List<Commentshop> getShopCommentList(String SjName,String Name,String Date,Integer integer, Integer integer2);

	*//**
	 * 查看评论
	 * 崔青山
	 * 2016年11月21日
	 * @param shopCommentId
	 * @return
	 *//*
	public Commentshop chakanShopComment(String shopCommentId);

	*//**
	 * 删除商家评论
	 * 崔青山
	 * 2016年11月21日
	 * @param key
	 *//*
	public void delShopComment(String key);

	*//**
	 * 评论分页
	 * 崔青山
	 * 2016年11月21日
	 * @param sjName
	 * @param name
	 * @param date
	 * @return
	 *//*
	public int getShopCommentfenye(String sjName, String name, String date);

	*//**
	 * 修改时查类别
	 * 崔青山
	 * 2016年11月21日
	 * @param shopId
	 * @return
	 *//*
	public List<Shopcategory> getShopcategoryId(String shopId);

	*//**
	 * 崔青山
	 * 2016年11月21日
	 *//*
	public void saveShopt(String shopId,String BJtu,String zhengF,String banner,String gtu1,String gtu2,
			String gtu3,String tous);

	*//**
	 * 崔青山
	 * 2016年11月21日
	 * @param shopId
	 * @return
	 *//*
	public List getjingwei(String shopId);

	*//**
	 * 崔青山
	 * 2016年11月21日
	 * @param shopId
	 * @return
	 *//*
	public List<Shopcategory> getShopId(String shopId);

	*/
	/**
	 * 崔青山
	 * 2016年11月21日
	 * @param delKeys
	 * @param pwd
	 * @param zhipwds
	 */
	public void savegaiRole(String delKeys, String pwd);

	/**
	 * 崔青山
	 * 2016年11月21日
	 * @param name
	 * @param tel
	 * @param ssEnable
	 * @param integer
	 * @param integer2
	 * @return
	 */
	public List<SysUser> getShopUserLists(String name,String startDate,String endDate, Integer integer, Integer integer2);

	/**
	 * 崔青山
	 * 2016年11月21日
	 * @param name
	 * @param tel
	 * @param ssEnable
	 * @return
	 */
	public int getShopUserfenyes(String name,String startDate,String endDate);

	/**
	 * 崔青山
	 * 2016年11月22日
	 * @param userId
	 * @return
	 */
	public SysUser getUserId(String userId);

	/**
	 * 崔青山
	 * 2016年11月22日
	 * @param sysUser
	 */
	public void saveSysUser(SysUser sysUser);

	/**
	 * 崔青山
	 * 2016年11月22日
	 * @param sjName
	 * @param name
	 * @param date
	 * @param integer
	 * @param integer2
	 * @return
	 */
	public List<CommentShop> getShopCommentList(String sjName, String name,
			String date, Integer integer, Integer integer2);

	/**
	 * 2016年12月26日下午1:21:31
	 *  阳朔
	 *  注释: 保存 经纬度 地址
	 */
	public void saveLocation(String shopId, String lng, String lat,
			String addres);

	/**
	 * 2016年12月26日下午1:45:34
	 *  阳朔
	 *  注释:查看经纬度 地址
	 */
	public Map getSysShopByShopId(String userId,String shopId);

	/**
	 * 2016年12月29日下午4:44:19
	 *  阳朔
	 *  注释:查询商家活动能够
	 */
	public Map<Object,Object> getShopActivityById(String shopId);

	/**
	 * 2017年1月3日下午12:05:59
	 *  阳朔
	 *  注释:
	 */
	public SysShop getShopByShopId(String shopId);

	/**
	 * 2017年1月16日下午2:41:49
	 *  阳朔
	 *  注释:
	 */
	public Map chaShopBili(String shopId);

	/**
	 * 2017年1月16日下午3:04:19
	 *  阳朔
	 *  注释:
	 */
	public void saveShopBili(String shopId, String onebili, String twobili,
			String threebili,String shopbili,String platbili);

	
}
