package com.cosin.web.service.shop;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cosin.web.controller.log.ILogManager;
import com.cosin.web.dao.shop.IServiceDao;
import com.cosin.web.entity.QrBackimg;
import com.cosin.web.entity.ShopType;
import com.cosin.web.entity.SysShop;

@Service  
@Transactional 
public class ServiceManager implements IServiceManager{
	@Autowired
	private IServiceDao serviceDao;
	@Autowired
	private ILogManager logManager;
	
	
	@Override
	public List<ShopType> getUserList(Integer start, Integer limit) {
		// TODO Auto-generated method stub
		List<ShopType> listStr = serviceDao.getPushMessage(start, limit);
		List listRes = new ArrayList();
		for (int i = 0; i < listStr.size(); i++) {
			ShopType user = listStr.get(i);
			Map map = new HashMap();
			map.put("typeName", user.getTypeName());
			map.put("enable", user.getEnable());
			map.put("orderNum", user.getOrderNum());
			map.put("img", user.getImg());
			map.put("shopTypeId", user.getShopTypeId());
			listRes.add(map);
		}
		return listRes;
	}
	
	@Override
	public int getinfofenye() {
		// TODO Auto-generated method stub
		return serviceDao.getinfofenye();
	}

	/* 
	 * 崔青山
	 * 2016年11月23日
	 * 下午12:15:59
	 */
	@Override
	public void savepushMessage(String loginUserId,String title, String imgCns, String type,
			String enable, String shopTypeId, String mode) {
		// TODO Auto-generated method stub
		if(mode.equals("edit")){
			ShopType push = serviceDao.getShopTypeId(shopTypeId);
			logManager.saveLog(loginUserId, "服务类型", "编辑", push.getTypeName()+"修改");

			push.setTypeName(title);
			if(!type.equals(""))
				push.setOrderNum(type);
			push.setImg(imgCns);
			push.setEnable(new Integer(enable));
			
			serviceDao.setPushMessage(push);
		}
		else{
			ShopType push = new ShopType();
			logManager.saveLog(loginUserId, "服务类型", "新建服务", "新建服务名为："+title);
			push.setTypeName(title);
			ShopType Servicetype = serviceDao.getShopTypeSuan();
			if(type.equals("")){
				if(Servicetype==null)
					push.setOrderNum("1");
				else
					push.setOrderNum((new Integer(Servicetype.getOrderNum())+1)+"");
					
				//push.setIsRead(0);
			}
			else
				push.setOrderNum(type);
			push.setImg(imgCns);
			push.setEnable(new Integer(enable));
			push.setIsDel(0);
			serviceDao.setPushMessage(push);
		}
	}

	/* 
	 * 崔青山
	 * 2016年11月23日
	 * 下午2:51:08
	 */
	@Override
	public void delPush(String loginUserId,String key) {
		ShopType shopType = serviceDao.getShopTypeId(key);
		logManager.saveLog(loginUserId, "服务类型", "删除操作", shopType.getTypeName()+"删除");
		serviceDao.delPush(key);
	}

	/* 
	 * 崔青山
	 * 2016年11月23日
	 * 下午3:03:07
	 */
	@Override
	public List<ShopType> getShopType() {
		// TODO Auto-generated method stub
		return serviceDao.getShopType();
	}

	/* 
	 * 崔青山
	 * 2016年11月23日
	 * 下午3:14:51
	 */
	@Override
	public ShopType getShopTypeId(String shopTypeId) {
		// TODO Auto-generated method stub
		return serviceDao.getShopTypeId(shopTypeId);
	}

	/**
	 * 2016年12月26日下午4:25:31
	 *  阳朔
	 *  注释:
	 */
	@Override
	public int chaSysShopByShopTypeId(String shopTypeId) {
		List<SysShop> list = serviceDao.chaSysShopByShopTypeId(shopTypeId);
		if(list.size() > 0)
			return 1;
		return 0;
	}

	/**
	 * 2017年1月13日下午2:39:14
	 *  阳朔
	 *  注释:
	 */
	@Override
	public List<QrBackimg> selectQrBackImgList(Integer start, Integer limit) {
		List<QrBackimg> listRes = serviceDao.getQrBackimgList(start,limit);
		List list = new ArrayList();
		for (int i = 0; i < listRes.size(); i++) {
			QrBackimg backimg = listRes.get(i);
			Map<Object,Object> map = new HashMap<Object, Object>();
			map.put("qrBackImgId", backimg.getQrBackImgId());
			map.put("backImg", backimg.getImg());
			map.put("enable", backimg.getEnable());
			list.add(map);
		}
		return list;
	}

	/**
	 * 2017年1月13日下午2:39:14
	 *  阳朔
	 *  注释:
	 */
	@Override
	public int getQrBackimgCount() {
		return serviceDao.getQrBackimgCount();
	}

	/**
	 * 2017年1月13日下午3:01:18
	 *  阳朔
	 *  注释:
	 */
	@Override
	public void saveQrBackImg(String loginUserId,String imgCns, String mode,String qrBackImgId) {
		if(mode.equals("add")){
			QrBackimg backimg = new QrBackimg();
			backimg.setCreateDate(new Timestamp(new Date().getTime()));
			backimg.setEnable(1);
			backimg.setIsDel(0);
			backimg.setImg(imgCns);
			serviceDao.saveObj(backimg);
			logManager.saveLog(loginUserId, "分享管理", "添加操作", "添加");
		}
		if(mode.equals("edit")){
			QrBackimg backimg = serviceDao.getQrBackimgById(qrBackImgId);
			backimg.setImg(imgCns);
			serviceDao.saveObj(backimg);
			logManager.saveLog(loginUserId, "分享管理", "编辑操作", "修改");
		}
	}

	/**
	 * 2017年1月13日下午3:13:13
	 *  阳朔
	 *  注释:
	 */
	@Override
	public void QrBackimgAbles(String loginUserId,String key) {
		QrBackimg backimg = serviceDao.getQrBackimgById(key);
		backimg.setEnable(1);
		serviceDao.saveObj(backimg);
		logManager.saveLog(loginUserId, "分享管理", "启用操作", "启用");
	}

	/**
	 * 2017年1月13日下午3:13:13
	 *  阳朔
	 *  注释:
	 */
	@Override
	public void disableQr(String loginUserId,String key) {
		QrBackimg backimg = serviceDao.getQrBackimgById(key);
		backimg.setEnable(0);
		serviceDao.saveObj(backimg);
		logManager.saveLog(loginUserId, "分享管理", "禁用操作", "禁用");
	}

	/**
	 * 2017年1月13日下午3:20:22
	 *  阳朔
	 *  注释:
	 */
	@Override
	public Map getQrBianjiById(String qrBackImgId) {
		QrBackimg qrImg = serviceDao.getQrBackimgById(qrBackImgId);
		Map<Object,Object> map = new HashMap<Object, Object>();
		map.put("qrBackImgId", qrImg.getQrBackImgId());
		map.put("backImg", qrImg.getImg());
		return map;
	}

	/**
	 * 2017年1月13日下午3:34:29
	 *  阳朔
	 *  注释:
	 */
	@Override
	public Map qrDel(String qrBackImgId) {
		QrBackimg qrImg = serviceDao.getQrBackimgById(qrBackImgId);
		qrImg.setIsDel(1);
		serviceDao.saveObj(qrImg);
		Map<Object,Object> map = new HashMap<Object, Object>();
		map.put("code", 100);
		return map;
	}


	
}
