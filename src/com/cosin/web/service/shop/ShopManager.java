package com.cosin.web.service.shop;


import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cosin.web.controller.log.ILogManager;
import com.cosin.web.dao.shop.IShopDao;
import com.cosin.web.entity.CommentShop;
import com.cosin.web.entity.ShopActivity;
import com.cosin.web.entity.SysShop;
import com.cosin.web.entity.SysUser;


@Service  
@Transactional 
public class ShopManager implements IShopManager{
	@Autowired
	private IShopDao shopDao;
	
	@Autowired
		protected SessionFactory sessionFactory;
	@Autowired
	private ILogManager logManager;

	@Override
	public List<SysUser> getSysUserList(String Name,String tel,String SsEnable,Integer integer, Integer integer2) {
		List<SysShop> listStr = shopDao.getSysUser(Name,tel,SsEnable,integer, integer2);
		List listRes = new ArrayList();
		for (int i = 0; i < listStr.size(); i++) {
			SysShop shop = listStr.get(i);
			Map map = new HashMap();
			map.put("shopName", shop.getShopName());
			map.put("createDate", shop.getCreateDate());
			map.put("shopId", shop.getShopId());
			map.put("mobile", shop.getCallTel());
			map.put("icon", shop.getSysUser().getIcon());
			map.put("enable", shop.getSysUser().getEnable());
//			if(shop.getSysUser().getSysArea().getAreaName()!=null)
//				map.put("address",shop.getSysUser().getSysArea().getSysArea().getSysArea().getAreaName()+shop.getSysUser().getSysArea().getSysArea().getAreaName()+
//						shop.getSysUser().getSysArea().getAreaName()+shop.getSysUser().getAddress());
//			else
				map.put("address", shop.getShopAddress());
			map.put("idCard", shop.getIdCard());
			map.put("license", shop.getLicense());
			map.put("topTime", shop.getTopTime());
			map.put("backImg", shop.getBackImg());
			map.put("commentNum", shop.getCommentNum());
			map.put("commScore", shop.getCommScore());
			map.put("lngLat", shop.getLng()+","+shop.getLat());
			map.put("typeName", shop.getShopType().getTypeName());
			map.put("type", shop.getType());
			map.put("linkMan", shop.getType());
			
			listRes.add(map);
		}
		return listRes;
	}


	@Override
	public int getShopfenye(String Name,String tel,String SsEnable) {
		// TODO Auto-generated method stub
		return shopDao.getShopFenye(Name,tel,SsEnable);
	}
	
	@Override
	public Map passReviewed(String userId,String delKeys) {
		// TODO Auto-generated method stub
		Map map = new HashMap();
		String[] keyArr = delKeys.split(",");
		for(int i=0; i<keyArr.length; i++)
		{
			String key = keyArr[i];
			if(!"".equals(key) && key !=null)
			map = shopDao.passShop(userId,key);
		}
		return map;
	}
	@Override
	public Map refuseReviewed(String userId,String delKeys, String reason) {
		// TODO Auto-generated method stub
		String[] keyArr = delKeys.split(",");
		Map map = new HashMap();
		for(int i=0; i<keyArr.length; i++)
		{
			String key = keyArr[i];
			if(!"".equals(key) && key !=null)
			map = shopDao.refuseShop(userId,key,reason);
		}
		return map;
	}
	@Override
	public void delShop(String key) {
		// TODO Auto-generated method stub
		shopDao.delShop(key);
	}
	
	@Override
	public List<SysUser> getShopLists(String name, String tel,String SsEnable,Integer integer, Integer integer2) {
		// TODO Auto-generated method stub
		List<SysShop> listStr = shopDao.getShops(name,tel,SsEnable,integer, integer2);
		List listRes = new ArrayList();
		for (int i = 0; i < listStr.size(); i++) {
			SysShop shop = listStr.get(i);
			Map map = new HashMap();
			map.put("shopName", shop.getShopName());
			map.put("createDate", shop.getCreateDate());
			map.put("shopId", shop.getShopId());
			map.put("mobile", shop.getCallTel());
			map.put("icon", shop.getSysUser().getIcon());
			map.put("enable", shop.getSysUser().getEnable());
			/*if(shop.getSysUser().getSysArea().getAreaName()!=null)
				map.put("address",shop.getSysUser().getSysArea().getSysArea().getSysArea().getAreaName()+shop.getSysUser().getSysArea().getSysArea().getAreaName()+
						shop.getSysUser().getSysArea().getAreaName()+shop.getSysUser().getAddress());
			else*/
				map.put("address", shop.getShopAddress());
			map.put("idCard", shop.getIdCard());
			map.put("license", shop.getLicense());
			if(shop.getTopTime() == null){
				map.put("topTime", "暂无");
			}else{
				map.put("topTime", shop.getTopTime());
			}
			map.put("backImg", shop.getBackImg());
			map.put("commentNum", shop.getCommentNum());
			map.put("commScore", shop.getCommScore());
			map.put("lngLat", shop.getLng()+","+shop.getLat());
			map.put("typeName", shop.getShopType().getTypeName());
			listRes.add(map);
		}
		return listRes;
	}
	
	@Override
	public int getShopfenyes(String name, String tel,String enable) {
		// TODO Auto-generated method stub
		return shopDao.getShopFenyes(name,tel,enable);
	}
	
	/** 查询手机号是否重复
	 * 崔青山
	 * 2016年8月1日
	 * 下午12:27:28
	 */
	@Override
	public List<SysUser> chaXunHao(String tels) {
		// TODO Auto-generated method stub
		return shopDao.chaXunHao(tels);
	}

	
	
	@Override
	public void saveMedia(String shopName, String tel, String pwd,String introduction,String mode,
			String userId,String img,String icon,String touxiangs) {
		// TODO Auto-generated method stub
		if(mode.equals("edit")){
			SysUser user = shopDao.getSysUserId(userId);
			user.setLoginName(tel);
			user.setMobile(tel);
			user.setIcon(touxiangs.split("/")[1]);
			user.setUserName(shopName);
			shopDao.saveUser(user);

			
		}
		else{
			SysUser user = new SysUser();
			user.setCreateDate(new Timestamp(new Date().getTime()));
			user.setLoginName(tel);
			user.setMobile(tel);
			user.setPwd(pwd);
			user.setType(2);
			user.setSex(1);
			user.setIsDel(0);
			user.setEnable(1);
			user.setIcon(touxiangs.split("/")[1]);
			user.setUserName(shopName);
			shopDao.saveUser(user);
			
			
		}
	}
	
	
	@Override
	public void shopAbles(String key) {
		// TODO Auto-generated method stub
		shopDao.shopAbles(key);
	}


	@Override
	public void disableShop(String userId,String key) {
		SysShop sysShop = logManager.getSysShopByShopId(key);
		logManager.saveLog(userId, "商家用户管理", "禁用", sysShop.getShopName()+"禁用");
		shopDao.disableShop(key);
	}
	
	@Override
	public SysShop findModif(String shopId) {
		// TODO Auto-generated method stub
		return shopDao.findModif(shopId);
	}
	
	/**
	 * 崔青山
	 * 2016年9月1日
	 * 下午6:34:01
	*/ 
	@Override
	public void savegaiRole(String delKeys, String pwd) {
		// TODO Auto-generated method stub
		shopDao.savegaiRole(delKeys,pwd);
	}


	/* 
	 * 崔青山
	 * 2016年11月21日
	 * 下午3:47:54
	 */
	@Override
	public List<SysUser> getShopUserLists(String name,String startDate,String endDate,Integer integer, Integer integer2) {
		// TODO Auto-generated method stub
		List<SysUser> listStr = shopDao.getSysUserputong(name,startDate,endDate,integer, integer2);
		List listRes = new ArrayList();
		for (int i = 0; i < listStr.size(); i++) {
			SysUser shop = listStr.get(i);
			Map map = new HashMap();
			map.put("shopName", shop.getUserName());
			map.put("createDate", shop.getCreateDate());
			map.put("shopId", shop.getUserId());
			map.put("loginName", shop.getLoginName());
			map.put("mobile", shop.getMobile());
			map.put("sex", shop.getSex());
			Double price = shop.getPrice();
			if(price == null)
				map.put("price", 0);
			else
				map.put("price", price);
			map.put("enable", shop.getEnable());
			map.put("userId", shop.getUserId());
			map.put("icon", shop.getIcon());
			map.put("address", shop.getAddress());
			
			listRes.add(map);
		}
		return listRes;
	}


	/* 
	 * 崔青山
	 * 2016年11月21日
	 * 下午3:53:24
	 */
	@Override
	public int getShopUserfenyes(String name,String startDate,String endDate) {
		// TODO Auto-generated method stub
		return shopDao.getShopUserfenyes(name,startDate,endDate);
	}


	/* 
	 * 崔青山
	 * 2016年11月22日
	 * 上午10:43:45
	 */
	@Override
	public SysUser getUserId(String userId) {
		// TODO Auto-generated method stub
		return shopDao.getUserId(userId);
	}


	/* 
	 * 崔青山
	 * 2016年11月22日
	 * 上午10:45:49
	 */
	@Override
	public void saveSysUser(SysUser sysUser) {
		// TODO Auto-generated method stub
		shopDao.saveSysUser(sysUser);
	}


	/* 
	 * 崔青山
	 * 2016年11月22日
	 * 下午4:46:15
	 */
	@Override
	public List<CommentShop> getShopCommentList(String sjName, String name,
			String date, Integer integer, Integer integer2) {
		// TODO Auto-generated method stub
		/*List<CommentShop> listStr = shopDao.getCommentShop(sjName,name,date,integer, integer2);
		List listRes = new ArrayList();
		for (int i = 0; i < listStr.size(); i++) {
			CommentShop shop = listStr.get(i);
			Map map = new HashMap();
			map.put("userName", shop.getSysUser().getUserName());
			map.put("shopName", shop.getSysShop().getSysUser().getUserName());
			map.put("userName", shop.getSysUser().getUserName());
			map.put("createDate", shop.getCreateDate());
			map.put("shopId", shop.getShopId());
			map.put("commScore", shop.getCommScore());
			map.put("lngLat", shop.getLng()+","+shop.getLat());
			map.put("typeName", shop.getShopType().getTypeName());
			map.put("type", shop.getType());
			listRes.add(map);
		}
		return listRes;*/
		return null;
	}


	/**
	 * 2016年12月26日下午1:21:42
	 *  阳朔
	 *  注释:
	 */
	@Override
	public void saveLocation(String shopId, String lng, String lat,
			String addres) {
		SysShop sysShop = shopDao.getSysShopByShopId(shopId);
		sysShop.setLng(new Double(lng));
		sysShop.setLat(new Double(lat));
		sysShop.setShopAddress(addres);
		shopDao.saveObj(sysShop);
	}


	/**
	 * 2016年12月26日下午1:45:53
	 *  阳朔
	 *  注释:
	 */
	@Override
	public Map getSysShopByShopId(String loginUserId,String shopId) {
		SysShop sysShop = shopDao.getSysShopByShopId(shopId);
		Map<Object,Object> map = new HashMap<Object, Object>();
		map.put("lng", sysShop.getLng());
		map.put("lat", sysShop.getLat());
		map.put("address", sysShop.getShopAddress());
		
		logManager.saveLog(loginUserId, "商家用户管理", "定位", sysShop.getShopName()+"定位");
		return map;
	}


	/**
	 * 2016年12月29日下午4:44:51
	 *  阳朔
	 *  注释:
	 */
	@Override
	public Map<Object, Object> getShopActivityById(String shopId) {
		Map<Object,Object> map = new HashMap<Object,Object>();
		ShopActivity activity = shopDao.getShopActivityByShopId(shopId);
		if(activity != null){
			map.put("content", activity.getContent());
		}
		return map;
	}


	/**
	 * 2017年1月3日下午12:06:09
	 *  阳朔
	 *  注释:
	 */
	@Override
	public SysShop getShopByShopId(String shopId) {
		SysShop shop = shopDao.getSysShopByShopId(shopId);
		return shop;
	}


	/**
	 * 2017年1月16日下午2:41:56
	 *  阳朔
	 *  注释:
	 */
	@Override
	public Map chaShopBili(String shopId) {
		SysShop shop = shopDao.getSysShopByShopId(shopId);
		Map map = new HashMap();
		map.put("oneBili", shop.getOneBili());
		map.put("twoBili", shop.getTwoBili());
		map.put("threeBili", shop.getThreeBili());
		map.put("shopBili", shop.getShopBili());
		map.put("platBili", shop.getPlatBili());
		
		return map;
	}


	/**
	 * 2017年1月16日下午3:04:24
	 *  阳朔
	 *  注释:
	 */
	@Override
	public void saveShopBili(String shopId, String onebili, String twobili,
			String threebili,String shopbili ,String platbili) {
		SysShop shop = shopDao.getSysShopByShopId(shopId);
		shop.setOneBili(new Double(onebili));
		shop.setTwoBili(new Double(twobili));
		shop.setThreeBili(new Double(threebili));
		shop.setShopBili(new Double(shopbili));
		shop.setPlatBili(new Double(platbili));
		shopDao.saveObj(shop);
	}

	
}
