package com.cosin.web.service.stall;

import java.util.List;

import com.cosin.web.entity.StallPrice;

public interface IStallManager {

	/**
	 * 崔青山
	 * 2016年11月15日
	 * @param integer
	 * @param integer2
	 * @return
	 */
	List<StallPrice> getDistanceList(Integer integer, Integer integer2);



	/**
	 * 崔青山
	 * 2016年11月15日
	 * @param distanceId
	 * @return
	 */
	StallPrice findbjDistance(String distanceId);



	/**
	 * 崔青山
	 * 2016年11月15日
	 * @param title
	 */
	void saveDistance(String loginUserId,String title);
	
	
	
	
	
}

