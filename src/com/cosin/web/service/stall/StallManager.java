package com.cosin.web.service.stall;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cosin.web.controller.log.ILogManager;
import com.cosin.web.dao.stall.IStallDao;
import com.cosin.web.entity.StallPrice;

@Service  
@Transactional 
public class StallManager implements IStallManager{
	@Autowired
	private IStallDao engineerDao;
	@Autowired
	private ILogManager logManager;

	/* 
	 * 崔青山
	 * 2016年11月15日
	 * 下午3:30:09
	 */
	@Override
	public List<StallPrice> getDistanceList(Integer integer, Integer integer2) {
		// TODO Auto-generated method stub
		List<StallPrice> listStr = engineerDao.getDistanceList(integer,integer2);
		List listRes = new ArrayList();
		for (int i = 0; i < listStr.size(); i++) {
			StallPrice user = listStr.get(i);
			Map map = new HashMap();
			map.put("stallId", user.getStallId());
			map.put("unitPrice", user.getUnitPrice()+"元/天");
			listRes.add(map);
		}
		return listRes;
	}

	/* 
	 * 崔青山
	 * 2016年11月15日
	 * 下午3:44:27
	 */
	@Override
	public StallPrice findbjDistance(String stallId) {
		// TODO Auto-generated method stub
		return engineerDao.findbjDistance(stallId);
	}





	/* 
	 * 崔青山
	 * 2016年11月15日
	 * 下午4:55:48
	 */
	@Override
	public void saveDistance(String loginUserId,String title) {
		
		logManager.saveLog(loginUserId, "头条价格", "修改头条价格", "修改为"+title);
		StallPrice distance = engineerDao.getDistance();
		distance.setUnitPrice(new Double(title));
		engineerDao.getsaveDistance(distance);
	}
}
