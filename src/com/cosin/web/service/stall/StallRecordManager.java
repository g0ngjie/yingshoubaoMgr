package com.cosin.web.service.stall;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cosin.web.controller.log.ILogManager;
import com.cosin.web.dao.stall.IStallRecordDao;
import com.cosin.web.entity.BindBank;
import com.cosin.web.entity.StallRecord;
import com.cosin.web.entity.SysUser;
import com.cosin.web.entity.WithdrawCashRecord;

@Service  
@Transactional 
public class StallRecordManager implements IStallRecordManager{
	@Autowired
	private IStallRecordDao recordDao;
	@Autowired
	private ILogManager logManager;

	@Override
	public List<StallRecord> getUserList(String ssuser,String SsJia,Integer start, Integer limit) {
		// TODO Auto-generated method stub
		List<StallRecord> listStr = recordDao.getPushMessage(ssuser,SsJia,start, limit);
		List listRes = new ArrayList();
		for (int i = 0; i < listStr.size(); i++) {
			StallRecord message = listStr.get(i);
			Map map = new HashMap();
			map.put("createDate", message.getCreateDate());
			map.put("shopName", message.getSysShop().getShopName());
			map.put("unitPrice", message.getUnitPrice());
			map.put("price", message.getPriceToDay());
			map.put("stallRecordId", message.getStallRecordId());
			listRes.add(map);
		}
		return listRes;
	}
	 
	/**
	 * 崔青山
	 * 2016年7月29日
	 * 上午10:27:48
	 */
	@Override
	public int getinfofenye(String ssuser,String SsJia) {
		// TODO Auto-generated method stub
		return recordDao.getinfofenye(ssuser,SsJia);
	}
	
	
	/**
	 * 崔青山
	 * 2016年9月2日
	 * 上午10:50:52
	 */
	@Override
	public void delPush(String loginUserId,String key) {
		StallRecord record = recordDao.getStallRecordById(key);
		logManager.saveLog(loginUserId, "头条购买记录", "删除", record.getSysShop().getShopName()+"头条记录删除");
		recordDao.delPush(key);
	}



	
}
