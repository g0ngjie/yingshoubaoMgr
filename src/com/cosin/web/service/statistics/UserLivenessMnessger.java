package com.cosin.web.service.statistics;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cosin.web.dao.statistics.IUserLivenessDao;
import com.cosin.web.entity.ActivDetail;
import com.cosin.web.entity.SysArea;
import com.cosin.web.entity.SysUser;

@Service  
@Transactional 
public class UserLivenessMnessger implements IUserLivenessManager{
	@Autowired
	private IUserLivenessDao userLivenessDao;

	/**
	 * 2017年1月6日下午12:06:27
	 *  阳朔
	 *  注释:
	 */
	@Override
	public List<SysUser> getUserLivenessListData(String type,String provinceId,String cityId,String startDate,String endDate, Integer start, Integer limit) {
		
		List list = new ArrayList();
		if(!type.equals("4"))//新增数量
		{
			List<ActivDetail> sysUserList = userLivenessDao.getActivDetailListData(type,provinceId,cityId,startDate,endDate, start , limit);
			for (int i = 0; i < sysUserList.size(); i++) {
				SysUser sysUser = sysUserList.get(i).getSysUser();
				Map<Object,Object> map = new HashMap<Object, Object>();
				map.put("userId", sysUser.getUserId());
				map.put("userName", sysUser.getUserName());
				String address = sysUser.getSysAreaByProvinceId().getAreaName()+" "+sysUser.getSysAreaByCityId().getAreaName();
				map.put("address", address);
				map.put("tel", sysUser.getMobile());
				map.put("sex", sysUser.getSex());
				map.put("icon", sysUser.getIcon());
				map.put("createDate", sysUser.getCreateDate());
				list.add(map);
			}
		}else{//注册总数
			
			List<SysUser> sysUserList = userLivenessDao.getUserLivenessListData(provinceId,cityId,startDate,endDate, start , limit);
			for (int i = 0; i < sysUserList.size(); i++) {
				SysUser sysUser = sysUserList.get(i);
				Map<Object,Object> map = new HashMap<Object, Object>();
				map.put("userId", sysUser.getUserId());
				map.put("userName", sysUser.getUserName());
				String address = sysUser.getSysAreaByProvinceId().getAreaName()+" "+sysUser.getSysAreaByCityId().getAreaName();
				map.put("address", address);
				map.put("tel", sysUser.getMobile());
				map.put("sex", sysUser.getSex());
				map.put("icon", sysUser.getIcon());
				map.put("createDate", sysUser.getCreateDate());
				list.add(map);
			}
		}
		
		return list;
	}

	/**
	 * 2017年1月6日下午12:06:27
	 *  阳朔
	 *  注释:
	 */
	@Override
	public int getUserLivenessListCount(String type,String provinceId,String cityId,String startDate,String endDate) {
		if(!type.equals("4"))//新增数量
		{
			return userLivenessDao.getActivDetailListCount(type,provinceId,cityId,startDate, endDate);
		}else{
			return userLivenessDao.getUserLivenessListCount(provinceId,cityId,startDate, endDate);
		}
	}

	/**
	 * 2017年1月6日下午5:18:22
	 *  阳朔
	 *  注释:
	 */
	@Override
	public List selectProList() {
		List<SysArea> proList = userLivenessDao.getSysAreaList();
		List list = new ArrayList();
		for (int i = 0; i < proList.size(); i++) {
			SysArea pro = proList.get(i);
			Map<Object, Object> map = new HashMap<Object, Object>();
			map.put("proId", pro.getAreaId());
			map.put("proName", pro.getAreaName());
			list.add(map);
		}
		return list;
	}

	/**
	 * 2017年1月6日下午5:22:38
	 *  阳朔
	 *  注释:
	 */
	@Override
	public List selectCityListByProId(String proId) {
		List<SysArea> cityList = userLivenessDao.getCityListByProId(proId);
		List list = new ArrayList();
		for (int i = 0; i < cityList.size(); i++) {
			SysArea city = cityList.get(i);
			Map<Object, Object> map = new HashMap<Object, Object>();
			map.put("cityId", city.getAreaId());
			map.put("cityName", city.getAreaName());
			list.add(map);
		}
		return list;
	}

	/**
	 * 2017年1月12日下午8:40:37
	 *  阳朔
	 *  注释:[{t3=0, t2=2, t1=2, createDate=2017-01-11 18:21:04.0}, {t3=1, t2=0, t1=0, createDate=2017-01-10 22:38:26.0}]
	 */
	@Override
	public List getUserLivenessListDataNew(String provinceId, String cityId,
			String startDate, String endDate, Integer integer, Integer integer2) {
		
		//List sysUserList = userLivenessDao.getActivDatilData(provinceId,cityId,startDate,endDate, integer ,integer2);
		List sysUserList = userLivenessDao.getUserLivenessListDataNew(provinceId,cityId,startDate,endDate, integer ,integer2);
		List list = new ArrayList();
		for (int i = 0; i < sysUserList.size(); i++) {
			Map map = (Map) sysUserList.get(i);
			String date = map.get("createDate").toString();
			int chaUserBeforeByDate = userLivenessDao.getUserBeforeByDate(date);
			date = date.split(" ")[0];
			int count1 = new Integer( map.get("t1").toString());
			int count2 = new Integer( map.get("t2").toString());
			int count3 = new Integer( map.get("t3").toString());
			Map subMap = new HashMap();
			subMap.put("date", date);
			subMap.put("count1", count1);
			subMap.put("count2", count2);
			subMap.put("count3", count3);
			subMap.put("chaUserBeforeByDate", chaUserBeforeByDate);
			list.add(subMap);
		}
		return list;
	}

	/**
	 * 2017年1月12日下午9:27:08
	 *  阳朔
	 *  注释:
	 */
	@Override
	public int getUserLivenessListCountNew(String provinceId, String cityId,
			String startDate, String endDate) {
		return userLivenessDao.getUserLivenessListCountNew(provinceId,cityId,startDate, endDate);
	}

	/**
	 * 2017年1月13日上午10:22:50
	 *  阳朔
	 *  注释:
	 */
	@Override
	public List getUserFigureData(String forYear, String forMonth) {

		if(forYear.equals("")){
			forYear = com.cosin.utils.DateUtils.parseDateToStr("YYYY",new Timestamp(new Date().getTime()));
		}
		
		List listRes = userLivenessDao.getUserFigureData(forYear,forMonth);
		List list = new ArrayList();
		
		if(forMonth.equals("")){
			for (int i = 0; i < 12; i++) {
				Map des = new HashMap();
				des.put("month", i+1);
				des.put("count1", 0);
				des.put("count2", 0);
				des.put("count3", 0);
				list.add(des);
			}
			for (int i = 0; i < listRes.size(); i++) {
				Map map = (Map) listRes.get(i);
				int date = new Integer(map.get("MONTH(t.createDate)").toString());
				int count1 = new Integer( map.get("t1").toString());
				int count2 = new Integer( map.get("t2").toString());
				int count3 = new Integer( map.get("t3").toString());
				Map subMap = new HashMap();
				subMap.put("month", date);
				subMap.put("count1", count1);
				subMap.put("count2", count2);
				subMap.put("count3", count3);
				list.remove(date-1);
				list.add(date-1, subMap);
			}
		}
		else{
			
			 SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");  
		       try {
				int days =com.cosin.utils.DateUtils.getDaysOfMonth(sdf.parse(forYear+"-"+forMonth));
				for (int i = 0; i < days; i++) {
					Map des = new HashMap();
					des.put("month", i+1);
					des.put("count1", 0);
					des.put("count2", 0);
					des.put("count3", 0);
					list.add(des);
				}
				
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}  
			
			
			for (int i = 0; i < listRes.size(); i++) {//[{COUNT(*)=7, MONTH(a.createDate)=1}, {COUNT(*)=36, MONTH(a.createDate)=1}]
				Map map = (Map) listRes.get(i);
				int day = new Integer(map.get("DAY(t.createDate)").toString());
				int count1 = new Integer( map.get("t1").toString());
				int count2 = new Integer( map.get("t2").toString());
				int count3 = new Integer( map.get("t3").toString());
				Map subMap = new HashMap();
				subMap.put("month", day);
				subMap.put("count1", count1);
				subMap.put("count2", count2);
				subMap.put("count3", count3);
				list.remove(day-1);
				list.add(day-1, subMap);
			}
			
		}
		
		return list;
	}





	
}
