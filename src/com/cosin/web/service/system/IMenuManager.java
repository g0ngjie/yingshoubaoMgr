package com.cosin.web.service.system;

import java.util.List;

import com.cosin.web.dao.system.IMenuDao;
import com.cosin.web.entity.SysMenu;
import com.cosin.web.entity.SysUserRole;

public interface IMenuManager {
	public void setMenuDao(IMenuDao menuDao);
	
	
	public List getMainMenuList(int start, int limit);
	
	public SysMenu findMenuById(String id);
	
	public List findMenuByParentMenuId(String id);
	
	//查询  接口
	public List getMainMenuList(String id);
	//保存  编辑  接口
	public void addMenu(String mode,String menukey,String name, String code, String url, String icon,
			Integer enable ,String type);
	//删除  接口
	public void delmenu(String menuId);
	//隐藏  接口
	public void hideById(String menuId);


	/**
	 * 2016年8月17日
	 * @param menuId
	 * @return
	 */
	public SysMenu getMainMenuById(String menuId);


	/**
	 * 2016年8月17日
	 * @param menuId
	 * @return
	 */
	public SysMenu getSubMenuById(String menuId);


	/**
	 * 2017年1月9日上午11:43:18
	 *  阳朔
	 *  注释:
	 */
	public SysUserRole getSysUserRoleByUserId(String userId);


	/**
	 * 2017年1月9日下午2:57:18
	 *  阳朔
	 *  注释:
	 */
	public SysMenu getMainTopMenuById(String powerCode);


	/**
	 * 2017年1月9日下午3:12:34
	 *  阳朔
	 *  注释:
	 */
	public SysMenu getMainLeftMenuById(String powerCode);
}
