/**
 * 
 */
package com.cosin.web.service.system;

import java.util.List;
import java.util.Map;

import com.cosin.web.entity.SysRolePower;

/**
 * @author 王思明
 * 2016年7月19日
 * 下午2:10:54
 */
public interface ISystemManager {

	/**
	 * 后台用户登录验证
	 * 王思明 
	 * 2016年7月19日
	 * 下午3:15:29
	 */
	Map<String, Object> getSysUserYZ(String loginName, String pwd);

	/**
	 * 2017年1月9日下午2:24:41
	 *  阳朔
	 *  注释:
	 */
	List<SysRolePower> getSysRolePower(String roleId);
	
	

}
