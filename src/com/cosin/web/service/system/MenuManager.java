package com.cosin.web.service.system;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cosin.web.dao.system.IMenuDao;
import com.cosin.web.entity.SysMenu;
import com.cosin.web.entity.SysUserRole;
@Service  
@Transactional 
public class MenuManager implements IMenuManager{
	@Autowired
	private IMenuDao menuDao;
	
	
	public IMenuDao getMenuDao() {
		return menuDao;
	}

	public void setMenuDao(IMenuDao menuDao) {
		this.menuDao = menuDao;
	}

	/**
	 * 获取一级菜单
	 * @return
	 */
	
	public List getMainMenuList(int start, int limit)
	{
		return menuDao.getMainMenuList(start, limit);
	}
	
	public SysMenu findMenuById(String id)
	{
		return menuDao.findMenuById(id);
	}
	
	public List findMenuByParentMenuId(String id)
	{
		return menuDao.findMenuByParentMenuId(id);
	}
	//查询   方法
	@Override
	public List getMainMenuList(String id) {
		return menuDao.getSysMenu(id);
	}

	/**
	 * 保存方法      编辑方法
	 */
	@Override
	public void addMenu(String mode,String menukey,String name, String code, String url, String icon,
			Integer enable ,String type) {
		if("edit".equals(mode)){
			SysMenu sysMenus = findMenuById(menukey);
			sysMenus.setMenuName(name);
			sysMenus.setMenuCode(code);
			sysMenus.setUrl(url);
			sysMenus.setIcon(icon);
			sysMenus.setEnable(enable);
			menuDao.saveSysMenu(sysMenus);
		}else{
			SysMenu sysMenus = new SysMenu();
			sysMenus.setCreateDate(new Timestamp(new Date().getTime()));
			sysMenus.setOrderNo(0);
			SysMenu sysMenu = findMenuById(menukey);
			sysMenus.setSysMenu(sysMenu);
			sysMenus.setMenuName(name);
			sysMenus.setMenuCode(code);
			sysMenus.setUrl(url);
			sysMenus.setIcon(icon);
			sysMenus.setEnable(enable);
			menuDao.saveSysMenu(sysMenus);
		}
	}
	//删除   方法
	@Override
	public void delmenu(String menuId) {
		SysMenu sysMenu = findMenuById(menuId);
		menuDao.delSysMenu(sysMenu);
	}
	
	//隐藏  方法
	@Override
	public void hideById(String menuId) {
		SysMenu sysMenu = findMenuById(menuId);
		sysMenu.setEnable(0);
		menuDao.saveSysMenu(sysMenu);
	}

	/* 
	 * 2016年8月17日
	 * 上午10:18:35
	 */
	@Override
	public SysMenu getMainMenuById(String menuId) {
		// TODO Auto-generated method stub
		return menuDao.getMainMenuById(menuId);
	}

	/* 
	 * 崔青山
	 * 2016年8月17日
	 * 上午10:39:32
	 */
	@Override
	public SysMenu getSubMenuById(String menuId) {
		// TODO Auto-generated method stub
		return menuDao.getSubMenuById(menuId);
	}

	/**
	 * 2017年1月9日上午11:43:24
	 *  阳朔
	 *  注释:
	 */
	@Override
	public SysUserRole getSysUserRoleByUserId(String userId) {
		return menuDao.getSysUserRoleByUserId(userId);
	}

	/**
	 * 2017年1月9日下午2:57:25
	 *  阳朔
	 *  注释:
	 */
	@Override
	public SysMenu getMainTopMenuById(String powerCode) {
		return menuDao.getMainTopMenuById(powerCode);
	}

	/**
	 * 2017年1月9日下午3:12:38
	 *  阳朔
	 *  注释:
	 */
	@Override
	public SysMenu getMainLeftMenuById(String powerCode) {
		return menuDao.getMainLeftMenuById(powerCode);
	}
		
}
