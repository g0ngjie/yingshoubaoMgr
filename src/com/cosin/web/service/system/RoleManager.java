package com.cosin.web.service.system;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cosin.utils.JsonUtils;
import com.cosin.utils.PageUtils;
import com.cosin.web.controller.log.ILogManager;
import com.cosin.web.dao.system.IRoleDao;
import com.cosin.web.entity.SysMenu;
import com.cosin.web.entity.SysPowerItem;
import com.cosin.web.entity.SysRole;
import com.cosin.web.entity.SysRolePower;
import com.cosin.web.entity.SysUser;
import com.cosin.web.entity.SysUserRole;

@Service  
@Transactional 
public class RoleManager implements IRoleManager{
	@Autowired
	private IRoleDao roleDao;
	@Autowired
	private ILogManager logManager;

	@Override
	public void setRoleDao(IRoleDao roleDao) {
		this.roleDao  = roleDao;
	}
	
	@Override
	public List getRoleList( String SsName,int start, int limit) {
		
		return roleDao.getRole(SsName,start, limit);
	}
	
	@Override
	public int getRolefenye(String SsName) {
		// TODO Auto-generated method stub
		return roleDao.getRolefenye(SsName);
	}
	
	@Override
	public List getRoleList(String roleId) {
		
		return roleDao.getByRoleIdList(roleId);
	}
	

	@Override
	public SysRole getByRoleId(String roleId) {
		
		return roleDao.getByRoleId(roleId);
	}
	
	@Override
	public SysUser getByUserId(String userId) {
		
		return roleDao.getByUserId(userId);
	}
	
	@Override
	public void saveRole(String userName, String enable, String roleId, String mode,String del){
		
		if(mode.equals("edit"))
		{
			SysRole sysRole =  getByRoleId(roleId);
			sysRole.setUserName(userName);
			sysRole.setEnable(new Integer(enable));
			roleDao.save(sysRole);
		}
		else{
			SysRole sysRole = new SysRole();
			sysRole.setUserName(userName);
			sysRole.setEnable(new Integer(enable));
			sysRole.setCreateDate(new Timestamp(new Date().getTime()));
			sysRole.setIsDel(0);
			sysRole.setType(1);
			roleDao.save(sysRole);
		}
	}

	@Override
	public void delRole(String roleId) {
		// TODO Auto-generated method stub
		roleDao.delRole(roleId);
		
	}

	@Override
	public void saveqyRole(String roleId) {
		// TODO Auto-generated method stub
		roleDao.saveqyRole(roleId);
	}

	@Override
	public void savejyRole(String roleId) {
		// TODO Auto-generated method stub
		roleDao.savejyRole(roleId);
	}

	@Override
	public List<Object> getUserList(String ssName,String roleName, int start, int limit) {
		List<SysUser> listStr = roleDao.getUser(ssName,roleName,start, limit);
		List listRes = new ArrayList();
		for (int i = 0; i < listStr.size(); i++) {
			SysUser sysuser = listStr.get(i);
			Map map = new HashMap();
			map.put("userName", sysuser.getUserName());
			map.put("enable", sysuser.getEnable());
			map.put("userId", sysuser.getUserId());
			/*SysUserRole sysUserRole = roleDao.getByIdSysUserRole(sysuser.getUserId());
			if(sysUserRole != null){
				map.put("userName", sysUserRole.getSysRole().getUserName());
				map.put("roleId", sysUserRole.getSysRole().getRoleId());
			}*/
			listRes.add(map);
		}
		int totle =roleDao.getRolef();
		Map mapRet = PageUtils.getPageParam(listRes, totle);
		String resStr = JsonUtils.fromObject(mapRet);
		return listRes;
	}

	@Override
	public void saveUser(String loginUserId,String managerName,String loginName,String doublePwd) {
			SysUser sysuser = new SysUser();
			sysuser.setUserName(managerName);
			sysuser.setLoginName(loginName);
			sysuser.setCreateDate(new Timestamp(new Date().getTime()));
			sysuser.setPwd(doublePwd);
			sysuser.setEnable(1);
			sysuser.setIsDel(0);
			sysuser.setIcon("empty_photo.png");
			sysuser.setType(3);
			roleDao.saveUser(sysuser);
			//查出管理员角色roleId = 2
			SysRole sysRole =  roleDao.getManagerRole();
			SysUserRole sysUserRole = new SysUserRole();
			sysUserRole.setSysUser(sysuser);
			sysUserRole.setSysRole(sysRole);
			
			logManager.saveLog(loginUserId, "后台用户管理", "添加操作", "添加用户："+managerName);
			roleDao.saveSysUserRole(sysUserRole);
	}

	

	private SysUserRole getByUserRoleId(String userId) {
		// TODO Auto-generated method stub
		return roleDao.getByUserRoleId(userId);
	}

	@Override
	public void delUser(String loginUserId,String userId) {
		SysUser sysUser = roleDao.getSysUserByUserId(userId);
		logManager.saveLog(loginUserId, "后台用户管理", "删除操作", "删除用户："+sysUser.getUserName());
		roleDao.delUser(userId);
	}

	@Override
	public SysUser findbjUser(String userId) {
		// TODO Auto-generated method stub
		return roleDao.getByUser(userId);
	}

	@Override
	public void saveqyUser(String loginUserId,String userId) {
		SysUser user = roleDao.getSysUserByUserId(userId);
		logManager.saveLog(loginUserId, "后台用户管理", "启用操作", "用户启用："+user.getUserName());
		roleDao.saveqyUser(userId);
	}

	@Override
	public void savejyUser(String loginUserId,String userId) {
		SysUser user = roleDao.getSysUserByUserId(userId);
		logManager.saveLog(loginUserId, "后台用户管理", "禁用操作", "用户禁用："+user.getUserName());
		roleDao.savejyUser(userId);
	}

	@Override
	public List getSysRole() {
		// TODO Auto-generated method stub
		return roleDao.getSysRole();
	}

	@Override
	public List<SysRole> chaRole(String userName) {
		// TODO Auto-generated method stub
		return roleDao.chaRole(userName);
	}

	@Override
	public List<SysUser> chaUser(String userName) {
		// TODO Auto-generated method stub
		return roleDao.chaUser(userName);
	}

	@Override
	public SysRole findbjRole(String roleId) {
		// TODO Auto-generated method stub
		return roleDao.getByRole(roleId);
	}

	

	@Override
	public int getUserfenye(String SsName,String roleName) {
		// TODO Auto-generated method stub
		return roleDao.getUserfenye(SsName,roleName);
	}

	@Override
	public List<SysUserRole> getRoleUserList(String key) {
		// TODO Auto-generated method stub
		return roleDao.getRoleUserList(key);
	}

	/* 
	 * 改密码
	 * 崔青山
	 * 2016年8月3日
	 * 上午12:13:16
	 */
	@Override
	public void savegaiRole(String delKeys,String pwd) {
		// TODO Auto-generated method stub
		roleDao.savegaiRole(delKeys,pwd);
	}

	
	/**
	 * 查询SysRolePower表中的数据
	 */
	@Override
	public List<SysRolePower> findRolePowerById(String roleId) {
		// TODO Auto-generated method stub
		return roleDao.findRolePowerById(roleId);
	}

	/**
	 * 查询SysRolePower表中的数据
	 */
	@Override
	public List<SysMenu> getMenuList(String parentMenuId) {
		// TODO Auto-generated method stub
		return roleDao.getMenuList(parentMenuId);
	}

	/**
	 * 根据指定数据查询
	 */
	@Override
	public boolean roleCheck(List<SysRolePower> list, String menuId) {
		// TODO Auto-generated method stub
		for(int i=0;i<list.size();i++)
		{
			SysRolePower sysRolePower = list.get(i);
			sysRolePower.getRolePowerId();
		}
		return false;
	}

	/* 
	 * 崔青山
	 * 2016年8月8日
	 * 上午9:20:40
	 */
	@Override
	public void delPower(String roleId) {
		// TODO Auto-generated method stub
		 roleDao.delPower(roleId);
	}

	/* 
	 * 崔青山
	 * 2016年8月8日
	 * 上午9:20:40
	 */
	@Override
	public void savePowerInRolePower(String roleId, String powerCode) {
		// TODO Auto-generated method stub
		SysRolePower rolePower = new SysRolePower();
		rolePower.setPowerCode(powerCode);
		SysRole role = findRoleById(roleId);
		rolePower.setSysRole(role);
		roleDao.savePowerInRolePower(rolePower);
	}

	/**
	 * 崔青山
	 * 2016年8月8日
	 * @param roleId
	 * @return
	 */
	private SysRole findRoleById(String roleId) {
		// TODO Auto-generated method stub
		return roleDao.findRoleById(roleId);
	}

	@Override
	public List<SysUser> getShopUserList(String ssName, String roleName,
			Integer integer, Integer integer2) {
		// TODO Auto-generated method stub
		List<SysUser> listStr = roleDao.getShopUserList(ssName,roleName,integer, integer2);
		List listRes = new ArrayList();
		for (int i = 0; i < listStr.size(); i++) {
			SysUser sysuser = listStr.get(i);
			Map map = new HashMap();
			map.put("zhanghao", sysuser.getUserName());
			map.put("enable", sysuser.getEnable());
			map.put("userId", sysuser.getUserId());
		/*	SysUserRole sysUserRole = roleDao.getByIdSysUserRole(sysuser.getUserId());
			if(sysUserRole != null){
				map.put("userName", sysUserRole.getSysRole().getUserName());
				map.put("roleId", sysUserRole.getSysRole().getRoleId());
			}*/
			listRes.add(map);
		}
		int totle =roleDao.getRolef();
		Map mapRet = PageUtils.getPageParam(listRes, totle);
		String resStr = JsonUtils.fromObject(mapRet);
		return listRes;
	}

	@Override
	public List<SysUser> getHuiyuanUserList(String ssName, String roleName,
			Integer integer, Integer integer2) {
		// TODO Auto-generated method stub
		List<SysUser> listStr = roleDao.getHuiyuanUserList(ssName,roleName,integer, integer2);
		List listRes = new ArrayList();
		for (int i = 0; i < listStr.size(); i++) {
			SysUser sysuser = listStr.get(i);
			Map map = new HashMap();
			map.put("zhanghao", sysuser.getUserName());
			map.put("enable", sysuser.getEnable());
			map.put("userId", sysuser.getUserId());
		/*	SysUserRole sysUserRole = roleDao.getByIdSysUserRole(sysuser.getUserId());
			if(sysUserRole != null){
				map.put("userName", sysUserRole.getSysRole().getUserName());
				map.put("roleId", sysUserRole.getSysRole().getRoleId());
			}*/
			listRes.add(map);
		}
		int totle =roleDao.getRolef();
		Map mapRet = PageUtils.getPageParam(listRes, totle);
		String resStr = JsonUtils.fromObject(mapRet);
		return listRes;
	}

	/* 
	 * 崔青山
	 * 2016年8月17日
	 * 上午10:10:07
	 */
	@Override
	public SysUserRole findUserRoleByUserId(String userId) {
		// TODO Auto-generated method stub
		return roleDao.findUserRoleByUserId(userId);
	}

	/* 
	 * 崔青山
	 * 2016年8月17日
	 * 上午10:15:40
	 */
	@Override
	public SysPowerItem getSysPowerItemById(String powerCode) {
		// TODO Auto-generated method stub
		return roleDao.getSysPowerItemById(powerCode);
	}

	/* 
	 * 崔青山
	 * 2016年8月21日
	 * 下午1:06:15
	 */
	@Override
	public int getUserfenyeShop(String ssName, String roleName) {
		// TODO Auto-generated method stub
		return roleDao.getUserfenyeShop(ssName,roleName);
	}

	/* 
	 * 崔青山
	 * 2016年8月21日
	 * 下午1:08:46
	 */
	@Override
	public int getUserfenyeHuiyuan(String ssName, String roleName) {
		// TODO Auto-generated method stub
		return roleDao.getUserfenyeHuiyuan(ssName,roleName);
	}

	/* 
	 * 崔青山
	 * 2016年9月21日
	 * 下午5:43:35
	 */
	@Override
	public SysUserRole getSysUserRole(String userId) {
		// TODO Auto-generated method stub
		return roleDao.getSysUserRole(userId);
	}

	/**
	 * 2017年1月9日上午11:17:35
	 *  阳朔
	 *  注释:
	 */
	@Override
	public List<SysUser> chaLoginName(String loginName) {
		return roleDao.chaLoginName(loginName);
	}

	/**
	 * 2017年1月9日下午12:17:32
	 *  阳朔
	 *  注释:
	 */
	@Override
	public Map submitUser(String loginUserId,String userId, String userName, String loginName) {
		SysUser sysUser = roleDao.getSysUserByUserId(userId);
		logManager.saveLog(loginUserId, "后台用户管理", "编辑操作", "编辑用户："+sysUser.getUserName());
		sysUser.setUserName(userName);
		sysUser.setLoginName(loginName);
		roleDao.saveUser(sysUser);
		Map map = new HashMap();
		map.put("code", 100);
		map.put("msg", "保存成功"); 
		return map;
	}

	
}
