/**
 * 
 */
package com.cosin.web.service.system;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cosin.utils.UserSession;
import com.cosin.web.dao.system.ISystemDao;
import com.cosin.web.entity.SysRolePower;
import com.cosin.web.entity.SysUser;
import com.cosin.web.entity.SysUserRole;

/**
 * @author 王思明
 * 2016年7月19日
 * 下午2:12:35
 */
@Service
@Transactional 
public class systemManager implements ISystemManager {

	@Autowired
	private ISystemDao systemDao;

	/* 
	 * 后台用户登录验证
	 * 王思明
	 * 2016年7月19日
	 * 下午3:15:46
	 */
	@Override
	public Map<String, Object> getSysUserYZ(String loginName, String pwd) {
		Map<String, Object> map = new HashMap<String,Object>();
		
		SysUser sysUser = systemDao.getByEmailNamePwd(loginName,pwd);
		if(sysUser == null)
		{
			map.put("code", 101);
			map.put("msg", "用户名或密码错误");
			
		}else{
			
			SysUserRole sysRole = systemDao.getSysRoleByUserId(sysUser.getUserId());
			map.put("roleId", sysRole.getSysRole().getRoleId());
			//返回值处理
			map.put("code", 100);
			map.put("msg", "登录成功");
			map.put("userId", sysUser.getUserId());
			map.put("mobile", sysUser.getMobile());
			map.put("name", sysUser.getUserName());
		}
		return map;
	}

	/**
	 * 2017年1月9日下午2:24:49
	 *  阳朔
	 *  注释:
	 */
	@Override
	public List<SysRolePower> getSysRolePower(String roleId) {
		return systemDao.getSysRolePower(roleId);
	}
	
	
	
}
